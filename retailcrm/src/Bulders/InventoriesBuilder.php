<?php

class InventoriesBuilder extends Builder
{

    /**
     * getInventories
     *
     * @return array
     */
    public function buildInventories($step, $chuck)
    {
        $query = $this->rule->getSQL('inventories');
        $handler = $this->rule->getHandler('InventoriesHandler');
        $this->sql = $this->container->db->prepare($query);
        $this->sql->bindParam(':step', $step, PDO::PARAM_INT);
        $this->sql->bindParam(':chuck', $chuck, PDO::PARAM_INT);

        return $this->build($handler);

    }
}
