<?php

if (
    function_exists('date_default_timezone_set')
    &&
    function_exists('date_default_timezone_get')
) {
    date_default_timezone_set(@date_default_timezone_get());
}

require_once 'bootstrap.php';

$container = Container::getInstance();
/** @var ApiClient $apiClient */

$db = $container->db;

if(isset($_GET['customerId'])) {
    $api = new RequestProxy(
        $container->settings['api']['url'],
        $container->settings['api']['key']
    );
    $customer = $api->customersGet($_GET['customerId']);

    $discount = $container->db->prepare("UPDATE m_mag_Users SET d_percent=:personalDiscount WHERE id=:id;");
    $discount->bindValue(':personalDiscount', $customer['customer']['cumulativeDiscount']);
    $discount->bindValue(':id', $_GET['customerId'], PDO::PARAM_INT);
    $discount->execute();

}