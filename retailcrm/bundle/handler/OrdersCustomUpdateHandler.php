<?php

class OrdersCustomUpdateHandler implements  HandlerInterface {

    public function prepare($data)
    {
        $settings = Container::getInstance()->settings;
        $summMargin = [];
        $ordersUpdate = [];
        foreach ($data as $value) {
            $min = min(explode(";", $value['cost']));
            $summMargin[$value['externalId']] =
                $summMargin[$value['externalId']] + $this->getMargin($min) * $value['quantity'];
        }
        foreach ($summMargin as $key => $margin) {
            $ordersUpdate[]  = array(
                'externalId' => $settings['general']['prefix_site'] . $key,
                'customFields' => array(
                    'margin2' => $margin,
                ),
            );
        }
        return $ordersUpdate;
    }
    /*
    790р Шаг 1 – от $0 до $30
    860р Шаг 2 – от $30 до $50
    1200р Шаг 3 – от $50 до $75
    1600р Шаг 4 – от $75 до $95
    1900р Шаг 5 – от $95 до $200
    4300р Шаг 6 – от $200 до $500
    7000р Шаг 7 – от $500 до $1000
    */
    public function getMargin($price)
    {
        if ($price <= 30) {
            return 790;
        } elseif ($price > 30 && $price <= 50) {
            return 860;
        } elseif ($price > 50 && $price <= 75) {
            return 1200;
        } elseif ($price > 75 && $price <= 95) {
            return 1600;
        } elseif ($price > 95 && $price <= 200) {
            return 1900;
        } elseif ($price > 200 && $price <= 500) {
            return 4300;
        } elseif ($price > 500 && $price <= 1000) {
            return 7000;
        }
    }

}
