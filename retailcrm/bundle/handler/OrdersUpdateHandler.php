<?php

class OrdersUpdateHandler implements  HandlerInterface {

    public $container;

    public function prepare($data)
    {
        $this->container = Container::getInstance();
        $update = [];
	$ids = [];
        foreach ($data as $order) {
            $order['externalId'] = $this->container->settings['general']['prefix_site'] . $order['externalId'];

            if (!in_array($order['externalId'], $ids)) {
                if ($updateManager = $this->ifManager($order)) {
                    $update[] = $updateManager;
                }
                $ids[] = $order['externalId'];
            }

            if (($item = $this->editItems($order)) && !$this->find($update, $order['externalId'], 'items')) {
                $update[] = $item;
                //запись externalId для обновления маржи
                file_put_contents($this->container->logDir . 'order/custom_update.log', substr($order['externalId'], 3) . ',', FILE_APPEND);
            }
            if ($status = $this->ifStatus($order)) {
                $update[] = $status;
            }
            if ($delivery = $this->ifAddress($order)) {
                $update[] = $delivery;
            }
            if ($status = $this->ifInfo($order)) {
                $update[] = $status;
            }
            if ($fio = $this->ifFIO($order)) {
                $update[] = $fio;
            }
            if ($courier = $this->ifCourier($order)) {
                $update[] = $courier;
            }
        }
        return $update;
    }

    public function find($array, $externalId, $findField)
    {
        foreach ($array as $key => $value) {
            if ($value['externalId'] == $externalId) {
                if (array_key_exists($findField, $value)) {
                    return true;
                }
            }
        }
        return false;
    }

    public function editItems($order)
    {
        if ($order['action'] != 'add' && $order['action'] != 'delete' && $order['action'] != 'change_kol') { return false; }

        $api = new RequestProxy($this->container->settings['api']['url'], $this->container->settings['api']['key']);
        $db = $this->container->db;
        $editItem = $db->query("
        SELECT
            id_catalog_data_order as productId,
            title as productName,
            kol as quantity,
            sum as initialPrice,
            articul_catalog_data_order as articul,
            margin,
            sklad
        FROM
            m_mag_OrdersSum
        WHERE id_order=" . substr($order['externalId'], 3)
        );
        foreach ($editItem as $key => $value) {
            $resultItems[$key] = array(
                'productId' => $value['productId'],
                'productName' => $value['productName'],
                'quantity' => $value['quantity'],
                'initialPrice' => $value['initialPrice'],
                'properties' => array(
                    ($value['articul'] != ''?
                    array(
                        'code' => 'article',
                        'name' => 'Артикул',
                        'value' => $value['articul']
                    ):null),
                    array(
                        'code' => 'margin',
                        'name' => 'Маржа',
                        'value' => $value['margin']
                    ),
                    array(
                        'code' => 'sklad',
                        'name' => 'Склад',
                        'value' => $value['sklad']
                    ),
                    array(
                        'code' => 'pol',
                        'name' => 'Пол',
                        'value' => trim($value['productName'])[1] == 'F' ? "Женский" : "Мужской"
                    ),
                ),
            );
        }
        $orderItem = array(
            'externalId' => $order['externalId'],
            'items' => $resultItems,
        );
        return $orderItem;
    }

    public function ifAddress($order)
    {
        if (explode('_', $order['action'])[0] != 'address' && $order['action'] != 'postcode') {return false;}
        if ($order['action'] == 'address_part') {return false;}

        $field = array(
            'address_city' => 'city',
            'address_corpus' => 'house',
            'address_domophone' => 'intercomCode',
            'address_flat' => 'flat',
            'address_floor' => 'floor',
            'address_house' => 'building',
            'address_region' => 'region',
            'address_settlement' => 'settlement',
            'address_street' => 'street',
            'address_stroenie' => 'block',
            'postcode' => 'postcode',
        );

        if ($order['action'] != 'address_settlement') {
            $address = array(
                'externalId' => $order['externalId'],
                'delivery' => array(
                    'address' => array($field[$order['action']] => $order['value'],)),
            );
        } else {
            $address = array(
                'externalId' => $order['externalId'],
                'customFields' => array(
                    'settlement_orders' => $order['value'],
            ));
        }
        return $address;
    }

    public function ifStatus($order)
    {
        if ($order['action'] != 'status') {return false;}
        $statusAll = $this->container->settings['status'];
        $status = array(
            'externalId' => $order['externalId'],
            'status' => $statusAll[$order['value']],
        );
        return $status;
    }

    public function ifInfo($order)
    {
        $field = array(
            'phone' => 'phone',
            'phone2' => 'additionalPhone',
            'email' => 'email',
            'pComment' => 'managerComment',
            'payment_type' => 'paymentType',
        );

        if (!array_key_exists($order['action'], $field)) {return false;}

        $info = array(
            'externalId' => $order['externalId'],
            $field[$order['action']] => $order['value'],
        );
        return $info;
    }

    public function ifFIO($order)
    {
        if ($order['action'] != 'fio') {return false;}
        $fio = DataHelper::explodeFIO($order['value']);
        //Проверить не затерается ли если null
        $orderUpdate = array(
            'externalId' => $order['externalId'],
            'lastName' => isset($fio['lastName']) ? $fio['lastName'] : null,
            'firstName' => isset($fio['firstName']) ? $fio['firstName'] : null,
            'patronymic' => isset($fio['patronymic']) ? $fio['patronymic'] : null,
        );
        return $orderUpdate;
    }

    public function ifCourier($order)
    {
        if ($order['action'] != 'courier_address_id') {return false;}

        $db = $this->container->db;

        $courier = $db->query('
            SELECT
            CONCAT(
               "Курьер: ", (SELECT name FROM couriers WHERE couriersId=`courier_id`), "\n",
               "Кол-во дней: ", `delivery_days`, "\n",
               "Тип: ", `delivery_type`, "\n",
               IF(`delivery_code`<>0, CONCAT("Код: ", `delivery_code`, "\n"), ""),
               "Адрес пункта самовывоза: ", `delivery_address`, "\n",
               "Оплата: ", `delivery_payment`
               ) as delivery_self
            FROM `couriers_addresses`
            WHERE  id=' . $order['value']
        );
        $orderCourier = array(
            'externalId' => $order['externalId'],
            'customFields' => array(
                'delivery_self' => $courier->fetch()['delivery_self'],
        ));
        return $orderCourier;
    }

    public function ifManager($order)
    {
        $db = $this->container->db;
        $manager = $db->query("
        SELECT
            id_manager as managerId
        FROM
            m_mag_Orders
        WHERE id=" . substr($order['externalId'], 3)
        );
        $managerId = $manager->fetch()['managerId'];
        if ($managerId == 0 && !array_key_exists($managerId, $this->container->settings['manager'])) {return false;}

        $orderManager = array(
            'externalId' => $order['externalId'],
            'managerId' => $this->container->settings['manager'][$managerId]
        );
        return $orderManager;
    }

}
