<?php

class InventoriesHandler implements HandlerInterface
{
    public function prepare($offers)
    {
        $offersCRM = [];
        foreach ($offers as $key => $value) {
            $offerCRM['externalId'] = $value['externalId'];
            for ($sklad=1; $sklad <= 18 ; $sklad++) {
                $offerCRM['stores'][$sklad - 1] = array(
                    'code' => 'sklad' . ($sklad <= 9 ? '0' . $sklad : $sklad),
                    'available' => $value['price_usd' . $sklad] != 0 ? 1 : 0,
                    'purchasePrice' =>  $value['price_usd' . $sklad],
                );
            }
            $offersCRM[] = $offerCRM;
        }
        return $offersCRM;
    }

}
