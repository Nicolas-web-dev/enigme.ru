<?php

class OrdersHistoryHandler implements HandlerInterface {

    /** @var ApiClient $api */
    private $api;

    /** @var PDO $db */
    private $db;

    private $container;

    public function prepare($data) {
        $this->container = Container::getInstance();
        /** @var ApiClient $apiClient */
        $this->api = new RequestProxy($this->container->settings['api']['url'], $this->container->settings['api']['key']);
        $this->db = Container::getInstance()->db;

        $existsOrders = array_filter($data, function($o){return isset($o['externalId']);});
        $newOrders = array_filter($data, function($o){return isset($o['created']);});
        $modifyOrders = array_filter(
            $existsOrders,
            function($o){
                return !isset($o['created']) && !isset($o['deleted']);
            }
        );

        foreach ($modifyOrders as $order) {
            $this->recordStatusIfNeed($order);
            $this->recordManagerIfNeed($order);
            $this->recordDiscountIfNeed($order);
            $this->recordOrderSum($order);
            $this->recordContactDataIfNeed($order);
            $this->recordDeliveryIfNeed($order);
            $this->recordItemsIfNeed($order, false);
        }
        foreach ($newOrders as $order) {
            $this->recordNewOrder($order);
            $this->recordStatusIfNeed($order);
            $this->recordManagerIfNeed($order);
            $this->recordDiscountIfNeed($order);
            $this->recordOrderSum($order);
            $this->recordContactDataIfNeed($order);
            $this->recordDeliveryIfNeed($order);
            $this->recordItemsIfNeed($order, true);
        }

    }

    private function daleteItem($externalId) {
        $deleteItem = $this->db->prepare("
            DELETE FROM m_mag_OrdersSum
            WHERE id_order=:orderId
            ");
        $deleteItem->bindValue(':orderId', $externalId, PDO::PARAM_INT);
        $deleteItem->execute();
        unset($deleteItem);
        return true;
    }

    private function addItem($item, $externalId, $customerId) {
        if (!isset($item)) {return false;}
        //Получаем данные для формирования имя товара
        $select = $this->db->prepare("
        SELECT
            DO.id as id,
            DO.type,
            DO.v,
            D.title as name,
            D.pol,
            (SELECT title FROM `m_catalog` WHERE id = D.id_catalog) as vendor
        FROM
            `m_catalog_data_order` as DO
        LEFT JOIN
            `m_catalog_data` as D
        ON
            DO.id_catalog_data = D.id
        WHERE
            DO.type <> '0'
            AND DO.id = :id
        ");
        $select->bindValue(':id', $item['offer']['externalId'], PDO::PARAM_INT);
        $select->execute();
        $offer = $select->fetch(PDO::FETCH_ASSOC);

        $addItem = $this->db->prepare("
        INSERT INTO m_mag_OrdersSum (id, id_order, id_user, id_brend, id_catalog_data, id_catalog_data_order, articul_catalog_data_order, sklad, margin, title, sum, kol)
        VALUES (NULL,
            :orderId,
            :userId,
            (SELECT id_catalog FROM `m_catalog_data` as D INNER JOIN  `m_catalog_data_order` as DO ON D.id = DO.id_catalog_data WHERE DO.id = :externalId),
            (SELECT id_catalog_data FROM m_catalog_data_order WHERE id = :externalId),
            :externalId,
            (SELECT articul FROM m_catalog_data_order WHERE id = :externalId),
            :sklad,
            :margin,
            :productName,
            :price,
            :quantity)
        ");
        //склад и маржа не добавляется
        if (isset($item['properties']['margin']['value'])) {
            $margin = $item['properties']['margin']['value'];
        } elseif (isset($item['properties']['marga']['value'])) {
            $margin = $item['properties']['marga']['value'];
        } else {
            $margin = 0;
        }

        $addItem->bindValue(':orderId', $externalId, PDO::PARAM_INT);
        $addItem->bindValue(':userId', isset($customerId) ? $customerId : 0, PDO::PARAM_INT);
        $addItem->bindValue(':externalId', $item['offer']['externalId'], PDO::PARAM_INT);
        $addItem->bindValue(':quantity', $item['quantity']);
        $addItem->bindValue(':price', $item['initialPrice']);
        $addItem->bindValue(':productName', isset($offer) ? DataHelper::getName($offer) : null);
        $addItem->bindValue(':margin', $margin, PDO::PARAM_INT);
        $addItem->bindValue(':sklad', (isset($item['properties']['sklad']['value']) ? $item['properties']['sklad']['value'] : ''));
        $addItem->bindValue(':article', (isset($item['properties']['article']['value']) ? $item['properties']['article']['value'] : ''));
        $addItem->execute();
        unset($addItem);
        return true;
    }

    private function recordItemsIfNeed($order, $isNew = false) {
        if (!isset($order['items']) || empty($order['items'])) {return false;}
        $orderId = substr($order['externalId'], 3);
        /*Формирования файла с номерами заказов */
        file_put_contents($this->container->logDir . 'order/custom_update.log', $orderId . ',', FILE_APPEND);

        $this->daleteItem($orderId);
        $r = $this->api->ordersGet($order['externalId']);
        foreach ($r['order']['items'] as $item) {

            $this->addItem($item, $orderId,
                isset($r['order']['customer']['externalId']) ? $r['order']['customer']['externalId'] : 0);
        }
        return true;
    }

//настроенно
    public function recordStatusIfNeed($order) {
        if (!isset($order['status'])) {return false;}
        $orderId = substr($order['externalId'], 3);
        //Маппинг групп статусов
        $standartGroups = array(
            'new' => '1', //Новый
            'approval' => '2',// Согласование
            'assembling' => '4', // Комплектация
            'delivery' => '4', // Доставка
            'complete' => '3', // Выполнен
            'cancel' => '0' // Отменен
        );
        $recordStatusQuery = $this->db->prepare("
          UPDATE m_mag_Orders SET status=:newStatus WHERE id=:orderId;
        ");
        //Получение статусов с CRM
        $statusGroups = $this->api->statusGroupsList()['statusGroups'];
        //Поиск нужной группы статуса
        foreach ($statusGroups as $key => $group) {
            if (in_array($order['status'], $group['statuses'])) {
                $recordStatusQuery->bindValue(':newStatus', $standartGroups[$key], PDO::PARAM_INT);
            }
        }
        $recordStatusQuery->bindValue(':orderId', $orderId, PDO::PARAM_INT);
        $recordStatusQuery->execute();
        return true;
    }

    //настроенно, но не проверенно
        public function recordManagerIfNeed($order) {
            if (!isset($order['managerId'])) {return false;}
            //CRM => БД
            $managerId = array_flip($this->container->settings['manager']);

            $orderId = substr($order['externalId'], 3);
            $recordManagerQuery = $this->db->prepare("
              UPDATE m_mag_Orders SET id_manager=:managerId WHERE id=:orderId;
            ");
            $recordManagerQuery->bindValue(':orderId', $orderId, PDO::PARAM_INT);
            $recordManagerQuery->bindValue(':managerId', array_key_exists($order['managerId'], $managerId) ? $managerId[$order['managerId']] : null, PDO::PARAM_INT);
            $recordManagerQuery->execute();
            return true;
        }

        //настроенно
        public function recordDiscountIfNeed($order) {
            if (!isset($order['discountPercent'])) {return false;}
            $orderId = substr($order['externalId'], 3);
            $recordManagerQuery = $this->db->prepare("
              UPDATE m_mag_Orders SET dPercent=:dPercent WHERE id=:orderId;
            ");
            $recordManagerQuery->bindValue(':orderId', $orderId, PDO::PARAM_INT);
            $recordManagerQuery->bindValue(':dPercent', $order['discountPercent'], PDO::PARAM_INT);
            $recordManagerQuery->execute();
            return true;
        }

    public function recordDeliveryIfNeed($order) {
        if (!isset($order['delivery'])) {return false;}
        $orderId = substr($order['externalId'], 3);
        $r = $this->api->ordersGet($order['externalId']);
        $delivery = $r['order']['delivery'];
        /** Изменился тип доставки */
        if (isset($delivery['code'])) {
            $editDeliveryCodeQuery = $this->db->prepare("
                UPDATE m_mag_Orders SET courier_address_id=:deliveryCode WHERE id=:orderId;
            ");
            $editDeliveryCodeQuery->bindParam(':deliveryCode', $delivery['code'], PDO::PARAM_INT);
            $editDeliveryCodeQuery->bindParam(':orderId', $orderId, PDO::PARAM_INT);
            $editDeliveryCodeQuery->execute();
            unset($editDeliveryCodeQuery);
        }
        /** Изменилась стоимость доставки */
        if (isset($delivery['cost'])) {
            $editDeliveryCost = $this->db->prepare("
                UPDATE m_mag_Orders SET delivery_cost=:deliveryCost WHERE id=:orderId;
            ");
            $editDeliveryCost->bindParam('deliveryCost', $delivery['cost'], PDO::PARAM_INT);
            $editDeliveryCost->bindParam('orderId', $orderId, PDO::PARAM_INT);
            $editDeliveryCost->execute();
            unset($editDeliveryCost);
            /** Обновим стоимость заказа */
            $this->recordOrderSum($r['order']);

        }
        $address = isset($delivery['address']) ? $delivery['address'] : array();
        /** Изменился город */
        if(isset($address['city'])) {
            $editCity = $this->db->prepare("
                UPDATE m_mag_Orders SET address_city=:city, pRegion=:region WHERE id=:orderId;
            ");
            $editCity->bindValue(':city', $address['city'], PDO::PARAM_STR);
            $editCity->bindValue(
                ':region',
                $address['city'] == "Москва" ? "moscow" : "region",
                PDO::PARAM_STR
            );
            $editCity->bindValue(':orderId', $orderId, PDO::PARAM_INT);
            $editCity->execute();
            unset($editCity);
        }
        /** Изменился регион */
        if (isset($address['region'])) {
            $editRegionQuery = $this->db->prepare("
                UPDATE m_mag_Orders SET address_region=:region WHERE id=:orderId;
            ");
            $editRegionQuery->bindValue(':region', $address['region'], PDO::PARAM_STR);
            $editRegionQuery->bindValue(':orderId', $orderId, PDO::PARAM_INT);
            $editRegionQuery->execute();
            unset($editRegionQuery);
        }
        /** Изменилась улица */
        if (isset($address['street'])) {
            $editStreetQuery = $this->db->prepare("
                UPDATE m_mag_Orders SET address_street=:street WHERE id=:orderId;
            ");
            $editStreetQuery->bindValue(':street', $address['street'], PDO::PARAM_STR);
            $editStreetQuery->bindValue(':orderId', $orderId, PDO::PARAM_INT);
            $editStreetQuery->execute();
            unset($editStreetQuery);
        }
        /** Изменился номер квартиры */
        if (isset($address['flat'])) {
            $editFlatQuery = $this->db->prepare("
                UPDATE m_mag_Orders SET address_flat=:flat WHERE id=:orderId;
            ");
            $editFlatQuery->bindValue(':flat', $address['flat'], PDO::PARAM_STR);
            $editFlatQuery->bindValue(':orderId', $orderId, PDO::PARAM_INT);
            $editFlatQuery->execute();
            unset($editFlatQuery);
        }
        /** Изменился подъезд */
        if (isset($address['block'])) {
            $editBlockQuery = $this->db->prepare("
                UPDATE m_mag_Orders SET address_stroenie=:block WHERE id=:orderId;
            ");
            $editBlockQuery->bindValue(':block', $address['block'], PDO::PARAM_STR);
            $editBlockQuery->bindValue(':orderId', $orderId, PDO::PARAM_INT);
            $editBlockQuery->execute();
            unset($editBlockQuery);
        }
        /** Изменилось строение/корпус */
        if (isset($address['house'])) {
            $editHouseQuery = $this->db->prepare("
                UPDATE m_mag_Orders SET address_corpus=:house WHERE id=:orderId;
            ");
            $editHouseQuery->bindValue(':house', $address['house'], PDO::PARAM_STR);
            $editHouseQuery->bindValue(':orderId', $orderId, PDO::PARAM_INT);
            $editHouseQuery->execute();
            unset($editHouseQuery);
        }
        /** Изменился этаж */
        if (isset($address['floor'])) {
            $editFloorQuery = $this->db->prepare("
                UPDATE m_mag_Orders SET address_floor=:floor WHERE id=:orderId;
            ");
            $editFloorQuery->bindValue(':floor', $address['floor'], PDO::PARAM_STR);
            $editFloorQuery->bindValue(':orderId', $orderId, PDO::PARAM_INT);
            $editFloorQuery->execute();
            unset($editFloorQuery);
        }
        /** Изменился дом */
        if (isset($address['building'])) {
            $editBuildingQuery = $this->db->prepare("
                UPDATE m_mag_Orders SET address_house=:building WHERE id=:orderId;
            ");
            $editBuildingQuery->bindValue(':building', $address['building'], PDO::PARAM_STR);
            $editBuildingQuery->bindValue(':orderId', $orderId, PDO::PARAM_INT);
            $editBuildingQuery->execute();
            unset($editBuildingQuery);
        }
        /** Изменился код домофона */
        if (isset($address['intercomCode'])) {
            $editCodeQuery = $this->db->prepare("
                UPDATE m_mag_Orders SET address_domophone=:intercomCode WHERE id=:orderId;
            ");
            $editCodeQuery->bindValue(':intercomCode', $address['intercomCode'], PDO::PARAM_STR);
            $editCodeQuery->bindValue(':orderId', $orderId, PDO::PARAM_INT);
            $editCodeQuery->execute();
            unset($editCodeQuery);
        }
        /** Изменился индекс */
        if (isset($address['index'])) {
            $editIndexQuery = $this->db->prepare("
                UPDATE m_mag_Orders SET postcode=:postCode WHERE id=:orderId;
            ");
            $editIndexQuery->bindValue(':postCode', $address['index'], PDO::PARAM_STR);
            $editIndexQuery->bindValue(':orderId', $orderId, PDO::PARAM_INT);
            $editIndexQuery->execute();
            unset($editIndexQuery);
        }
        /** Изменилось метро */
        /*
        if (isset($address['metro'])) {
            $editMetroQuery = $this->db->prepare("
                UPDATE m_mag_Orders SET metro=:metro WHERE id=:orderId;
            ");
            $editMetroQuery->bindValue(':metro', $address['metro'], PDO::PARAM_STR);
            $editMetroQuery->bindValue(':orderId', $orderId, PDO::PARAM_INT);
            $editMetroQuery->execute();
            unset($editMetroQuery);
        }*/
        /** Изменить текст адреса */
        if (isset($address['text'])) {
            $editTextQuery = $this->db->prepare("
                UPDATE m_mag_Orders SET pAddress=:address WHERE id=:orderId;
            ");
            $editTextQuery->bindValue(':address', $address['text'], PDO::PARAM_STR);
            $editTextQuery->bindValue(':orderId', $orderId, PDO::PARAM_INT);
            $editTextQuery->execute();
            unset($editTextQuery);
        }
        /** Изменить населенный пункт */
        if (isset($order['customFields']['settlement_orders'])) {
            $editSettlementQuery = $this->db->prepare("
                UPDATE m_mag_Orders SET address_settlement=:settlement WHERE id=:orderId;
            ");
            $editSettlementQuery->bindValue(':settlement', $order['customFields']['settlement_orders'], PDO::PARAM_STR);
            $editSettlementQuery->bindValue(':orderId', $orderId, PDO::PARAM_INT);
            $editSettlementQuery->execute();
            unset($editSettlementQuery);
        }
        return true;
    }

    public function recordContactDataIfNeed($order) {
        $r = $this->api->ordersGet($order['externalId']);
        $orderId = substr($order['externalId'], 3);
        /** Изменения в ФИО */
        if (isset($order['firstName']) || isset($order['lastName']) || isset($order['patronymic'])) {
            $fio = (isset($order['lastName']) ? $order['lastName'] : '') . ' ' .
                (isset($order['firstName']) ? $order['firstName'] : '') . ' ' .
                (isset($order['patronymic']) ? $order['patronymic'] : '');
            $fio = trim($fio);
            $editFIO = $this->db->prepare("
                UPDATE m_mag_Orders SET pFio=:fio WHERE id=:id;
            ");
            $editFIO->bindValue(':fio', $fio, PDO::PARAM_STR);
            $editFIO->bindValue(':id', $orderId, PDO::PARAM_INT);
            $editFIO->execute();
            unset($editFIO);
        }

        /** Изменился телефон */
        if (isset($order['phone'])) {
            $editPhoneName = $this->db->prepare("
                UPDATE m_mag_Orders SET pPhone=:phone WHERE id=:id;
            ");
            $editPhoneName->bindValue(':phone', $order['phone'], PDO::PARAM_STR);
            $editPhoneName->bindValue(':id', $orderId, PDO::PARAM_INT);
            $editPhoneName->execute();
            unset($editPhoneName);
        }

        /** Изменился email */
        if (isset($order['email'])) {
            $editEmailName = $this->db->prepare("
                UPDATE m_mag_Orders SET pEmail=:email WHERE id=:id;
            ");
            $editEmailName->bindValue(':email', $order['email'], PDO::PARAM_STR);
            $editEmailName->bindValue(':id', $orderId, PDO::PARAM_INT);
            $editEmailName->execute();
            unset($editEmailName);
        }

        /** Изменился комментарий менеджера */
        if (isset($order['managerComment'])) {
            $editCommentName = $this->db->prepare("
                UPDATE m_mag_Orders SET pComment=:comment WHERE id=:id;
            ");
            $editCommentName->bindValue(':comment', $order['managerComment'], PDO::PARAM_STR);
            $editCommentName->bindValue(':id', $orderId, PDO::PARAM_INT);
            $editCommentName->execute();
            unset($editCommentName);
        }
    }

    public function recordOrderSum($order) {
        if (!isset($order['summ'])) {return false;}
        $r = $this->api->ordersGet($order['externalId']);
        $orderId = substr($order['externalId'], 3);
        $updateSum = $this->db->prepare("
            UPDATE m_mag_Orders SET Sum=:sum, dSum=:dSum WHERE id=:orderId;
        ");
        $updateSum->bindValue('sum', $order['summ'], PDO::PARAM_INT);
        $updateSum->bindValue('dSum', $r['order']['totalSumm'], PDO::PARAM_INT);
        $updateSum->bindValue('orderId', $orderId, PDO::PARAM_INT);
        $updateSum->execute();
        unset($updateSum);
        return true;
    }

    public function recordNewOrder(&$newOrder) {
        if(!isset($newOrder['customer']['externalId'])) {
            $recordNewCustomer = $this->db->prepare("
                INSERT INTO m_mag_Users(date_register, email)
                VALUES (:createdAt, :email)
            ");
            $recordNewCustomer->bindValue(':createdAt', $newOrder['customer']['createdAt']);
            $recordNewCustomer->bindValue(':email', uniqid() . '@crm.ru');
            $recordNewCustomer->execute();
            $customerId = $this->db->lastInsertId();
            /** Синхронизируем номера */
            $this->api->customersFixExternalIds(
                array(
                    array('id' => $newOrder['customer']['id'], 'externalId' => $customerId)
                )
            );

            $newOrder['customer']['externalId'] = $customerId;
        }

        //$this->recordNewCustomers($newOrder['customer']);
        $this->recordContactDataIfNeedC($newOrder['customer']);
        $this->recordAddressIfNeed($newOrder['customer']);
        $this->recordCustomFieldsIfNeed($newOrder['customer']);
        $this->recordPercentIfNeed($newOrder['customer']);

        /** Создадим новый заказ в БД */
        $recordNewOrder = $this->db->prepare("
            INSERT INTO m_mag_Orders(id, id_user, DateAdd, DateEdit, delivery_cost, payment_type)
            VALUES (NULL, :customerId, :createdAt, :updatedAt, :deliveryCost, :payment_type)
        ");
        $recordNewOrder->bindValue(':customerId', $newOrder['customer']['externalId']);
        $recordNewOrder->bindValue(':createdAt', $newOrder['createdAt']);
        $recordNewOrder->bindValue(':updatedAt', date('Y-m-d H:i:s'));
        $recordNewOrder->bindValue(
            ':deliveryCost',
            (isset($newOrder['delivery']) && isset($newOrder['delivery']['cost'])) ? $newOrder['delivery']['cost'] : 0
        );
        $recordNewOrder->bindValue(
            ':payment_type',
            (isset($newOrder['payment_type'])) ? $newOrder['payment_type'] : ''
        );
        $recordNewOrder->execute();
        $orderId = $this->container->settings['general']['prefix_site'] . $this->db->lastInsertId();

        /** Синхронизируем номера */
        $this->api->ordersFixExternalIds(
            array(
                array('id' => $newOrder['id'], 'externalId' => $orderId)
            )
        );
        $this->api->ordersEdit(array('externalId' => $orderId, 'number' => $orderId));
        $newOrder['externalId'] = $orderId;
        return true;
    }

    public function recordAddressIfNeed($customer) {
        if (!isset($customer['address'])) {return false;}
        $customerId = $customer['externalId'];

        $address = isset($address['address']) ? $address['address'] : array();
        /** Изменился город */
        if(isset($address['city'])) {
            $editCity = $this->db->prepare("
                UPDATE m_mag_Users SET city=:city, region=:region, address_city=:city WHERE id=:customerId;
            ");
            $editCity->bindValue(':city', $address['city'], PDO::PARAM_STR);
            $editCity->bindValue(
                ':region',
                $address['city'] == "Москва" ? "moscow" : "region",
                PDO::PARAM_STR
            );
            $editCity->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editCity->execute();
            unset($editCity);
        }
        /** Изменился регион */
        if (isset($address['region'])) {
            $editRegionQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_region=:region WHERE id=:customerId;
            ");
            $editRegionQuery->bindValue(':region', $address['region'], PDO::PARAM_STR);
            $editRegionQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editRegionQuery->execute();
            unset($editRegionQuery);
        }
        /** Изменилась улица */
        if (isset($address['street'])) {
            $editStreetQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_street=:street WHERE id=:customerId;
            ");
            $editStreetQuery->bindValue(':street', $address['street'], PDO::PARAM_STR);
            $editStreetQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editStreetQuery->execute();
            unset($editStreetQuery);
        }
        /** Изменился номер квартиры */
        if (isset($address['flat'])) {
            $editFlatQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_flat=:flat WHERE id=:customerId;
            ");
            $editFlatQuery->bindValue(':flat', $address['flat'], PDO::PARAM_STR);
            $editFlatQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editFlatQuery->execute();
            unset($editFlatQuery);
        }
        /** Изменился подъезд */
        if (isset($address['block'])) {
            $editBlockQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_stroenie=:block WHERE id=:customerId;
            ");
            $editBlockQuery->bindValue(':block', $address['block'], PDO::PARAM_STR);
            $editBlockQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editBlockQuery->execute();
            unset($editBlockQuery);
        }
        /** Изменилось строение/корпус */
        if (isset($address['house'])) {
            $editHouseQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_corpus=:house WHERE id=:customerId;
            ");
            $editHouseQuery->bindValue(':house', $address['house'], PDO::PARAM_STR);
            $editHouseQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editHouseQuery->execute();
            unset($editHouseQuery);
        }
        /** Изменился этаж */
        if (isset($address['floor'])) {
            $editFloorQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_floor=:floor WHERE id=:customerId;
            ");
            $editFloorQuery->bindValue(':floor', $address['floor'], PDO::PARAM_STR);
            $editFloorQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editFloorQuery->execute();
            unset($editFloorQuery);
        }
        /** Изменился дом */
        if (isset($address['building'])) {
            $editBuildingQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_house=:building WHERE id=:customerId;
            ");
            $editBuildingQuery->bindValue(':building', $address['building'], PDO::PARAM_STR);
            $editBuildingQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editBuildingQuery->execute();
            unset($editBuildingQuery);
        }
        /** Изменился код домофона */
        if (isset($address['intercomCode'])) {
            $editCodeQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_domophone=:intercomCode WHERE id=:customerId;
            ");
            $editCodeQuery->bindValue(':intercomCode', $address['intercomCode'], PDO::PARAM_STR);
            $editCodeQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editCodeQuery->execute();
            unset($editCodeQuery);
        }
        /** Изменился индекс */
        if (isset($address['index'])) {
            $editIndexQuery = $this->db->prepare("
                UPDATE m_mag_Users SET postcode=:postCode WHERE id=:customerId;
            ");
            $editIndexQuery->bindValue(':postCode', $address['index'], PDO::PARAM_STR);
            $editIndexQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editIndexQuery->execute();
            unset($editIndexQuery);
        }
        /** Изменилось метро */
        /*
        if (isset($address['metro'])) {
            $editMetroQuery = $this->db->prepare("
                UPDATE m_mag_Users SET metro=:metro WHERE id=:customerId;
            ");
            $editMetroQuery->bindValue(':metro', $address['metro'], PDO::PARAM_STR);
            $editMetroQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editMetroQuery->execute();
            unset($editMetroQuery);
        }*/
        /** Изменить текст адреса */
        if (isset($address['text'])) {
            $editTextQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address=:address WHERE id=:customerId;
            ");
            $editTextQuery->bindValue(':address', $address['text'], PDO::PARAM_STR);
            $editTextQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editTextQuery->execute();
            unset($editTextQuery);
        }

    }

    public function recordCustomFieldsIfNeed($customer) {
        if (!isset($customer['customFields'])) {return false;}
        $customerId = $customer['externalId'];
        /** Изменился пол */
        if (isset($customer['customFields']['gender'])) {
            $editGender = $this->db->prepare("
                UPDATE m_mag_Users SET pol=:pol WHERE id=:id;
            ");
            $editGender->bindValue(':pol', strtoupper($customer['customFields']['gender']), PDO::PARAM_STR);
            $editGender->bindValue(':id', $customer['externalId'], PDO::PARAM_INT);
            $editGender->execute();
            unset($editGender);
        }

        /** Изменилось поле получения рассылки */
        if (isset($customer['customFields']['maillist'])) {
            $editMaillist = $this->db->prepare("
                UPDATE m_mag_Users SET is_maillist=:maillist WHERE id=:id;
            ");
            $editMaillist->bindValue(':maillist', $customer['customFields']['maillist'], PDO::PARAM_STR);
            $editMaillist->bindValue(':id', $customer['externalId'], PDO::PARAM_INT);
            $editMaillist->execute();
            unset($editMaillist);
        }

        /** Изменить населенный пункт */
        if (isset($customer['customFields']['settlement'])) {
            $editSettlementQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_settlement=:settlement WHERE id=:customerId;
            ");
            $editSettlementQuery->bindValue(':settlement', $customer['customFields']['settlement'], PDO::PARAM_STR);
            $editSettlementQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editSettlementQuery->execute();
            unset($editSettlementQuery);
        }

        /** Изменить накопленную сумму */
        if (isset($customer['customFields']['nakop_summ'])) {
            $editSummQuery = $this->db->prepare("
                UPDATE m_mag_Users SET d_sum_all=:summ WHERE id=:customerId;
            ");
            $editSummQuery->bindValue(':summ', $customer['customFields']['nakop_summ'], PDO::PARAM_STR);
            $editSummQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editSummQuery->execute();
            unset($editSummQuery);
        }
    }

    public function recordPercentIfNeed($customer) {
        if (!isset($customer['personalDiscount'])) {return false;}
        /** Изменился персональную скидку */
        if (isset($customer['personalDiscount'])) {
            $editGender = $this->db->prepare("
                UPDATE m_mag_Users SET d_percent=:percent WHERE id=:id;
            ");
            $editGender->bindValue(':percent', $customer['personalDiscount'], PDO::PARAM_STR);
            $editGender->bindValue(':id', $customer['externalId'], PDO::PARAM_INT);
            $editGender->execute();
            unset($editGender);
        }
    }

    public function recordContactDataIfNeedC($customer) {
        $r = $this->api->customersGet($customer['externalId']);

        /** Изменения в ФИО */
        if (isset($customer['firstName']) || isset($customer['lastName']) || isset($customer['patronymic'])) {
            $fio = (isset($r['customer']['lastName']) ? $r['customer']['lastName'] : '') . ' ' .
                (isset($r['customer']['firstName']) ? $r['customer']['firstName'] : '') . ' ' .
                (isset($r['customer']['patronymic']) ? $r['customer']['patronymic'] : '');
            $fio = trim($fio);
            $editfio = $this->db->prepare("
                UPDATE m_mag_Users SET fio=:fio WHERE id=:id;
            ");
            $editfio->bindValue(':fio',$fio, PDO::PARAM_STR);
            $editfio->bindValue(':id', $customer['externalId'], PDO::PARAM_INT);
            $editfio->execute();
            unset($editfio);
        }

        /** Изменился телефон */
        if (isset($customer['phones'][0]['number']) || isset($customer['phones'][1]['number'])) {
            $editPhone = $this->db->prepare("
                UPDATE m_mag_Users SET phone=:phone1, phone2=:phone2 WHERE id=:id;
            ");
            $editPhone->bindValue(':phone1',$customer['phones'][0]['number'], PDO::PARAM_STR);
            $editPhone->bindValue(':phone2', (isset($customer['phones'][1]['number']) ? $customer['phones'][1]['number']:0), PDO::PARAM_STR);
            $editPhone->bindValue(':id', $customer['externalId'], PDO::PARAM_INT);
            $editPhone->execute();
            unset($editPhone);
        }

        /** Изменился email */
        if (isset($customer['email'])) {
            $editEmailName = $this->db->prepare("
                UPDATE m_mag_Users SET email=:email WHERE id=:id;
            ");
            $editEmailName->bindValue(':email', $customer['email'], PDO::PARAM_STR);
            $editEmailName->bindValue(':id', $customer['externalId'], PDO::PARAM_INT);
            $editEmailName->execute();
            unset($editEmailName);
        }

        /** Изменилось день рождение */
        if (isset($customer['birthday'])) {
            $editBirthday = $this->db->prepare("
                UPDATE m_mag_Users SET date_birthday=:birthday WHERE id=:id;
            ");
            $editBirthday->bindValue(':birthday', $customer['birthday'], PDO::PARAM_STR);
            $editBirthday->bindValue(':id', $customer['externalId'], PDO::PARAM_INT);
            $editBirthday->execute();
            unset($editBirthday);
        }

    }
}
