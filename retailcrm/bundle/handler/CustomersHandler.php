<?php

class CustomersHandler implements HandlerInterface
{
    public function prepare($data)
    {
        echo date("Y-m-d H:i:s") . "\n";
        var_dump('Общее кол-во клиентов отданных на проверку: '. count($data));
        $settings = Container::getInstance()->settings;
        $api = new RequestProxy($settings['api']['url'], $settings['api']['key']);
        $customers = array();
        foreach ($data as $customer) {
            //Проверяем нет ли этого клиента в CRM
            $r = $api->customersList(array('email' => $customer['email']));
            if (!$r['customers']) {
                $customerFIO = DataHelper::explodeFIO($customer['fio']);
                $customers[] = array(
                    'externalId' => $customer['externalId'],
                    'email' => $customer['email'],
                    'createdAt' => $customer['createdAt'],
                    'firstName' => $customerFIO['firstName'],
                    'lastName' => $customerFIO['lastName'],
                    'patronymic' => $customerFIO['patronymic'],
                    'birthday' => ($customer['birthday'] != '0000-00-00'?$customer['birthday']:null),
                    'phones' => array(
                        array('number' => $customer['phone']),
                        array('number' => $customer['phone2'])
                    ),
                    'personalDiscount' => $customer['d_percent'],
                    'address' => array(
                        'index' => $customer['postcode'],
                        'region' => $customer['region'],
                        'city' => $customer['city'],
                        'street' => $customer['street'],
                        'building' => $customer['building'],
                        'block' => $customer['block'],
                        'house' => $customer['house'],
                        'floor' => $customer['floor'],
                        'metro' => $customer['metro'],
                        'intercomCode' => $customer['intercomCode'],
                        'flat' => $customer['flat'],
                        'text' => $customer['address']
                    ),
                    'source' => array(
                        'source' => $customer['utm_source_first'],
                        'medium' => $customer['utm_medium_first'],
                        'campaign' => $customer['utm_campaign_first'],
                        'keyword' => $customer['utm_term_first'],
                    ),
                    //Не забыть создать пользовательские поля
                    'customFields' => array(
                        'settlement' => $customer['settlement'], //Населенный пункт если есть
                        'nakop_summ' => $customer['d_sum_all'], //Маржа
                        'gender' =>  (isset($customer['pol'])?strtolower($customer['pol']):null), //пол
                        'maillist' => ($customer['is_maillist'] == '1'?true:false), //рассылка
                ));
                $customers = array_filter($customers);
                echo ($customer['externalId'] . ' : ' .  $customer['email'] . "\n");
            }
        }
        var_export($customers);
        var_dump('Кол-во клиентов прошедших проверку: '. count($customers));
        return $customers;
    }

}
