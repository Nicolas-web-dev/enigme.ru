<?php


class CategoriesHandler implements  HandlerInterface {

    public function prepare($data)
    {
        $categories = [];
        //print_r($data);die;
        foreach ($data as $value) {
            $value['name'] = str_replace('&amp;#039;', '&apos;', str_replace('&', '&amp;', $value['name']));
            $categories[] = array(
                'id' => $value['id'],
                'name' => $value['name'],
            );

        }
        return $categories;
    }
}
