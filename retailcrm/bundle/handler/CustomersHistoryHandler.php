<?php

class customersHistoryHandler implements HandlerInterface {

    /** @var ApiClient $api */
    private $api;

    /** @var PDO $db */
    private $db;

    private $container;
    //Сделано, но не проверенно
    public function prepare($data) {
        $this->container = Container::getInstance();
        /** @var ApiClient $apiClient */
        $this->api = new RequestProxy($this->container->settings['api']['url'], $this->container->settings['api']['key']);
        $this->db = Container::getInstance()->db;

        $existsCustomers = array_filter($data, function($c){return isset($c['externalId']);});
        $newCustomers = array_filter($data, function($c){return isset($c['created']);});
        $modifyCustomers = array_filter(
            $existsCustomers,
            function($c){
                return !isset($c['created']) && !isset($c['deleted']);
            }
        );

        foreach ($newCustomers as $customer) {
            $this->recordNewCustomers($customer);
            $this->recordContactDataIfNeed($customer);
            $this->recordAddressIfNeed($customer);
            $this->recordCustomFieldsIfNeed($customer);
            $this->recordPercentIfNeed($customer);
        }

        foreach ($modifyCustomers as $customer) {
            $this->recordContactDataIfNeed($customer);
            $this->recordAddressIfNeed($customer);
            $this->recordCustomFieldsIfNeed($customer);
            $this->recordPercentIfNeed($customer);
        }
    }

    public function recordAddressIfNeed($customer) {
        if (!isset($customer['address'])) {return false;}
        $customerId = $customer['externalId'];

        $address = isset($address['address']) ? $address['address'] : array();
        /** Изменился город */
        if(isset($address['city'])) {
            $editCity = $this->db->prepare("
                UPDATE m_mag_Users SET city=:city, region=:region, address_city=:city WHERE id=:customerId;
            ");
            $editCity->bindValue(':city', $address['city'], PDO::PARAM_STR);
            $editCity->bindValue(
                ':region',
                $address['city'] == "Москва" ? "moscow" : "region",
                PDO::PARAM_STR
            );
            $editCity->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editCity->execute();
            unset($editCity);
        }
        /** Изменился регион */
        if (isset($address['region'])) {
            $editRegionQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_region=:region WHERE id=:customerId;
            ");
            $editRegionQuery->bindValue(':region', $address['region'], PDO::PARAM_STR);
            $editRegionQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editRegionQuery->execute();
            unset($editRegionQuery);
        }
        /** Изменилась улица */
        if (isset($address['street'])) {
            $editIndexQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_street=:street WHERE id=:customerId;
            ");
            $editStreetQuery->bindValue(':street', $address['street'], PDO::PARAM_STR);
            $editStreetQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editStreetQuery->execute();
            unset($editStreetQuery);
        }
        /** Изменился номер квартиры */
        if (isset($address['flat'])) {
            $editFlatQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_flat=:flat WHERE id=:customerId;
            ");
            $editFlatQuery->bindValue(':flat', $address['flat'], PDO::PARAM_STR);
            $editFlatQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editFlatQuery->execute();
            unset($editFlatQuery);
        }
        /** Изменился подъезд */
        if (isset($address['block'])) {
            $editBlockQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_stroenie=:block WHERE id=:customerId;
            ");
            $editBlockQuery->bindValue(':block', $address['block'], PDO::PARAM_STR);
            $editBlockQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editBlockQuery->execute();
            unset($editBlockQuery);
        }
        /** Изменилось строение/корпус */
        if (isset($address['house'])) {
            $editHouseQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_corpus=:house WHERE id=:customerId;
            ");
            $editHouseQuery->bindValue(':house', $address['house'], PDO::PARAM_STR);
            $editHouseQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editHouseQuery->execute();
            unset($editHouseQuery);
        }
        /** Изменился этаж */
        if (isset($address['floor'])) {
            $editFloorQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_floor=:floor WHERE id=:customerId;
            ");
            $editFloorQuery->bindValue(':floor', $address['floor'], PDO::PARAM_STR);
            $editFloorQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editFloorQuery->execute();
            unset($editFloorQuery);
        }
        /** Изменился дом */
        if (isset($address['building'])) {
            $editBuildingQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_house=:building WHERE id=:customerId;
            ");
            $editBuildingQuery->bindValue(':building', $address['building'], PDO::PARAM_STR);
            $editBuildingQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editBuildingQuery->execute();
            unset($editBuildingQuery);
        }
        /** Изменился код домофона */
        if (isset($address['intercomCode'])) {
            $editCodeQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_domophone=:intercomCode WHERE id=:customerId;
            ");
            $editCodeQuery->bindValue(':intercomCode', $address['intercomCode'], PDO::PARAM_STR);
            $editCodeQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editCodeQuery->execute();
            unset($editCodeQuery);
        }
        /** Изменился индекс */
        if (isset($address['index'])) {
            $editIndexQuery = $this->db->prepare("
                UPDATE m_mag_Users SET postcode=:postCode WHERE id=:customerId;
            ");
            $editIndexQuery->bindValue(':postCode', $address['index'], PDO::PARAM_STR);
            $editIndexQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editIndexQuery->execute();
            unset($editIndexQuery);
        }
        /** Изменилось метро */
        /*
        if (isset($address['metro'])) {
            $editMetroQuery = $this->db->prepare("
                UPDATE m_mag_Users SET metro=:metro WHERE id=:customerId;
            ");
            $editMetroQuery->bindValue(':metro', $address['metro'], PDO::PARAM_STR);
            $editMetroQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editMetroQuery->execute();
            unset($editMetroQuery);
        }*/
        /** Изменить текст адреса */
        if (isset($address['text'])) {
            $editTextQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address=:address WHERE id=:customerId;
            ");
            $editTextQuery->bindValue(':address', $address['text'], PDO::PARAM_STR);
            $editTextQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editTextQuery->execute();
            unset($editTextQuery);
        }

    }

    public function recordContactDataIfNeed($customer) {
        $r = $this->api->customersGet($customer['externalId']);

        /** Изменения в ФИО */
        if (isset($customer['firstName']) || isset($customer['lastName']) || isset($customer['patronymic'])) {
            $fio = (isset($r['customer']['lastName']) ? $r['customer']['lastName'] : '') . ' ' .
                (isset($r['customer']['firstName']) ? $r['customer']['firstName'] : '') . ' ' .
                (isset($r['customer']['patronymic']) ? $r['customer']['patronymic'] : '');
            $fio = trim($fio);
            $editFIO = $this->db->prepare("
                UPDATE m_mag_Users SET fio=:fio WHERE id=:id;
            ");
            $editFIO->bindValue(':fio', $fio, PDO::PARAM_STR);
            $editFIO->bindValue(':id', $customer['externalId'], PDO::PARAM_INT);
            $editFIO->execute();
            unset($editFIO);
        }

        /** Изменился телефон */
        if (isset($customer['phones'][0]['number']) || isset($customer['phones'][1]['number'])) {
            $editPhone = $this->db->prepare("
                UPDATE m_mag_Users SET phone=:phone1, phone2=:phone2 WHERE id=:id;
            ");
            $editPhone->bindValue(':phone1',$customer['phones'][0]['number'], PDO::PARAM_STR);
            $editPhone->bindValue(':phone2', $customer['phones'][1]['number'], PDO::PARAM_STR);
            $editPhone->bindValue(':id', $customer['externalId'], PDO::PARAM_INT);
            $editPhone->execute();
            unset($editPhone);
        }

        /** Изменился email */
        if (isset($customer['email'])) {
            $editEmailName = $this->db->prepare("
                UPDATE m_mag_Users SET email=:email WHERE id=:id;
            ");
            $editEmailName->bindValue(':email', $customer['email'], PDO::PARAM_STR);
            $editEmailName->bindValue(':id', $customer['externalId'], PDO::PARAM_INT);
            $editEmailName->execute();
            unset($editEmailName);
        }

        /** Изменилось день рождение */
        if (isset($customer['birthday'])) {
            $editBirthday = $this->db->prepare("
                UPDATE m_mag_Users SET date_birthday=:birthday WHERE id=:id;
            ");
            $editBirthday->bindValue(':birthday', $customer['birthday'], PDO::PARAM_STR);
            $editBirthday->bindValue(':id', $customer['externalId'], PDO::PARAM_INT);
            $editBirthday->execute();
            unset($editBirthday);
        }

    }

    public function recordCustomFieldsIfNeed($customer) {
        if (!isset($customer['customFields'])) {return false;}
        /** Изменился пол */
        if (isset($customer['customFields']['gender'])) {
            $editGender = $this->db->prepare("
                UPDATE m_mag_Users SET pol=:pol WHERE id=:id;
            ");
            $editGender->bindValue(':pol', strtoupper($customer['customFields']['gender']), PDO::PARAM_STR);
            $editGender->bindValue(':id', $customer['externalId'], PDO::PARAM_INT);
            $editGender->execute();
            unset($editGender);
        }

        /** Изменилось поле получения рассылки */
        if (isset($customer['customFields']['maillist'])) {
            $editMaillist = $this->db->prepare("
                UPDATE m_mag_Users SET is_maillist=:maillist WHERE id=:id;
            ");
            $editMaillist->bindValue(':maillist', $customer['customFields']['maillist'], PDO::PARAM_STR);
            $editMaillist->bindValue(':id', $customer['externalId'], PDO::PARAM_INT);
            $editMaillist->execute();
            unset($editMaillist);
        }

        /** Изменить населенный пункт */
        if (isset($customer['customFields']['settlement'])) {
            $editSettlementQuery = $this->db->prepare("
                UPDATE m_mag_Users SET address_settlement=:settlement WHERE id=:customerId;
            ");
            $editSettlementQuery->bindValue(':settlement', $customer['customFields']['settlement'], PDO::PARAM_STR);
            $editSettlementQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editSettlementQuery->execute();
            unset($editSettlementQuery);
        }

        /** Изменить накопленную сумму */
        if (isset($customer['customFields']['nakop_summ'])) {
            $editSummQuery = $this->db->prepare("
                UPDATE m_mag_Users SET d_sum_all=:summ WHERE id=:customerId;
            ");
            $editSummQuery->bindValue(':summ', $customer['customFields']['nakop_summ'], PDO::PARAM_STR);
            $editSummQuery->bindValue(':customerId', $customerId, PDO::PARAM_INT);
            $editSummQuery->execute();
            unset($editSummQuery);
        }
    }

    public function recordPercentIfNeed($customer) {
        if (!isset($customer['personalDiscount'])) {return false;}
        /** Изменился персональную скидку */
        if (isset($customer['personalDiscount'])) {
            $editGender = $this->db->prepare("
                UPDATE m_mag_Users SET d_percent=:percent WHERE id=:id;
            ");
            $editGender->bindValue(':percent', $customer['personalDiscount'], PDO::PARAM_STR);
            $editGender->bindValue(':id', $customer['externalId'], PDO::PARAM_INT);
            $editGender->execute();
            unset($editGender);
        }
    }

    public function recordNewCustomers(&$newCustomer) {
        /** Создадим новый заказ в БД */
        // Добавить пароль при создании в клиента
            $recordNewCustomer = $this->db->prepare("
            INSERT INTO m_mag_Users(id, date_register)
            VALUES (NULL, :createdAt)
        ");
        $recordNewCustomer->bindValue(':createdAt', $newCustomer['createdAt']);
        $recordNewCustomer->execute();
        $customerId = $this->db->lastInsertId();
        /** Синхронизируем номера */
        $this->api->customersFixExternalIds(
            array(
                array('id' => $newCustomer['id'], 'externalId' => $customerId)
            )
        );
        $this->api->customersEdit(array('externalId' => $customerId));
        $newCustomer['externalId'] = $customerId;
        print_r($newCustomer);
        return true;
    }
}
