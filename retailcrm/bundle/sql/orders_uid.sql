SELECT
    O.id as externalId,
    IF(id_user=0, (
        SELECT id FROM `m_mag_Users` WHERE phone=pPhone LIMIT 1
        ), id_user
    ) as customerId,
    IF(id_user=0, pFio, fio) as fio,
    IF(id_user=0, pPhone, phone) as phone,
    IF(id_user=0, pEmail, email) as email,
    pAddress,
    U.postcode as postcode,
    U.address_region  as region,
    U.address_city as city,
    U.address_settlement as settlement,
    U.address_street as street,
    U.address_house as building,
    U.address_corpus as house,
    U.address_stroenie as block,
    U.address_floor as floor,
    U.address_domophone as intercomCode,
    U.address_flat as flat,
    DateAdd as createdAt,
    id_manager as managerId,
    status,
    DateEnd,
    dPercent as discountPercent,
    payment_type as paymentType,
    pComment as managerComment,
    is_phone,
    utm_source_last,
    utm_medium_last,
    utm_campaign_last,
    utm_term_last,
    (SELECT
        GROUP_CONCAT(CONCAT_WS('~', id_catalog_data_order, title, kol, sum, articul_catalog_data_order, margin, sklad) SEPARATOR '|') as items
        FROM
        m_mag_OrdersSum as I
        WHERE I.id_order=externalId
        GROUP BY I.id_order
    ) as items,
    (SELECT
        CONCAT(
           "Курьер: ", (SELECT name FROM couriers WHERE couriersId=`courier_id`), "\n",
           "Кол-во дней: ", `delivery_days`, "\n",
           "Тип: ", `delivery_type`, "\n",
           IF(`delivery_code`<>0, CONCAT("Код: ", `delivery_code`, "\n"), ''),
           "Адрес пункта самовывоза: ", `delivery_address`, "\n",
           "Оплата: ", `delivery_payment`
       )
    FROM `couriers_addresses` where  id=O.courier_address_id) as delivery_self,
O.courier_address_id
FROM
    `m_mag_Orders` as O
    LEFT JOIN
    `m_mag_Users` as U
    ON
    O.id_user = U.id
WHERE
    FIND_IN_SET(O.id, :orderIds)
