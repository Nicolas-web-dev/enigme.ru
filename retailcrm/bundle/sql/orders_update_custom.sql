SELECT
    OS.id_order as externalId,
    OS.id_catalog_data_order as id_catalog_do,
    OS.kol as quantity,
    (
        SELECT
        CONCAT_WS(
            ';',
            IF(price_usd1 <> 0, price_usd1, null),
            IF(price_usd2 <> 0, price_usd2, null),
            IF(price_usd3 <> 0, price_usd3, null),
            IF(price_usd4 <> 0, price_usd4, null),
            IF(price_usd5 <> 0, price_usd5, null),
            IF(price_usd6 <> 0, price_usd6, null),
            IF(price_usd7 <> 0, price_usd7, null),
            IF(price_usd8 <> 0, price_usd8, null),
            IF(price_usd9 <> 0, price_usd9, null),
            IF(price_usd10 <> 0, price_usd10, null),
            IF(price_usd11 <> 0, price_usd11, null),
            IF(price_usd12 <> 0, price_usd12, null),
            IF(price_usd13 <> 0, price_usd13, null),
            IF(price_usd14 <> 0, price_usd14, null),
            IF(price_usd15 <> 0, price_usd15, null),
            IF(price_usd16 <> 0, price_usd16, null),
            IF(price_usd17 <> 0, price_usd17, null),
            IF(price_usd18 <> 0, price_usd18, null)
        )
        FROM `m_catalog_data_order` WHERE id=id_catalog_do) as cost
FROM
`m_mag_OrdersSum` as OS
WHERE
    FIND_IN_SET(id_order, :orderIds)
