SELECT
    DO.id as id,
    DO.articul as article,
    DO.type,
    DO.v,
    DO.price as price,
    DO.sklad,
    D.title as name,
    D.id as productId,
    D.id_catalog as categoryId,
    article as article1,
    article2,
    article3,
    article4,
    article5,
    article6,
    article7,
    article8,
    article9,
    article10,
    article11,
    article12,
    article13,
    article14,
    article15,
    article16,
    article17,
    article18,
    price_usd1,
    price_usd2,
    price_usd3,
    price_usd4,
    price_usd5,
 	price_usd6,
 	price_usd7,
 	price_usd8,
 	price_usd9,
 	price_usd10,
 	price_usd11,
 	price_usd12,
 	price_usd13,
	price_usd14,
    price_usd15,
    price_usd16,
    price_usd17,
    price_usd18,
    IF(D.img<>'', CONCAT('http://enigme.ru/images/uploads/catalog/', D.id, '/small/', D.img), NULL)  as picture,
    IF(D.url<>'', CONCAT('http://enigme.ru/catalog/', (SELECT url FROM `m_catalog` WHERE id = D.id_catalog), '/', D.url), NULL)  as url,
    D.pol,
    (SELECT title FROM `m_catalog` WHERE id = D.id_catalog) as vendor
FROM
    `m_catalog_data_order` as DO
LEFT JOIN
    `m_catalog_data` as D
ON
    DO.id_catalog_data = D.id
WHERE
    DO.type <> '0'
LIMIT :step , :chuck
