SELECT
    pid as externalId,
    action,
    value_after as value,
    date as createdAt,
    text as items
FROM `m_mag_Log`
WHERE date > :lastSync
