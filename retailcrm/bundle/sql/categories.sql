SELECT
    id,
    title as name
FROM `m_catalog`
ORDER BY title ASC
