<?php
$per_page= 25;


$aTypes = $db->fetchAll($db->select()->from('m_catalog_type')->where('sale=1'));
foreach ($aTypes as $type) {
    $type_pages[] = $type['slug'];
}

//check canon names
if ($canon_name2 && $canon_name2!='page' && $canon_name3!='page' && !in_array($canon_name2,$type_pages)){
    err_301($canon_name1);
}
if (!$canon_name4 && $canon_name3 && !is_numeric($canon_name3)){
    err_301($canon_name1);
}

if ($canon_name4 && !is_numeric($canon_name4)){
    err_301($canon_name1);
}



$page = ($canon_name3)?$canon_name3:1;
if ($canon_name4){
    $page = ($canon_name4)?$canon_name4:1;
}



$page = (int)(($page));


if ($page>1){
    $constructor['meta_noindex'] = true;
}




$select = $db->select()
    ->from(['a'=>'m_catalog_data'],['a.id','a.title','a.url','a.pol','a.price_s','a.img','a.text','a.tag'])
    ->joinLeft(['b'=>'m_catalog'],' b.id=a.id_catalog',['brend_title'=>'b.title','brend_url'=>'b.url'])
    ->joinLeft(['o'=>'m_catalog_data_order'],'o.id_catalog_data=a.id','')
    ->where('a.id_cat=82 AND a.is_spec=1')
    ->group('o.id_catalog_data')
    ->limitPage($page,$per_page);

if (in_array($canon_name2,$type_pages)){
    if ($aromat_type = $db->fetchRow($db->select()->from('m_catalog_type')->where('slug=?',$canon_name2))){
        $view->h1 = $aromat_type['sale_h1'];
        $constructor['meta_title'] = $aromat_type['sale_title'];;
        $constructor['meta_keywords'] = $aromat_type['sale_keywords'];
        $constructor['meta_description'] = $aromat_type['sale_description'];
        $view->meta_body = $aromat_type['sale_body'];
    }
}else{
    $aromat_type = $db->fetchRow($db->select()->from('m_catalog_type')->where('slug=?','all'));
    $view->h1 = $aromat_type['sale_h1'];
    $constructor['meta_title'] = $aromat_type['sale_title'];;
    $constructor['meta_keywords'] = $aromat_type['sale_keywords'];
    $constructor['meta_description'] = $aromat_type['sale_description'];
    $view->meta_body = $aromat_type['sale_body'];
}

if ($aromat_type['type_ident']!='' && $aromat_type['type_ident']!='all'){
    $select->where('o.type=?',$aromat_type['type_ident']);
}



$aItems = $db->fetchAll($select);


foreach ($aItems as $c_catalog) {
    if ($c_catalog['img']=='') {
        $img = '/images/noimage.jpg';
        $img_big = '/images/noimage.jpg';
    }else {
        $img = '/images/uploads/catalog/'.$c_catalog['id'].'/small/'.$c_catalog['img'];
        $img_big = '/images/uploads/catalog/'.$c_catalog['id'].'/big/'.$c_catalog['img'];
    }

    if (!$og_image){
        $og_image = $img;
    }

    if ($c_catalog['tag'] != '') {
        $tag = explode(',', $c_catalog['tag']);
        foreach ($tag as $key => $value) {

            if ($value == '') continue;

            $item_tag[] = array(
                'url' => translit($value),
                'title' => $value,
            );
        }
    }

    /* -=========================================
	���������
	*/
    if(!empty($ses_note->id)){
        if (in_array($c_catalog['id'], $ses_note->id)){
            $is_note = true;
        }else{
            $is_note = false;
        }
    }else {
        $is_note = false;
    }

    if ($aromat_type['slug']!='all'){
        $c_catalog['url'] = $c_catalog['url'].'/'.$aromat_type['slug'];
    }


    $catalog_item[] = array(
        'id' 			=> $c_catalog['id'],
        'pol' 			=> $c_catalog['pol'],
        'title' 		=> $c_catalog['title'],
        'brend_title' 	=> $c_catalog['brend_title'],
        'price_s' 		=> $c_catalog['price_s']+$_SESSION['_GMARGIN'],
        'img' 			=> $img,
        'img_big' 		=> $img_big,
        'url' 			=> '/catalog/'.$c_catalog['brend_url'].'/'.$c_catalog['url'].'/',
        'anot' 			=> maxsite_str_word(html_entity_decode($c_catalog['text']), 10) .' ...',
        'tag' 			=> $item_tag,
        'is_note' 		=> $is_note,
    );

    $item_tag = null;
}

$q_count = $db->select()->from('m_catalog_data')->where('is_spec=1');
if ($aromat_type['type_ident']!='' && $aromat_type['type_ident']!='all'){
    $q_count->joinLeft('m_catalog_data_order','m_catalog_data_order.id_catalog_data=m_catalog_data.id','')
        ->where('m_catalog_data_order.type=?',$aromat_type['type_ident'])
        ->where('m_catalog_data_order.price!=0')
        ->group('m_catalog_data.id');

}

$count =  $db->fetchOne($db->select()->from(new Zend_Db_Expr('('.$q_count.')'),new Zend_Db_Expr('count(*)')));


$p 		= pages_content($count, $per_page, ($canon_name4)?$canon_name4:$canon_name3, 'pages.tpl');

$view->catalog_item = $catalog_item;
$view->p = $p;

$constructor['og:title'] = $constructor['meta_title'];
$constructor['og:image'] = $og_image;
$constructor['og:description'] =$constructor['meta_description'];
$constructor['og:url'] =  $_SERVER['REQUEST_URI'];

$constructor['content'] = $view->render('content_sale.php');
?>