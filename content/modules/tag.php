<?php

if ($canon_name4 && !is_numeric($canon_name4)){
    err_301($canon_name1.'/'.$canon_name2.'/');
}

if ($canon_name3 && $canon_name3=='page' && !$canon_name4){
    err_301($canon_name1.'/'.$canon_name2.'/');
}

if ($canon_name5){
    err_301($canon_name1.'/'.$canon_name2.'/');
}



if (isset($canon_name2) or $canon_name3 == 'page') {

	if (strlen($canon_name2) > 100) err_404();
	$find = eregi_replace('^ a-zA-Z0-9]', '', $canon_name2);
	

	$all_items 	= $db->fetchOne("SELECT count(*) FROM m_catalog_data WHERE tagl LIKE '%".$find."%' AND price_s!=0 AND id_cat=82 ");
	$p 			= pages_content($all_items, 40, $canon_name4, 'pages.tpl');
	$start 		= $p['start']; $limit = 40; $pagis = $p['pages'];
	

	$select = "SELECT "
	."a.id, a.title, a.url, a.price_s, a.img, a.text, "
	."b.title AS brend_title, b.url AS brend_url "
	."FROM m_catalog_data AS a "
	."JOIN m_catalog AS b ON b.id=a.id_catalog "
	."WHERE a.tagl LIKE '%".$find."%' AND a.id_cat=82 "
	."LIMIT $start, $limit";

	$q_catalog = $db->query($select);
	while ($c_catalog = $q_catalog->fetch()){

		if (!$og_image){
			if ($c_catalog['img'] != '') {
				$og_image =  'http://enigme.ru/images/uploads/catalog/' . $c_catalog['id'] . '/big/' . $c_catalog['img'];
			}
		}


		if ($c_catalog['img']=='') {
			$img = '/images/noimage.jpg';
			$img_big = '/images/noimage.jpg';
		}else {
			$img = '/images/uploads/catalog/'.$c_catalog['id'].'/small/'.$c_catalog['img'];
			$img_big = '/images/uploads/catalog/'.$c_catalog['id'].'/big/'.$c_catalog['img'];
		}

		$catalog_item[] = array(
		'title' 		=> $c_catalog['title'],
		'brend_title' 	=> $c_catalog['brend_title'],
		'price_s' 		=> $c_catalog['price_s']+$_SESSION['_GMARGIN'],
		'img' 			=> $img,
		'img_big' 		=> $img_big,
		'url' 			=> '/catalog/'.$c_catalog['brend_url'].'/'.$c_catalog['url'].'/',
		'anot' 			=> maxsite_str_word(html_entity_decode($c_catalog['text']), 10) .' ...'
		);
	}
	if(is_array($catalog_item))
	$view->catalog_item = $catalog_item;

	$title_tag = $db->fetchOne("SELECT title FROM m_catalog_data_tag WHERE url='$canon_name2' ");
	$view->title_tag = $title_tag;
	$view->pages = $pagis;

	/* -=========================================
	META
	*/
	if($canon_name4!=null){
		$constructor['meta_title'] = "{$title_tag} - ����, ������ � ����� ������� \"{$title_tag}\". �������� <<{$canon_name4}>> �� Enigme.ru";
		$constructor['meta_description'] = "����� ������ � ������� \"{$title_tag}\" �������� <<{$canon_name4}>>";
	}else {
		$constructor['meta_title'] = "{$title_tag} - ����, ������ � ����� ������� \"{$title_tag}\"";	
		$title_tab_upper = strtoupper($title_tag);
		$constructor['meta_description'] = "��������� ���� � �������� {$title_tab_upper} �
��������-�������� ���������� Enigme.ru.";
	}

	$constructor['og:title'] = $constructor['meta_title'];
	$constructor['og:image'] = $og_image;
	$constructor['og:description'] = $constructor['meta_description'];
	$constructor['og:url'] =  $_SERVER['REQUEST_URI'];


    if ($aromat_type = $db->fetchRow($db->select()->from('m_catalog_type')->where('slug=?', $canon_name1))) {
        foreach ($aromat_type as &$col) {
            $col = str_replace(':title_tag:', $title_tag, $col);
        }

        $view->h1 = $aromat_type['h1'];
        $constructor['meta_title'] = $aromat_type['title'];;
        $constructor['meta_keywords'] = $aromat_type['keywords'];
        $constructor['meta_description'] = $aromat_type['description'];
        $view->meta_body = $aromat_type['body'];

    }

    if ($canon_name4!=''){
        $constructor['meta_noindex'] = true;
    }



    $constructor['content'] = $view->render('content_tag.php');

}else {

	/*include_once ('includes_auto/tag.php');
	$sm->assign('tags', $tags );
	$constructor['content'] = $sm->fetch('blocks/tag.tpl');*/

}

?>