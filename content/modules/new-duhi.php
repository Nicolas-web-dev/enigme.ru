<?php
$per_page= 26;

$aTypes = $db->fetchAll($db->select()->from('m_catalog_type'));
foreach ($aTypes as $type) {
    $type_pages[] = $type['slug'];
}

//check canon names
if ($canon_name2 && $canon_name2!='page' && $canon_name3!='page' && !in_array($canon_name3,$type_pages) && !in_array($canon_name2,$type_pages)){
    err_301($canon_name1);
}
if (!$canon_name4 && $canon_name3 && !is_numeric($canon_name3) && !in_array($canon_name3,$type_pages)){
    err_301($canon_name1);
}

if ($canon_name5){
    if ($canon_name5 && (!is_numeric($canon_name5) )){
        err_301($canon_name1);
    }
}else{
    if ($canon_name4 && (!is_numeric($canon_name4) )){
        err_301($canon_name1);
    }
}








$page = ($canon_name3)?$canon_name3:1;
if ($canon_name4){
    $page = ($canon_name4)?$canon_name4:1;
}
if ($canon_name5){
    $page = ($canon_name5)?$canon_name5:1;
}



if ($page>1){
    $constructor['meta_noindex'] = true;
}



$select = $db->select()
    ->from(['a'=>'m_catalog_data'],['a.id','a.title','a.url','a.price_s','a.img','a.text'])
    ->joinLeft(['b'=>'m_catalog'],'b.id=a.id_catalog',['brend_title'=>'b.title','brend_url'=>'b.url'])

    ->where('a.id_cat=82')
    ->where('a.text!=""')
    ->group('a.id')
    ->limitPage($page,$per_page);




if (in_array($canon_name2,$type_pages)){
    $slug = $canon_name2;
    if ($canon_name3=='women'){
        $slug .= '/women';
    }
    if ($canon_name3=='men'){
        $slug .= '/men';
    }


    if ($aromat_type = $db->fetchRow($db->select()->from('m_catalog_type')->where('slug=?',$slug))){
        $view->h1 = $aromat_type['new_h1'];
        $constructor['meta_title'] = $aromat_type['new_title'];;
        $constructor['meta_keywords'] = $aromat_type['new_keywords'];
        $constructor['meta_description'] = $aromat_type['new_description'];
        $view->meta_body = $aromat_type['new_body'];
    }
}else{
    $aromat_type = $db->fetchRow($db->select()->from('m_catalog_type')->where('slug=?','all'));
    $view->h1 = $aromat_type['new_h1'];
    $constructor['meta_title'] = $aromat_type['new_title'];;
    $constructor['meta_keywords'] = $aromat_type['new_keywords'];
    $constructor['meta_description'] = $aromat_type['new_description'];
    $view->meta_body = $aromat_type['new_body'];
}



//rules
if ($canon_name2 && $canon_name2=='men'){
    $select->where('a.pol=?','M');
}

if ($canon_name2 && $canon_name2=='women'){
    $select->where('a.pol=?','F');
}


if ($aromat_type['type_ident']!=''){
    $select->joinInner(['p2'=>$db->select()
        ->from('m_catalog_data_order',['type','min'=>new Zend_Db_Expr('min(price)'),'id_catalog_data'])
        ->where('type=?',$aromat_type['type_ident'])->where('price!=0')
        ->group('id_catalog_data')],'a.id=p2.id_catalog_data');

    if ($canon_name3 && $canon_name3=='men'){
        $select->where('a.pol=?','M');
    }

    if ($canon_name3 && $canon_name3=='women'){
        $select->where('a.pol=?','F');
    }
}




$q_catalog = $db->query($select);
while ($c_catalog = $q_catalog->fetch()) {

	if ($c_catalog['img']=='') {
		$img = '/images/noimage.jpg';
		$img_big = '/images/noimage.jpg';
	}else {
		$img = '/images/uploads/catalog/'.$c_catalog['id'].'/small/'.$c_catalog['img'];
		$img_big = '/images/uploads/catalog/'.$c_catalog['id'].'/big/'.$c_catalog['img'];
	}

    $brand['title'] = $c_catalog['brend_title'];
    $brand['url'] = $c_catalog['brend_url'];


    $catalog_item[] = prepare_catalog_item($c_catalog, $brand,$aromat_type);

}
$view->catalog_item = $catalog_item;




$count 	= $db->fetchOne("SELECT count(*) FROM m_catalog_data WHERE text!='' AND id_cat=82 ");
if ($canon_name5){
    $p 		= pages_content($count, $per_page, $canon_name5, 'pages.tpl');
}elseif ($canon_name4){
    $p 		= pages_content($count, $per_page, $canon_name4, 'pages.tpl');
}elseif($canon_name3=='women' ||$canon_name3=='men'  ){
    $p 		= pages_content($count, $per_page, $canon_name5, 'pages.tpl');
}else{
    $p 		= pages_content($count, $per_page, $canon_name3, 'pages.tpl');
}



$view->p = $p;


$constructor['content'] = $view->render('content_new-duhi.php');
?>