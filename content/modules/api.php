<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14.02.14
 * Time: 18:35
 */

header('content-type: application/json');

switch ($canon_name2) {
    /**
     * ��������� ������� ������
     */
    case 'rate':
        $data['target_type'] = 'parfum';
        $data['target_id'] = $_POST['target_id'];
        $data['value'] = $_POST['val'];
        $data['ip'] = $_SERVER['REMOTE_ADDR'];

        if ($data['target_id']!='' && $data['value']!=''){
            $db->insert('m_catalog_rate',$data);
        }

        $rate = $db->fetchOne($db->select()->from('m_catalog_rate',new Zend_Db_Expr('AVG(value) as rate'))->where('target_id=?',$data['target_id'])->group('target_id'));

        $ratingCount = $db->fetchOne($db->select()->from('m_catalog_rate',new Zend_Db_Expr('count(*) as cnt'))->where('target_id=?',$data['target_id'])->group('target_id'));



        $where = $db->quoteInto('articul = ?', $data['target_id']);
        $db->update('m_catalog_data',['rate'=>$rate,'ratingCount'=>$ratingCount],$where);
        break;
    case 'image':
        $articul = $_GET['articul'];

        $select = "SELECT id,img FROM m_catalog_data WHERE articul=?";

        $oProduct = $db->fetchRow($select, $articul);



        $result = array(
            'small' => "http://enigme.ru/images/uploads/catalog/{$oProduct['id']}/small/{$oProduct['img']}",
            'big' => "http://enigme.ru/images/uploads/catalog/{$oProduct['id']}/big/{$oProduct['img']}"
        );

        echo json_encode($result);
        break;
    case "logistic_get":

        $addr_id = (int)$_GET['k'];

        // ������� �����
        $city = "";

        $select = "SELECT couriers_addresses.*, couriers.name AS courier_name FROM couriers_addresses
                        LEFT JOIN couriers ON couriers_addresses.courier_id = couriers.couriersId
                        WHERE couriers_addresses.id = ? ORDER BY delivery_metro ASC";
        $row = $db->fetchRow($select, $addr_id);

        // ���� ������ �� ������ - ������ ����� ������ �� ���������
        if ($row == null) {
            $row['id'] = 0;
        }

        /**
         * ����������� id � �������� ����� ������ ������
         * @var array
         */
        $_paymentType = array(
            0 => "�� �������",
            1 => "��������� ��� ���������",
            2 => "���������� �� ���������",
            3 => "���������� Robokassa"
        );

        if (!isset($row['delivery_cost_3'])) {
            $row['delivery_cost_3'] = 375;
        }

        // $_GET['t'] ~ playment_type ~ id
        if (isset($_GET['t']) && ((int) $_GET['t'] !== 0 && (int) $_GET['t'] !== 1)) {
            $row['delivery_cost'] = (int) $row['delivery_cost_1'];
        } else {
            $row['delivery_cost'] = (int) $row['delivery_cost_3'];
        }

        if (!isset($_GET['t'])) {
            $row['delivery_cost'] = (int) $row['delivery_cost_3'];
        }

        foreach ($row as $k => $v) {
            $row[$k] = iconv('windows-1251', 'utf-8', $v);
        }

        header('Content-Type: application/json; charset=windows-1251');
        echo json_encode($row);
        break;
    case "logistic":

        $kladr_id = ltrim($_GET['k'], "0");

        // ������� �����
        $city = "";

        $select = "SELECT couriers_addresses.*, couriers.name AS courier_name FROM couriers_addresses
                        LEFT JOIN couriers ON couriers_addresses.courier_id = couriers.couriersId
                        WHERE kladr_id_city = ? ORDER BY delivery_metro ASC, delivery_address";
        $result = $db->fetchAll($select, $kladr_id);


        // ���� ������� ��� ���������� ����� ������
        $add_post = true;

        // ������������ ������� ������� ����������� ��� ��
        $couriers = array();

        // ������ ���������� �������� ����������� ��������
        $outputFormat = array(
            'delivery_type',
            'delivery_payment',
            'courier_id',
            'delivery_days',
            'courier_name',
            'delivery_cost',
            'id',
            'delivery_metro',
            'delivery_address',
            'delivery_how_to',
            'name_city',
            'delivery_news_to_operator'
        );

        // ������ �������� ��������� �������
        $ses_cart = new Zend_Session_Namespace('korzina');

        // ��������� ������ � ������ ������ �� ������� �����������
        foreach ($result as $k => &$row) {

            $city = $row['name_city'];

            if ($row['courier_name'] == '����� ������' && $row['is_active'] == 0) {
                $add_post = false;
            }

            // ������������� �������� �� ���������, ���� ��� ������
            $row['delivery_type'] = $row['delivery_type'] == null ? '���������' : $row['delivery_type'];
            $row['delivery_payment'] = $row['delivery_payment'] == null ? '��������� ��� ���������' : $row['delivery_payment'];

            // ������� ��� ���������� ���  ������������ �������
            if ($row['is_active'] == 0) {
                //($row['delivery_type'] != '���������' && ($row['delivery_cost_1'] == 0 || $row['delivery_cost_3'] == 0))) {
                unset($result[$k]);
                continue;
            }

            // ��������� �������� ����� �����
            if ($row['courier_name'] == '����� ������') {
                unset($result[$k]);
                continue;
            }
        }



        /**
         * ����������� id � �������� ����� ������ ������
         * @var array
         */
        $_paymentType = array(
            0 => "�� �������",
            1 => "��������� ��� ���������",
            2 => "���������� �� ���������",
            3 => "���������� Robokassa"
        );

        /**
         * todo: �������� �� ����� ��� 100 �� ��� �����!
         * ������ ��������� ...
         */
        foreach ($result as $k => &$item) {
            foreach ($item as $key => $v) {

                // ��������� ��������� ��������
                // $_GET['t'] ~ playment_type ~ id
                if ($key == 'delivery_cost_1' && (isset($_GET['t']) && ((int) $_GET['t'] !== 0 && (int) $_GET['t'] !== 1))) {
                    $item['delivery_cost'] = (int) $item['delivery_cost_1'];
                } elseif ($key == 'delivery_cost_3' && (isset($_GET['t']) && ((int) $_GET['t'] === 0 || (int) $_GET['t'] === 1))) {
                    $item['delivery_cost'] = (int) $item['delivery_cost_3'];
                } elseif (!isset($_GET['t']) && ($key == 'delivery_cost_1' || $key == 'delivery_cost_3')) {
                    $item['delivery_cost'] = (int) $item['delivery_cost_3'];
                }

                // ��������� ��������� ������
                if (!in_array($key, $outputFormat)) {
                    unset($item[$key]);
                    continue;
                }

                $item[$key] = iconv('windows-1251', 'utf-8', $v);
            }
        }

        // ��������� �������� ������ ������ ��������������� ��� ����� ��
        foreach ($result as $courier) {
            $couriers[$courier['delivery_type']][$courier['courier_id']]['name'] = $courier['courier_name'];
            $couriers[$courier['delivery_type']][$courier['courier_id']]['addresses'][] = $courier;
        }

        // ��������� ����� ������, ���� ����������
        if ($add_post) {

            $post = iconv('windows-1251', 'utf-8', '�����');

            $delivery_cost_post = isset($_GET['t']) && ((int) $_GET['t'] === 0 || (int) $_GET['t'] === 1) || (!isset($_GET['t'])) ? 375 : 375;

            $couriers[$post][5]['name'] = iconv('windows-1251', 'utf-8', '����� ������');
            $couriers[$post][5]['addresses'][] = array(
                'delivery_days' => '5-12',
                'delivery_type' => iconv('windows-1251', 'utf-8', '���������'),
                'delivery_payment' => iconv('windows-1251', 'utf-8', '��������� ��� ���������'),
                'delivery_cost' =>  $delivery_cost_post,
                'courier_id' => 5,
                'courier_name' => iconv('windows-1251', 'utf-8', '����� ������'),
                'id' => 0,
                'name_city' => iconv('windows-1251', 'utf-8', $city),
                'delivery_news_to_operator' => iconv('windows-1251', 'utf-8', "������ ���������� � ������� 1 ��� ���������� �������� 1 ������. �������� � �������� ����� �� ������� �� ����� �������� ���������. � ������ �������� ������� �� �������� ��������� �� ����� ������. ��� ������ ���������� �������� ����� ������ ������� �������� �� 2 �� 5 % �� ������� ����� �� ��� ����.")
            );
        }

        header('Content-Type: application/json; charset=windows-1251');
        echo json_encode($couriers);
        break;
    case 'geo':
        $action = $_GET['action'];
        if ($action == '') {
            $action = $_POST['action'];
        }
        if ($action == 'get_cities') {
            $region_id = (int)$_GET['region_id'];
            if ($region_id == 0) {
                $sql = "SELECT a.*,b.region_name FROM m_geo_city a  LEFT JOIN m_geo_region b on a.region_id=b.id  WHERE is_top=1 ORDER BY a.city_name ";
            } else {
                $sql = "SELECT a.*,b.region_name FROM m_geo_city a  LEFT JOIN m_geo_region b on a.region_id=b.id WHERE a.region_id =? ORDER BY a.city_name";
            }

            $aCities = $db->fetchAll($sql, $region_id);
            foreach ($aCities as $key => $city) {
                $aCities[$key]['city_name'] = iconv('windows-1251', 'utf-8', $city['city_name']);
                $aCities[$key]['region_name'] = iconv('windows-1251', 'utf-8', $city['region_name']);
                $letter = mb_substr($aCities[$key]['city_name'], 0, 2);
                $aLetter[$letter][] = $key;
            }
            echo json_encode(array('aCities' => $aCities, 'aLetters' => $aLetter));
            //set location
        } elseif ($action == 'set_location') {

            $city_id = $_GET['city_id'];
            $sql = "SELECT a.*,b.region_name FROM m_geo_city a  LEFT JOIN m_geo_region b on a.region_id=b.id WHERE a.id =? ORDER BY a.city_name";
            if ($row = $db->fetchRow($sql, $city_id)) {
                //capitals moscow and spb
                if ($city_id == 1250 or $city_id == 123) {
                    $row['region_name'] = $row['city_name'];
                }

                // send to session utf-8
                $ses_location = new Zend_Session_Namespace('location');
                $ses_location->location = $row;

                //answer win-1251, fucking encoding
                $row['city_name'] = iconv('windows-1251', 'utf-8', $row['city_name']);
                $row['region_name'] = iconv('windows-1251', 'utf-8', $row['region_name']);
                echo json_encode(array('city' => $row));
            } else {
                if ($_POST['city_name'] != '') {
                    $row['city_name'] = iconv('utf-8', 'windows-1251', $_POST['city_name']);
                    $row['region_name'] = iconv('utf-8', 'windows-1251', $_POST['region_name']);
                }

                $ses_location = new Zend_Session_Namespace('location');
                $ses_location->location = $row;
            };
        }
        break;

    case 'cart':
        $action = $_POST['action'];
        $uid = $_POST['uid'];

        $cartClass = new Cart();
        switch ($action) {
            case 'asc':
                $cartClass->change_amount($uid, $action);
                break;
            case 'desc':
                $cartClass->change_amount($uid, $action);
                break;
        }

        $result = $cartClass->getFormated();

        foreach ($result as &$item) {
            if (!is_array($item)){
                $item = iconv('cp1251','utf8',$item);
            }
        }

        foreach ($result['items'] as &$item) {
            foreach ($item as &$subitem) {
                $subitem = iconv('cp1251','utf8',$subitem);
            }


        }

        foreach ($result['user'] as &$item) {
            if (!is_array($item)){
                $item = iconv('cp1251','utf8',$item);
            }
        }

        echo json_encode($result);
        break;

    case 'dadata':

        $action = $_POST['action'];
        //$result = dadata_get('http://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/fio', '����');
        //print_r($result);

        switch ($action) {
            case 'name':
                $data = dadata_get('http://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/fio', $data);
                break;

            case 'address':
                break;
        }
}