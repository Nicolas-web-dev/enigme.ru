<?php
$sql_m_catalog_data_a_block = '';
$sql_m_catalog_data_block = '';
$sql_m_catalog_block = '';
$sql_m_catalog_a_block = '';
//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown, Kilian
if ($ses_user->is_login == false) {
	$sql_m_catalog_data_a_block = ' AND a.id_catalog != 95 AND a.id_catalog != 78 AND a.id_catalog != 145 AND a.id_catalog != 487  AND a.id_catalog != 453 ';
	$sql_m_catalog_data_block = ' AND id_catalog != 95 AND id_catalog != 78 AND id_catalog != 145 AND id_catalog != 487  AND id_catalog != 453 ';
	$sql_m_catalog_block = ' AND id != 95 AND id != 78 AND id != 145 AND id != 487 AND id != 453 ';
	$sql_m_catalog_a_block = ' AND a.id != 95 AND a.id != 78 AND a.id != 145 AND a.id != 487  AND a.id != 453 ';
}

if ($_GET['ajax_catalog_filter'] == 1) {
	$result_ajax = array();
	/*
	$select = $db->select()->from('m_catalog_data')->where('id_catalog=?',intval($_POST['id_catalog']));
	*/
	$select = 'SELECT * FROM m_catalog_data WHERE id_catalog = '.intval($_POST['id_catalog']).$sql_m_catalog_data_block;
	if ($_POST['sort']!=''){
        switch($_POST['sort']){
            case 'rate':
                //$select->order('rate DESC');
				$select .= ' ORDER BY rate DESC';
                break;
            case 'price':
                //$select->order('price_s ASC');
				$select .= ' ORDER BY price_s ASC';
                break;
            case 'abc':
                //$select->order('title ASC');
				$select .= ' ORDER BY title ASC';
                break;
            case 'new':
                //$select->order('id DESC');
				$select .= ' ORDER BY id DESC';
                break;
            }
    }
	$brend_title = $_POST['brend_title'];
	$brend_url = $_POST['brend_url'];
	$q_catalog_data = $db->query($select);
        while ($c_catalog_data = $q_catalog_data->fetch()) {
            if (!$og_image){
                if ($c_catalog_data['img'] != '') {
                    $og_image =  'http://' . $_SERVER['HTTP_HOST'] . '/images/uploads/catalog/' . $c_catalog_data['id'] . '/big/' . $c_catalog_data['img'];
                }
            }
            if ($c_catalog_data['img'] == '') {
                $img = '/images/noimage.jpg';
                $img_big = '/images/noimage.jpg';
            } else {
                $img = '/images/uploads/catalog/' . $c_catalog_data['id'] . '/small/' . $c_catalog_data['img'];
                $img_big = '/images/uploads/catalog/' . $c_catalog_data['id'] . '/big/' . $c_catalog_data['img'];
            }
			/*
            if ($c_catalog_data['pol'] == 'F') {
                $catalog_item_f[] = array(
                    'id' => iconv("CP1251", "UTF-8",$c_catalog_data['id']),
                    'title' => iconv("CP1251", "UTF-8",$c_catalog_data['title']),
                    'brend_title' => iconv("CP1251", "UTF-8",$brend_title),
                    'url' => iconv("CP1251", "UTF-8",'/catalog/' . $brend_url . '/' . $c_catalog_data['url'] . '/'),
                    'img' => iconv("CP1251", "UTF-8",$img),
                    'is_spec' => iconv("CP1251", "UTF-8",$c_catalog_data['is_spec']),
                    'pol' => iconv("CP1251", "UTF-8",$c_catalog_data['pol']),
                    'img_big' => iconv("CP1251", "UTF-8",$img_big),
                    'price_s' => iconv("CP1251", "UTF-8",$c_catalog_data['price_s'] + $_SESSION['_GMARGIN']),
					'price_e' => iconv("CP1251", "UTF-8",$c_catalog_data['price_e'] + $_SESSION['_GMARGIN'])
                );
            } elseif ($c_catalog_data['pol'] == 'M') {
                $catalog_item_m[] = array(
                    'id' => iconv("CP1251", "UTF-8",$c_catalog_data['id']),
                    'title' => iconv("CP1251", "UTF-8",$c_catalog_data['title']),
                    'brend_title' => iconv("CP1251", "UTF-8",$brend_title),
                    'url' => iconv("CP1251", "UTF-8",'/catalog/' . $brend_url . '/' . $c_catalog_data['url'] . '/'),
                    'img' => iconv("CP1251", "UTF-8",$img),
                    'pol' => iconv("CP1251", "UTF-8",$c_catalog_data['pol']),
                    'is_spec' => iconv("CP1251", "UTF-8",$c_catalog_data['is_spec']),
                    'img_big' => iconv("CP1251", "UTF-8",$img_big),
                    'price_s' => iconv("CP1251", "UTF-8",$c_catalog_data['price_s'] + $_SESSION['_GMARGIN']),
					'price_e' => iconv("CP1251", "UTF-8",$c_catalog_data['price_e'] + $_SESSION['_GMARGIN'])
                );
            }
			*/
			$catalog_prod_items[] = array (
					'id' => iconv("CP1251", "UTF-8",$c_catalog_data['id']),
                    'title' => iconv("CP1251", "UTF-8",$c_catalog_data['title']),
                    'brend_title' => iconv("CP1251", "UTF-8",$brend_title),
                    'url' => iconv("CP1251", "UTF-8",'/catalog/' . $brend_url . '/' . $c_catalog_data['url'] . '/'),
                    'img' => iconv("CP1251", "UTF-8",$img),
                    'is_spec' => iconv("CP1251", "UTF-8",$c_catalog_data['is_spec']),
                    'pol' => iconv("CP1251", "UTF-8",$c_catalog_data['pol']),
                    'img_big' => iconv("CP1251", "UTF-8",$img_big),
                    'price_s' => iconv("CP1251", "UTF-8",$c_catalog_data['price_s'] + $_SESSION['_GMARGIN']),
					'price_e' => iconv("CP1251", "UTF-8",$c_catalog_data['price_e'] + $_SESSION['_GMARGIN'])
			);
			
        }
	$tmpl = "ajax.php";
	//$result_ajax = array('catalog_item_f' => $catalog_item_f, 'catalog_item_m' => $catalog_item_m);
	$result_ajax = $catalog_prod_items;
	echo json_encode($result_ajax);
} else {
	
$aTypes = $db->fetchAll($db->select()->from('m_catalog_type'));
foreach ($aTypes as $type) {
    $brand_pages[] = $type['slug'];
}

if ($canon_name4!='' && $canon_name3==''){
    err_404();
}

if ($canon_name3!='' && $canon_name2==''){
    err_404();
}

if ($canon_name2!='' && $canon_name1==''){
    err_404();
}



//check canon names
if ($canon_name4 && $canon_name4 != 'send' && $canon_name4 != 'men' && $canon_name4 != 'women' && !in_array($canon_name4,
        $brand_pages)
) {
    err_301($canon_name1 . '/' . $canon_name2 . '/' . $canon_name3 . '/');
}




if ($canon_name4 == 'send') {

    /* -=========================================
    ��������� ��������
    */

    if (isset($_POST['com_submit']) && isset($_POST['security']) && $_POST['security']=='202cb962ac59075b964b07152d234b70') {
        $tmpl = '';
        $brend = $db->fetchRow("SELECT id,title,url FROM m_catalog WHERE id_cat=82 AND url='$canon_name2' ".$sql_m_catalog_block);
        $catalog_data = $db->fetchRow("SELECT id,title,url FROM m_catalog_data WHERE url='$canon_name3' AND id_catalog='{$brend['id']}' ".$sql_m_catalog_data_block);

        if ($catalog_data != false) {
            $filter = new anti_mate();
            $com_name = iconv('utf8','cp1251',$_POST['com_name']);
            $com_text = iconv('utf8','cp1251',$_POST['com_comment']);

            if ($ses_user->is_login) {
                $user_id = intval($ses_user->user['id']);
            } else {
                $user_id = 0;
            }

            if ($com_name != '' AND $com_text != '') {
                $m_com = array(
                    'id_catalog_data' => $catalog_data['id'],
                    'id_user' => $user_id,
                    'user_name' => $com_name,
                    'user_text' => $com_text,
                    'user_date' => new Zend_Db_Expr('NOW()'),
                    'catalog_title' => "{$brend['title']} {$catalog_data['title']}",
                    'catalog_url' => "http://enigme.ru/catalog/{$brend['url']}/{$catalog_data['url']}/",
                );

                $db->insert('m_catalog_com', $m_com);

            }
            echo 'ok';
            //go
            //('/catalog/' . $canon_name2 . '/' . $canon_name3 . '/');
        } else {
            //go('/');
        }
    }
} elseif (isset($canon_name3) AND isset($canon_name2) AND !in_array($canon_name3, $brand_pages)) {

    if ($canon_name3 !== mb_strtolower($canon_name3) || $canon_name2 !== mb_strtolower($canon_name2)) {
        $canon_name2 = mb_strtolower($canon_name2);
        $canon_name3 = mb_strtolower($canon_name3);
        err_301('catalog/' . $canon_name2 . '/' . $canon_name3 . '/');
    }

    $sel_fetch_row = "SELECT * FROM m_catalog WHERE id_cat = 82 AND url = '$canon_name2' AND is_block = 0".$sql_m_catalog_block;
	$brend = $db->fetchRow($sel_fetch_row);
    //$brend = $db->fetchRow($db->select()->from('m_catalog')->where('id_cat=82')->where('url=?',$canon_name2)->where('is_block=?',0));
    if ($brend == false) {
        err_404();
    }
    $brand= $brend;
    $view->brand = $brand;;
    $view->brend = $brand;;


    //$result = $db->fetchAll($db->select()->from('m_catalog_data')->where('url=?',$canon_name3)->where('id_catalog=?',$brend['id']));
	$result = $db->fetchAll("SELECT * FROM m_catalog_data WHERE url = '$canon_name3' AND id_catalog = {$brend['id']}".$sql_m_catalog_data_block);
    foreach ($result as $c_catalog_data) {
        $id_catalog_data = $c_catalog_data['id'];
        $tag = $c_catalog_data['tag'];
        $price = $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'];

        if ($c_catalog_data['img'] == '') {
            $img = '/images/noimage.jpg';
        } else {
            $img = '/images/uploads/catalog/' . $c_catalog_data['id'] . '/big/' . $c_catalog_data['img'];
        }

        if ($c_catalog_data['pol'] == 'M') {
            $pol = '�������';
        } else {
            $pol = '�������';
        }

        $catalog_full = array(
            'id' => $c_catalog_data['id'],
            'title' => $c_catalog_data['title'],
            'is_spec' => $c_catalog_data['is_spec'],
            'title_1' => $c_catalog_data['title_1'],
            'meta_title' => $c_catalog_data['meta_title'],
            'articul' => $c_catalog_data['articul'],
            'url' => $c_catalog_data['url'],
            'img' => $img,
            'rate' => ($c_catalog_data['rate']),
            'ratingCount' => $c_catalog_data['ratingCount'],
            'alt' => $c_catalog_data['alt'],
            'text' => word_to_link($c_catalog_data['text'], '\.', 'http://' . BASE_URL),
            'text2' => word_to_link($c_catalog_data['text2'], '\.', 'http://' . BASE_URL),
            'pol' => $pol,
            'pol_src' => $c_catalog_data['pol'],
			'price_s' => $c_catalog_data['price_s']
        );
    }

   /* $select = "SELECT "
        . "id, id_catalog, title, title_1, img, alt, text, url, pol, tag, price_s,articul, meta_title, meta_title_1, meta_description,is_spec,rate,ratingCount  "
        . "FROM m_catalog_data "
        . "WHERE url='$canon_name3' and id_catalog='{$brend['id']}' "
        . "LIMIT 1";

    $q_catalog_data = $db->query($select);
    while ($c_catalog_data = $q_catalog_data->fetch()) {


    }*/
    if (!is_array($catalog_full)) {
        err_404();
    }
    $view->catalog_full = $catalog_full;
    $view->brend_title = $brend['title'];
    $view->brend_url = $canon_name2;


    /* -=========================================
    ���������
    */
    if (!empty($ses_note->id)) {
        if (in_array($catalog_full['id'], $ses_note->id)) {
            $view->is_note = true;
        } else {
            $view->is_note = false;
        }
    } else {
        $view->is_note = false;
    }

    /* -=========================================
    ������ �����������
    */
    $select = $db->select()
        ->from('m_catalog_data_order')
        ->where('id_catalog_data=?', $id_catalog_data)
        ->where('price!=0')
        ->order('price ASC');


    if ($catalog_full['title_1']==''){
        $catalog_full['title_1']= $catalog_full['title'];
    }
    if ($brend['title_1']==''){
        $brend['title_1']= $brend['title'];
    }

    //��� �������� ������� ��������
    if ($canon_name4 != '' || $canon_name4=='reviews') {
        err_301($canon_name1.'/'.$canon_name2.'/'.$canon_name3.'/');

        if ($aromat_type = $db->fetchRow($db->select()->from('m_catalog_type')->where('slug=?', $canon_name4))) {
            if ($canon_name4=='tester'){
                $select->where('type in (?)', ['TTW','TT','DT','OT','PT','BLT','SGT','PDT','AST','ASBT']);
            }else{
                $select->where('type=?', $aromat_type['type_ident']);
            }





            foreach ($aromat_type as &$col) {
                $col = str_replace(':title:', $brend['title'], $col);
                $col = str_replace(':title_1:', $brend['title_1'], $col);
                $col = str_replace(':name:', $catalog_full['title'], $col);
                if ($catalog_full['title_1']==''){
                    $catalog_full['title_1']= $catalog_full['title'];
                }
                $col = str_replace(':name_1:', $catalog_full['title_1'], $col);
                if ($catalog_full['pol_src']=='M'){
                    $col = str_replace(':pol:', '������� for man', $col);
                    $col = str_replace(':pol_1:', '�������', $col);
                    $col = str_replace(':pol_2:', '������� (for man)', $col);
                }
                if ($catalog_full['pol_src']=='F'){
                    $col = str_replace(':pol:', '������� for woman', $col);
                    $col = str_replace(':pol_1:', '�������', $col);
                    $col = str_replace(':pol_2:', '������� (for woman)', $col);
                }
                $col = str_replace(':pol:', '', $col);
                $col = str_replace(':pol_1:', '', $col);
                $col = str_replace(':pol_2:', '', $col);
                $col = str_replace('  ', ' ', $col);
                $col = str_replace(' ,', ',', $col);
            }

        } else {
            err_404();
        }
    }

	$view->catalog_id = $id_catalog_data;
    $num = 0;
    $q_catalog_order = $db->query($select);
    while ($c_catalog_order = $q_catalog_order->fetch()) {

        if ($c_catalog_order['type'] == 'N') {
            $type = $c_catalog_order['v'];
        } else {
            $type = $aroma_type[$c_catalog_order['type']] . ' ' . $c_catalog_order['v'] . ' ��';
        }

        if ($c_catalog_order['price_usd1'] > 0) {
            $is_spec = 1;
        } else {
            $is_spec = 0;
        }

        $catalog_item_order[] = array(
            'title' => $title,
            'is_spec' => $is_spec,
            'type' => mb_ucfirst($type),
            't' => $c_catalog_order['type'],
            'id' => $c_catalog_order['id'],
            'is_block' => $c_catalog_order['is_block'],
            'price' => $c_catalog_order['price'] + $_SESSION['_GMARGIN'],
            'price_old' => $c_catalog_order['price_old'],
            'bg' => ($num % 2 == 0) ? '1' : '2',
        );

        $num++;
    }
    if (is_array($catalog_item_order)) {
        $view->catalog_item_order = $catalog_item_order;
    }


    /* -=========================================
    TAG
    */
    if ($tag != '') {
        $tag = explode(',', $tag);
        foreach ($tag as $key => $value) {
            if ($value == '') {
                continue;
            }
            $item_tag[] = array(
                'url' => translit($value),
                'title' => $value,
            );
        }
        $view->catalog_item_tag = $item_tag;
    }

    /* -=========================================
    ������ ������������
    */


    $q_catalog_com = $db->select()->from('m_catalog_com')->where('id_catalog_data=?',$id_catalog_data)->order('id DESC');

    foreach ($db->fetchAll($q_catalog_com) as $c_catalog_com) {
        $catalog_item_com[] = array(
            'id' => $c_catalog_com['id'],
            'user_name' => $c_catalog_com['user_name'],
            'user_text' => $c_catalog_com['user_text'],
            'user_date' => $c_catalog_com['user_date'],
        );
    }


    $aReviewsSrc = $db->fetchAll($db->select()->from('reviews')->where('chan_channel=?','/catalog/'.$canon_name2.'/'.$canon_name3.'/')->where('status=?','approved'));

    foreach ($aReviewsSrc as $item) {
        if ($item['author_name']==''){
            $item['author_name'] = $item['anonym_name'];
        }
        $aReviews[] =[
          'author_name'=>$item['author_name'],
          'author_provider'=>$item['author_provider'],
          'comment'=>$item['comment'],
          'pros'=>$item['pros'],
          'cons'=>$item['cons'],
          'star'=>$item['star'],
          'created'=>$item['created'],
          'author_avatar'=>($item['author_avatar'])?$item['author_avatar']:'http://cackle.me/widget/img/anonym2.png'
        ];
    }



    $view->catalog_item_com = $catalog_item_com;
    $view->aReviews = $aReviews;

    /* -=========================================
    ������� �� ������ ����
    */
    if ($price != 0) {
        $price_start = $price - 500;
        $price_end = $price + 500;

        $select = "SELECT "
            . "a.id, a.title, a.title_1, a.url, a.price_s, a.price_e, a.img,a.is_spec, "
            . "b.title AS brend_title, b.title_1 AS brend_title_1, b.url AS brend_url "
            . "FROM m_catalog_data AS a "
            . "JOIN m_catalog AS b ON b.id=a.id_catalog "
            . "WHERE a.id_cat=82 AND a.id!='$id_catalog_data' AND a.price_s > '$price_start' AND a.price_e < '$price_end' ".$sql_m_catalog_data_a_block
            . "ORDER BY price_s "
            . "LIMIT 12";
    } else {
        $select = "SELECT "
            . "a.id, a.title, a.title_1, a.url, a.price_s, a.price_e, a.img,is_spec, "
            . "b.title AS brend_title, b.title_1 AS brend_title_1, b.url AS brend_url "
            . "FROM m_catalog_data AS a "
            . "JOIN m_catalog AS b ON b.id=a.id_catalog "
            . "WHERE a.id_cat=82 AND a.id!='$id_catalog_data' AND a.price_e < 2000 ".$sql_m_catalog_data_a_block
            . "ORDER BY price_s "
            . "LIMIT 30";
    }
    $q_catalog_pre = $db->query($select);
    while ($c_catalog_pre = $q_catalog_pre->fetch()) {
        if ($c_catalog_pre['img'] == '') {
            $img = '/images/noimage.jpg';
        } else {
            $img = '/images/uploads/catalog/' . $c_catalog_pre['id'] . '/small/' . $c_catalog_pre['img'];
        }

        $catalog_item_pre[] = array(
            'title' => ($c_catalog_pre['title_1'] != '') ? $c_catalog_pre['title_1'] : $c_catalog_pre['title'],
            'brend_title' => ($c_catalog_pre['brend_title'] != '') ? $c_catalog_pre['brend_title_1'] : $c_catalog_pre['brend_title'],
            'price' => $c_catalog_pre['price_s'] + $_SESSION['_GMARGIN'],
            'url' => '/catalog/' . $c_catalog_pre['brend_url'] . '/' . $c_catalog_pre['url'] . '/',
            'img' => $img,
            'is_spec' => $c_catalog_pre['is_spec'],
            'pol' => $c_catalog_pre['pol'],

        );
    }

    shuffle($catalog_item_pre);
    $catalog_item_pre = array_slice($catalog_item_pre, 0, 6);


    $view->catalog_item_pre = $catalog_item_pre;


    /* -=========================================
    ������� ������
    */
    $select = "SELECT "
        . "a.id, a.title, a.title_1, a.url, a.price_s, a.price_e, a.img,a.is_spec,rate, "
        . "b.title AS brend_title, b.title_1 AS brend_title_1, b.url AS brend_url "
        . "FROM m_catalog_data AS a "
        . "JOIN m_catalog AS b ON b.id=a.id_catalog "
        . "WHERE a.id_catalog='{$brend['id']}' AND a.id!='$id_catalog_data' ".$sql_m_catalog_data_a_block
        . "ORDER BY price_s "
        . "LIMIT 6";
    $q_catalog_br = $db->query($select);
    while ($c_catalog_br = $q_catalog_br->fetch()) {
        if ($c_catalog_br['img'] == '') {
            $img = '/images/noimage.jpg';
        } else {
            $img = '/images/uploads/catalog/' . $c_catalog_br['id'] . '/small/' . $c_catalog_br['img'];
        }

        $catalog_item_br[] = array(
			'id'	=> $c_catalog_br['id'],
            'title' => ($c_catalog_br['title_1'] != '') ? $c_catalog_br['title_1'] : $c_catalog_br['title'],
            'brend_title' => ($c_catalog_br['brend_title'] != '') ? $c_catalog_br['brend_title_1'] : $c_catalog_br['brend_title'],
            'price' => $c_catalog_br['price_s'] + $_SESSION['_GMARGIN'],
			'price_e' => $c_catalog_br['price_e'] + $_SESSION['_GMARGIN'],
            'url' => '/catalog/' . $c_catalog_br['brend_url'] . '/' . $c_catalog_br['url'] . '/',
            'img' => $img,
            'is_spec' => $c_catalog_br['is_spec'],
        );
    }
    $view->catalog_item_br = $catalog_item_br;


    /* -=========================================
    ������� ������ ������
    */

	/*
    $catalog_item = $db->fetchAll($db->select()
        ->from('m_catalog_data')
        ->where('id_catalog=?', $brend['id'])
        ->order('title'));
	*/
	$catalog_item = $db->fetchAll("SELECT * FROM m_catalog_data WHERE id_catalog = {$brend['id']}".$sql_m_catalog_data_block." ORDER BY title");
		
    $view->catalog_item = $catalog_item;
    $view->catalog_count_f = $db->fetchOne("SELECT count(*) FROM m_catalog_data WHERE pol='F' AND id_catalog='{$brend['id']}' ".$sql_m_catalog_data_block);
    $view->catalog_count_m = $db->fetchOne("SELECT count(*) FROM m_catalog_data WHERE pol='M' AND id_catalog='{$brend['id']}' ".$sql_m_catalog_data_block);


    //��������
    $view->dostavka = $db->fetchRow("SELECT text_2,text_3 FROM m_text WHERE id=2 ");

    /* -=========================================
    META
    */


    if ($canon_name4 != '' && $canon_name5=='') {
        if ($aromat_type = $db->fetchRow($db->select()->from('m_catalog_type')->where('slug=?', $canon_name4))) {
            foreach ($aromat_type as &$col) {
                $col = str_replace(':title:', $brend['title'], $col);
                $col = str_replace(':title_1:', $brend['title_1'], $col);
                $col = str_replace(':name:', $catalog_full['title'], $col);
                $col = str_replace(':name_1:', $catalog_full['title_1'], $col);
                if ($catalog_full['pol_src']=='M'){
                    $col = str_replace(':pol:', '������� for man', $col);
                    $col = str_replace(':pol_1:', '�������', $col);
                    $col = str_replace(':pol_2:', '������� (for man)', $col);
                }
                if ($catalog_full['pol_src']=='F'){
                    $col = str_replace(':pol:', '������� for woman', $col);
                    $col = str_replace(':pol_1:', '�������', $col);
                    $col = str_replace(':pol_2:', '������� (for woman)', $col);
                }
                $col = str_replace(':pol:', '', $col);
                $col = str_replace(':pol_1:', '', $col);
                $col = str_replace(':pol_2:', '', $col);
                $col = str_replace('  ', ' ', $col);
                $col = str_replace(' ,', ',', $col);
            }
            $view->h1 = $aromat_type['card_h1'];
            $constructor['meta_title'] = $aromat_type['card_title'];;
            $constructor['meta_keywords'] = $aromat_type['card_keywords'];
            $constructor['meta_description'] = $aromat_type['card_description'];
            $constructor['og:title'] = $brend['title'].' '.$catalog_full['title'];
            $constructor['og:image'] = 'http://' . $_SERVER['HTTP_HOST'] . $catalog_full['img'];
            $constructor['og:description'] = $brend['title'].' '.$catalog_full['title'].' - ������ ���� � ��������'  . 'Enigme.ru';
            $constructor['og:url'] =  'http://' . $_SERVER['HTTP_HOST'] . '/catalog/' . $brend['url'] . '/' . $catalog_full['url'] . '/';
            $view->meta_body = $aromat_type['card_body'];
        }
		if ($ses_user->is_login == true) {
        $select = $db->select()->from('m_catalog_data','pol')
            ->joinLeft('m_catalog_data_order','m_catalog_data_order.id_catalog_data=m_catalog_data.id','type')
            ->joinLeft('m_catalog_type','m_catalog_type.type_ident=m_catalog_data_order.type',['name','slug','men_name','women_name','no_pol'])
            ->where('m_catalog_data_order.price!=0')
            ->where('id_catalog=?',$brand['id'])
            ->where('m_catalog_data.id=?',$catalog_full['id'])
            ->where('is_sidebar=1')
            ->where('m_catalog_type.id IS NOT NULL')
            ->group('m_catalog_type.id')
            ->group('pol');
		} else {
		$select = $db->select()->from('m_catalog_data','pol')
            ->joinLeft('m_catalog_data_order','m_catalog_data_order.id_catalog_data=m_catalog_data.id','type')
            ->joinLeft('m_catalog_type','m_catalog_type.type_ident=m_catalog_data_order.type',['name','slug','men_name','women_name','no_pol'])
            ->where('m_catalog_data_order.price!=0')
			->where('m_catalog_data.id_catalog!=95')
			->where('m_catalog_data.id_catalog!=78')
			->where('m_catalog_data.id_catalog!=145')//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown, Kilian
			->where('m_catalog_data.id_catalog!=487')
			->where('m_catalog_data.id_catalog!=453')
            ->where('id_catalog=?',$brand['id'])
            ->where('m_catalog_data.id=?',$catalog_full['id'])
            ->where('is_sidebar=1')
            ->where('m_catalog_type.id IS NOT NULL')
            ->group('m_catalog_type.id')
            ->group('pol');
		}	

        //echo $select->__toString();
        $links_types = $db->fetchAll($select);

        foreach ($links_types as $link_type) {
            $aLinkType[$link_type['type']][]=$link_type;
        }


        $view->aromat_links_types = $aLinkType;


        /* -=========================================
   ������ ������������
   */
        $aComments = $db->fetchAll($db->select()->from('m_catalog_com')->where('id_catalog_data=?',$id_catalog_data)->order('user_date DESC'));


        /*$db->select()->from('m_catalog_com')->where([])
        $select = "SELECT "
            . "id, user_name, user_text, user_date "
            . "FROM m_catalog_com "
            . "WHERE id_catalog_data='$id_catalog_data' "
            . "ORDER BY user_date ";

        $q_catalog_com = $db->query($select);*/
        foreach($aComments as $c_catalog_com){
            $catalog_item_com[] = array(
                'id' => $c_catalog_com['id'],
                'user_name' => $c_catalog_com['user_name'],
                'user_text' => $c_catalog_com['user_text'],
                'user_date' => $c_catalog_com['user_date'],
            );
        }
        $view->catalog_item_com = $catalog_item_com;

        if ($canon_name4=='reviews'){
            $constructor['content'] = $view->render('content_catalog_full/reviews.php');
        }else{
            $constructor['content'] = $view->render('content_catalog_full/default.php');

        }

        //
    } else {
        if ($aromat_type = $db->fetchRow($db->select()->from('m_catalog_type')->where('slug=?', 'all'))) {
            foreach ($aromat_type as &$col) {
                $col = str_replace(':title:', $brend['title'], $col);
                $col = str_replace(':title_1:', $brend['title_1'], $col);
                $col = str_replace(':name:', $catalog_full['title'], $col);
                $col = str_replace(':name_1:', $catalog_full['title_1'], $col);
                if ($catalog_full['pol_src']=='M'){
                    $col = str_replace(':pol:', '������� for man', $col);
                    $col = str_replace(':pol_1:', '�������', $col);
                    $col = str_replace(':pol_2:', '������� (for man)', $col);
                }
                if ($catalog_full['pol_src']=='F'){
                    $col = str_replace(':pol:', '������� for woman', $col);
                    $col = str_replace(':pol_1:', '�������', $col);
                    $col = str_replace(':pol_2:', '������� (for woman)', $col);
                }
                $col = str_replace(':pol:', '', $col);
                $col = str_replace(':pol_1:', '', $col);
                $col = str_replace(':pol_2:', '', $col);
                $col = str_replace('  ', ' ', $col);
                $col = str_replace(' ,', ',', $col);
            }
			//$t_group_product = $db->fetchRow($db->select()->from('m_catalog_data')->where('id_cat=82')->where('id_catalog=?',$brend['id'])->where('url=?',$canon_name3)->where('is_block=?',0));
			$t_group_product = $db->fetchRow("SELECT * FROM m_catalog_data WHERE id_cat = 82 AND id_catalog = {$brend['id']} AND url = '$canon_name3' AND is_block = 0".$sql_m_catalog_data_block);
			$t_brend = $brend['title'];
            $view->h1 = $aromat_type['card_h1'];
			if (strtolower($t_brend{0}) == 's' || strtolower($t_brend{0}) == 'd' || strtolower($t_brend{0}) == 'f' || strtolower($t_brend{0}) == 'h' || strtolower($t_brend{0}) == 'o') {
				if ($t_group_product['pol'] == 'M') {
					$constructor['meta_title'] = $t_group_product['title'] . ' ����. ��������� ���� ' . $brend['title'] . ' ' . $t_group_product['title_1'] . ' ������� ������ ��������';
				} else {
					$constructor['meta_title'] = $t_group_product['title'] . ' ����. ��������� ���� ' . $brend['title'] . ' ' . $t_group_product['title_1'] . ' ������� ������ ��������';
				}
			} else {
				$constructor['meta_title'] = $aromat_type['card_title'];
			}
            
			if (strtolower($t_brend{0}) == 't' || strtolower($t_brend{0}) == 'd' || strtolower($t_brend{0}) == 'f' || strtolower($t_brend{0}) == 'h' || strtolower($t_brend{0}) == 'o') {
				if ($t_group_product['pol'] == 'M') {
					$constructor['meta_description'] = $t_group_product['title'] . ' ' . $brend['title'] . ' ��������� ���� ������ �� ������ ����. ������� ������ ' . $t_group_product['title_1'] . ' ' . $brend['title_1'] . ' � ������. ������� � ��������, � ����� �������� � ��������� �� ����� enigme.ru.';
				} else {
					$constructor['meta_description'] = $t_group_product['title'] . ' ' . $brend['title'] . ' ��������� ���� ������ �� ������ ����. ������� ������ ' . $t_group_product['title_1'] . ' ' . $brend['title_1'] . ' � ������. ������� � ��������, � ����� �������� � ��������� �� ����� enigme.ru.';
				}
			} else {
				$constructor['meta_description'] = $aromat_type['card_description'];
			}
            $constructor['meta_keywords'] = $aromat_type['card_keywords'];
            $constructor['og:title'] = $brend['title'].' '.$catalog_full['title'];
            $constructor['og:image'] = 'http://' . $_SERVER['HTTP_HOST'] . $catalog_full['img'];
            $constructor['og:description'] = $brend['title'].' '.$catalog_full['title'].' - ������ ���� � ��������' . 'Enigme.ru';
            $constructor['og:url'] =  'http://' . $_SERVER['HTTP_HOST'] . '/catalog/' . $brend['url'] . '/' . $catalog_full['url'] . '/';
            $view->meta_body = $aromat_type['card_body'];


        }

        if ($db->fetchRow("SELECT * FROM m_catalog_rate WHERE target_id=:target_id and ip=:ip",['target_id'=>$catalog_full['articul'],'ip'=>$_SERVER['REMOTE_ADDR']])){
            $view->rated = true;
        }

        if ($canon_name4!=''){
            err_404();
        }

		if ($ses_user->is_login == true) {
        $select = $db->select()->from('m_catalog_data','pol')
            ->joinLeft('m_catalog_data_order','m_catalog_data_order.id_catalog_data=m_catalog_data.id','type')
            ->joinLeft('m_catalog_type','m_catalog_type.type_ident=m_catalog_data_order.type',['name','slug','men_name','women_name','no_pol'])
            ->where('m_catalog_data_order.price!=0')
            ->where('id_catalog=?',$brand['id'])
            ->where('m_catalog_data.id=?',$catalog_full['id'])
            ->where('is_sidebar=1')
            ->where('m_catalog_type.id IS NOT NULL')
            ->group('m_catalog_type.id')
            ->group('pol');
		} else {
		$select = $db->select()->from('m_catalog_data','pol')
            ->joinLeft('m_catalog_data_order','m_catalog_data_order.id_catalog_data=m_catalog_data.id','type')
            ->joinLeft('m_catalog_type','m_catalog_type.type_ident=m_catalog_data_order.type',['name','slug','men_name','women_name','no_pol'])
            ->where('m_catalog_data_order.price!=0')
			->where('m_catalog_data.id_catalog!=95')
			->where('m_catalog_data.id_catalog!=78')
			->where('m_catalog_data.id_catalog!=145')//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown, Kilian
			->where('m_catalog_data.id_catalog!=487')
			->where('m_catalog_data.id_catalog!=453')
            ->where('id_catalog=?',$brand['id'])
            ->where('m_catalog_data.id=?',$catalog_full['id'])
            ->where('is_sidebar=1')
            ->where('m_catalog_type.id IS NOT NULL')
            ->group('m_catalog_type.id')
            ->group('pol');
		}

        //echo $select->__toString();
        $links_types = $db->fetchAll($select);

        foreach ($links_types as $link_type) {
            $aLinkType[$link_type['type']][]=$link_type;
        }
		
		if ($ses_user->is_login == true) {
			$fetch_all_db = $db->select()
            ->from('m_catalog_data','pol')
            ->joinLeft('m_catalog_data_order','m_catalog_data_order.id_catalog_data=m_catalog_data.id','type')
            ->where('m_catalog_data_order.price!=0')
            ->where('id_catalog=?',$brand['id'])
            ->where('m_catalog_data.id=?',$catalog_full['id'])
            ->where('m_catalog_data_order.type in (?)',['TTW','TT','DT','OT','PT','BLT','SGT','PDT','AST','ASBT'])
            ->group('pol');
		} else {
			$fetch_all_db = $db->select()
            ->from('m_catalog_data','pol')
            ->joinLeft('m_catalog_data_order','m_catalog_data_order.id_catalog_data=m_catalog_data.id','type')
            ->where('m_catalog_data_order.price!=0')
			->where('m_catalog_data.id_catalog!=95')
			->where('m_catalog_data.id_catalog!=78')
			->where('m_catalog_data.id_catalog!=145')//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown, Kilian
			->where('m_catalog_data.id_catalog!=487')
			->where('m_catalog_data.id_catalog!=453')
            ->where('id_catalog=?',$brand['id'])
            ->where('m_catalog_data.id=?',$catalog_full['id'])
            ->where('m_catalog_data_order.type in (?)',['TTW','TT','DT','OT','PT','BLT','SGT','PDT','AST','ASBT'])
            ->group('pol');
		}
        if ($aTester = $db->fetchAll($fetch_all_db)){
            $link_type = $db->fetchRow($db->select()->from('m_catalog_type')->where('type_ident=?','TEST'));
            $aLinkType[$link_type['type_ident']][]=$link_type;
        }



        $view->aromat_links_types = $aLinkType;




        /* -=========================================
        ��������
        */
        $constructor['is_right'] = 1;
        if (isset($_GET['template']) && $_GET['template'] == 'new') {
            $constructor['meta_noindex'] = true;
            $constructor['content'] = $view->render('content_catalog_full_new.php');
        } else {
            $constructor['content'] = $view->render('content_catalog_full.php');
        }

        if (isset($_GET['test']) && $_GET['test'] == 1) {
            $constructor['meta_noindex'] = true;
            $constructor['content'] = $view->render('content_catalog_clear.php');
        }
    }


} elseif (isset($canon_name3) AND in_array($canon_name3, $brand_pages)) {
    err_301($canon_name1.'/'.$canon_name2);
    //if (!$brand = $db->fetchRow($db->select()->from('m_catalog')->where('id_cat=82')->where('url = ?', $canon_name2))) {
	if (!$brand = $db->fetchRow("SELECT * FROM m_catalog WHERE id_cat = 82 AND url = '$canon_name2'".$sql_m_catalog_block)) {
        err_404();
    };
    $view->brand = $brand;
    if ($brand['title_1'] == '') {
        $brand['title_1'] = $brand['title'];
    }


    /**
     * �������������� �������� ������
     */
	if ($ses_user->is_login == true) {
		$query = $db->select()->from('m_catalog_data')->where('id_catalog=?', $brand['id']);
	} else {
		$query = $db->select()->from('m_catalog_data')->where('id_catalog=?', $brand['id'])->where('m_catalog_data.id_catalog!=95')->where('m_catalog_data.id_catalog!=78')->where('m_catalog_data.id_catalog!=145')->where('m_catalog_data.id_catalog!=487')->where('m_catalog_data.id_catalog!=453');//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown, Kilian
	}

    $select_sidebar = $query->reset(Zend_Db_Select::ORDER)->order('title ASC');
    $aCatalogItemSidebar = $db->fetchAll($select_sidebar);
    foreach ($aCatalogItemSidebar as $c_catalog_data) {
        if ($c_catalog_data['pol'] == 'F') {
            $catalog_item_sidebar_f[] = array(
                'id' => $c_catalog_data['id'],
                'title' => $c_catalog_data['title'],
                'brend_title' => $brend['title'],
                'url' => '/catalog/' . $canon_name2 . '/' . $c_catalog_data['url'] . '/',
                'brend_url' => $canon_name2,
                'img' => $img,
                'is_spec' => $c_catalog_data['is_spec'],
                'pol' => $c_catalog_data['pol'],
                'img_big' => $img_big,
                'alt' => $c_catalog_data['alt'],
                'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
				'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
            );
        } elseif ($c_catalog_data['pol'] == 'M') {
            $catalog_item_sidebar_m[] = array(
                'id' => $c_catalog_data['id'],
                'title' => $c_catalog_data['title'],
                'brend_title' => $brend['title'],
                'url' => '/catalog/' . $canon_name2 . '/' . $c_catalog_data['url'] . '/',
                'brend_url' => $canon_name2,
                'img' => $img,
                'pol' => $c_catalog_data['pol'],
                'is_spec' => $c_catalog_data['is_spec'],
                'img_big' => $img_big,
                'alt' => $c_catalog_data['alt'],
                'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
				'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
            );
        }
    }
    if (is_array($catalog_item_sidebar_f)) {
        $view->catalog_item_sidebar_f = $catalog_item_sidebar_f;
    }
    if (is_array($catalog_item_sidebar_m)) {
        $view->catalog_item_sidebar_m = $catalog_item_sidebar_m;
    }

    $aCatalogData = $db->fetchAll($query);
    foreach ($aCatalogData as $item) {
        if ($item['pol'] == 'F') {
            $catalog_item_f[] = prepare_catalog_item($item, $brand);
        } elseif ($item['pol'] == 'M') {
            $catalog_item_m[] = prepare_catalog_item($item, $brand);
        }

    }

    if (is_array($catalog_item_f)) {
        $view->catalog_item_f = $catalog_item_f;
    }
    if (is_array($catalog_item_m)) {
        $view->catalog_item_m = $catalog_item_m;
    }

    $view->brand = $brand;
    $view->brend = $brand;

    //links reviews
	if ($ses_user->is_login == true) {
    $select = $db->select()->from('m_catalog_data','')
        ->joinLeft('m_catalog_com','m_catalog_com.id_catalog_data=m_catalog_data.id')
        ->where('id_catalog=?',$brand['id'])->where('m_catalog_com.id IS NOT NULL');
	} else {
	$select = $db->select()->from('m_catalog_data','')
        ->joinLeft('m_catalog_com','m_catalog_com.id_catalog_data=m_catalog_data.id')
        ->where('id_catalog=?',$brand['id'])->where('m_catalog_com.id IS NOT NULL')->where('m_catalog_data.id_catalog!=95')->where('m_catalog_data.id_catalog!=78')->where('m_catalog_data.id_catalog!=145')->where('m_catalog_data.id_catalog!=487')->where('m_catalog_data.id_catalog!=453');//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown, Kilian	
	}

    $view->links_reviews = $db->fetchAll($select);
	if ($ses_user->is_login == true) {
    $select = $db->select()->from('m_catalog_data','pol')
        ->joinLeft('m_catalog_data_order','m_catalog_data_order.id_catalog_data=m_catalog_data.id','type')
        ->joinLeft('m_catalog_type','m_catalog_type.type_ident=m_catalog_data_order.type',['name','slug','men_name','women_name','no_pol'])
        ->where('m_catalog_data_order.price!=0')
        ->where('id_catalog=?',$brand['id'])
        ->where('is_sidebar=1')
        ->where('m_catalog_type.id IS NOT NULL')
        ->group('m_catalog_type.id')
        ->group('pol');
	} else {
	 $select = $db->select()->from('m_catalog_data','pol')
        ->joinLeft('m_catalog_data_order','m_catalog_data_order.id_catalog_data=m_catalog_data.id','type')
        ->joinLeft('m_catalog_type','m_catalog_type.type_ident=m_catalog_data_order.type',['name','slug','men_name','women_name','no_pol'])
        ->where('m_catalog_data_order.price!=0')
		->where('m_catalog_data.id_catalog!=95')
		->where('m_catalog_data.id_catalog!=78')
		->where('m_catalog_data.id_catalog!=145')//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown, Kilian
		->where('m_catalog_data.id_catalog!=487')
		->where('m_catalog_data.id_catalog!=453')
        ->where('id_catalog=?',$brand['id'])
        ->where('is_sidebar=1')
        ->where('m_catalog_type.id IS NOT NULL')
        ->group('m_catalog_type.id')
        ->group('pol');	
		
			
	}

    //echo $select->__toString();
    $links_types = $db->fetchAll($select);

    foreach ($links_types as $link_type) {
        $aLinkType[$link_type['type']][]=$link_type;
    }



    $view->links_types = $aLinkType;

    include_once('brend_pages.php');

//brand page without ext pages
} elseif (isset($canon_name2)) {
    if (strlen($canon_name2) == 1) {
        $liter = strtolower($canon_name2{0});

        $liter = eregi_replace('[^a-z]', '', $liter);
        if ($liter == '') {
            err_404();
        }

        $view->liter = $liter;
        $constructor['meta_title'] = '�������� ���� - ' . BASE_URL;

        $constructor['content'] = $view->render('content_liter_item.php');

    } elseif (preg_match('/^utm.*/', $canon_name2)) {
        $constructor['meta_title'] = '�������� ���� - ' . BASE_URL;
        $constructor['content'] = $view->render('content_brend_all.php');
    } else {
        //$brend = $db->fetchRow($db->select()->from('m_catalog')->where('id_cat=82')->where('url=?', $canon_name2)->where('is_block=0'));
		$brend = $db->fetchRow("SELECT * FROM m_catalog WHERE id_cat = 82 AND url = '$canon_name2' AND is_block=0".$sql_m_catalog_block);
        //echo $canon_name2;
        if ($brend == false) {
            err_404();
        }

        //links

        //links reviews
		if ($ses_user->is_login == true) {
        $select = $db->select()->from('m_catalog_data','')
            ->joinLeft('m_catalog_com','m_catalog_com.id_catalog_data=m_catalog_data.id')
            ->where('id_catalog=?',$brend['id'])->where('m_catalog_com.id IS NOT NULL');
		} else {
			$select = $db->select()->from('m_catalog_data','')
            ->joinLeft('m_catalog_com','m_catalog_com.id_catalog_data=m_catalog_data.id')
            ->where('id_catalog=?',$brend['id'])->where('m_catalog_com.id IS NOT NULL')->where('m_catalog_data.id_catalog!=95')->where('m_catalog_data.id_catalog!=78')->where('m_catalog_data.id_catalog!=145')->where('m_catalog_data.id_catalog!=487')->where('m_catalog_data.id_catalog!=453');//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown, Kilian	
		}

        $view->links_reviews = $db->fetchAll($select);
		if ($ses_user->is_login == true) {
        $select = $db->select()->from('m_catalog_data','pol')
            ->joinLeft('m_catalog_data_order','m_catalog_data_order.id_catalog_data=m_catalog_data.id','type')
            ->joinLeft('m_catalog_type','m_catalog_type.type_ident=m_catalog_data_order.type',['name','slug','men_name','women_name','no_pol'])
            ->where('m_catalog_data_order.price!=0')
            ->where('id_catalog=?',$brend['id'])
            ->where('is_sidebar=1')
            ->where('m_catalog_type.id IS NOT NULL')
            ->group('m_catalog_type.id')
            ->group('pol');
		} else {
		$select = $db->select()->from('m_catalog_data','pol')
            ->joinLeft('m_catalog_data_order','m_catalog_data_order.id_catalog_data=m_catalog_data.id','type')
            ->joinLeft('m_catalog_type','m_catalog_type.type_ident=m_catalog_data_order.type',['name','slug','men_name','women_name','no_pol'])
            ->where('m_catalog_data_order.price!=0')
			->where('m_catalog_data.id_catalog!=95')
			->where('m_catalog_data.id_catalog!=78')
			->where('m_catalog_data.id_catalog!=145')//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown, Kilian
			->where('m_catalog_data.id_catalog!=487')
			->where('m_catalog_data.id_catalog!=453')
            ->where('id_catalog=?',$brend['id'])
            ->where('is_sidebar=1')
            ->where('m_catalog_type.id IS NOT NULL')
            ->group('m_catalog_type.id')
            ->group('pol');
		}

        //echo $select->__toString();
        $links_types = $db->fetchAll($select);
        foreach ($links_types as $link_type) {
            $aLinkType[$link_type['type']][]=$link_type;
        }
		if ($ses_user->is_login == true) {
			$sel_fetchall_db = $db->select()
            ->from('m_catalog_data','pol')
            ->joinLeft('m_catalog_data_order','m_catalog_data_order.id_catalog_data=m_catalog_data.id','type')
            ->where('id_catalog=?',$brend['id'])
            ->where('m_catalog_data_order.type in (?)',['TTW','TT','DT','OT','PT','BLT','SGT','PDT','AST','ASBT'])
            ->group('pol');
		} else {
			$sel_fetchall_db = $db->select()
            ->from('m_catalog_data','pol')
            ->joinLeft('m_catalog_data_order','m_catalog_data_order.id_catalog_data=m_catalog_data.id','type')
            ->where('id_catalog=?',$brend['id'])
			->where('m_catalog_data.id_catalog!=95')
			->where('m_catalog_data.id_catalog!=78')
			->where('m_catalog_data.id_catalog!=145')//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown, Kilian
			->where('m_catalog_data.id_catalog!=487')
			->where('m_catalog_data.id_catalog!=453')
            ->where('m_catalog_data_order.type in (?)',['TTW','TT','DT','OT','PT','BLT','SGT','PDT','AST','ASBT'])
            ->group('pol');
		}
        if ($aTester = $db->fetchAll($sel_fetchall_db)){
            $link_type = $db->fetchRow($db->select()->from('m_catalog_type')->where('type_ident=?','TEST'));
            $aLinkType[$link_type['type_ident']][]=$link_type;
        }





        //print_r($aLinkType);

        $view->links_types = $aLinkType;





	if ($ses_user->is_login == true) {
		$select = $db->select()->from('m_catalog_data')->where('id_catalog=?',$brend['id']);
	} else {
		  $select = $db->select()->from('m_catalog_data')->where('id_catalog=?',$brend['id'])
				->where('m_catalog_data.id_catalog!=95')
				->where('m_catalog_data.id_catalog!=78')
				->where('m_catalog_data.id_catalog!=145')
				->where('m_catalog_data.id_catalog!=487')
				->where('m_catalog_data.id_catalog!=453');//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown, Kilian
	}
      

        if ($_GET['sort']!=''){
            switch($_GET['sort']){
                case 'rate':
                    $select->order('rate DESC');
                    break;
                case 'price':
                    $select->order('price_s ASC');
                    break;
                case 'abc':
                    $select->order('title ASC');
                    break;
                case 'new':
                    $select->order('id DESC');
                    break;
            }
        }else{
            $select->order('rate DESC');
        }

        //parfum sidebar




        $q_catalog_data = $db->query($select);
        while ($c_catalog_data = $q_catalog_data->fetch()) {
            if (!$og_image){
                if ($c_catalog_data['img'] != '') {
                    $og_image =  'http://' . $_SERVER['HTTP_HOST'] . '/images/uploads/catalog/' . $c_catalog_data['id'] . '/big/' . $c_catalog_data['img'];
                }
            }

            if ($c_catalog_data['img'] == '') {
                $img = '/images/noimage.jpg';
                $img_big = '/images/noimage.jpg';
            } else {
                $img = '/images/uploads/catalog/' . $c_catalog_data['id'] . '/small/' . $c_catalog_data['img'];
                $img_big = '/images/uploads/catalog/' . $c_catalog_data['id'] . '/big/' . $c_catalog_data['img'];
            }
			
			
            if ($c_catalog_data['pol'] == 'F') {
                $catalog_item_f[] = array(
                    'id' => $c_catalog_data['id'],
                    'title' => $c_catalog_data['title'],
                    'brend_title' => $brend['title'],
                    'url' => '/catalog/' . $canon_name2 . '/' . $c_catalog_data['url'] . '/',
                    'brend_url' => $canon_name2,
                    'img' => $img,
                    'is_spec' => $c_catalog_data['is_spec'],
                    'pol' => $c_catalog_data['pol'],
                    'img_big' => $img_big,
                    'alt' => $c_catalog_data['alt'],
                    'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
					'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                    'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
                );
            } elseif ($c_catalog_data['pol'] == 'M') {
                $catalog_item_m[] = array(
                    'id' => $c_catalog_data['id'],
                    'title' => $c_catalog_data['title'],
                    'brend_title' => $brend['title'],
                    'url' => '/catalog/' . $canon_name2 . '/' . $c_catalog_data['url'] . '/',
                    'brend_url' => $canon_name2,
                    'img' => $img,
                    'pol' => $c_catalog_data['pol'],
                    'is_spec' => $c_catalog_data['is_spec'],
                    'img_big' => $img_big,
                    'alt' => $c_catalog_data['alt'],
                    'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
					'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                    'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
                );
            }
			
			
			$catalog_prod_items[] = array (
					'id' => $c_catalog_data['id'],
                    'title' => $c_catalog_data['title'],
                    'brend_title' => $brend['title'],
                    'url' => '/catalog/' . $canon_name2 . '/' . $c_catalog_data['url'] . '/',
                    'brend_url' => $canon_name2,
                    'img' => $img,
                    'pol' => $c_catalog_data['pol'],
                    'is_spec' => $c_catalog_data['is_spec'],
                    'img_big' => $img_big,
                    'alt' => $c_catalog_data['alt'],
                    'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
					'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                    'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
			);
        }
		
        if (is_array($catalog_item_f)) {
            $view->catalog_item_f = $catalog_item_f;
        }
        if (is_array($catalog_item_m)) {
            $view->catalog_item_m = $catalog_item_m;
        }
		
		$view->catalog_prod_items = $catalog_prod_items;
		 
        $select_sidebar = $select->reset(Zend_Db_Select::ORDER)->order('title ASC');
        $aCatalogItemSidebar = $db->fetchAll($select_sidebar);
        foreach ($aCatalogItemSidebar as $c_catalog_data) {
            if ($c_catalog_data['pol'] == 'F') {
                $catalog_item_sidebar_f[] = array(
                    'id' => $c_catalog_data['id'],
                    'title' => $c_catalog_data['title'],
                    'brend_title' => $brend['title'],
                    'url' => '/catalog/' . $canon_name2 . '/' . $c_catalog_data['url'] . '/',
                    'brend_url' => $canon_name2,
                    'img' => $img,
                    'is_spec' => $c_catalog_data['is_spec'],
                    'pol' => $c_catalog_data['pol'],
                    'img_big' => $img_big,
                    'alt' => $c_catalog_data['alt'],
                    'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
					'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                    'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
                );
            } elseif ($c_catalog_data['pol'] == 'M') {
                $catalog_item_sidebar_m[] = array(
                    'id' => $c_catalog_data['id'],
                    'title' => $c_catalog_data['title'],
                    'brend_title' => $brend['title'],
                    'url' => '/catalog/' . $canon_name2 . '/' . $c_catalog_data['url'] . '/',
                    'brend_url' => $canon_name2,
                    'img' => $img,
                    'pol' => $c_catalog_data['pol'],
                    'is_spec' => $c_catalog_data['is_spec'],
                    'img_big' => $img_big,
                    'alt' => $c_catalog_data['alt'],
                    'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
					'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                    'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
                );
            }
        }
        if (is_array($catalog_item_sidebar_f)) {
            $view->catalog_item_sidebar_f = $catalog_item_sidebar_f;
        }
        if (is_array($catalog_item_sidebar_m)) {
            $view->catalog_item_sidebar_m = $catalog_item_sidebar_m;
        }


        $view->brend = $brend;
        $view->brend_title = $brend['title'];
        $view->brend_anot = word_to_link(to_1251($brend['anot']), '\.', 'http://' . BASE_URL);
        $view->brend_url = $canon_name2;

        /* -=========================================
        META
        */
        $meta_title = $brend['meta_title'];
        $meta_title_1 = $brend['meta_title_1'];

        $view->meta_title = $meta_title;
        $view->meta_title_1 = $meta_title;

        if ($brend['title_1'] == '') {
            $m_brend_title_1 = $brend['title'];
        } else {
            $m_brend_title_1 = $brend['title_1'];
        }

        $slug = $canon_name1;


        if ($aromat_type = $db->fetchRow($db->select()->from('m_catalog_type')->where('slug=?',
            $slug)->orWhere('slug=?', $slug))
        ) {
            foreach ($aromat_type as &$col) {
                $col = str_replace(':title:', $brend['title'], $col);
                $col = str_replace(':title_1:', $brend['title_1'], $col);
                $col = str_replace('  ', ' ', $col);
                $col = str_replace(' ,', ',', $col);
            }
            $view->h1 = $aromat_type['listing_h1'];
            $constructor['meta_title'] = $aromat_type['listing_title'];;
            $constructor['meta_keywords'] = $aromat_type['listing_keywords'];
            $constructor['meta_description'] = $aromat_type['listing_description'];
            $constructor['og:title'] = $aromat_type['listing_title'];
            $constructor['og:image'] = $og_image;
            $constructor['og:description'] = $aromat_type['listing_description'];
            $constructor['og:url'] =  $_SERVER['REQUEST_URI'];

            $view->meta_body = $aromat_type['listing_body'];
        }

        $constructor['content'] = $view->render('content_catalog.php');
    }

} else {


    if ($aromat_type = $db->fetchRow($db->select()->from('m_catalog_type')->where('slug=?', $canon_name1))) {
        $view->h1 = $aromat_type['h1'];
        $constructor['meta_title'] = $aromat_type['title'];;
        $constructor['meta_keywords'] = $aromat_type['keywords'];
        $constructor['meta_description'] = $aromat_type['description'];
        $view->meta_body = $aromat_type['body'];
    }


    $constructor['content'] = $view->render('content_brend_all.php');
}

}