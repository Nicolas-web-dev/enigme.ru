<?php

if ($canon_name2 != '' && !in_array($canon_name2, ['women', 'men'])) {
    err_301($canon_name1 . '/');
}

$sql_m_catalog_data_a_block = '';
$sql_m_catalog_data_block = '';
//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown, Kilian
if ($ses_user->is_login == false) {
	$sql_m_catalog_data_a_block = ' AND a.id_catalog != 95 AND a.id_catalog != 78 AND a.id_catalog != 145 AND a.id_catalog != 487  AND a.id_catalog != 453';
	$sql_m_catalog_data_block = ' AND id_catalog != 95 AND id_catalog != 78 AND id_catalog != 145 AND id_catalog != 487  AND id_catalog != 453';
}

$slug = $canon_name1;

$select = " SELECT a.id, a.title, a.url, a.price_s, a.img, a.text,b.url AS brend_url,b.title AS brend_title,b.title AS brend_title, b.url AS brend_url
 FROM m_catalog_data AS a
JOIN m_catalog AS b ON b.id=a.id_catalog
WHERE a.id_catalog IN (SELECT id FROM m_catalog WHERE is_lux=1) ".$sql_m_catalog_data_a_block
    . "ORDER BY RAND() "
    . "LIMIT 10";


if ($canon_name2 != '' && in_array($canon_name2, ['women', 'men'])) {
    if ($canon_name2 == 'women') {
        $pol = 'F';
        $slug .= '/women';
    }
    if ($canon_name2 == 'men') {
        $pol = 'M';
        $slug .= '/men';
    }



    $select = " SELECT a.id, a.title, a.url, a.price_s, a.img, a.text,b.url AS brend_url,b.title AS brend_title,b.title AS brend_title, b.url AS brend_url
         FROM m_catalog_data AS a
        JOIN m_catalog AS b ON b.id=a.id_catalog
        WHERE a.id_catalog IN (SELECT id FROM m_catalog WHERE is_lux=1) AND a.pol='$pol' ".$sql_m_catalog_data_a_block
        . "ORDER BY RAND() "
        . "LIMIT 10";

}





$q_catalog = $db->query($select);

while ($c_catalog = $q_catalog->fetch()) {
    if ($c_catalog['img'] == '') {
        $img = '/images/noimage.jpg';
        $img_big = '/images/noimage.jpg';
    } else {
        $img = '/images/uploads/catalog/' . $c_catalog['id'] . '/small/' . $c_catalog['img'];
        $img_big = '/images/uploads/catalog/' . $c_catalog['id'] . '/big/' . $c_catalog['img'];
    }

    if (!$og_image){
        $og_image = $img;
    }

    $catalog_item[] = array(
        'title' => $c_catalog['title'],
        'brend_title' => $c_catalog['brend_title'],
        'price_s' => $c_catalog['price_s'] + $_SESSION['_GMARGIN'],
        'img' => $img,
        'img_big' => $img_big,
        'url' => '/catalog/' . $c_catalog['brend_url'] . '/' . $c_catalog['url'] . '/',
        'anot' => maxsite_str_word(html_entity_decode($c_catalog['text']), 10) . ' ...'
    );

}

unset($brend_item);
$select = "SELECT "
    . "title, url, is_top, pol "
    . "FROM m_catalog "
    . "WHERE id_cat=82 AND is_block=0 and is_lux=1  "
    . "ORDER BY title ";

$stmt = $db->query($select);
while ($content = $stmt->fetch()) {
    $liter = strtolower($content['title']{0});
    $aLiter[$liter] = $liter;

    $brend_item[] = array(
        'title' => $content['title'],
        'url' => $content['url'],
        'liter' => $liter,
        'is_top' => $content['is_top'],
        'pol' => $content['pol'],
    );
}

foreach ($aLiter as $liter) {
    $liter_lux[] = $liter;
}


$view->brend_item = $brend_item;
$view->brend_liter_lux = $liter_lux;
$view->catalog_item = $catalog_item;



if ($aromat_type = $db->fetchRow($db->select()->from('m_catalog_type')->where('slug=?',$slug))){
    $view->h1 = $aromat_type['h1'];
    $constructor['meta_title'] = $aromat_type['title'];;
    $constructor['meta_keywords'] = $aromat_type['keywords'];
    $constructor['meta_description'] = $aromat_type['description'];
    $view->meta_body = $aromat_type['body'];
}

$constructor['og:title'] = $constructor['meta_title'];
$constructor['og:image'] = $og_image;
$constructor['og:description'] =$constructor['meta_description'];
$constructor['og:url'] =  $_SERVER['REQUEST_URI'];


$constructor['content'] = $view->render('content_lux.php');
?>