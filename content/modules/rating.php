<?php
$per_page= 25;



$page = ($canon_name3)?$canon_name3:1;
if ($canon_name4){
    $page = ($canon_name4)?$canon_name4:1;
}



$page = (int)(($page));

if ($page>1){
    $constructor['meta_noindex'] = true;
}



$select = $db->select()
    ->from(['a'=>'m_catalog_data'],['a.id','a.title','a.url','a.pol','a.price_s','a.img','a.text','a.tag'])
    ->joinLeft(['b'=>'m_catalog'],' b.id=a.id_catalog',['brend_title'=>'b.title','brend_url'=>'b.url'])
    ->joinLeft(['o'=>'m_catalog_data_order'],'o.id_catalog_data=a.id','')
    ->where('a.id_cat=82 AND a.is_spec=1')
    ->group('o.id_catalog_data')
    ->order('a.rating DESC')
    ->limitPage($page,$per_page);



if ($canon_name1 && $canon_name1=='rating-muzhskih-duhov'){
    $select->where('a.pol=?','M');
    if ($aromat_type = $db->fetchRow($db->select()->from('m_catalog_type')->where('slug=?',$canon_name1))){
        $view->h1 = $aromat_type['h1'];
        $constructor['meta_title'] = $aromat_type['title'];;
        $constructor['meta_keywords'] = $aromat_type['keywords'];
        $constructor['meta_description'] = $aromat_type['description'];
        $view->meta_body = $aromat_type['body'];
    }
}

if ($canon_name1 && $canon_name1=='rating-zhenskih-duhov'){
    $select->where('a.pol=?','F');
    if ($aromat_type = $db->fetchRow($db->select()->from('m_catalog_type')->where('slug=?',$canon_name1))){
        $view->h1 = $aromat_type['h1'];
        $constructor['meta_title'] = $aromat_type['title'];;
        $constructor['meta_keywords'] = $aromat_type['keywords'];
        $constructor['meta_description'] = $aromat_type['description'];
        $view->meta_body = $aromat_type['body'];
    }
}

$q_catalog = $db->query($select);
while ($c_catalog = $q_catalog->fetch()) {

    if ($c_catalog['img']=='') {
        $img = '/images/noimage.jpg';
        $img_big = '/images/noimage.jpg';
    }else {
        $img = '/images/uploads/catalog/'.$c_catalog['id'].'/small/'.$c_catalog['img'];
        $img_big = '/images/uploads/catalog/'.$c_catalog['id'].'/big/'.$c_catalog['img'];
    }

    if (!$og_image){
        $og_image = $img;
    }

    $catalog_item[] = array(
        'title' 		=> $c_catalog['title'],
        'brend_title' 	=> $c_catalog['brend_title'],
        'price_s' 		=> $c_catalog['price_s']+$_SESSION['_GMARGIN'],
        'img' 			=> $img,
        'img_big' 		=> $img_big,
        'url' 			=> '/catalog/'.$c_catalog['brend_url'].'/'.$c_catalog['url'].'/',
        'anot' 			=> maxsite_str_word(html_entity_decode($c_catalog['text']), 10) .' ...'
    );
}
$view->catalog_item = $catalog_item;



$count =  $db->fetchOne('SELECT count(*) FROM m_catalog_data WHERE is_spec=1');


$p 		= pages_content($count, $per_page, $canon_name3, 'pages.tpl');

$view->catalog_item = $catalog_item;
$view->p = $p;
$view->aromat_type = $aromat_type;

$constructor['og:title'] = $constructor['meta_title'];
$constructor['og:image'] = $og_image;
$constructor['og:description'] =$constructor['meta_description'];
$constructor['og:url'] =  $_SERVER['REQUEST_URI'];

$constructor['content'] = $view->render('content_rating.php');
?>