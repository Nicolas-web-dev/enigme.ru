<?

$slug = $canon_name1;

if ($aromat_type = $db->fetchRow($db->select()->from('m_catalog_type')->where('slug=?',$slug)->orWhere('slug_sin=?',$slug))){
    $view->h1 = $aromat_type['h1'];
    $constructor['meta_title'] = $aromat_type['title'];;
    $constructor['meta_keywords'] = $aromat_type['keywords'];
    $constructor['meta_description'] = $aromat_type['description'];
    $view->meta_body = $aromat_type['body'];
}

$per_page= 26;



$page = ($canon_name3)?$canon_name3:1;
if ($canon_name4){
    $page = ($canon_name4)?$canon_name4:1;
}



$page = (int)(($page));


if ($page>1){
    $constructor['meta_noindex'] = true;
}



$select = $db->select()
    ->from(['a'=>'m_catalog_data'],['a.id','a.title','a.url','a.pol','a.price_s','a.img','a.text','a.tag'])
    ->joinLeft(['b'=>'m_catalog'],' b.id=a.id_catalog',['brend_title'=>'b.title','brend_url'=>'b.url'])
    ->joinInner(['p2'=>$db->select()
        ->from('m_catalog_data_order',['type','min'=>new Zend_Db_Expr('min(price)'),'id_catalog_data'])
        ->where('type=?',$aromat_type['type_ident'])->where('price!=0')
        ->group('id_catalog_data')],'a.id=p2.id_catalog_data')
    ->where('a.id_cat=82')
    ->order('a.rating DESC')
    ->limitPage($page,$per_page);

if ($slug=='balzamy-posle-britya' || $slug=='geli-posle-britya'){
    $select->where('a.pol=?','M');
}





$q_catalog = $db->query($select);
while ($c_catalog = $q_catalog->fetch()) {

    if ($c_catalog['img']=='') {
        $img = '/images/noimage.jpg';
        $img_big = '/images/noimage.jpg';
    }else {
        $img = '/images/uploads/catalog/'.$c_catalog['id'].'/small/'.$c_catalog['img'];
        $img_big = '/images/uploads/catalog/'.$c_catalog['id'].'/big/'.$c_catalog['img'];
    }

    $c_catalog['url'].='/'.$aromat_type['slug'];

    $catalog_item[] = array(
        'title' 		=> $c_catalog['title'],
        'brend_title' 	=> $c_catalog['brend_title'],
        'price_s' 		=> $c_catalog['price_s']+$_SESSION['_GMARGIN'],
        'img' 			=> $img,
        'img_big' 		=> $img_big,
        'url' 			=> '/catalog/'.$c_catalog['brend_url'].'/'.$c_catalog['url'].'/',
        'anot' 			=> maxsite_str_word(html_entity_decode($c_catalog['text']), 10) .' ...'
    );
}
$view->catalog_item = $catalog_item;

if (count($catalog_item)==0){
    err_404();
}

$q_count = $db->select()->from(['a'=>'m_catalog_data_order'])->where('type=?',$aromat_type['type_ident'])->where('price!=0')->group('id_catalog_data');
$count =  $db->fetchOne('SELECT count(*) as cnt FROM ('.$q_count->__toString().') sq');


$p 		= pages_content($count, $per_page, $canon_name3, 'pages.tpl');

$view->catalog_item = $catalog_item;
$view->aromat_type = $aromat_type;
$view->p = $p;


$constructor['content'] = $view->render('content_type.php');
?>