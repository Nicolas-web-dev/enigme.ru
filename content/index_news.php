<?php

$select = "SELECT "
."id, url, title, anot, img, alt, registerDate "
."FROM m_news "
."WHERE is_block=0 and id_cat=89 "
."ORDER BY id desc "
."LIMIT 5";

// QUERY
$stmt = $db->query($select);
while ($content = $stmt->fetch()) {
	$news_item_89[] = array(
	'id' 	=> $content['id'],
	'url' 	=> $content['url'],
	'title' => $content['title'],
	'img' => $content['img'],
	'alt' => ($content['alt']=='') ? $content['title'] : $content['alt'],
	'date' => $content['registerDate'],
	'anot' 	=> word_to_link(to_1251($content['anot']), '\.', 'http://'.BASE_URL),
	);
}
$view->news_item_89 = $news_item_89;


$select = "SELECT "
."id, url, title, anot, img, alt, registerDate "
."FROM m_news "
."WHERE is_block=0 and id_cat=84 "
."ORDER BY id desc "
."LIMIT 5";

// QUERY
$stmt = $db->query($select);
while ($content = $stmt->fetch()) {
	$news_item_84[] = array(
	'id' 	=> $content['id'],
	'url' 	=> $content['url'],
	'title' => $content['title'],
	'img' => $content['img'],
	'alt' => ($content['alt']=='') ? $content['title'] : $content['alt'],
	'date' => $content['registerDate'],
	'anot' 	=> word_to_link(to_1251($content['anot']), '\.', 'http://'.BASE_URL),
	);
}
$view->news_item_84 = $news_item_84;

?>