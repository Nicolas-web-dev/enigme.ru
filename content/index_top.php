<?php

$sql_m_catalog_data_a_block = '';
//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown, Kilian
if ($ses_user->is_login == false) {
	$sql_m_catalog_data_a_block = 'AND a.id_catalog != 95 AND a.id_catalog != 78 AND a.id_catalog != 145 AND a.id_catalog != 487  AND a.id_catalog != 453 ';
}

$select = "SELECT "
."a.id, a.title, a.url, a.price_s, a.price_e, a.img, a.text, a.pol,a.is_spec, "
."b.title AS brend_title, b.url AS brend_url "
."FROM m_catalog_data AS a "
."JOIN m_catalog AS b ON b.id=a.id_catalog "
."WHERE a.id_cat=82 AND a.is_main=1 ".$sql_m_catalog_data_a_block
."ORDER BY RAND() "
."LIMIT 40";

$q_catalog_top = $db->query($select);
while ($c_catalog_data = $q_catalog_top->fetch()) {
	if (!$og_image){
		$og_image = 'http://enigme.ru/images/uploads/catalog/'.$c_catalog_data['id'].'/small/'.$c_catalog_data['img'];
	}

	$catalog_item_top[] = array(
	'id_catalog' 	=> $c_catalog_data['id'],
	'title' 		=> $c_catalog_data['title'],
	'is_spec' 		=> $c_catalog_data['is_spec'],
	'brend_title' 	=> $c_catalog_data['brend_title'],
	'price_s' 		=> $c_catalog_data['price_s']+$_SESSION['_GMARGIN'],
	'price_e' 		=> $c_catalog_data['price_e']+$_SESSION['_GMARGIN'],
	'pol' 			=> ($c_catalog_data['pol'] == 'M') ? '������� ����������' : '������� ����������',
	'img' 			=> '/images/uploads/catalog/'.$c_catalog_data['id'].'/small/'.$c_catalog_data['img'],
	'img_big'		=> '/images/uploads/catalog/'.$c_catalog_data['id'].'/big/'.$c_catalog_data['img'],
	'url' 			=> '/catalog/'.$c_catalog_data['brend_url'].'/'.$c_catalog_data['url'].'/',
	'anot' 			=> maxsite_str_word($c_catalog_data['text'], 9) .' ...'
	);
}
$view->catalog_item_top = $catalog_item_top;

?>