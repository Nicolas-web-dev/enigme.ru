<?php



require_once $_SERVER['DOCUMENT_ROOT'] . "/app/Order.php";
if ($_SESSION['admin'] == 'allow') {

    $table = "m_mag_Orders";

    // GET PARAM
    $status = $_GET['status'];
    $is_sale = intval($_GET['is_sale']);

    /* -=========================================
    PAGES
    */
    $page = $_POST['page'];
    $rp = $_POST['rp'];
    if (!$page) {
        $page = 1;
    }
    if (!$rp) {
        $rp = 10;
    }
    $start = (($page - 1) * $rp);
    $limit = "LIMIT $start, $rp";

    /* -=========================================
    SORT
    */
    $sortname = $_POST['sortname'];
    $sortorder = $_POST['sortorder'];
    if (!$sortname) {
        $sortname = 'name';
    }
    if (!$sortorder) {
        $sortorder = 'desc';
    }

    $sort = "ORDER BY $sortname $sortorder";


    /* -=========================================
    FILTERS
    */
    $query = $_POST['query'];
    $qtype = $_POST['qtype'];

    if ($query) {
        $where = "WHERE $qtype='$query' ";
    } else {
        if ($is_sale == 1) {
            $where = "WHERE status='$status' AND is_sale=1";
        } else {
            $where = "WHERE status='$status' ";
        }
    }

    $select = "$table.*,
			 DateAdd,
			 DateEnd";

    /* -=========================================
    ORDERS__FILTER
    */

    $is_filter = false;
    foreach ($_GET as $key => $value) {
        if (preg_match('/^(filter_).*/', $key)) {
            $is_filter = true;
        }
    }

    // ���� ���� �������� ������ �� �������, �� ������������ �� ��� ����
    if ($is_filter) {

        header('Content-Type: application/json; charset=utf-8');
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");

        require_once 'Zend/Date.php';
        $locale = new Zend_Locale('ru_RU');

        // ������ �������� ���������� � �������� ������
        $tableSynonyms = array(
            'product' => 'm_catalog_data_order.',
            'default' => 'm_mag_Orders.',
            'productOrders' => 'OrdersSum.',
            'user' => 'm_mag_Users.',
            'manager' => 'base_users.',
            'couriers' => 'couriers.',
            'couriersAddress' => 'couriers_addresses.'
        );

        // ����� � �������� �������������� ������� ������
        $statusSynonyms = array(
            0 => array(
                'color' => 'red',
                'name' => '�����������'
            ),
            1 => array(
                'color' => 'green',
                'name' => '�����'
            ),
            2 => array(
                'color' => 'blue',
                'name' => '��������������'
            ),
            3 => array(
                'color' => 'black',
                'name' => '������������'
            ),
            40 => array(
                'color' => 'orange',
                'name' => '� ���������'
            ),
            41 => array(
                'color' => 'green',
                'name' => '���������'
            ),
            5 => array(
                'color' => 'red',
                'name' => '�� ������ (�����������)'
            ),
            6 => array(
                'color' => 'red',
                'name' => '�� ������ (��������)'
            )
        );

        // ���������� ������� �������
        $page = $_POST['page'];
        $rowsPerPage = $_POST['rp'];

        if (!$page) {
            $page = 1;
        }
        if (!$rowsPerPage) {
            $rowsPerPage = 10;
        }
        $start = (($page - 1) * $rowsPerPage);

        $select = "SELECT DISTINCT " . $tableSynonyms['default'] . "*";
        $join = " LEFT JOIN m_mag_OrdersSum AS OrdersSum ON OrdersSum.id_order = m_mag_Orders.id"
            . " LEFT JOIN m_catalog_data_order ON OrdersSum.id_catalog_data_order = m_catalog_data_order.id"
            . " LEFT JOIN m_mag_Users ON m_mag_Orders.id_user = m_mag_Users.id"
            . " LEFT JOIN base_users ON base_users.id = m_mag_Orders.id_manager"
            . " LEFT JOIN couriers_addresses ON m_mag_Orders.courier_address_id = couriers_addresses.id"
            . " LEFT JOIN couriers ON couriers_addresses.courier_id = couriers.couriersId";
        $from = " FROM " . rtrim($tableSynonyms['default'], '.');
        $count = " COUNT(DISTINCT " . $tableSynonyms['default'] . "id) AS cnt";
        $limit = " LIMIT " . $start . ", " . $rowsPerPage;
        $order = " ORDER BY DateAdd DESC";
        $aSelect = array();
        $aWhere = array();
        $prevMatches = null;
        $prevValue = null;
        $allowedKeys = array(
            'id',
            'DateAdd',
            'DateEnd',
            'action',
            'user_obj',
            'price',
            'sum'
        );

        // ���� ��� ������������ � ������� ���������� ��������� ���� �������� "���������"
        // ���������, ����� ��������� �������� ������ � �������
        // ��� ��������: ��������� � ������ ��������: ����� ������
        $is_delivered_self = false;

        // ���� ��� ��������, ��� �������������� ������� ���
        // ���������� ���� ������������ � ��������� ��� ��������� �� ����
        $is_self_used = false;

        // ��������� ������ ���������
        array_filter($_GET, 'strlen');

        // ��������� �� ���� ���������� ����������
        foreach ($_GET as $key => $value) {

            // �������� ��������� ��� where
            $operator = " = ";

            // �������� ������ � ����������� �������
            if (!preg_match('/^(filter_).*/', $key)) {
                continue;
            }

            // ������� filter_ � ������ ����� �����
            $key = ltrim(preg_replace('/filter/', '', $key), "_");

            // ��������� ��� ���������
            // source: filter_user__email-active
            // output: user; email; active;
            // source: filter_id_manager
            // output: ; id_manager; ;
            preg_match('/(\w+__{1}|^)(\w+)?-?(\w+)?$/', $key, $matches);

            // ��������� ���������� ��������
            if ($matches[3] == "active") {

                $prevMatches = $matches;
                $prevValue = $value;
            }

            // ��������� �������� ������ ��� ���������
            $refTable = $matches[1] == null ? $tableSynonyms['default'] : $tableSynonyms[rtrim($matches[1], '__')];

            // ��������� �� � ��������?
            if ($matches[3] == "active" && $value == 1) {

                // ��������� � ������ ���������� ���������
                $allowedKeys[] = $matches[2];

                if ($refTable == $tableSynonyms['default']) {
                    continue;
                }

                $aSelect[] = $refTable . $matches[2];
                // �� ����� ��������� ��������� � active
                // �� ������ ��������� select, �� ��������� �� ������
                continue;
            }

            // �������� -1 ���������� ������ � select ��� ����������� "�� �����"
            if ((int)$value == -1 || (string)$value === "" || (is_numeric($value) && (int)$value === 0 && $matches[2] != 'status') || $matches[0] === 'status-active') {
                continue;
            }

            // ��� id ������ ���� int
            if (preg_match('/.*(id|status|delivery_cost_fact|^type|couriersId|track).*/', $matches[2])) {

                $value = (float)$value;
            } elseif ($matches[3] != "start" && $matches[3] != "end") {

                // ������������ ����������
                if (VLJsonConvertIn(htmlspecialchars(trim($value))) == "���������") {
                    $is_delivered_self = true;
                }

                $operator = " LIKE ";
                $value = "'%" . VLJsonConvertIn(htmlspecialchars(trim($value))) . "%'";
            }

            // ��������� ���������� ��� ���
            if ($matches[3] == "start" || $matches[3] == "end") {

                $operator = $matches[3] == "start" ? " >= " : " <= ";
                $value = $matches[3] == "start" ? $value." 00:00" : $value." 23:59";

                // ���������� ������� ����, ��� ���� ������
                if ($matches[2] != "DateAdd" && $matches[2] != "DateEnd") {
                    $aSelect[] = "(" . $tableSynonyms['default'] . $matches[2] . ") AS " . $matches[2];
                }

                $zDate = new Zend_Date($value, "MM/dd/YYYY HH:mm");
                $value = "'" . $zDate->toString(Zend_Date::ISO_8601) . "'";
            }

            // ���� ��������� ������ �� ����������
            // OR courier_address_id = 0, ��� ����, ����� �������� � ����� ������
            // ��� ������� ���������� ������ � �������� 0
            // ����� ������ ���� � ���, ��� �� ������� ���������
            // ����� �� ��������� ���������� ������� �� courier_address_id = 0
            if($is_delivered_self) {
                $aWhere[] = "(".$refTable . $matches[2] . $operator . $value ." OR ".$tableSynonyms['default'] . "courier_address_id = 0)";
                $is_self_used = true;
                $is_delivered_self = false;
                continue;
            }

            // %___% ���������, ��� ��� ������ ����� ��������� ��������� ���� ������
            // ����� �� ������� ��������� WHERE ������
            if ($matches[2] == "couriersId" && (int)$value === 5 && !$is_self_used) {

                $operator = " = ";
                $aWhere[] = $tableSynonyms['default'] . "courier_address_id" . $operator . "0";
            } elseif ($matches[2] == "couriersId" && (int)$value === 5 && $is_self_used) {

                continue;
            } else {

                // ��������� ��� �������� �������� � ������� � �������� WHERE
                $aWhere[] = $refTable . $matches[2] . $operator . $value;
            }
        }

        // ��������� ������� ���� � ����� �������
        $aSelect[] = "(DateAdd) AS DateAdd, (DateEnd) AS DateEnd";

        $aSelect = implode(", ", array_unique($aSelect));

        if ($aSelect != "") {
            $aSelect = ", " . $aSelect;
        }

        // ��������� ������������ ��������� ������� �� �������
        // ���������� ��� ��������� �������, ����� ��������������� ������� � �������
        if (isset($_GET['get_only_keys']) && $_GET['get_only_keys'] == 1) {

            $keysMatches = array(
                'id' => array(
                    'display' => 'ID',
                    'name' => 'id',
                    'align' => 'left',
                    'sortable' => false
                ),
                'DateAdd' => array(
                    'display' => VLJsonConvertOut('���� ������'),
                    'name' => 'DateAdd',
                    'align' => 'left',
                    'sortable' => false
                ),
                'DateEnd' => array(
                    'display' => VLJsonConvertOut('���� ��������'),
                    'name' => 'DateEnd',
                    'align' => 'left',
                    'sortable' => false
                ),
                'action' => array(
                    'display' => VLJsonConvertOut('��������'),
                    'name' => 'action',
                    'align' => 'left',
                    'sortable' => false
                ),
                'user_obj' => array(
                    'display' => VLJsonConvertOut('������'),
                    'name' => 'user_obj',
                    'align' => 'left',
                    'sortable' => false
                ),
                'price' => array(
                    'display' => VLJsonConvertOut('����'),
                    'name' => 'price',
                    'align' => 'left',
                    'sortable' => false
                ),
                'sum' => array(
                    'display' => VLJsonConvertOut('�����'),
                    'name' => 'sum',
                    'align' => 'left',
                    'sortable' => false
                ),
                'id_user' => array(
                    'display' => VLJsonConvertOut('Id �������'),
                    'name' => 'id_user',
                    'align' => 'left',
                    'sortable' => false
                ),
                'phone' => array(
                    'display' => VLJsonConvertOut('������� �������'),
                    'name' => 'phone',
                    'align' => 'left',
                    'sortable' => false
                ),
                'fio' => array(
                    'display' => VLJsonConvertOut('��� �������'),
                    'name' => 'fio',
                    'align' => 'left',
                    'sortable' => false
                ),
                'email' => array(
                    'display' => VLJsonConvertOut('Email �������'),
                    'name' => 'email',
                    'align' => 'left',
                    'sortable' => false
                ),
                'city' => array(
                    'display' => VLJsonConvertOut('����� �������'),
                    'name' => 'city',
                    'align' => 'left',
                    'sortable' => false
                ),
                'source' => array(
                    'display' => VLJsonConvertOut('�������� ������ �� ����������'),
                    'name' => 'source',
                    'align' => 'left',
                    'sortable' => false
                ),
                'login' => array(
                    'display' => VLJsonConvertOut('����� ���������'),
                    'name' => 'login',
                    'align' => 'left',
                    'sortable' => false
                ),
                'status' => array(
                    'display' => VLJsonConvertOut('������ ������'),
                    'name' => 'status',
                    'align' => 'left',
                    'sortable' => false
                ),
                'DateSendCourier' => array(
                    'display' => VLJsonConvertOut('���� �������� � ��'),
                    'name' => 'DateSendCourier',
                    'align' => 'left',
                    'sortable' => false
                ),
                'DateDelivery' => array(
                    'display' => VLJsonConvertOut('���� ��������'),
                    'name' => 'DateDelivery',
                    'align' => 'left',
                    'sortable' => false
                ),
                'DateMoneyIncome' => array(
                    'display' => VLJsonConvertOut('���� ��������� �����'),
                    'name' => 'DateMoneyIncome',
                    'align' => 'left',
                    'sortable' => false
                ),
                'Sum' => array(
                    'display' => VLJsonConvertOut('��������� ��� ������'),
                    'name' => 'Sum',
                    'align' => 'left',
                    'sortable' => false
                ),
                'dSum' => array(
                    'display' => VLJsonConvertOut('��������� �� �������'),
                    'name' => 'dSum',
                    'align' => 'left',
                    'sortable' => false
                ),
                'logistic' => array(
                    'display' => VLJsonConvertOut('������ ��������� � ������'),
                    'name' => 'logistic',
                    'align' => 'left',
                    'sortable' => false
                ),
                'articul' => array(
                    'display' => VLJsonConvertOut('������� ������ � ������'),
                    'name' => 'articul',
                    'align' => 'left',
                    'sortable' => false
                ),
                'delivery_type' => array(
                    'display' => VLJsonConvertOut('��� ��������'),
                    'name' => 'delivery_type',
                    'align' => 'left',
                    'sortable' => false
                ),
                'delivery_cost_fact' => array(
                    'display' => VLJsonConvertOut('��������� ��������'),
                    'name' => 'delivery_cost_fact',
                    'align' => 'left',
                    'sortable' => false
                ),
                'payment_type' => array(
                    'display' => VLJsonConvertOut('��� ������'),
                    'name' => 'payment_type',
                    'align' => 'left',
                    'sortable' => false
                ),
                'title' => array(
                    'display' => VLJsonConvertOut('�������� ������ � ������'),
                    'name' => 'title',
                    'align' => 'left',
                    'sortable' => false
                ),
                'couriersId' => array(
                    'display' => VLJsonConvertOut('ID ���������� ������'),
                    'name' => 'couriersId',
                    'align' => 'left',
                    'sortable' => false
                ),
                'firm' => array(
                    'display' => VLJsonConvertOut('�����'),
                    'name' => 'firm',
                    'align' => 'left',
                    'sortable' => false
                ),
                'track' => array(
                    'display' => VLJsonConvertOut('����� �����������'),
                    'name' => 'track',
                    'align' => 'left',
                    'sortable' => false
                ),
                'deliveryTarif' => array(
                    'display' => VLJsonConvertOut('�����'),
                    'name' => 'deliveryTarif',
                    'align' => 'left',
                    'sortable' => false
                ),
                'deliveryIns' => array(
                    'display' => VLJsonConvertOut('���������'),
                    'name' => 'deliveryIns',
                    'align' => 'left',
                    'sortable' => false
                )
            );

            // ������������� ������ � �������
            $allowedKeys = array_unique($allowedKeys);

            // ���� �� ���� ��������� �������������� ����,
            // �� ������� � ������� �� ���� ������
            if (count($allowedKeys) === 7) {
                $output = null;
                // ����� ��� ���������� ������� ������ ����� ��������
            } else {
                foreach ($allowedKeys as $k) {
                    $output[] = $keysMatches[$k];
                }
            }



            echo Zend_Json::encode($output);
            exit();
        }

        // ������������ ���������
        $note = $_SESSION['orders_note'];
        $result = mysql_query(
            $select . $aSelect . $from . $join . " WHERE " . implode(" AND ", $aWhere) . $order . $limit
        );

        while ($content = mysql_fetch_assoc($result)) {

            if ($content['status'] == 4) {
                $status = '<span style="color:' . $statusSynonyms[$content['status'] . $content['is_sale']]["color"] . '"><strong>' . $statusSynonyms[$content['status'] . $content['is_sale']]["name"] . '</strong></span>';
            } else {
                $status = '<span style="color:' . $statusSynonyms[$content['status']]["color"] . '"><strong>' . $statusSynonyms[$content['status']]["name"] . '</strong></span>';
            }

            if (in_array( $content['id'], $note )) {
                $is_note = '<span style="color:green"><strong>� ��������</strong></span>';
            } else {
                $is_note = '';
            }

            $action_link = "<a class=\"print_action_link\" target=\"blank\" href=\"?main=print&module=mag&id_orders={$content['id']}&id_client={$content['id_user']}\"><img src=\"images/print.png\" border=0></a>";

            $price = "�����<br />";
            if ($content['dType'] == 1) {
                $price .= "������ (���)<br />";
            } elseif ($content['dType'] == 2) {
                $price .= "������ (���)<br />";
            } else {
                $price .= "---<br />";
            }
            $price .= "<strong>�����:</strong><br />";

            $big_sum = "&nbsp&nbsp<strong style='color:black'>{$content['Sum']}</strong><br />";
            $big_sum .= "&nbsp&nbsp<strong style='color:black'>{$content['dPercent']} %</strong><br />";

            if ($content['dPercent'] == 0) {
                $big_sum .= "&nbsp&nbsp<strong style='color:red'>{$content['Sum']}</strong><br />";
            } else {
                $big_sum .= "&nbsp&nbsp<strong style='color:red'>{$content['dSum']}</strong><br />";
            }

            if ($content['id_user'] != 0) {

                $client = mysql_fetch_array(mysql_query("SELECT * FROM m_mag_Users WHERE id={$content['id_user']} "));
                $client['fio'] = ($client['name'] != '' || $client['surname'] != '' || $client['patronymic'] != '') ? $client['surname'] . ' ' . $client['name'] . ' ' . $client['patronymic'] : $client['fio'];

                $client_info = "<span style='color:#000;'>";
                if ($client['region'] == 'region') {
                    $client_info .= "<strong>������</strong><br />";
                } else {
                    $client_info .= "<strong>������</strong><br />";
                }

                if (!empty($client['email'])) {
                    $client_info .= '<a onclick="javascript:openURL0(\'?main=mag&module=mag_client&action=edit&id_client=' . $client['id'] . '&is_menu=0\', 700, 650)" onmouseover="ddrivetip(\'������� �������� �������\')" onmouseout="hideddrivetip()" href="#">' . $client['email'] . '</a><br />';
                }
                if (!empty($client['fio'])) {
                    $client_info .= "{$client['fio']}<br />";
                }
                $client_info .= "</span>";

            } else {
                $client_info = "<span style='color:#777777;'>";
                if (!empty($content['pFio'])) {
                    $client_info .= "{$content['pFio']}<br />";
                }
                if (!empty($content['pEmail'])) {
                    $client_info .= "{$content['pEmail']}<br />";
                }
                $client_info .= "</span>";
            }

            $output = array(
                VLJsonConvertOut(
                    "<strong>{$content['id']}<br /><a href=\"?main=mag&module=mag_orders&action=edit&id_order={$content['id']}&status={$content['status']}\">�������</a></strong>"
                ),
                VLJsonConvertOut(rusdate($content['DateAdd'], 3) . "<br />{$status}<br />{$is_note}"),
                VLJsonConvertOut(rusdate($content['DateEnd'], 3)),
                VLJsonConvertOut($action_link),
                VLJsonConvertOut($client_info),
                VLJsonConvertOut($price),
                VLJsonConvertOut($big_sum)
            );

            // ���������� �������� ������
            $content = array_intersect_key($content, array_flip($allowedKeys));

            // ���������� �������������� �������� � ���������, �� �������������
            $i = 0;
            foreach ($allowedKeys as $v) {

                // ���������� ����������� �������
                if ($i < 7) {
                    ++$i;
                    continue;
                }

                $output[] = VLJsonConvertOut("<span>$content[$v]</span>");
            }

            $rows[] = array(
                'id' => $content['id'],
                'cell' => $output
            );
        }

        $result = mysql_query("SELECT " . $count . $from . $join . " WHERE " . implode(" AND ", $aWhere));
        $content = mysql_fetch_array($result);

        $output = array();
        $output['total'] = $content['cnt'];
        $output['page'] = $page;
        $output['rows'] = $rows;
        $output['select'] = VLJsonConvertOut(
            $select . $aSelect . $from . $join . " WHERE " . implode(" AND ", $aWhere) . $order . $limit
        );
        $output['countSelect'] = VLJsonConvertOut(
            "SELECT " . $count . $from . $join . " WHERE " . implode(" AND ", $aWhere)
        );

        echo Zend_Json::encode($output);
        die();
    }

    /* -=========================================
    SQL
    */
    $sql = "
		SELECT 
			$select
		FROM 
			$table
			$where 
			$sort
			$limit 
	";

    if ($status == 0) {

        $select .= ",
                         GROUP_CONCAT(`cause_name`) AS `cause`";

        $sql = "SELECT $select FROM $table
                LEFT JOIN `m_cancel_causes` ON  FIND_IN_SET(`m_cancel_causes`.`cause_id`, `m_mag_Orders`.`cancel_cause_id`)
                $where 
                GROUP BY `m_mag_Orders`.`id`
                $sort
		      	$limit";
    }


    $result = runSQL($sql);
    $total = countRec('m_mag_Orders.id', $table, $where);


    /* -=========================================
    DATA
    */
    $item['page'] = $page;
    $item['total'] = $total;
    $item['rows'] = array();
    $note = $_SESSION['orders_note'];

    while ($content = @mysql_fetch_array($result)) {

        if (in_array($content['id'], $note)) {
            $is_note = '<span style="color:green"><strong>� ��������</strong></span>';
        } else {
            $is_note = '';
        }

        //prefix/region
        $objOrder = new Order();
        $order = $objOrder->getById($content['id']);

        /* -=========================================
        CLIENT
        */
        if ($content['id_user'] != 0) {

            $client = mysql_fetch_array(mysql_query("SELECT * FROM m_mag_Users WHERE id={$content['id_user']} "));
            $client['fio'] = ($client['name'] != '' || $client['surname'] != '' || $client['patronymic'] != '') ? $client['surname'] . ' ' . $client['name'] . ' ' . $client['patronymic'] : $client['fio'];

            $client_info = "<span style='color:#000;'>";

            $client_info .= $order->isRegion() ? "<strong>������</strong><br />" : "<strong>������</strong><br />";

            if (!empty($client['email'])) {
                $client_info .= '<a onclick="javascript:openURL0(\'?main=mag&module=mag_client&action=edit&id_client=' . $client['id'] . '&is_menu=0\', 700, 650)" onmouseover="ddrivetip(\'������� �������� �������\')" onmouseout="hideddrivetip()" href="#">' . $client['email'] . '</a><br />';
            }

            if (!empty($client['fio'])) {
                $client_info .= "{$client['fio']}<br />";
            }

            /*if(!empty($client['phone']))
            $client_info .= "{$client['phone']}<br />";
            if(!empty($client['address']))
            $client_info .= "{$client['address']}<br />";
            if(!empty($content['pComment']))
            $client_info .= "����������: {$content['pComment']}";*/

            $client_info .= "</span>";

        } else {

            $client_info = "<span style='color:#777777;'>";

            if (!empty($content['pFio'])) {
                $client_info .= "{$content['pFio']}<br />";
            }
            if (!empty($content['pEmail'])) {
                $client_info .= "{$content['pEmail']}<br />";
            }

            /*if(!empty($content['pPhone']))
            $client_info .= "{$content['pPhone']}<br />";
            if(!empty($content['pAddress']))
            $client_info .= "{$content['pAddress']}<br />";
            if(!empty($content['pComment']))
            $client_info .= "����������: {$content['pComment']}";*/

            $client_info .= "</span>";

        }


        $content['prefix'] = $order->isRegion() ? "RE" : "ME";

        /* -=========================================
        ORDERS
        */
        $orders_id = explode(',', $content['id_order']);
        $orders_kol = explode(',', $content['kol']);

        //$title = "<span style='color:#457c2f;'>$title</span>";
        /*$q_sum = mysql_query("SELECT kol,sum,title,is_sale FROM m_mag_OrdersSum WHERE id_order='{$content['id']}' ");
        while ($c_sum = mysql_fetch_array($q_sum)){
            $price 	.= $c_sum['sum']. ' * ' .$c_sum['kol']. '<br>';
            $sum 		.= '='. $c_sum['sum'] * $c_sum['kol']. '<br>';
            $big_sum 	= $big_sum + $c_sum['sum'] * $c_sum['kol'];

            if($c_sum['is_sale']==1){
                $title 	.= $c_sum['title'] . ' <span style="color:red"><strong>(sale)</strong></span><br>';
            }else {
                $title 	.= $c_sum['title'] . '<br>';
            }

        }
        $sum = "<span style='color:#547587;'><strong>$sum</strong></span>";
        */

        /* -=========================================
        ������� PRICE
        */
        $big_sum = "&nbsp&nbsp<strong style='color:black'>{$content['Sum']}</strong><br />";
        $big_sum .= "&nbsp&nbsp<strong style='color:black'>{$content['dPercent']} %</strong><br />";

        if ($content['dPercent'] == 0) {
            $big_sum .= "&nbsp&nbsp<strong style='color:red'>{$content['Sum']}</strong><br />";
        } else {
            $big_sum .= "&nbsp&nbsp<strong style='color:red'>{$content['dSum']}</strong><br />";
        }


        /* -=========================================
        ������� SUM
        */
        //$price = "<span style='color:#547587;'><strong>$price</strong></span>";
        $price .= "�����<br />";
        if ($content['dType'] == 1) {
            $price .= "������ (���)<br />";
        } elseif ($content['dType'] == 2) {
            $price .= "������ (���)<br />";
        } else {
            $price .= "---<br />";
        }
        $price .= "<strong>�����:</strong><br />";


        /* -=========================================
        ACTION
        */
        $action_link = "<a class=\"print_action_link\" target=\"blank\" href=\"?main=print&module=mag&id_orders={$content['id']}&id_client={$content['id_user']}\"><img src=\"images/print.png\" border=0></a>";

        $cause = '';
        switch ($content['status']) {
            case 1:
                $status = '<span style="color:green"><strong>�����</strong></span>';
                break;
            case 2:
                $status = '<span style="color:blue"><strong>��������������</strong></span>';
                break;
            case 3:
                $status = '<span style="color:black"><strong>������������</strong></span>';
                break;
            case 4:
                if ($content['is_sale'] == 1) {
                    $status = '<span style="color:green"><strong>���������</strong></span>';
                } else {
                    $status = '<span style="color:orange"><strong>� ���������</strong></span>';
                }

                break;
            case 5:
                $status = '<span style="color:red"><strong>�� ������ (�����������)</strong></span>';
                break;
            case 6:
                $status = '<span style="color:red"><strong>�� ������ (��������)</strong></span>';
                break;
            case 0:
                $cause = str_replace(',', '<br />', $content['cause']);
                $cause .= ($content['cancel_cause'] == '' || $content['cancel_cause'] === '�� �������') ? '' : '<br />������: ' . $content['cancel_cause'];
                $status = '<span style="color:red"><strong>�����������</strong></span>';
                break;

            default:
                break;
        }


        $rows[] = array(
            'id' => $content['id'],
            'cell' => array(
                VLJsonConvertOut(
                    "<strong>{$content['prefix']}-{$content['id']}<br /><a href=\"?main=mag&module=mag_orders&action=edit&id_order={$content['id']}&status={$content['status']}\">�������</a></strong>"
                ),
                VLJsonConvertOut(rusdate($content['DateAdd'], 3) . "<br />{$status}<br />{$is_note}"),
                VLJsonConvertOut(rusdate($content['DateEnd'], 3)),
                VLJsonConvertOut($action_link),
                VLJsonConvertOut($client_info),
                /*VLJsonConvertOut($title),*/
                VLJsonConvertOut($price),
                VLJsonConvertOut($big_sum),
                VLJsonConvertOut($cause),
            )
        );

        // clear
        $title = '';
        $price = '';
        $sum = '';
        $big_sum = '';

    }
    $item['rows'] = $rows;
    $json = Zend_Json::encode($item);

    VLJsonPrint($json);
}