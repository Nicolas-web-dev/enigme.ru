<?php
if ($_SESSION['admin'] == 'allow'){

	$table = "m_mag_Users";

	// GET PARAM

	/* -=========================================
	PAGES
	*/
	$page = $_POST['page']; $rp = $_POST['rp'];
	if (!$page) $page = 1; if (!$rp) $rp = 10;
	$start = (($page-1) * $rp);
	$limit = "LIMIT $start, $rp";

	/* -=========================================
	SORT
	*/
	$sortname 			= $_POST['sortname'];
	$sortorder 			= $_POST['sortorder'];
	if (!$sortname) 	$sortname = 'name';
	if (!$sortorder) 	$sortorder = 'desc';

	$sort = "ORDER BY $sortname $sortorder";


	/* -=========================================
	FILTERS
	*/
	$query = $_POST['query'];
	$qtype = $_POST['qtype'];

	if ($query) {
		$where = "WHERE $qtype LIKE '%$query%' ";
	}


	/* -=========================================
	SQL
	*/
	$sql = "
		SELECT 
			*
		FROM
		$table 
		$where 
		$sort
		$limit 
	";

	$result 	= runSQL($sql);
	$total 		= countRec('id', $table, $where);


	/* -=========================================
	DATA
	*/
	$item['page'] = $page;
	$item['total'] = $total;
	$item['rows'] = array();

	while ($content = @mysql_fetch_array($result)) {

        $content['fio'] = ($content['name']!='' || $content['surname']!='' || $content['patronymic']!='')?$content['surname'].' '.$content['name'].' '.$content['patronymic']:$content['fio'];

		$title = "<a href=\"?main=mag&module=mag_client&action=edit&id_client={$content['id']}\">{$content['email']}</a>";
		$is_block = ($content['is_block'] == 0) ? '<img src=images/green.gif border=0>' : '<img src=images/red.gif border=0>';

		if(!empty($content['note'])){
			$note = explode(',', $content['note']);
			$note = count($note);
		}else{
			$note = 0;
		}

		$rows[] = array(
		'id' 	=> $content['id'],
		'cell' 	=> array(
		rusdate($content['date_register'],3),
		$content['id'],
		$title,
		VLJsonConvertOut($content['fio']),
		$content['d_sum_all'],
		$content['d_percent'],
		$content['d_sum_eco'],
		$note,
		rusdate($content['date_last_visit'],3),
		$is_block,
		)
		);

	}
	$item['rows'] = $rows;
	$json = Zend_Json::encode($item);

	VLJsonPrint($json);
}
?>