<?php
if ($_SESSION['admin'] == 'allow'){

    $data['email'] =$_POST['email'];
    $data['fio'] =$_POST['fio'];
    $data['phone'] = preg_replace('/[^0-9]/', '', $_POST['phone']);
    $data['phone2'] = preg_replace('/[^0-9]/', '', $_POST['phone2']);
    $data['date_birthday'] = $_POST['date_birthday'];
    $data['pol'] = $_POST['pol'];



    if ($_POST['fio_data']!=''){
        $data['fio_data'] = $_POST['fio_data'];
    }

    if ($_POST['address_data']!=''){
        $data['address_data'] = $_POST['address_data'];
    }

    $data['address_region'] = $_POST['region'];
    $data['address_part'] = $_POST['part'];
    $data['address_other'] = $_POST['other'];
    $data['address_city'] = $_POST['city'];
    $data['address_settlement'] = $_POST['settlement'];
    $data['address_street'] = $_POST['street'];
    $data['address_house'] = $_POST['house'];
    $data['address_corpus'] = $_POST['corpus'];
    $data['address_stroenie'] = $_POST['stroenie'];
    $data['address_floor'] = $_POST['floor'];
    $data['address_flat'] = $_POST['flat'];
    $data['address_domophone'] = $_POST['domophone'];
    $data['postcode'] = $_POST['regs_postcode'];
    $_POST['postcode'] = $_POST['regs_postcode'];
    $data['metro'] = $_POST['metro'];
    $data['region'] = (iconv('utf-8','cp1251',$data['address_region']) == '� ������') ? 'moscow' : 'region';

    //���������� ��� ������� ��������
    if ($data['fio_data']!=''){
        $dadata = json_decode(iconv('cp1251','utf-8',$data['fio_data']));
        $dadata->value = iconv('utf-8','cp1251',$dadata->value);
        if ($dadata->value==$data['fio']){
            if ($dadata->data->name!=null){
                $data['name'] = iconv('utf-8','cp1251',$dadata->data->name);
            }
            if ($dadata->data->patronymic!=null){
                $data['patronymic'] = iconv('utf-8','cp1251',$dadata->data->patronymic);
            }
            if ($dadata->data->surname!=null){
                $data['surname'] = iconv('utf-8','cp1251',$dadata->data->surname);
            }
            if ($dadata->data->gender!=null){
                if ($dadata->data->gender=='FEMALE'){
                    $data['pol'] = 'F';
                }
                if ($dadata->data->gender=='MALE'){
                    $data['pol'] = 'M';
                }
            }
        }
    }



    $id = $_POST['user_id'];
    $id_order = $_POST['id_order'];

    $user_before = $db->fetchRow($db->select()->from('m_mag_Users')->where('id=?',$id));

    //update
    if ($id){
        if ($data['email']==''){
            $data['email'] = $id.'@enigme.ru';
        }
        $update = $db->update('m_mag_Users',$data,'id ='.(int)$id);
    //add
    }else{

        $data['date_register'] = new Zend_Db_Expr('NOW()');
        $data['date_last_visit'] = new Zend_Db_Expr('NOW()');

        //enigme email
        if ($data['email']==''){
            $data['email'] = md5(time());
            $db->insert('m_mag_Users',$data);
            $id= $db->lastInsertId();
            //update enigme email
            $update = $db->update('m_mag_Users',array('email'=>$id.'@enigme.ru'),'id ='.(int)$id);

            //������� � ����� � json
            $_POST['email'] =$id.'@enigme.ru';
        }else{
            $db->insert('m_mag_Users',$data);
        }
    }

    $select = $db->select()->from('m_mag_Users')->where('id=?',$id);
    $user_after =  $db->fetchRow($select);




    header("Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT" );
    header("Cache-Control: no-cache, must-revalidate" );
    header("Pragma: no-cache" );
    header("Content-type: text/x-json");

    //����� � ��������
    if ($_POST['part']){
        foreach($capital_districts as $capital){
            foreach($capital as $key=>$item){
                if ($key==$_POST['part']){
                    $_POST['part_name'] = iconv('cp1251','utf-8',$item);
                }
            }
        }
    }else{
        $_POST['part_name'] = '';
    }

    //metro
    $q_metro = $db->fetchAll($db->select()->from('m_geo_metro'));
    foreach($q_metro as &$metro){
        $aMetro[$metro['id']] = array(
            'name' =>$metro['name'],
            'id' =>$metro['id'],
        );
    }

    if ($aMetro[$_POST['metro']]['name']!=null){
        $_POST['metro'] = $aMetro[$_POST['metro']]['name'];
    }else{
        $_POST['metro']='';
    }


    $_POST['user_id'] = $user_after['id'];
    $_POST['d_percent'] = $user_after['d_percent'];
    $_POST['d_sum_all'] = $user_after['d_sum_all'];



    //log
    $result = array_diff_assoc($user_after,$user_before);

    foreach($result as $key=>$diff){

        switch($key){
            case 'address_region':
                $aLog[] = array(
                    'action'=>'address_region',
                    'value_before'=>$user_before['address_region'],
                    'value_after'=>$user_after['address_region'],
                );
                break;
            case 'address_part':
                $aLog[] = array(
                    'action'=>'address_part',
                    'value_before'=>$user_before['address_part'],
                    'value_after'=>$user_after['address_part'],
                );
                break;
            case 'address_other':
                $aLog[] = array(
                    'action'=>'address_other',
                    'value_before'=>$user_before['address_other'],
                    'value_after'=>$user_after['address_other']
                );
                break;
            case 'address_city':
                $aLog[] = array(
                    'action'=>'address_city',
                    'value_before'=>$user_before['address_city'],
                    'value_after'=>$user_after['address_city'],
                );
                break;
            case 'address_settlement':
                $aLog[] = array(
                    'action'=>'address_settlement',
                    'value_before'=>$user_before['address_settlement'],
                    'value_after'=>$user_after['address_settlement'],
                );
                break;
            case 'address_street':
                $aLog[] = array(
                    'action'=>'address_street',
                    'value_before'=>$user_before['address_street'],
                    'value_after'=>$user_after['address_street'],
                );
                break;
            case 'address_house':
                $aLog[] = array(
                    'action'=>'address_house',
                    'value_before'=>$user_before['address_house'],
                    'value_after'=>$user_after['address_house'],
                );
                break;
            case 'address_corpus':
                $aLog[] = array(
                    'action'=>'address_corpus',
                    'value_before'=>$user_before['address_corpus'],
                    'value_after'=>$user_after['address_corpus'],
                );
                break;
            case 'address_stroenie':
                $aLog[] = array(
                    'action'=>'address_stroenie',
                    'value_before'=>$user_before['address_stroenie'],
                    'value_after'=>$user_after['address_stroenie'],
                );
                break;
            case 'address_floor':
                $aLog[] = array(
                    'action'=>'address_floor',
                    'value_before'=>$user_before['address_floor'],
                    'value_after'=>$user_after['address_floor'],
                );
                break;
            case 'address_flat':
                $aLog[] = array(
                    'action'=>'address_flat',
                    'value_before'=>$user_before['address_flat'],
                    'value_after'=>$user_after['address_flat'],
                );
                break;
            case 'address_domophone':
                $aLog[] = array(
                    'action'=>'address_domophone',
                    'value_before'=>$user_before['address_domophone'],
                    'value_after'=>$user_after['address_domophone'],
                );
                break;
            case 'postcode':
                $aLog[] = array(
                    'action'=>'postcode',
                    'value_before'=>$user_before['postcode'],
                    'value_after'=>$user_after['postcode'],
                );
                break;
            case 'fio':
                $aLog[] = array(
                    'action'=>'fio',
                    'value_before'=>$user_before['fio'],
                    'value_after'=>$user_after['fio'],
                );
                break;
            case 'email':
                $aLog[] = array(
                    'action'=>'email',
                    'value_before'=>$user_before['email'],
                    'value_after'=>$user_after['email'],
                );
                break;
            case 'phone':
                $aLog[] = array(
                    'action'=>'phone',
                    'value_before'=>$user_before['phone'],
                    'value_after'=>$user_after['phone'],
                );
                break;
            case 'phone2':
                $aLog[] = array(
                    'action'=>'phone2',
                    'value_before'=>$user_before['phone2'],
                    'value_after'=>$user_after['phone2'],
                );
                break;
            case 'pol':
                $aLog[] = array(
                    'action'=>'pol',
                    'value_before'=>$user_before['pol'],
                    'value_after'=>$user_after['pol'],
                );
                break;



        }
    }

    if (isset($id_order)){
        foreach($aLog as $log){
            $log['user_id'] = $_SESSION['user_id'];
            $log['pid'] = $id_order;
            $log['module_name'] = 'mag_orders';
            $log['date'] = new Zend_Db_Expr('NOW()');

            if (is_null($log['value_before'])){
                $log['value_before'] ='';
            }

            $db->insert('m_mag_Log',$log);
        }
    }





    $json = Zend_Json::encode($_POST);
    echo $json;
}
?>