<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/Order.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/app/User/Row/Item.php";

if ($_SESSION['admin'] == 'allow') {

    // �������� ���������� �������
    $sm = new Smarty_Admin($valinorConfig['interface.themes']);

    $status = intval($_GET['status']);
    $date = $_GET['date'];

    // items
    $items = rtrim($_POST['items'], ",");
    $items_array = explode(',', $items);

    if (isset($_GET['items']) && ($_GET['items'] != "," && $_GET['items'] != "")) {


    }

    $filename = "print_all.html";
    $filename_path = $_SERVER['DOCUMENT_ROOT'] . "/images/uploads/$filename";
    @unlink($filename_path);
    $order_items = null;
    $total = 0;

    $resourceOrders = new Order();
    $orders = $resourceOrders->getByArray($items_array);

    /**
     * @var $content Order_Row_Item
     */
    foreach($orders as $content) {

        $id_orders = $content->id;
        $id_client = $content->id_user;

        // �������
        $q_sum = mysql_query("SELECT * FROM m_mag_OrdersSum WHERE id_order='{$content->id}' ");
        $i = 0;
        while ($c_sum = mysql_fetch_array($q_sum)) {
            $item[] = array(
                'kol' => $c_sum['kol'],
                'sum' => $c_sum['sum'],
                'title' => $c_sum['title'],
                'bgcolor' => ($i % 2 == 0) ? '#ffffff' : '#f8f8f8',
            );
            $i++;
        }

        if ($id_client != 0) {
            $client = User_Row_Item::collectUserInfo($id_client, $content);
            $client['fio'] = $client['fullName'];
            $client['address'] = $client['fullAddress'];
        } else {
            $client['region'] = '';
            $client['fio'] = $content->pFio;
            $client['phone'] = User_Row_Item::formatPhone($content->pPhone);
            $client['address'] = $content->pAddress;
        }

        foreach ($item as $sum) {
            $total += $sum['sum'] * $sum['kol'];
        }

        $objOrder = $content;
        $content = $content->toArray();
        $content['totalCost'] = $objOrder->getTotal(true);
        $content['delivery_cost'] = $objOrder->getDeliveryCost();
        $content['incurance_cost'] = $objOrder->getInsuranceCost();
        $content['delivery_type'] = $objOrder->getDeliveryType();
        $content['delivery_metro'] = $objOrder->getDeliveryMetro();
        $content['courier_name'] = $objOrder->getCourier();
        $content['is_region'] = $objOrder->isRegion();
        $content['payment_type'] = $objOrder->getPaymentType();
        $content['is_self'] = $objOrder->isLogisticSelf();
        $content['delivery_code'] = $objOrder->getDeliveryCode();
        $content['id'] = $objOrder->getFullId();
        $content['deliveryDate'] = $objOrder->getDeliveryDate();

        $sm->assign('orders', $content);
        $sm->assign('total', $total);
        $sm->assign('item', $item);
        $sm->assign('client', $client);

        $order_items .= $sm->fetch("print_orders_all_item.tpl");
        $item = array();
    }

    $sm->assign('order_items', $order_items);

    file_put_contents($filename_path, $sm->fetch("print_orders_all.tpl"));

    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
    header("Content-type: text/x-json");

    //$item['query'] = $sql;
    $item['total'] = $total;

    $json = Zend_Json::encode($item);
    echo $json;
}