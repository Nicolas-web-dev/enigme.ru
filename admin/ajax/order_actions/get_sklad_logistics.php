<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/Order.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/app/User/Row/Item.php";

if ($_SESSION['admin'] == 'allow') {

    $items = rtrim($_POST['items'], ",");
    $items_array = explode(',', $items);


    $filename = "enigme-logistics.csv";
    $filename_path = $_SERVER["DOCUMENT_ROOT"] . "/images/uploads/$filename";
    @unlink($filename_path);
    $csv = new CCSVData();

    $csv->SaveFile(
        "$filename_path",
        array(
            '��� ������',
            '���� ��������',
            '����� (�)',
            '�����(��)',
            '����� ������',
            '����� �����������',
            '����� ����������',
            '����� � ������ ����������',
            '����������',
            '����������',
            '�������',
            '���',
            '�����',
            '��������� ������',
            '��������� ���������',
        )
    );

    $resourceOrders = new Order();
    $orders = $resourceOrders->getByArray($items_array);

    /**
     * @var $content Order_Row_Item
     */
    foreach ($orders as $content) {

        $c_user = User_Row_Item::collectUserInfo($content->id_user, $content);

        if ($content->isLogisticSelf()) {
            $nko = "�24��";
        } else {
            $nko = "24��";
        }

        $city = $c_user['city'];
        $address = preg_replace(
            '/ , /',
            ' ',
            trim(
                trim(
                    implode(
                        ', ',
                        array(
                            $c_user['address_street'],
                            $c_user['address_house'] == null ? "" : "� " . $c_user['address_house'],
                            $c_user['address_corpus'] == null ? "" : "���� " . $c_user['address_corpus'],
                            $c_user['address_stroenie'] == null ? "" : "��� " . $c_user['address_stroenie'],
                            $c_user['address_flat'] == null ? "" :"�� " . $c_user['address_flat']
                        )
                    ),
                    " "
                ),
                ","
            )
        );
        $fio = $c_user['fullName'];
        $phone = $c_user['phone'];
        $logistic = $content->getLogistic();
        $delivery_code = $content->getDeliveryCode();;

        $price = $content->getTotal(true);

        $csv->SaveFile(
            $filename_path,
            array(
                $nko,
                $content->DateEnd,
                "",
                "",
                $content->getFullId(),
                "������",
                preg_replace('/� /', '', $city),
                $address,
                $delivery_code,
                $fio,
                $phone,
                "0,9",
                "",
                $price,
                $price,
            )
        );
    }

    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
    header("Content-type: text/x-json");

    echo Zend_Json::encode(array('ok'));
}