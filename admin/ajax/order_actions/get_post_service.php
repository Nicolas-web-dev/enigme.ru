<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/Order.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/app/User/Row/Item.php";

if ($_SESSION['admin'] == 'allow') {

    $items = rtrim($_POST['items'], ",");
    $items_array = explode(',', $items);


    $filename = "enigme-postservice.csv";
    $filename_path = $_SERVER["DOCUMENT_ROOT"] . "/images/uploads/$filename";
    @unlink($filename_path);
    $csv = new CCSVData();

    $csv->SaveFile(
        "$filename_path",
        array(
            '��',
            '������',
            '����������',
            '',
            '������',
            '�����',
            '����� ����, ������ (���)',
            '�������',
            '����� ������',
            '���������',
            '��. �����',
            '�����������,
��, ����, ������ �����, � �.�.',
            '���� ��������',
            '�������� �������� c',
            '�������� �������� ��',
            '���',
            '��'
        )
    );

    $resourceOrders = new Order();
    $orders = $resourceOrders->getByArray($items_array);

    /**
     * @var $content Order_Row_Item
     */
    foreach ($orders as $ordernumber=>$content) {

        $c_user = User_Row_Item::collectUserInfo($content->id_user, $content);

        if ($content->isLogisticSelf()) {
            $nko = "�24��";
        } else {
            $nko = "24��";
        }

        $city = $c_user['city'];


        $phone = $c_user['phone'];
        $logistic = $content->getLogistic();
        $delivery_code = $content->getDeliveryCode();;

        $price = $content->getTotal(true);

        $aAddress = [];
        $aAddress[] = $c_user['address_city'];
        $aAddress[] = $c_user['address_settlement'];
        $aAddress[] = $c_user['address_street'];
        $aAddress = array_filter($aAddress);
        $sAddress = implode(',',$aAddress);

        $aAddress = [];
        $aAddress['address_house'] = $c_user['address_house'];
        $aAddress['address_corpus'] = $c_user['address_corpus'];
        $aAddress['address_stroenie'] = $c_user['address_stroenie'];
        $aAddress['address_floor'] = $c_user['address_floor'];
        $aAddress['address_domophone'] = $c_user['address_domophone'];
        $aAddress['address_flat'] = $c_user['address_flat'];

        $aAddress = array_filter($aAddress);
        $sAddress2 = '';
        foreach ($aAddress as $key=>&$address) {
            if ($key=='address_house'){
                $address = '��� '.$address;
            }
            if ($key=='address_corpus'){
                $address = '������ '.$address;
            }
            if ($key=='address_stroenie'){
                $address = '�������� '.$address;
            }
            if ($key=='address_floor'){
                $address = '���� '.$address;
            }
            if ($key=='address_domophone'){
                $address = '������� '.$address;
            }
            if ($key=='address_flat'){
                $address = '�������� '.$address;
            }

        }

        $sAddress2 = implode(',',$aAddress);




        $csv->SaveFile(
            $filename_path,
            array(
                $ordernumber+1,
                $nko,
                $c_user['fio'],
                "",
                $c_user['address_region'],
                $sAddress,
                $sAddress2,
                $phone,
                $price,
                $price,
                $content->getFullId(),
                $content->pComment,
                $content->DateEnd,
                10,
                18,
                '0,9'

            )
        );
    }

    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
    header("Content-type: text/x-json");

    echo Zend_Json::encode(array('ok'));
}