<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/Order.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/app/User/Row/Item.php";

if ($_SESSION['admin'] == 'allow') {

    $items = rtrim($_POST['items'], ",");
    $items_array = explode(',', $items);

    $filename = "enigme-ru-courier-2.xml";
    $filename_path = $_SERVER["DOCUMENT_ROOT"] . "/images/uploads/$filename";
    unlink($filename_path);

    $strAll = "<?xml version='1.0' encoding='UTF-8'?>\r\n";
    $strAll .= "<orders>\r\n";

    $resourceOrders = new Order();
    $orders = $resourceOrders->getByArray($items_array);

    /**
     * @var $content Order_Row_Item
     */
    foreach($orders as $content) {

        $kol = '';
        $item_names = '';

        $q_mod = mysql_query("SELECT * FROM m_mag_OrdersSum WHERE id_order='{$content->id}' ");
        while ($c_mod = mysql_fetch_assoc($q_mod)) {

            $order = mysql_fetch_array(
                mysql_query("SELECT * FROM m_catalog_data_order WHERE id='{$c_mod['id_catalog_data_order']}' ")
            );

            $title = format_text_out($c_mod['title']);

            $item_names .= "\t\t<item>\r\n";
            $item_names .= "\t\t<articul>{$c_mod['articul_catalog_data_order']}</articul>\r\n";
            $item_names .= "\t\t<name>$title</name>\r\n";
            $item_names .= "\t\t<count>{$c_mod['kol']}</count>\r\n";
            $item_names .= "\t\t<weight>0.1</weight>\r\n";
            $item_names .= "\t\t<summ>{$c_mod['sum']}</summ>\r\n";
            $item_names .= "\t\t</item>\r\n";

        }
        $item_names = str_replace('&', ' ', $item_names);

        $data = User_Row_Item::collectUserInfo($content->id_user, $content);
        $contact = "{$data['fullName']}, {$data['phone']}";
        $fio = $data['fullName'];
        $phone = $data['phone'];
        $address = $data['fullAddress'];
        $total = $content->getTotal(true);
        $id = $content->getFullId();

        $strAll .= "\t<order>\r\n";
        $strAll .= "\t\t<date>{$content->DateEnd}</date>\r\n";
        $strAll .= "\t\t<number>{$id}</number>\r\n";
        $strAll .= "\t\t<service>Msk</service>\r\n";
        $strAll .= "\t\t<order_type>�������� � ����� ����������</order_type>\r\n";
        $strAll .= "\t\t<order_subtype>����������</order_subtype>\r\n";
        $strAll .= "\t\t<dimensions>0.1:0.1:0.1</dimensions>\r\n";
        $strAll .= "\t\t<email></email>\r\n";
        $strAll .= "\t\t<storage_code>msk</storage_code>\r\n";
        $strAll .= "\t\t<items>{$item_names}</items>\r\n";

        $strAll .= "\t\t<weight>0.9</weight>\r\n";
        $strAll .= "\t\t<address>{$address}</address>\r\n";
        $strAll .= "\t\t<contact_info>{$contact}</contact_info>\r\n";
        $strAll .= "\t\t<fio>{$fio}</fio>\r\n";
        $strAll .= "\t\t<phone>{$phone}</phone>\r\n";
        $strAll .= "\t\t<summ>{$total}</summ>\r\n";
        $strAll .= "\t\t<delivery_type>��������</delivery_type>\r\n";

        $strAll .= "\t</order>\r\n";
    }

    $strAll .= "</orders>";
    $strAll = iconv('WINDOWS-1251', 'UTF-8', $strAll);

    if ($fp = fopen($filename_path, 'wb')) {
        fwrite($fp, $strAll);
        fclose($fp);
    }

    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
    header("Content-type: text/x-json");

    echo Zend_Json::encode(array('ok'));
}