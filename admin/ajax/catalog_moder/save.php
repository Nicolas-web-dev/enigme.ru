<?php
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");

header("Content-type: text/x-json;charset=cp1251");

/*
error_reporting(E_ALL);
ini_set("display_errors", 1);
*/


if ($_SESSION['admin'] == 'allow') {
    $id_cat = rtrim($_POST['id_cat']);
    if ($id_cat == 96) {
        $table_prefix = 'k_';
        $folder = 'catalog_k';
    } elseif ($id_cat == 82) {
        $table_prefix = 'm_';
        $folder = 'catalog';
    }

    $id_category = (int)$_POST['id_category'];
    if ($id_category == 0 && $id_cat != 82) $errors[] = iconv("WINDOWS-1251", "UTF-8", "���� ��������� �� �������\n");

    $id_brand = (int)$_POST['id_brand'];
    if ($id_brand == 0) $errors[] = iconv("WINDOWS-1251", "UTF-8", "���� ����� �� �������\n");

    $id_product = (int)$_POST['id_product'];
    //��������� ���� ��� ������ ��������
    if ($id_product == 0) {
        $title = $_POST['title'];
        if ($title == '') $errors[] = iconv("WINDOWS-1251", "UTF-8", "���� ��� �������� �� ���������\n");
        $url = $_POST['url'];
        if ($url == '') $errors[] = iconv("WINDOWS-1251", "UTF-8", "���� url �������� �� ���������\n");
    }
    $url = translit($_POST['url']);
    $pol = $_POST['pol'];
    $meta_title = $_POST['meta_title'];
    $meta_title_1 = $_POST['meta_title_1'];
    $meta_description = $_POST['meta_description'];
    $text = $_POST['text'];
    $sort = $_POST['sort'];

    //�� ������������ � �������
    $mod['sklad'] = (int)$_POST['sklad_id'];
    $mod['article_sklad'] = $_POST['article'];
    $mod['price_usd'] = $_POST['price_usd'];
    $mod['type'] = $_POST['type'];
    $mod['v'] = $_POST['v'];
    $mod['modification_id'] = $_POST['modification_id'];

    if ($mod['price_usd'] == '') $errors[] = iconv("WINDOWS-1251", "UTF-8", "���� '����' ����������� �� ���������\n");
    if ($mod['article_sklad'] == '') $errors[] = iconv("WINDOWS-1251", "UTF-8", "���� '�������' ��� ����������� �� ���������\n");
    if ($mod['v'] == '' && $mod['modification_id'] == '') $errors[] = iconv("WINDOWS-1251", "UTF-8", "���� '�����' ��� ����������� �� ���������\n");

    /*
    if ($id_product>0){
        $sql = "SELECT * FROM {$table_prefix}catalog_data WHERE id=$id_product";
        $product = mysql_fetch_assoc(mysql_query($sql));

        if ($product['img']=='' && $_POST['image_path']==''){
            $errors[] = iconv("WINDOWS-1251", "UTF-8", "���������� �� ���������\n");
        }
    }else{
        if ($_POST['image_path']==''){
            $errors[] = iconv("WINDOWS-1251", "UTF-8", "���������� �� ���������\n");
        }

    }
    */
    if ($id_product>0){
        //�������� �� ������������� ����������� � ������ �������
        $sql = "SELECT * FROM {$table_prefix}catalog_data_order WHERE type='{$mod['type']}' AND v='{$mod['v'] }' AND id_catalog_data={$id_product}";
        if ($row = mysql_fetch_assoc(mysql_query($sql))){
            $errors[] = iconv("WINDOWS-1251", "UTF-8", "��� ���������� ����������� � ������������ ����� � ������\n");
        }
    }



    /**
     * Save
     */
    if (count($errors) == 0) {
        /**
         * Product is new
         */
        if ($id_product == 0) {

            //����������� �������
            $sql = "(SELECT `articul` FROM {$table_prefix}catalog) UNION ALL
                    (SELECT `articul` FROM {$table_prefix}catalog_data) UNION ALL
                    (SELECT `articul` FROM {$table_prefix}catalog_data_order) ORDER BY articul DESC LIMIT 1";
            $articul = mysql_fetch_assoc(mysql_query($sql));
            $articul_number = str_replace('PF', '', $articul['articul']);

            $new_articul = 'PF' . str_pad($articul_number + 1, 7, '0', STR_PAD_LEFT);


            $sql = "INSERT INTO {$table_prefix}catalog_data
                (`id_cat`, `id_catalog`, `id_type`,
                 `articul`, `title`, `title_1`, `text`,
                 `img`, `alt`, `registerDate`, `is_block`,
                 `is_main`, `is_subs`, `is_sale`, `sort`,
                  `url`, `tag`, `tagl`,
                 `pol`, `meta_title`, `meta_title_1`,`meta_description`
                 ) VALUES (
                 '{$id_cat}','{$id_brand}','{$id_category}',
                 '{$new_articul}','{$title}','{$title_1}','{$text}',
                 '{$img}','{$alt}',NOW(),'{$is_block}',
                 '{$is_main}','{$is_subs}','{$is_sale}','{$sort}',
                 '{$url}','{$tag}','{$tag_l}',
                 '{$pol}','{$meta_title}','{$meta_title_1}','{$meta_description}'
                 ) ";
            $sql = iconv('utf-8', 'windows-1251', $sql);
            mysql_query($sql);
            //���� �����������
            if (mysql_affected_rows() > 0) {
                $log_title = iconv('utf-8', 'windows-1251',$title);
                //$aLog[] = "�������� ������: ".$log_title.",���: $pol ";
                $aLogData[] = array(
                    'action'=>'�������� ������',
                    'articul'=>'',
                    'sklad'=>0,
                    'articul_sklad'=>'',
                    'price'=>'',
                    'type'=>'',
                    'volume'=>'',
                    'text'=>"�������� ������: ".$log_title.",���: $pol "
                );

                $id = DGetLast();
                $id_product = $id;
                //��������� �����������
                $price_cell = 'price_usd' . $mod['sklad'];
                if ($mod['sklad'] > 1) {
                    $article_cell = 'article' . $mod['sklad'];
                } else {
                    $article_cell = 'article';
                }

                $sql = "(SELECT `articul` FROM {$table_prefix}catalog) UNION ALL
                    (SELECT `articul` FROM {$table_prefix}catalog_data) UNION ALL
                    (SELECT `articul` FROM {$table_prefix}catalog_data_order) ORDER BY articul DESC LIMIT 1";
                $articul = mysql_fetch_assoc(mysql_query($sql));
                $articul_number = str_replace('PF', '', $articul['articul']);
                $new_articul = 'PF' . str_pad($articul_number + 1, 7, '0', STR_PAD_LEFT);

                $sql = "INSERT INTO {$table_prefix}catalog_data_order
                (`id_catalog_data`,`articul`,`{$article_cell}`,`{$price_cell}`,`type`,`v`) VALUES (
                '{$id}','{$new_articul}','{$mod['article_sklad']}','{$mod['price_usd']}','{$mod['type']}','{$mod['v']}'
                )";
                $sql = iconv('utf-8', 'windows-1251', $sql);
                mysql_query($sql);

                $log_article_sklad = iconv('utf-8', 'windows-1251',$mod['article_sklad']);
                $log_type = (isset($aroma_type[$mod['type']]))?$aroma_type[$mod['type']]:'��� ���������';
                $log_v = iconv('utf-8', 'windows-1251',$mod['v']);
                //$aLog[] = "��������� �����������: �������: ".$new_articul.",������� �� ������ � {$mod['sklad']}: $log_article_sklad, �����: {$mod['price_usd']}, ���: $log_type, �����:$log_v  ";

                $aLogData[] = array(
                    'action'=>'��������� �����������',
                    'articul'=>$new_articul,
                    'sklad'=>$mod['sklad'],
                    'articul_sklad'=>$log_article_sklad,
                    'price'=>$mod['price_usd'],
                    'type'=>$log_type,
                    'volume'=>$log_v,
                    'text'=>"��������� �����������: �������: ".$new_articul.",������� �� ������ � {$mod['sklad']}: $log_article_sklad, �����: {$mod['price_usd']}, ���: $log_type, �����:$log_v  "
                );
            }
            //edit isset product
        } elseif ($id_product > 0) {
            //get data from product
            //��������� �������� ���� ����
            /*
            if ($_POST['image_path'] != '') {
                $image_path = $_POST['image_path'];

                // ������������ ����������
                $path = $_SERVER['DOCUMENT_ROOT'] . '/images/';
                $mkdir = 'uploads/' . $folder . '/' . $id_product;
                $mkdir_trumbs = $mkdir . '/trumbs/';
                $mkdir_small = $mkdir . '/small/';
                $mkdir_big = $mkdir . '/big/';

                if (file_exists($path . $mkdir)) {
                    mkdir($path . $mkdir);
                    mkdir($path . $mkdir_trumbs);
                    mkdir($path . $mkdir_small);
                    mkdir($path . $mkdir_big);
                }

                $time = time();
                $img = upload_image($_SERVER['DOCUMENT_ROOT'] . $image_path, $time, $path . $mkdir_small, $valinorConfig['catalog.small.w'], $valinorConfig['catalog.small.w']);
                upload_image($_SERVER['DOCUMENT_ROOT'] . $image_path, $time, $path . $mkdir_big, $valinorConfig['catalog.big.w'], $valinorConfig['catalog.big.w']);

                $sql = "UPDATE {$table_prefix}catalog_data SET `img`='{$img}' WHERE `id`={$id_product}";
                $sql = iconv('utf-8', 'windows-1251', $sql);
                mysql_query($sql);
            }
            */
            //work only with modifications
            $price_cell = 'price_usd' . $mod['sklad'];
            if ($mod['sklad'] > 1) {
                $article_cell = 'article' . $mod['sklad'];
            } else {
                $article_cell = 'article';
            }

            //update isset
            if (isset($mod['modification_id'])) {
                $sql = "UPDATE {$table_prefix}catalog_data_order SET `$article_cell`='{$mod['article_sklad']}', `$price_cell`='{$mod['price_usd']}' WHERE `id`={$mod['modification_id']}";
                $sql = iconv('utf-8', 'windows-1251', $sql);
                mysql_query($sql);

                //fetch modification
                $sql = mysql_query("SELECT * FROM {$table_prefix}catalog_data_order WHERE `id`={$mod['modification_id']}");
                $fetchedMod = mysql_fetch_assoc($sql);


                $log_article_sklad = iconv('utf-8', 'windows-1251',$mod['article_sklad']);
                $log_type = (isset($aroma_type[$fetchedMod['type']]))?$aroma_type[$fetchedMod['type']]:'��� ���������';
                $log_v = $fetchedMod['v'];
                $aLog[] = "��������� �����������: �������: ".$fetchedMod['articul'].",������� �� ������ � {$mod['sklad']}: $log_article_sklad, �����: {$mod['price_usd']}, ���: $log_type, �����:$log_v  ";
                $aLogData[] = array(
                    'action'=>'��������� �����������',
                    'articul'=>$fetchedMod['articul'],
                    'sklad'=>$mod['sklad'],
                    'articul_sklad'=>$log_article_sklad,
                    'price'=>$mod['price_usd'],
                    'type'=>$log_type,
                    'volume'=>$log_v,
                    'text'=>"��������� �����������: �������: ".$fetchedMod['articul'].",������� �� ������ � {$mod['sklad']}: $log_article_sklad, �����: {$mod['price_usd']}, ���: $log_type, �����:$log_v  "
                );



                //add new
            } else {
                //generate new article for modification
                //����������� �������
                $sql = "(SELECT `articul` FROM {$table_prefix}catalog) UNION ALL
                    (SELECT `articul` FROM {$table_prefix}catalog_data) UNION ALL
                    (SELECT `articul` FROM {$table_prefix}catalog_data_order) ORDER BY articul DESC LIMIT 1";
                $articul = mysql_fetch_assoc(mysql_query($sql));
                $articul_number = str_replace('PF', '', $articul['articul']);
                $new_articul = 'PF' . str_pad($articul_number + 1, 7, '0', STR_PAD_LEFT);

                $sql = "INSERT INTO {$table_prefix}catalog_data_order
                (`id_catalog_data`,`articul`,`{$article_cell}`,`{$price_cell}`,`type`,`v`) VALUES (
                '{$id_product}','{$new_articul}','{$mod['article_sklad']}','{$mod['price_usd']}','{$mod['type']}','{$mod['v']}'
                )";

                $sql = iconv('utf-8', 'windows-1251', $sql);
                mysql_query($sql);

                //�������� �������
                $log_article_sklad = iconv('utf-8', 'windows-1251',$mod['article_sklad']);
                $log_type = (isset($aroma_type[$mod['type']]))?$aroma_type[$mod['type']]:'��� ���������';
                $log_v = iconv('utf-8', 'windows-1251', $mod['v']);
                $aLogData[] = array(
                  'action'=>'��������� �����������',
                  'articul'=>$new_articul,
                  'sklad'=>$mod['sklad'],
                  'articul_sklad'=>$log_article_sklad,
                  'price'=>$mod['price_usd'],
                  'type'=>$log_type,
                  'volume'=>$log_v,
                  'text'=>"��������� �����������: �������: ".$new_articul.",������� �� ������ � {$mod['sklad']}: $log_article_sklad, �����: {$mod['price_usd']}, ���: $log_type, �����:$log_v  "
                );




                $aLog[] = "��������� �����������: �������: ".$new_articul.",������� �� ������ � {$mod['sklad']}: $log_article_sklad, �����: {$mod['price_usd']}, ���: $log_type, �����:$log_v  ";


            }
        }

        $aCategory_opt = explode(',',$_POST['category_opt']);


        if (count($aCategory_opt)>0){
            $sql= "DELETE FROM k_catalog_option WHERE `product_id`={$id_product}";
            mysql_query($sql);
            foreach($aCategory_opt as $option){
                if ($option!=''){
                    $sql = "INSERT INTO k_catalog_option (`product_id`,`option_id`) VALUES ($id_product,$option)";
                    mysql_query($sql);
                }

            }
        }

        $id_moder = (int)$_POST['id_moder'];

        $sql = "UPDATE m_catalog_moder SET `block`=1, `child`=$id_product, `id_cat`=$id_cat WHERE id=$id_moder";
        mysql_query($sql);


        //�����������
        $source = mysql_result(mysql_query("SELECT name FROM `m_catalog_moder` WHERE id=$id_moder"),0,0);


        $user_id = $_SESSION['user_id'];
        $target_id = $id_product;
        $id_cat = $id_cat;
        /*
        foreach($aLog as $text){
            //�������� �������
            $text = mysql_real_escape_string($text);
            $sql = "INSERT INTO `m_catalog_history` (`user_id`,`target_id`,`text`,`id_cat`,`source`) VALUES ($user_id,$id_product,'{$text}',$id_cat,'{$source}')";
            mysql_query($sql);
        }
        */

        foreach($aLogData as $data){
            //�������� �������
            $text = mysql_real_escape_string($data['text']);
            $sql = "INSERT INTO `m_catalog_history` (
                `user_id`,
                `target_id`,
                `action`,
                `articul`,
                `articul_sklad`,
                `sklad`,
                `price`,
                `type`,
                `volume`,
                `text`,
                `id_cat`,
                `source`)
                VALUES (
                $user_id,
                $id_product,
                '{$data['action']}',
                '{$data['articul']}',
                '{$data['articul_sklad']}',
                '{$data['sklad']}',
                '{$data['price']}',
                '{$data['type']}',
                '{$data['volume']}',
                '{$text}',
                 $id_cat,
                '{$source}')";
            mysql_query($sql);
        }


        echo $json = Zend_Json::encode(array('complete' => $id_product));


    } else {
        echo $json = Zend_Json::encode(array('errors' => implode('', $errors)));
    }

    //�������� ������
}
?>