<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/app/Order.php";
if ($_SESSION['admin'] == 'allow'){

	$table = "m_mag_Orders";

	/* -=========================================
	FILTERS
	*/
	$note = $_SESSION['orders_note'];
	if(count($note)!=0){
		foreach ($note as $i) {
			$note_item .= $i . ',';
		}
		$note_item = rtrim($note_item, ',');
		$where = "WHERE id IN ($note_item) ";

		/* -=========================================
		SQL
		*/
		$sql = "SELECT *
			FROM 
				$table 
				$where 
			ORDER BY DateAdd DESC";
		$result 	= runSQL($sql);
		$total 	= countRec('id', $table, $where);

	}else{
		$total = 0;
	}


	/* -=========================================
	DATA
	*/
	$item['total'] = $total;
	$item['rows'] = array();

	while ($content = @mysql_fetch_array($result)) {


		/* -=========================================
		CLIENT
		*/
		if ($content['id_user']!=0){

			$client = mysql_fetch_array(mysql_query("SELECT * FROM m_mag_Users WHERE id={$content['id_user']} "));

			$client_info  = "<span style='color:#000;'>";

			if($client['region']=='region'){
				$client_info .= "<strong>������</strong><br />";
			}else{
				$client_info .= "<strong>������</strong><br />";
			}
			
			if(!empty($client['email']))
			$client_info .= '<a onclick="javascript:openURL0(\'?main=mag&module=mag_client&action=edit&id_client='.$client['id'].'&is_menu=0\', 700, 650)" onmouseover="ddrivetip(\'������� �������� �������\')" onmouseout="hideddrivetip()" href="#">'.$client['email'].'</a><br />';

			if(!empty($client['fio'])) $client_info .= "{$client['fio']}<br />";
			
			$client_info .= "</span>";

		}else{

			$client_info  = "<span style='color:#777777;'>";

			if(!empty($content['pFio'])) $client_info .= "{$content['pFio']}<br />";
			if(!empty($content['pEmail'])) $client_info .= "{$content['pEmail']}<br />";
			
			$client_info .= "</span>";

		}

        //prefix
        $objOrder = new Order();
        $order = $objOrder->getById($content['id']);

        $content['prefix'] = $order->isRegion() ? "RE" : "ME";

        /* -=========================================
        ORDERS
        */
		$orders_id 	= explode(',', $content['id_order']);
		$orders_kol = explode(',', $content['kol']);

		/*$title = "<span style='color:#457c2f;'>$title</span>";
		$q_sum = mysql_query("SELECT kol,sum,title FROM m_mag_OrdersSum WHERE id_order='{$content['id']}' ");
		while ($c_sum = mysql_fetch_array($q_sum)){
			$price 		.= $c_sum['sum']. ' * ' .$c_sum['kol']. '<br>';
			$sum 		.= '='. $c_sum['sum'] * $c_sum['kol']. '<br>';
			$big_sum 	= $big_sum + $c_sum['sum'] * $c_sum['kol'];
			$title 		.= $c_sum['title'] . '<br>';
		}
		$sum = "<span style='color:#547587;'><strong>$sum</strong></span>";*/
		
		/* -=========================================
		������� PRICE
		*/
		$big_sum = "&nbsp&nbsp<strong style='color:black'>{$content['Sum']}</strong><br />";
		$big_sum .= "&nbsp&nbsp<strong style='color:black'>{$content['dPercent']} %</strong><br />";

		if($content['dPercent']==0){
			$big_sum .= "&nbsp&nbsp<strong style='color:red'>{$content['Sum']}</strong><br />";
		}else {
			$big_sum .= "&nbsp&nbsp<strong style='color:red'>{$content['dSum']}</strong><br />";
		}


		/* -=========================================
		������� SUM
		*/
		$price .= "�����<br />";
		if( $content['dType']==1 ){
			$price .= "������ (���)<br />";
		}elseif ( $content['dType']==2 ){
			$price .= "������ (���)<br />";
		}else{
			$price .= "---<br />";
		}
		$price .= "<strong>�����:</strong><br />";

		/* -=========================================
		ACTION
		*/
		$action_link = "<a class=\"print_action_link\" target=\"blank\" href=\"?main=print&module=mag&id_orders={$content['id']}&id_client={$content['id_user']}\"><img src=\"images/print.png\" border=0></a>";
		
		switch ($content['status']) {
			case 1:
				$status = '<span style="color:green"><strong>�����</strong></span>';
				break;
			case 2:
				$status = '<span style="color:blue"><strong>��������������</strong></span>';
				break;
			case 3:
				$status = '<span style="color:black"><strong>������������</strong></span>';
				break;
			case 0:
				$status = '<span style="color:red"><strong>�����������</strong></span>';
				break;
		
			default:
				break;
		}

		$rows[] = array(
		'id' 	=> $content['id'],
		'cell' 	=> array(
		VLJsonConvertOut("<strong>{$content['prefix']}-{$content['id']}<br /><a href=\"?main=mag&module=mag_orders&action=edit&id_order={$content['id']}&status={$content['status']}\">�������</a></strong>"),
		VLJsonConvertOut(rusdate($content['DateAdd'],3) . "<br />{$status}<br />{$is_note}"),
		VLJsonConvertOut(rusdate($content['DateEnd'],3)),
		VLJsonConvertOut($action_link),
		VLJsonConvertOut($client_info),
		VLJsonConvertOut($price),
		VLJsonConvertOut($big_sum),
		)
		);

		// clear
		$title = '';
		$price = '';
		$sum = '';
		$big_sum = '';

	}
	$item['rows'] = $rows;
	$json = Zend_Json::encode($item);

	VLJsonPrint($json);
}
?>