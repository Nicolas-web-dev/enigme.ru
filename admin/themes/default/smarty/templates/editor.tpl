{literal}
<!-- tinyMCE -->
<script language="javascript" type="text/javascript" src="tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	editor_deselector : "NoEditor",
	extended_valid_elements : "iframe[src|width|height|name|align|frameborder],script[src|type]",
	relative_urls : false,
	language : "ru",
	plugins : "safari,style,table,advhr,advimage,advlink,inlinepopups,media,contextmenu,paste,noneditable,nonbreaking,xhtmlxtras,template,pagebreak,filemanager,fullscreen",
	theme_advanced_buttons1_add : "fontselect,fontsizeselect",
	theme_advanced_buttons2_add : "separator,media,advhr,ltr,rtl,separator,forecolor,backcolor",
	theme_advanced_buttons2_add_before: "cut,copy,pastetext",
	theme_advanced_buttons2_add_after : "separator,media,advhr,ltr,rtl,separator,forecolor,backcolor",
	theme_advanced_buttons3_add_before : "fullscreen,separator,tablecontrols",
	theme_advanced_buttons3_add : "",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resize_horizontal : false,
	theme_advanced_resizing : true,
	apply_source_formatting : true,
	force_br_newlines : false
});
function toggleEditor(id) {
	if (!tinyMCE.get(id))
	tinyMCE.execCommand('mceAddControl', false, id);
	else
	tinyMCE.execCommand('mceRemoveControl', false, id);
}
</script>


<!-- /tinyMCE -->
{/literal}