 {include file="editor.tpl"}

{strip}


<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=edit&id_category={$id}" method="post" enctype="multipart/form-data" name="form">

<div id="tabs" class="flora">
	<ul class="ui-tabs-nav">
		<li class="ui-tabs-selected"><a id="tab1" href="#tab-1"><span>�������� ������</span></a></li>
		<li class=""><a id="tab3" href="#tab-3"><span>��������� ������</span></a></li>
		<li class=""><a id="tab4" href="#tab-4"><span>��������� ����������</span></a></li>
		<li class=""><a id="tab5" href="#tab-5"><span>����</span></a></li>
		<li class=""><a id="tab6" href="#tab-6"><span>������</span></a></li>
	</ul>
</div>

<div style="display: block;" class="ui-tabs-panel" id="tab-1">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	
	
	<tr>
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;�������� ��������� :</td>
		<td align="left" width="80%">
			<input type="text" name="name" value="{$name}" style="width:100%" onkeyup="seo_url_convert();" onchange="seo_url_convert();">
		</td>
	</tr>
		
	<tr>
		<td align="left">
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" style="border:0px;"><span style="color:red;">*</span>&nbsp;SEO URL:</td>
					<td align="right" style="border:0px;">
					<a href="#" 
					onmouseover="ddrivetip('��������!��� ������ �������������� URL, �� ������ ���� ������������. ����������� ������������ � �������� ����������� ������ -', 200)" onmouseout="hideddrivetip()"
					>
					<img src="images/warnings.png" width="16" height="16" border="0"></a>
					</td>
				</tr>
			</table>
		</td>
		<td align="left">
			<input type="text" name="seo_url" value="{$seo_url}" style="width:100%;">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;HTML-��� H1:</td>
		<td align="left">
			<input type="text" name="seo_h1" value="{$seo_h1}" style="width:100%">
		</td>
	</tr>
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;���� ����
		</td>
	</tr>
	
	
	<tr>
		<td align="left">&nbsp;HTML-��� Title:</td>
		<td align="left">
			<input type="text" name="meta_title" value="{$meta_title}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;����-��� Keywords:</td>
		<td align="left">
			<input type="text" name="meta_keywords" value="{$meta_keywords}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;����-��� Description:</td>
		<td align="left">
			<input type="text" name="meta_description" value="{$meta_description}" style="width:100%" >
		</td>
	</tr>
	
	
</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-3">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;�������� ���������:
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<textarea id="text" name="description" style="width:100%" rows=20>{$description}</textarea>
		</td>
	</tr>
	
</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-4">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">

	<tr>
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;������� ����������:</td>
		<td align="left" width="80%">
			<input type="text" name="sort_order" value="{$sort_order}" style="width:50px">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;������ (�������� / ���������):</td>
		<td align="left">
			<input type="checkbox" name="active" value="" {$active}>
		</td>
	</tr>

</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-5">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr>
		<td align="left" width="20%">&nbsp;&nbsp;������:</td>
		<td align="left" width="80%">
			<img id="cat_img" src="{$img_src}" align="left" style="margin:10px 10px;" alt="" />
			<input type="file" name="img" style="width:100%;" value="">
			<input id="del_img" type="button" name="del_img" style="width:150px; margin-top: 5px; margin-bottom: 5px;" value="������� �����������">
			<input id="img_old" type="hidden" name="img_old" value="{$img}">
		</td>
	</tr>
	
	<tr>
		<td align="left" >&nbsp;&nbsp;�������� ���������� (ALT):</td>
		<td align="left" >
			<input type="text" name="alt_img" style="width:100%" value="{$alt_img}">
		</td>
	</tr>
</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-6">

<table width="100%" border="0" cellpadding="1" cellspacing="1" class="form">
	<!-- ��������� ������� ������ -->
	<tr align="center" class="table_header">
		<td width="3%" align="center" height="25px"></td>
      		<td width="3%"><div align="center"><b>N</b></div></td>
			<td width="5%"><div align="center"><b>ID</b></div></td>
      		<td width="15%" align="left"><strong>&nbsp;�����</strong></td>
      		<td width="3%" align="center"><strong>���</strong></td>
		
		<td width="" align="left"><strong>&nbsp;���������</strong></td>
		
		<td width="10%" align="right"><b>����</b>&nbsp;</td>
		<td width="3%" align="center"><b>A</b></td>
		
	</tr>
	<!-- ��������� ������� ������ -->
 
	<!-- ����� ������ -->
	{section name=i loop=$item}
	
	<tr style="background-color:{$item[i].bgcolor_prod};">
		<td align="center" height="22px"><input class="id_check" type="checkbox" name="id[]" value="{$item[i].id_prod}" {$item[i].check_product}></td>
		<td align="center">{$item[i].n_prod}.</td>
		<td align="center">{$item[i].id_prod}</td>
		<td align="left">&nbsp;{$item[i].catalog_name_prod}</td>
		<td align="center">{$item[i].pol_prod}</td>
		
		<td align="left">
			&nbsp;{$item[i].title_prod}
		</td>
		
		<td align="right">{$item[i].price_prod}</td>
		<td align="center"><img src="images/{$item[i].is_block_prod}" width="14" height="14" border="0"></td>
		
	</tr>
	{/section}
	<!-- ------------ -->
</table>
{include file="table_footer_category.tpl"}
</div>

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_edit">
	<tr class="table_bg_action">
	<td align="left" colspan="2">
		<input type="submit" name="save" value="���������" style="width:100px">&nbsp;
	</td>
	</tr>
</table>
<input id="tab_page" type="hidden" value="{$tab_category_edit}">
</form>


{/strip} 

{literal}
<script type="text/javascript">
$(document).ready(function(){
	if ($('#tab_page').val() == 6) {
		setTimeout(function(){
			$('#tab6').trigger('click');
			}, 10);
	}	
	
	$('.id_check').change(function(event){
		var id_prod = 0;
		var check_prod = 0;
		
		if ($(this).is(':checked')) {

			id_prod = $(this).val();
			check_prod = 1;
		}
		else {
			id_prod = $(this).val();
			check_prod = 0;
		}
		
		$.ajax({
			type: "POST",
			url: "/admin/modules/category/category.php?ajax_check=1",
			data: "id_prod_check=" + id_prod + "&check_prod=" + check_prod,
		}).done(function( msg ) {

		});
	});
	
	
	$('#del_img').click(function(event){
		$('#img_old').val('');
		$("#cat_img").attr("src","");
		$('#del_img').hide();
	});
	
	if ($('#img_old').val() == '') {
		$('#del_img').hide();
	}
	else {
		$('#del_img').show();
	}
	
});
</script>
{/literal}