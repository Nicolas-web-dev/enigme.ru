{include file="editor.tpl"}
{strip}

<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=add" method="post" enctype="multipart/form-data" name="form">

<div id="tabs" class="flora">
	<ul class="ui-tabs-nav">
		<li class="ui-tabs-selected"><a href="#tab-1"><span>�������� ������</span></a></li>
		<li class=""><a href="#tab-3"><span>��������� ������</span></a></li>
		<li class=""><a href="#tab-4"><span>��������� ����������</span></a></li>
		<li class=""><a href="#tab-5"><span>����</span></a></li>
	</ul>
</div>

<div style="display: block;" class="ui-tabs-panel" id="tab-1">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	
	
	<tr>
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;�������� ��������� :</td>
		<td align="left" width="80%">
			<input type="text" name="name" value="" style="width:100%" onkeyup="seo_url_convert();" onchange="seo_url_convert();">
		</td>
	</tr>
		
	<tr>
		<td align="left">
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" style="border:0px;"><span style="color:red;">*</span>&nbsp;SEO URL:</td>
					<td align="right" style="border:0px;">
					<a href="#" 
					onmouseover="ddrivetip('��������!��� ������ �������������� URL, �� ������ ���� ������������. ����������� ������������ � �������� ����������� ������ -', 200)" onmouseout="hideddrivetip()"
					>
					<img src="images/warnings.png" width="16" height="16" border="0"></a>
					</td>
				</tr>
			</table>
		</td>
		<td align="left">
			<input type="text" name="seo_url" value="" style="width:100%;">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;HTML-��� H1:</td>
		<td align="left">
			<input type="text" name="seo_h1" value="{$seo_h1}" style="width:100%">
		</td>
	</tr>
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;���� ����
		</td>
	</tr>
	
	
	<tr>
		<td align="left">&nbsp;HTML-��� Title:</td>
		<td align="left">
			<input type="text" name="meta_title" value="{$meta_title}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;����-��� Keywords:</td>
		<td align="left">
			<input type="text" name="meta_keywords" value="{$meta_keywords}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;����-��� Description:</td>
		<td align="left">
			<input type="text" name="meta_description" value="{$meta_description}" style="width:100%" >
		</td>
	</tr>
	
	
</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-3">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;�������� ���������:
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<textarea id="text" name="description" style="width:100%" rows=20></textarea>
		</td>
	</tr>
	
</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-4">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">

	<tr>
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;������� ����������:</td>
		<td align="left" width="80%">
			<input type="text" name="sort_order" value="0" style="width:50px">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;������ (�������� / ���������):</td>
		<td align="left">
			<input type="checkbox" name="active">
		</td>
	</tr>

</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-5">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr>
		<td align="left" width="20%">&nbsp;&nbsp;������:</td>
		<td align="left" width="80%">
			<input type="file" name="img" style="width:100%;">
		</td>
	</tr>
	
	<tr>
		<td align="left" >&nbsp;&nbsp;�������� ���������� (ALT):</td>
		<td align="left" >
			<input type="text" name="alt_img" style="width:100%" value="{$alt_img}">
		</td>
	</tr>
</table>
</div>

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg_action">
	<td align="left" colspan="2">
		<input type="submit" name="save" value="���������" style="width:100px">&nbsp;
		<input type="submit" name="save_add" value="��������� � �������� ���">&nbsp;
	</td>
	</tr>
</table>

</form>
{/strip} 
