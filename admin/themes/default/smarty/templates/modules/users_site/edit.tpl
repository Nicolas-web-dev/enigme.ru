{include file="editor.tpl"}
{include file="calendar.tpl"}
{strip}
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<form action="?main={$smarty.get.main}&worker={$smarty.get.module}&action=edit&id={$smarty.get.id}" method="post" enctype="multipart/form-data" name="form">
		
	<tr>
		<td align="left" colspan="2" height="25px" bgcolor="#eef1f7">
		<b>&nbsp;&nbsp;����������</b>
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;�������������:</td>
		<td align="left">
			<input type="checkbox" name="is_block" {$is_block}>
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;������� � ���������� �����:</td>
		<td align="left">
			<input type="checkbox" name="is_public" {$is_public}>
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;���� ��������:</td>
		<td align="left">
			<input type="text" name="date1" id="date1" value="{$date1}" />
			&nbsp;<input type="button" id="trigger1" value=" ... ">
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;������ ����������:</td>
		<td align="left">
			<input type="text" name="sort" value="{$sort}" style="width:50px">
		</td>
	</tr>
	
	<tr>
		<td align="left" colspan="2" height="25px" bgcolor="#eef1f7">
		<b>&nbsp;&nbsp;�����</b>
		</td>
	</tr>
	
	<tr>
		<td width="30%" align="left"><span style="color:red;">*</span>&nbsp;���������:</td>
		<td width="70%" align="left">
			<select name="capacity" style="width:100%">
			    <option value='null'>-- �������� ��������� --</option>
			    {html_options values=$sel_id output=$sel_names selected=$is_active}
			</select>
		</td>
	</tr>
	
	<tr>
		<td width="30%" align="left"><span style="color:red;">*</span>&nbsp;����� ������� (����):</td>
		<td width="70%" align="left">
			<select name="sel_role" style="width:100%">
			    <option value='null'>-- �������� ���� --</option>
			    {html_options values=$role_id output=$role_names selected=$role_active}
			</select>
		</td>
	</tr>
	
	
	
	<tr>
		<td align="left">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" style="border:0px"><span style="color:red;">*</span>&nbsp;�����:</td>
				<td align="right" style="border:0px">
				<a href="#" 
					onmouseover="return overlib('
					����� ����� ���� �� ������ 30 ������. <br>
					������������ ������ ��������� ����� [A-Z] � ����� [0-9]
					', LEFT);" 
					onmouseout="return nd();" >
					<img src="images/warnings.png" width="16" height="16" border="0"></a>
				</td>
			</tr>
			</table>
		</td>
		<td align="left">
			<input type="text" name="login" value="{$login}" style="width:100%;color:blue;font-weight:bold;">
		</td>
	</tr>
	
	<tr>
		<td align="left">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" style="border:0px">&nbsp;&nbsp;������:</td>
				<td align="right" style="border:0px">
				<a href="#" 
					onmouseover="return overlib('
					������ ����� ���� �� ����� 30 ������
					', LEFT);" 
					onmouseout="return nd();" >
					<img src="images/warnings.png" width="16" height="16" border="0"></a>
				</td>
			</tr>
			</table>
		</td>
		<td align="left">
			<input type="password" name="password" value="" style="width:100%;color:red;font-weight:bold;">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;��������� ������:</td>
		<td align="left">
			<input type="password" name="re_password" value="" style="width:100%;color:red;font-weight:bold;">
		</td>
	</tr>
	
	<tr>
		<td align="left" colspan="2" height="25px" bgcolor="#eef1f7">
		<b>&nbsp;&nbsp;������ �����</b>
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;���:</td>
		<td align="left">
			<input type="text" name="full_name" value="{$fullName}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;e-mail:</td>
		<td align="left">
			<input type="text" name="email" value="{$email}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left" width="30%">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" style="border:0px"><span style="color:red;">*</span>&nbsp;�������� �������:</td>
				<td align="right" style="border:0px">
				<a href="#" 
					onmouseover="return overlib('
					�� ������ 180px.
					', LEFT);" 
					onmouseout="return nd();" >
					<img src="images/warnings.png" width="16" height="16" border="0"></a>
				</td>
			</tr>
			</table>
		</td>
		
		<td align="left" width="70%">
			<input type="file" name="img" style="width:100%;">
		</td>
	</tr>
	
	<tr>
		<td align="left" colspan="2">
			&nbsp;&nbsp;� ����<br>
			<textarea name="user_text" style="width:100%" rows=10>{$user_text}</textarea>
		</td>
	</tr>

	<tr>
	<td align="left" colspan="2">
		<input type="submit" name="add" value="���������" style="width:100px">
	</td>
	</tr>
</table>
</form>
{/strip}

{literal}
<script type="text/javascript">
	Calendar.setup({
	inputField     :    "date1",
	ifFormat       :    "%Y-%m-%d %H:%M:%S",
	timeFormat     :    "24",
	button         :    "trigger1",
	singleClick    :    true,
	step           :    1
	});
</script>
{/literal}