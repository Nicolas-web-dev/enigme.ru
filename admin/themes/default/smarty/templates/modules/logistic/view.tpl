{include file="editor.tpl"}
{strip}
    <form action="?main={$smarty.get.main}
        &worker={$smarty.get.module}
        &id={$smarty.get.id}
        &action=save" method="post" enctype="application/x-www-form-urlencoded" name="formMod">

        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;���������� ������:</td>
                <td align="left" width="80%">
                    <select name="courier_id" id="sel_catalog" style="width:100%">
                        {foreach from=$couriers item=option key=key}
                            <option {if $option.id==$courier_id}selected="selected"{/if} value="{$option.id}">{$option.name}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;kladr_id ��� �������:</td>
                <td align="left" width="80%">
                    <input type="text" name="kladr_id_region" value="{$kladr_id_region}" style="width:100%">
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;kladr_id ��� ������:</td>
                <td align="left" width="80%">
                    <input type="text" name="kladr_id_city" value="{$kladr_id_city}" style="width:100%">
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;������:</td>
                <td align="left" width="80%">
                    <input type="text" name="name_region" value="{$name_region}" style="width:100%">
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;�����:</td>
                <td align="left" width="80%">
                    <input type="text" name="name_city" value="{$name_city}" style="width:100%">
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;��� ������� � ��:</td>
                <td align="left" width="80%">
                    <input type="text" name="code_region" value="{$code_region}" style="width:100%">
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;��� ������ � ��:</td>
                <td align="left" width="80%">
                    <input type="text" name="code_city" value="{$code_city}" style="width:100%">
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;���� �� ��������:</td>
                <td align="left" width="80%">
                    <input type="text" name="delivery_days" value="{$delivery_days}" style="width:100%">
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;��� �������� (��������/���������):</td>
                <td align="left" width="80%">
                    <input type="text" name="delivery_type" value="{$delivery_type}" style="width:100%">
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;��� ������ ����������:</td>
                <td align="left" width="80%">
                    <input type="text" name="delivery_code" value="{$delivery_code}" style="width:100%">
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;����� ������ ����������:</td>
                <td align="left" width="80%">
                    <input type="text" name="delivery_address" value="{$delivery_address}" style="width:100%">
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;��� ����� ����� ����������:</td>
                <td align="left" width="80%">
                    <textarea name="delivery_how_to" style="width:100%">{$delivery_how_to}</textarea>
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;��� ������:</td>
                <td align="left" width="80%">
                    <input type="text" name="delivery_payment" value="{$delivery_payment}" style="width:100%">
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;��������� �������� 1��:</td>
                <td align="left" width="80%">
                    <input type="text" name="delivery_cost_1" value="{$delivery_cost_1}" style="width:100%">
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;��������� �������� 3��:</td>
                <td align="left" width="80%">
                    <input type="text" name="delivery_cost_3" value="{$delivery_cost_3}" style="width:100%">
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;������� � ��������� � �������� ������:</td>
                <td align="left" width="80%">
                    <textarea name="delivery_news_to_operator" style="width:100%">{$delivery_news_to_operator}</textarea>
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;����� ���������� � ������ ��������:</td>
                <td align="left" width="80%">
                    <textarea name="delivery_about_to_operator" style="width:100%">{$delivery_about_to_operator}</textarea>
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;��� ������ �������� � ����� ����:</td>
                <td align="left" width="80%">
                    <input type="text" name="delivery_our_code" value="{$delivery_our_code}" style="width:100%">
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;������� ��������� ������������:</td>
                <td align="left" width="80%">
                    <input type="text" name="percentage_kass" value="{$percentage_kass}" style="width:100%">
                </td>
            </tr>
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;������� ���������:</td>
                <td align="left" width="80%">
                    <input type="text" name="percentage_insurance" value="{$percentage_insurance}" style="width:100%">
                </td>
            </tr>
            <input type="hidden" name="id" value="{$id}">
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
            <tbody>
                <tr class="table_bg_action">
                    <td align="left" colspan="2">
                        <input type="submit" name="save" value="���������" style="width:100px">&nbsp;
                        <input type="submit" name="save_list" value="��������� � ������� � ������">
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
{/strip}