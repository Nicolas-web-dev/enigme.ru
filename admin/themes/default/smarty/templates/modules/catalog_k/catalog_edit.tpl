{include file="editor.tpl"}
{strip}
<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&id_cat={$smarty.get.id_cat}
	&id_catalog={$smarty.get.id_catalog}
	&action=catalog_edit" method="post" enctype="multipart/form-data" id="form">

<div id="tabs" class="flora">
	<ul class="ui-tabs-nav">
		<li class="ui-tabs-selected"><a href="#tab-1"><span>�������� ������</span></a></li>
		<li class=""><a href="#tab-2"><span>��������� ��������</span></a></li>
		<li class=""><a href="#tab-3"><span>��������� ����������</span></a></li>
	</ul>
</div>

<div style="display: block;" class="ui-tabs-panel" id="tab-1">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr>
		<td align="left" width="20%" ><span style="color:red;">*</span>&nbsp;������������ ������:</td>
		<td align="left" width="80%">
			<select name="sel_catalog" style="width:100%">
				<option value="0">-- �������� ������ --</option>
				{$sel_catalog}
			</select>
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;�������� �������:</td>
		<td align="left">
			<input type="text" name="title" value="{$title}" style="width:100%;" onkeyup="url_convert();" onchange="url_convert();">
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;������������ ��� (��������):</td>
		<td align="left">
			<input type="text" name="url" value="{$url}" style="width:100%;">
		</td>
	</tr>
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;���� ����
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;TITLE:</td>
		<td align="left">
			<input type="text" name="meta_title" value="{$meta_title}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;TITLE_1:</td>
		<td align="left">
			<input type="text" name="meta_title_1" value="{$meta_title_1}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left" valign="top">&nbsp;DESCRIPTION:</td>
		<td align="left">
			<input type="text" name="meta_description" value="{$meta_description}" style="width:100%" >
		</td>
	</tr>
</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-2">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;���������:
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<textarea id="anot" name="anot" style="width:100%" rows=20>{$anot}</textarea>
		</td>
	</tr>
</table>
</div>
	
<div class="ui-tabs-panel ui-tabs-hide" id="tab-3">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr>
		<td align="left" width="20%">&nbsp;&nbsp;�������������:</td>
		<td align="left" width="80%">
			<input type="checkbox" name="is_block" {$is_block}>
		</td>
	</tr>
	
	<tr>
		<td align="left" width="20%">&nbsp;&nbsp;TOP:</td>
		<td align="left" width="80%">
			<input type="checkbox" name="is_top" {$is_top}>
		</td>
	</tr>
	
	<select name="pol">
		{html_options values=$sel_pol_id output=$sel_pol_name selected=$sel_pol_act}
	</select>&nbsp;
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;������ ����������:</td>
		<td align="left">
			<input type="text" name="sort" value="{$sort}" style="width:50px">
		</td>
	</tr>
</table>
</div>

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg_action">
	<td align="left" colspan="2">
		<input type="submit" name="save" value="���������" style="width:100px">&nbsp;
		<input type="submit" name="save_add" value="��������� � �������� ���">&nbsp;
		<input type="submit" name="save_list" value="��������� � ������� � ������">
	</td>
	</tr>
</table>

</form>
{/strip}