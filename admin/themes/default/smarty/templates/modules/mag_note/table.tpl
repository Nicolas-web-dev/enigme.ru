{literal}

<style type="text/css">
#grid_table tr {height: 55px;}
</style>

<script type="text/javascript">

$(document).ready(function(){

	$("#grid_table").flexigrid({
		url: '{/literal}?ajax={$smarty.get.module}&action=mod{literal}',
		dataType: 'json',
		colModel : [
            {display: 'ID', 		name : 'id', 		width : 50, 	sortable : false, 	align: 'center'},
            {display: '���� ������',name : 'DateAdd', 	width : 100, 	sortable : true, 	align: 'center'},
            {display: '���� ��������',name : 'DateEnd', width : 100, 	sortable : true, 	align: 'center'},
            {display: '��������', 	name : 'action', 	width : 100, 	sortable : false, 	align: 'center'},
            {display: '������', 	name : 'title', 	width : 200, 	sortable : false, 	align: 'left'},
            {display: 'PRICE', 		name : 'price', 	width : 100, 	sortable : false, 	align: 'right'},
            {display: 'SUM', 		name : 'sum', 		width : 40, 	sortable : false, 	align: 'left'}
		],
		buttons : [
            {name: '�������� ���', bclass: 'check', onpress : test},{separator: true},
            {name: '����� ���������', bclass: 'uncheck', onpress : test},{separator: true},{separator: true},{separator: true},{separator: true},
            {name: '�������� �������', bclass: '', onpress : test},{separator: true},{separator: true},{separator: true},{separator: true},
            /*{name: '������������ ��������� �� �������', bclass: '', onpress : test},{separator: true},
            {name: 'Print-Logistics', bclass: '', onpress : test},{separator: true},
            {name: 'IM-Logistics', bclass: '', onpress : test},{separator: true},{separator: true},
            {name: '�����������', bclass: '', onpress : test},{separator: true},{separator: true},
            {name: '�����', bclass: '', onpress : test},{separator: true},{separator: true}*/
            {name: '�������� � ��������', bclass: '', onpress : orderActions},{separator: true},{separator: true}
		],
		sortname: "DateAdd",
		sortorder: "desc",
		usepager: false,
		showTableToggleBtn: false,
		width: 'auto',
		height: 'auto',
		useRp: false,
		procmsg: '���������, ����������, ��������� ...',
		nomsg: '��� ���������'
	});
});

function test(com,grid){
	if (com=='�������� �������'){
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "{/literal}?ajax={$smarty.get.module}&action=note_clear{literal}",
			data: "",
			success: function(data){
				$("#count_note").html( '0' );
				$("#grid_table").flexReload();
			}});


	}else if (com=='������������ ��������� �� �������'){
		
		if($('.trSelected',grid).length>0){
			var items = $('.trSelected',grid);
			var itemlist ='';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "{/literal}?ajax={$smarty.get.module}&action=get_sklad{literal}",
				data: "items="+itemlist,
				success: function(data){
					window.location = 'http://{/literal}{$smarty.const.BASE_URL}{literal}/images/uploads/note_sklad.csv';
					$("#grid_table").flexReload();
				}});
		}else{
			return false;
		}
		
	}else if (com=='Print-Logistics'){
		if($('.trSelected',grid).length>0){
			var items = $('.trSelected',grid);
			var itemlist ='';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "{/literal}?ajax={$smarty.get.module}&action=sklad_print_logistics&status=4&date={$smarty.get.date}&folder={$smarty.get.folder}{literal}",
				data: "items="+itemlist,
				success: function(data){
					$("#grid_table").flexReload();
					window.open ('http://{/literal}{$smarty.const.BASE_URL}{literal}/images/uploads/print_all_logistics.html', 'print');
				}});
		}else{
			return false;
		}
		
	}else if (com=='IM-Logistics'){
		
		if($('.trSelected',grid).length>0){
			var items = $('.trSelected',grid);
			var itemlist ='';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "{/literal}?ajax={$smarty.get.module}&action=get_sklad_logistics{literal}",
				data: "items="+itemlist,
				success: function(data){
					window.location = 'http://{/literal}{$smarty.const.BASE_URL}{literal}/images/uploads/enigme-logistics.csv';
					$("#grid_table").flexReload();
				}});
		}else{
			return false;
		}
		
	}else if (com=='�����������'){
		if($('.trSelected',grid).length>0){
			var items = $('.trSelected',grid);
			var itemlist ='';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "{/literal}?ajax={$smarty.get.module}&action=sklad_print_post&status=4&date={$smarty.get.date}&folder={$smarty.get.folder}{literal}",
				data: "items="+itemlist,
				success: function(data){
					$("#grid_table").flexReload();
					window.open ('http://{/literal}{$smarty.const.BASE_URL}{literal}/images/uploads/print_all_post.html', 'print');
				}});
		}else{
			return false;
		}
    }else if (com=='�����'){

            if($('.trSelected',grid).length>0){
                var items = $('.trSelected',grid);
                var itemlist ='';
                for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{/literal}?ajax={$smarty.get.module}&action=get_sklad_post{literal}",
                    data: "items="+itemlist,
                    success: function(data){
                        window.location = 'http://{/literal}{$smarty.const.BASE_URL}{literal}/images/uploads/enigme-post.csv';
                        $("#grid_table").flexReload();
                    }});
            }else{
                return false;
            }
		
	}else if (com=='�������� ���'){
		$('.bDiv tbody tr',grid).addClass('trSelected');

	}else if (com=='����� ���������'){
		$('.bDiv tbody tr',grid).removeClass('trSelected');
	}
}
</script>
{/literal}

<table width="100%" border="0" cellpadding="0" cellspacing="0"> 
<tr>
<td width="250">
	<div><h1><a href="?main=mag&module=mag_note&action=mod">������� (<span id="count_note">{$order_note_count}</span>)</a></h1></div>
</td>
</tr>
</table>


<br />


<table id="grid_table" class="flexigrid" style="display:none"></table>
<div class="modal modal__black fade" id="download__modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">���������� ��������</h4>
            </div>
            <div class="modal-body">
                <p class="hidden text-warning"></p>
                <ul class="list-unstyled" id="download__list" style="margin-bottom: 0;">
                    <li class="option"><span class="option-title" data-action="get_sklad" data-url="note_sklad.csv">CSV ���������</span></li>
                    <li class="option"><span class="option-title" data-action="sklad_print" data-url="print_all.html" data-addict="&status=4&date={$smarty.get.date}&folder={$smarty.get.folder}">�����������</span></li>
                    <li class="option"><span class="option-title" data-action="sklad_print_logistics" data-url="print_all_logistics.html" data-addict="&status=4&date={$smarty.get.date}&folder={$smarty.get.folder}">���������� ��� IM-Logistics</span></li>
                    <!--li class="option"><span class="option-title" data-action="sklad_print_post" data-url="print_all_post.html" data-addict="&status=4&date={$smarty.get.date}&folder={$smarty.get.folder}">���������� ��� ����� ������</span></li-->
                    <li class="divider option"></li>
                    <li class="option"><span class="option-title" data-action="get_sklad_logistics" data-url="enigme-logistics.csv">CSV ��� IM-Logistics</span></li>
					<li class="option"><span class="option-title" data-action="get_post_service" data-url="enigme-postservice.csv">��������� ������</span></li>
                    <!--li class="option"><span class="option-title" data-action="get_sklad_courier" data-url="enigme-ru-courier.xml">XML ��� Ru-Courier</span></li-->
                    <li class="option"><span class="option-title" data-action="get_sklad_courier_2" data-url="enigme-ru-courier-2.xml">XML ��� Ru-Courier</span></li>
                    <li class="option"><span class="option-title" data-action="get_sklad_post" data-url="enigme-post.csv">CSV ��� ����� ������</span></li>
                    <li class="option"><span class="option-title" data-action="get_sklad_bxb" data-url="enigme-boxberry.xml">XML ��� Boxberry</span></li>
                </ul>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>