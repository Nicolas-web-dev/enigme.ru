<?php /* Smarty version 2.6.18, created on 2015-02-02 05:59:26
         compiled from modules/mag_orders/edit_reg_js.tpl */ ?>
<script>
<?php echo '

$(function () {
    $(\'.dostavka_item [data-toggle=tooltip]\').hover(function () {
        var elem = $(this)
        var content = elem.data(\'content\')
        if (elem.data(\'width\') != undefined) {
            var width = elem.data(\'width\')
            var elem_l = -145
        } else {
            var width = 100
            var elem_l = -65
        }
        var elem_t = elem.offset().top - elem.height() - 105

        if (typeof elem.data(\'placement\') != \'undefined\') {
            if (elem.data(\'placement\') == \'left\') {

                elem.parent().append(\'<div style="top:0;margin-left:0;right:115%;width:\' + width + \'px" class="dostavka_tooltip left">\' + content + \'</div>\')
            }
        } else {
            elem.parent().append(\'<div style="bottom:110%;left:50%;margin-left:\' + elem_l + \'px;width:\' + width + \'px" class="dostavka_tooltip">\' + content + \'</div>\')
        }


        $(\'.dostavka_tooltip\').fadeIn(150)
    }, function () {
        var elem = $(\'.dostavka_tooltip\')
        elem.fadeOut(150)
        setTimeout(function () {
            elem.remove()
        }, 151)
    })
})

/**
 *
 *  ����������� ������� ��� ������������� ������
 *
 *  @param {Number} user_percent - ������ ������ ������������
 *  @param {Number} sum - ����� ��������� ������
 *  @param {Number} sale - ������ � ������
 *  @returns {Array} data - ������ �� �������� � �������
 *
 */
function out_discount(user_percent, sum, sale) {

    var percent_o = reload_users_percent_one(sum),
        summary = sum - sale,
        data = [];

    /*if (user_percent != 0) {

        // �������
        if (percent_o > user_percent) {
            data["dType"] = 2;
            data["percent"] = percent_o;
            data["big_sum"] = show_users_percent(summary, percent_o);

        //�������������
        } else {
            data["dType"] = 1;
            data["percent"] = user_percent;
            data["big_sum"] = show_users_percent(summary, user_percent);
        }

    // �������
    } else {
        data["dType"] = 2;
        data["percent"] = percent_o;
        data["big_sum"] = show_users_percent(summary, percent_o);
    }*/
    data["dType"] = 1;
    data["percent"] = user_percent;
    data["big_sum"] = show_users_percent(summary, user_percent);

    data["big_sum"] += sale;

    return data;
}

// ���������� ������ �������
function reload_users_percent_one(sum) {

    var percent = 0;

    if (sum >= 3000 && sum < 5000) {
        percent = 3;
    } else if (sum >= 5000 && sum < 7000) {
        percent = 4;
    } else if (sum >= 7000 && sum < 10000) {
        percent = 5;
    } else if (sum >= 10000 && sum < 15000) {
        percent = 7;
    } else if (sum >= 15000) {
        percent = 10;
    } else {
        percent = 0;
    }

    percent = 0;

    return percent;
}

// ���������� ����� � ������ ������
function show_users_percent(sum, percent) {

    var sum = parseInt(sum);
    var percent = parseInt(percent);

    if (percent == 0) {
        data = sum;
    } else {
        data = sum - (sum * percent) / 100;
    }
    return Math.ceil(data);
}




$(document).ready(function () {

    //���������� �������
     recountPrice();

    // ������� �������
    $(".s_letters a").click(function () {
        var letter = $(this).text();
        $("#sel_brend option").each(function (i) {

            var option_val = $(this).html();
            var first_char = option_val.charAt(0);
            if (first_char == letter) {
                $(this).attr(\'selected\', \'selected\');
                return false;
            }
        });
        return false;
    });


// �������� ����� ���������
    $("#submit_form").live("click", function () {
        if ($(".id_orders").attr(\'value\') != null) {
            $(".form_add_reg").submit();
        } else {
            alert(\'������ �� ������������ !\');
            return false;
        }
        var date1 = $("#date1").attr(\'value\');
        if (date1 == \'0000-00-00 00:00:00\' || date1 == \'\') {
            alert(\'������� ���� ��������. !\');
            return false;
        }
    });

// ������� �������
    $("#sel_brend").click(function () {

        // ��������� �����
        var is_user = $("#table_info_users").text();
        if (is_user == \'\') {
            alert(\'�� ������ ������!\');
            return false;
        }

// ��������� �����
        var selected = $("#sel_brend option:selected");

        $("#msg_order").html(\'\');
        $("#msg_order_title").html(\'\');
        $("#msg_order_img").html(\'\');
        $("#msg").html("������� ... ");

// ������ ������
        $.post("'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=select_catalog_data&id_cat=<?php echo $_GET['id_cat']; ?>
<?php echo '", {
            id_brend: selected.val()
        }, function (xml) {
            $("message", xml).each(function (id) {

                $("#msg").remove();

                message = $("message", xml).get(id);
                var ul_html = \'<div class="abs_sel"> \\
								<a href="">aromat 1</a> <br> \\
								<a href="">aromat 2</a> <br> \\
							  </select>\'
                $("#msg1").html(\'<h4 style="color:#f55d5c">��� ���</h4>\' + $("msg1", message).text() + \'<div style="clear:both;"></div>\');
                $("#msg2").html(\'<h4 style="color:#569ddb">��� ����</h4>\' + $("msg2", message).text());
                $(\'#msg3\').html(\'<h4>��� ���</h4><div class="abs_sel">\' + $("msg3", message).text() + \'</div>\')
                $(\'#msg4\').html(\'<h4>��� ����</h4><div class="abs_sel">\' + $("msg4", message).text() + \'</div>\')
            });
        });
    });

//������� ������� �� ������
    $(\'.search\').on(\'click\', function (e) {
        e.preventDefault();

        var search_text = $(\'.search_text\').val();

        // ������ ������
        $.post("'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=select_catalog_data&id_cat=<?php echo $_GET['id_cat']; ?>
<?php echo '", {
            search_text: search_text
        }, function (xml) {

            $("message", xml).each(function (id) {
                $("#msg").remove();
                message = $("message", xml).get(id);
                var ul_html = \'<div class="abs_sel"> \\
                                        <a href="">aromat 1</a> <br> \\
                                        <a href="">aromat 2</a> <br> \\
                                      </select>\'
                $("#msg1").html(\'<h4 style="color:#f55d5c">��� ���</h4>\' + $("msg1", message).text() + \'<div style="clear:both;"></div>\');
                $("#msg2").html(\'<h4 style="color:#569ddb">��� ����</h4>\' + $("msg2", message).text());
                $(\'#msg3\').html(\'<h4>��� ���</h4><br><div class="abs_sel">\' + $("msg3", message).text() + \'</div>\')
                $(\'#msg4\').html(\'<h4>��� ����</h4><br><div class="abs_sel">\' + $("msg4", message).text() + \'</div>\')
            });
        });

    })


// ������� ����������� �������
    $(".sel_catalog_data").live("click", function () {

        $("#ar_comment").html(\'\');
        $("#ar_comment_form").hide();


        $("#msg_order").html("������� ... ");

        var selected = $(this).attr(\'title\');

        $.post("'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=select_catalog_data_order<?php echo '", {
            id_catalog_data: selected
        }, function (xml) {
            $("message", xml).each(function (id) {
                message = $("message", xml).get(id);

                $("#msg_order").html($("msg", message).text());
                $("#msg_order_title").html($("title", message).text());
                $("#msg_order_pol").html($("pol", message).text());
                $("#msg_order_img").html($("img", message).text());

                $("#ar_comment_form_add").show();

            });
        });

        $.post("'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=select_catalog_data_wiki<?php echo '", {
            id_catalog_data: selected
        }, function (xml) {
            $("message", xml).each(function (id) {
                message = $("message", xml).get(id);

                $("#ar_comment").html($("title", message).text());
            });
        });

    });

// form comment
    $("#ar_comment_form_add").click(function () {
        $("#ar_comment_form").toggle();
    });

// send comment wiki
    $("#ar_wiki").live("click", function () {

        var text = $(\'#ar_comment_text\').val();
        var id_catalog_data = $(\'#ar_catalog_data_id\').val();
        var type = \'WIKI\';

        if (text == \'\') {
            alert(\'�������� �����\');
            return false;
        }

        $.post("'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=add_catalog_data_wiki<?php echo '", {
            id_catalog_data: id_catalog_data,
            text: text,
            type: type
        }, function (xml) {
            $("message", xml).each(function (id) {
                message = $("message", xml).get(id);

                $("#ar_comment_text").attr("value", "");
                $("#ar_comment_form").hide();

                $("#ar_comment_table").append(\'<tr ><td align="left" width="50px"><img src="../admin/images/table_info.gif"  ></td><td align="left"><span>\' + text + \'</span></td></tr>\');
            });
        });


    });

// send comment error
    $("#ar_error").live("click", function () {

        var text = $(\'#ar_comment_text\').val();
        var id_catalog_data = $(\'#ar_catalog_data_id\').val();
        var type = \'ERROR\';

        if (text == \'\') {
            alert(\'�������� �����\');
            return false;
        }


        $.post("'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=add_catalog_data_wiki<?php echo '", {
            id_catalog_data: id_catalog_data,
            text: text,
            type: type
        }, function (xml) {
            $("message", xml).each(function (id) {
                message = $("message", xml).get(id);

                $("#ar_comment_text").attr("value", "");
                $("#ar_comment_form").hide();

                $("#ar_comment_table").append(\'<tr ><td align="left" width="50px"><img src="../admin/images/warnings.png"  ></td><td align="left"><span>\' + text + \'</span></td></tr>\');
            });
        });


    });


// �������, ��������� �������.
    $(".sel_catalog_data_order").live("click", function () {

        var id_order = $(this).attr(\'title\');
        var articul = $(this).data(\'articul\');

        // ����������
        var brend_title = $("#sel_brend option:selected").text();
        var aromat_title = $("#msg_order_title").text();
        var order_title = $("#orders_title_" + id_order).text();
        var order_price = parseInt($("#orders_price_" + id_order).text());
        var client_percent = parseInt($("#d_percent").text());

        console.log(\'client_percent: \' + client_percent)


        // ���� ���� �������
        if ($("#kol_orders_" + id_order).length) {

            var new_kol = parseInt($("#kol_orders_" + id_order).val()) + 1;
            var new_sum = order_price * new_kol;

            $("#kol_orders_" + id_order).attr("value", new_kol);
            $("#price_orders_sum_" + id_order).html(new_sum);

            new_kol = 0;

            // ���� ��� �������
        } else {

            var new_sum = order_price * 1;

            $("#table_add_orders").append(\'<tr class="tr_item_orders_clear" id="tr_item_orders_\' + id_order + \'"> \\
			<td align="center" > \\
				<input type="button" name="\' + id_order + \'" class="delete_list" alt="\' + order_price + \'" value="X" style="width:20px;color:red;font-weight: bold;border:0;background:none;"> \\
			</td> \\
			<td align="left" > \\
				<input class="id_orders" type="hidden" name="id_orders[]" value="\' + id_order + \'"><b>\' + brend_title + \' \' + aromat_title + \'</b> <br />\' + order_title + \' \\
			</td> \\
			<td align="center" > \\
				<span id="price_orders_\' + id_order + \'">\\
				\' + order_price + \' \\
				</span>\\
			</td> \\
			<td align="left" > \\
				<input style="width:25px;padding:1px;float:left;height:18px;" type="text" id="kol_orders_\' + id_order + \'" name="kol_orders_\' + id_order + \'" value="1"  onkeydown="return false;">\\
				<input style="width:25px;padding:1px;float:left;height:18px;display: none" type="text" id="ordersSum_kol_\'+ articul + \'" name="ordersSum_kol[\' + articul + \']" value="1"  onkeydown="return false;">\\
				<a class="up" data-articul="\' + articul + \'" alt="\' + id_order + \'"><img src="images/nav/up.gif" style=""></a><br /><a data-articul="\' + articul + \'" alt="\' + id_order + \'" class="dn"><img src="images/nav/dn.gif" style=""></a>\\
			</td> \\
			<td align="left" > \\
				<span class="price_item_sum" id="price_orders_sum_\' + id_order + \'">\\
				\' + new_sum + \' \\
				</span>\\
			</td> \\
			<td align="center" > \\
				<span style="color:blue;">0</span>\\
			</td> \\
			</tr>\');

        }
        //���������� �������
        recountPrice();


    });

// �������, +
    $(".up").live("click", function () {

        var id_order = $(this).attr(\'alt\');
        var articul = $(this).data(\'articul\');

        // ����������
        var order_price = parseInt($("#price_orders_" + id_order).text());
        var new_kol = parseInt($("#kol_orders_" + id_order).val()) + 1;
        var big_sum = order_price + parseInt($("#big_sum").text());
        var client_percent = parseInt($("#d_percent").text());

        // ������ ����������
        $("#kol_orders_" + id_order).attr("value", new_kol);
        $("#ordersSum_kol_" + articul).attr("value", new_kol);

        // ������ ����� kol*price
        var new_sum = order_price * new_kol;
        $("#price_orders_sum_" + id_order).html(new_sum);
        //���������� �������
        recountPrice();

    });

// �������, -
    $(".dn").live("click", function () {

        var id_order = $(this).attr(\'alt\');
        var articul = $(this).data(\'articul\');

        var kol_is = parseInt($(\'#kol_orders_\' + id_order).attr(\'value\'));
        if (kol_is > 1) {

            // ����������
            var new_kol = kol_is - 1;
            var order_price = parseInt($("#price_orders_" + id_order).text());
            var big_sum = parseInt($("#big_sum").text()) - order_price;
            var client_percent = parseInt($("#d_percent").text());

            // ������ ����������
            $("#kol_orders_" + id_order).attr("value", new_kol);
            $("#ordersSum_kol_" + articul).attr("value", new_kol);

            // ������ ����� kol*price
            var new_sum = order_price * new_kol;
            $("#price_orders_sum_" + id_order).html(new_sum);

            //���������� �������
            recountPrice();

        }

    });


//Pay checkboxes
    $(\'.checkbox_group_pay .dostavka_item__checkbox\').on(\'click\', function () {
        var elem = $(this),
            elems = $(\'.checkbox_group_pay .dostavka_item__checkbox\');

        elems.removeClass(\'checked\');
        elem.addClass(\'checked\');

        $(\'.checkbox_group_pay .payment_type\').val(elem.data(\'value\'))

        $.ajax({
            url: "/api/logistic_get/",
            data: {
                k: $("#courier_address_id").val(),
                t: $(\'input[name="payment_type"]\').val()
            },
            success: function(resp) {

                // ���� ����� �������, �� ���� ��� ������ ������ ��������
                if (resp.id != null && resp.id != 0) {

                    fillCouriers(resp.kladr_id_city, resp.name_city, resp);

                    // ���� �� ������ �������� �� ��� ������ ������ ������ �� ������ �����
                } else {

                    var addr = [],
                            result,
                            answer;

                    // ���������� ������ ������ �� ����� ������������

                    //��������� ������������ ������ ������
                    if ($(\'#part_name\').text() != "") {

                        fillCouriers($(\'#select_part\').val(), resp.name_city, resp);
                    } else {

                        addr.push($(\'#region\').val());
                        // ���������� ������ ������ �� ����� ������������
                        if ($(\'#region\').val() != $(\'#city\').val()){
                            addr.push($(\'#city\').val());
                        }
                        addr.push($(\'#step2_settlement\').val());

                        addr = addr.join(" ");

                        answer = DadataApi.clean(addr);
                        answer.then(function(msg) {
                            if (typeof msg != \'undefined\') {
                                result = AddressSuggestions.getCleanData(msg);
                                fillCouriers(result.kladr_id, addr, resp);
                            }
                        });
                    }
                }
            }
        });
    });




//delivery checkboxes
    $(\'.checkbox_group_delivery .dostavka_item__checkbox\').on(\'click\', function () {
        var elem = $(this)
        var elems = $(\'.checkbox_group_delivery .dostavka_item__checkbox\')
        elems.removeClass(\'checked\')
        elem.addClass(\'checked\')
    })


//

// �������, delete
    $(".delete_list").live("click", function () {

        var id_order = $(this).attr(\'name\');

        // ����������
        var last_sum = parseInt($("#big_sum").text());
        var delete_sum = parseInt($(\'#price_orders_\' + id_order).text());
        var kol = parseInt($(\'#kol_orders_\' + id_order).attr(\'value\'));
        var big_sum = last_sum - (delete_sum * kol);
        var client_percent = parseInt($("#d_percent").text());

        // ������� �������
        $(\'#tr_item_orders_\' + id_order).remove();

        //���������� �������
        recountPrice();
    });


    /**
     * User edit
     */

    $(\'#table_info_users .edit\').on(\'click\', function (e) {
        e.preventDefault();
        edit_user();
    })

    function edit_user(){
        $(\'#table_info_users span\').hide();
        $(\'#table_info_users\').find(\'input, select\').removeClass(\'hide\');
        $(\'#table_info_users .address_edit\').show();
        $(\'#table_info_users .address\').hide();
    }

    function saveUser(){
        var inputs = $(\'#table_info_users\').find(\'input:enabled,select:enabled\'),
            data = [];

        $.each(inputs, function (i, v) {
            var name = $(v).attr(\'name\'),
                value = $(v).val();

            data.push({name:name,value:value})
        });

        data.push({name:\'id_order\',value:$(\'[name=order_id]\').val()});

        $.post("'; ?>
?ajax=mag_client&action=save&<?php echo '",data,function(result){
            $(\'#table_info_users span\').show();
            $(\'#table_info_users\').find(\'input, select\').addClass(\'hide\');
            $(\'#table_info_users .address_edit\').hide();
            $(\'#table_info_users .address\').show();

            $.each(result,function(i,v){

                $(\'span#\' + i).text(v);

                if (i==\'regs_postcode\'){
                    $(\'postcode#\' + i).text(v)
                }
            })
        });

        $.ajax({
            url: "/api/logistic_get/",
            data: {
                k: $("#courier_address_id").val()
            },
            success: function(resp) {

                var addr = [],
                        result,
                        answer;

                // ���������� ������ ������ �� ����� ������������

                //��������� ������������ ������ ������
                if ($(\'#part_name\').text() != "") {

                    fillCouriers($(\'#select_part\').val(), resp.name_city, resp);
                } else {

                    addr.push($(\'#region\').val());
                    // ���������� ������ ������ �� ����� ������������
                    if ($(\'#region\').val() != $(\'#city\').val()){
                        addr.push($(\'#city\').val());
                    }
                    addr.push($(\'#step2_settlement\').val());

                    addr = addr.join(" ");

                    answer = DadataApi.clean(addr);
                    answer.then(function(msg) {
                        if (typeof msg != \'undefined\') {
                            result = AddressSuggestions.getCleanData(msg);
                            fillCouriers(result.kladr_id, addr, resp);
                        }
                    });
                }
            }
        });
    }

    $(\'#table_info_users .save\').on(\'click\', function (e) {
        e.preventDefault();
        saveUser();
    })


    /**
     * logistic edit
     */
    $(\'.checkbox_logistic .dostavka_item__checkbox.allow\').on(\'click\', function (e) {
        e.preventDefault();
        if ($(\'.checkbox_logistic\').hasClass(\'edit\')){
            var elem = $(this)
            var elems = $(\'.checkbox_logistic .dostavka_item__checkbox\')

            if (elem.hasClass(\'checked\')){
                elem.removeClass(\'checked\')
            }else{
                elem.addClass(\'checked\')
            }

            var aLogistic = [];
            $.each($(\'.checkbox_logistic .dostavka_item__checkbox.checked\'),function(i,v){
                aLogistic.push($(v).data(\'value\'))
            })

            var sLogistic = aLogistic.join(\';\')

            $(\'.checkbox_logistic .logistic\').val(sLogistic)
        }
    })




    $(\'.checkbox_logistic .edit\').on(\'click\', function (e) {
        e.preventDefault();
        $(\'.checkbox_logistic\').addClass(\'edit\');
        $(\'.checkbox_logistic input.allow\').attr(\'disabled\',false);
        $(\'.checkbox_logistic .save\').show();
    })

    $(\'.checkbox_logistic .save\').on(\'click\', function (e) {
        e.preventDefault();
        $(\'.checkbox_logistic\').removeClass(\'edit\');
        var inputs = $(\'.checkbox_logistic\').find(\'input:enabled,select:enabled\')
        var data = [];
        $.each(inputs, function (i, v) {
            var name = $(v).attr(\'name\')
            var value = $(v).val();
            data.push({name:name,value:value})
        })
        $.post("'; ?>
?ajax=mag_orders&action=save_logistic&<?php echo '",data,function(result){
            $(\'.checkbox_logistic input\').attr(\'disabled\',true);
            $(\'.checkbox_logistic .save\').hide();
         })
    })

    $(\'[name="phone"]\').mask("7(999) 999-99-99")


});

'; ?>


</script>
