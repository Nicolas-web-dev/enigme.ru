<?php /* Smarty version 2.6.18, created on 2015-12-25 14:21:36
         compiled from modules/mag_orders_sklad/table.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "calendar.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '

    <style type="text/css">
        #grid_table tr {
            height: 55px;
        }

        .folders {
            float: left;
            margin-right: 10px;
        }

        .flexigrid {
            float: left
        }
    </style>


    <style type="text/css">

        .brend_liter a {
            background-color: none !important;
            color: #000 !important;
            padding: 5px;
            font-weight: bold;
        }

        .brend_liter a:hover {
            background-color: #000 !important;
            color: #fff !important;
            cursor: pointer;
        }

        .cat_item {
            width: 80px;
            float: left;
            height: 150px;
        }

        #sel_brend {
            font: 12pt Arial;
        }

        .sel_data {
            width: 100%;
            font-size: 11px;
            color: #000;
            background: #fff;
            padding: 2px;
        }

        .sel_catalog_data {
            cursor: pointer;
        }

        .sel_catalog_data_order {
            cursor: pointer;
            color: blue;
        }

        .sel_find_users {
            cursor: pointer;
        }

        .up {
            cursor: pointer;
        }

        .dn {
            cursor: pointer;
        }

        .mod_table td {
            background: #fff;
        }

    </style>

<script type="text/javascript">

$(document).ready(function () {

    // ������� �������
    $(".s_letters a").click(function () {
        var letter = $(this).text();
        $("#sel_brend option").each(function (i) {

            var option_val = $(this).html();
            var first_char = option_val.charAt(0);
            if (first_char == letter) {
                $(this).attr(\'selected\', \'selected\');
                return false;
            }
        });
        return false;
    });

    // ������� �������
    $("#sel_brend").click(function () {

        // ��������� �����
        /*if ( $("#br_fio").val()==\'\' || $("#br_phone").val()==\'\' || $("#br_address").val()==\'\' ) {
         alert( \'��������� ���� (���, �������, �����)\' );
         return false;
         }*/

        // ��������� �����
        var selected = $("#sel_brend option:selected");

        $("#msg_order").html(\'\');
        $("#msg_order_title").html(\'\');
        $("#msg_order_img").html(\'\');
        $("#msg").html("������� ... ");

        // ������ ������
        $.post("'; ?>
?ajax=mag_orders&action=select_catalog_data&id_cat=<?php echo $_GET['id_cat']; ?>
<?php echo '", {
            id_brend: selected.val()
        }, function (xml) {
            $("message", xml).each(function (id) {

                $("#msg").remove();

                message = $("message", xml).get(id);


                $("#msg1").html(\'<h1>������� �������</h1>\' + $("msg1", message).text() + \'<div style="clear:both;"></div>\');
                $("#msg2").html(\'<h1>������� �������</h1>\' + $("msg2", message).text());
            });
        });
    });


    // ������� ����������� �������
    $(".sel_catalog_data").live("click", function () {

        $("#ar_comment").html(\'\');
        $("#ar_comment_form").hide();


        $("#msg_order").html("������� ... ");

        var selected = $(this).attr(\'title\');

        $.post("'; ?>
?ajax=mag_orders&action=select_catalog_data_order<?php echo '", {
            id_catalog_data: selected
        }, function (xml) {
            $("message", xml).each(function (id) {
                message = $("message", xml).get(id);

                $("#msg_order").html($("msg", message).text());
                $("#msg_order_title").html($("title", message).text());
                $("#msg_order_pol").html($("pol", message).text());
                $("#msg_order_img").html($("img", message).text());

                $("#ar_comment_form_add").show();

            });
        });

        $.post("'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=select_catalog_data_wiki<?php echo '", {
            id_catalog_data: selected
        }, function (xml) {
            $("message", xml).each(function (id) {
                message = $("message", xml).get(id);

                $("#ar_comment").html($("title", message).text());
            });
        });

    });

    $("#sel_status").change(function () {
        var sel = $("#sel_status option:selected");
        window.location = \'?main=mag&module=mag_orders_sklad&action=mod&status=\' + sel.val();
    });

    $("#sel_date").click(function () {
        var date = $("#date1").val();
        window.location = \''; ?>
?main=mag&module=mag_orders_sklad&action=mod&status=2&date=<?php echo '\' + date;
    });


    $("#grid_table").flexigrid({
        url: \''; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=mod&status=<?php echo $_GET['status']; ?>
&date=<?php echo $_GET['date']; ?>
&folder=<?php echo $_GET['folder']; ?>
<?php echo '\',
        dataType: \'json\',
        colModel: [
            {display: \'ID\', name: \'id\', width: 50, sortable: false, align: \'center\'},
            {display: \'���� ������\', name: \'DateAdd\', width: 150, sortable: true, align: \'center\'},
            {display: \'���� ��������\', name: \'DateEnd\', width: 100, sortable: true, align: \'center\'},
            {display: \'��������\', name: \'action\', width: 100, sortable: false, align: \'center\'},
            {display: \'������\', name: \'title\', width: 200, sortable: false, align: \'left\'},
            {display: \'PRICE\', name: \'price\', width: 100, sortable: false, align: \'right\'},
            {display: \'SUM\', name: \'sum\', width: 40, sortable: false, align: \'left\'}
        ],
        buttons: [

            {name: \'�������� ���\', bclass: \'check\', onpress: test},
            {separator: true},
            {name: \'����� ���������\', bclass: \'uncheck\', onpress: test},
            {separator: true},
            {separator: true},
            {separator: true},
            {name: \'�������� � ����������� �������� <strong>&#9658;</strong>\', bclass: \'\'},
            {separator: true},
                '; ?>
<?php if ($_GET['status'] == '2'): ?><?php echo '{name: \'�� �����\', bclass: \'\', onpress: test},
            {separator: true},
            '; ?>
<?php endif; ?><?php echo '
                '; ?>
<?php if ($_GET['folder'] != '' && $_GET['status'] == '4'): ?><?php echo '{name: \'������� (� ��������������)\', bclass: \'\', onpress: test},
            {separator: true},
            {separator: true},
            '; ?>
<?php endif; ?><?php echo '
                '; ?>
<?php if ($_GET['folder'] != '' && $_GET['status'] == '4'): ?><?php echo '{name: \'������\', bclass: \'\', onpress: test},
            {separator: true},
            '; ?>
<?php endif; ?><?php echo '
            {name: \'�������� � ��������\', bclass: \'\', onpress : orderActions},{separator: true},{separator: true},
                '; ?>
<?php if ($_GET['folder'] != '' && $_GET['status'] == '4' && $_SESSION['admin_user_login'] == 'admin'): ?><?php echo '{separator: true},
            {separator: true},
            {separator: true},
            {name: \'���������\', bclass: \'\', onpress: test},
            {separator: true},
            '; ?>
<?php endif; ?><?php echo '
                '; ?>
<?php if ($_GET['status'] == '5' || $_GET['status'] == '6'): ?><?php echo '{name: \'������� � �����\', bclass: \'\', onpress: test},
            {separator: true},
            '; ?>
<?php endif; ?><?php echo '
        ],
        searchitems: [
            {display: \'ID ������\', name: \'id\', isdefault: true}
        ],
        sortname: "DateAdd",
        sortorder: "desc",
        usepager: true,
        showTableToggleBtn: true,
        width: \'auto\',
        height: \'auto\',
        useRp: false,
        rpOptions: [300],
        rp: 300,
        pagestat: \'�������� � {from} �� {to} �� {total} �������\',
        procmsg: \'���������, ����������, ��������� ...\',
        nomsg: \'��� ���������\'
    });
});


function toggleAromat() {
    $(\'#aromat_finder\').slideToggle(\'medium\', function () {
        if ($(\'#aromat_finder\').is(\':visible\'))
            $(\'#aromat_finder\').css(\'display\', \'inline-block\');
    });
}

function test(com, grid) {
    if (com == \'�� �����\') {
        if ($(\'.trSelected\', grid).length > 0) {
            var items = $(\'.trSelected\', grid);
            var itemlist = \'\';
            for (i = 0; i < items.length; i++) {
                itemlist += items[i].id.substr(3) + ",";
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=sklad_in&status=2&date=<?php echo $_GET['date']; ?>
<?php echo '",
                data: "items=" + itemlist,
                success: function (data) {
                    $("#grid_table").flexReload();

                    $("#folders").append(\'<div class="folders" ><a href="'; ?>
?module=<?php echo $_GET['module']; ?>
&action=mod&status=4&date=<?php echo $_GET['date']; ?>
<?php echo '&folder=\' + data.folder_id + \'" ><strong>����� (\' + data.folder_count + \')</strong></a></div>\');


                }});
        } else {
            return false;
        }

    } else if (com == \'������� (� ��������������)\') {
        if ($(\'.trSelected\', grid).length > 0) {
            var items = $(\'.trSelected\', grid);
            var itemlist = \'\';
            for (i = 0; i < items.length; i++) {
                itemlist += items[i].id.substr(3) + ",";
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=sklad_out&status=2&date=<?php echo $_GET['date']; ?>
<?php echo '",
                data: "items=" + itemlist,
                success: function (data) {
                    $("#grid_table").flexReload();
                }});
        } else {
            return false;
        }

    } else if (com == \'���������\') {
        if ($(\'.trSelected\', grid).length > 0) {
            var items = $(\'.trSelected\', grid);
            var itemlist = \'\';
            for (i = 0; i < items.length; i++) {
                itemlist += items[i].id.substr(3) + ",";
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=sklad_ok&status=4&date=<?php echo $_GET['date']; ?>
&folder=<?php echo $_GET['folder']; ?>
<?php echo '",
                data: "items=" + itemlist,
                success: function (data) {
                    $("#grid_table").flexReload();
                }});
        } else {
            return false;
        }

    } else if (com == \'������\') {
        if ($(\'.trSelected\', grid).length > 0) {
            var items = $(\'.trSelected\', grid);
            var itemlist = \'\';
            for (i = 0; i < items.length; i++) {
                itemlist += items[i].id.substr(3) + ",";
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=sklad_sale&status=4&date=<?php echo $_GET['date']; ?>
&folder=<?php echo $_GET['folder']; ?>
<?php echo '",
                data: "items=" + itemlist,
                success: function (data) {
                    $("#grid_table").flexReload();
                }});
        } else {
            return false;
        }

    } else if (com == \'�����������\') {
        if ($(\'.trSelected\', grid).length > 0) {
            var items = $(\'.trSelected\', grid);
            var itemlist = \'\';
            for (i = 0; i < items.length; i++) {
                itemlist += items[i].id.substr(3) + ",";
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=sklad_print&status=4&date=<?php echo $_GET['date']; ?>
&folder=<?php echo $_GET['folder']; ?>
<?php echo '",
                data: "items=" + itemlist,
                success: function (data) {
                    $("#grid_table").flexReload();
                    window.open(\'http://'; ?>
<?php echo @BASE_URL; ?>
<?php echo '/images/uploads/print_all.html\', \'print\');
                }});
        } else {
            return false;
        }

    } else if (com == \'Print-Logistics\') {
        if ($(\'.trSelected\', grid).length > 0) {
            var items = $(\'.trSelected\', grid);
            var itemlist = \'\';
            for (i = 0; i < items.length; i++) {
                itemlist += items[i].id.substr(3) + ",";
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=sklad_print_logistics&status=4&date=<?php echo $_GET['date']; ?>
&folder=<?php echo $_GET['folder']; ?>
<?php echo '",
                data: "items=" + itemlist,
                success: function (data) {
                    $("#grid_table").flexReload();
                    window.open(\'http://'; ?>
<?php echo @BASE_URL; ?>
<?php echo '/images/uploads/print_all_logistics.html\', \'print\');
                }});
        } else {
            return false;
        }

    } else if (com == \'�����������\') {
        if ($(\'.trSelected\', grid).length > 0) {
            var items = $(\'.trSelected\', grid);
            var itemlist = \'\';
            for (i = 0; i < items.length; i++) {
                itemlist += items[i].id.substr(3) + ",";
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=sklad_print_post&status=4&date=<?php echo $_GET['date']; ?>
&folder=<?php echo $_GET['folder']; ?>
<?php echo '",
                data: "items=" + itemlist,
                success: function (data) {
                    $("#grid_table").flexReload();
                    window.open(\'http://'; ?>
<?php echo @BASE_URL; ?>
<?php echo '/images/uploads/print_all_post.html\', \'print\');
                }});
        } else {
            return false;
        }

    } else if (com == \'������� � �����\') {
        if ($(\'.trSelected\', grid).length > 0) {
            var items = $(\'.trSelected\', grid);
            var itemlist = \'\';
            for (i = 0; i < items.length; i++) {
                itemlist += items[i].id.substr(3) + ",";
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=folder_out&status=<?php echo $_GET['status']; ?>
&date=<?php echo $_GET['date']; ?>
<?php echo '",
                data: "items=" + itemlist,
                success: function (data) {
                    $("#grid_table").flexReload();
                }});
        } else {
            return false;
        }

    } else if (com == \'���������\') {

        if ($(\'.trSelected\', grid).length > 0) {
            var items = $(\'.trSelected\', grid);
            var itemlist = \'\';
            for (i = 0; i < items.length; i++) {
                itemlist += items[i].id.substr(3) + ",";
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=get_sklad<?php echo '",
                data: "items=" + itemlist,
                success: function (data) {
                    window.location = \'http://'; ?>
<?php echo @BASE_URL; ?>
<?php echo '/images/uploads/note_sklad.csv\';
                    $("#grid_table").flexReload();
                }});
        } else {
            return false;
        }

    } else if (com == \'ru-courier\') {

        if ($(\'.trSelected\', grid).length > 0) {
            var items = $(\'.trSelected\', grid);
            var itemlist = \'\';
            for (i = 0; i < items.length; i++) {
                itemlist += items[i].id.substr(3) + ",";
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=get_sklad_courier<?php echo '",
                data: "items=" + itemlist,
                success: function (data) {
                    window.location = \'http://'; ?>
<?php echo @BASE_URL; ?>
<?php echo '/images/uploads/enigme-ru-courier.xml\';
                    $("#grid_table").flexReload();
                }});
        } else {
            return false;
        }
    } else if (com == \'ru-courier-2\') {

        if ($(\'.trSelected\', grid).length > 0) {
            var items = $(\'.trSelected\', grid);
            var itemlist = \'\';
            for (i = 0; i < items.length; i++) {
                itemlist += items[i].id.substr(3) + ",";
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=get_sklad_courier_2<?php echo '",
                data: "items=" + itemlist,
                success: function (data) {
                    window.location = \'http://'; ?>
<?php echo @BASE_URL; ?>
<?php echo '/images/uploads/enigme-ru-courier-2.xml\';
                    $("#grid_table").flexReload();
                }});
        } else {
            return false;
        }

    } else if (com == \'IM-Logistics\') {

        if ($(\'.trSelected\', grid).length > 0) {
            var items = $(\'.trSelected\', grid);
            var itemlist = \'\';
            for (i = 0; i < items.length; i++) {
                itemlist += items[i].id.substr(3) + ",";
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=get_sklad_logistics<?php echo '",
                data: "items=" + itemlist,
                success: function (data) {
                    window.location = \'http://'; ?>
<?php echo @BASE_URL; ?>
<?php echo '/images/uploads/enigme-logistics.csv\';
                    $("#grid_table").flexReload();
                }});
        } else {
            return false;
        }
    } else if (com == \'�����\') {

        if ($(\'.trSelected\', grid).length > 0) {
            var items = $(\'.trSelected\', grid);
            var itemlist = \'\';
            for (i = 0; i < items.length; i++) {
                itemlist += items[i].id.substr(3) + ",";
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=get_sklad_post<?php echo '",
                data: "items=" + itemlist,
                success: function (data) {
                    window.location = \'http://'; ?>
<?php echo @BASE_URL; ?>
<?php echo '/images/uploads/enigme-post.csv\';
                    $("#grid_table").flexReload();
                }});
        } else {
            return false;
        }

    } else if (com == \'�������� ���\') {
        $(\'.bDiv tbody tr\', grid).addClass(\'trSelected\');

    } else if (com == \'����� ���������\') {
        $(\'.bDiv tbody tr\', grid).removeClass(\'trSelected\');
    }
}
</script>
'; ?>


<table border="0" cellpadding="5" cellspacing="0" style="float: left">
    <!--<tr>
<td width="210">
	<table id="table_find" width="200" border="0" cellpadding="0" cellspacing="3" style="border: 1px #3e726e solid; background-color: #f4edc9;color:#3e726e;">
		<tr>
			<td width="100%">
				<select size="4" name="sel_status" id="sel_status" style="width:100%;color:#000;padding:5px;background-color: #fff;font-size:10pt;font-weight : bold;">
					<option value="2" <?php if ($_GET['status'] == '2'): ?>selected<?php endif; ?>>��������������</option>
				</select>
			</td>
		</tr>
	</table>
</td>-->

    <td align="left" height="22" width="250" style="border-right:1px solid grey;">
        �������� ����:
        <input style="border:1px solid blue;width:70px" type="text" name="date1" id="date1" value="<?php echo $_GET['date']; ?>
"/>
        &nbsp;<input style="border:1px solid grey;" type="button" id="sel_date" value="�������">
        <!--<?php if ($_GET['date'] != ''): ?>
	&nbsp;&nbsp;
	<input style="border:1px solid red;" type="button" id="add_folder" value="������� �����">
	<?php endif; ?>-->
    </td>

    <td style="padding-left:20px;border-right:1px solid grey;" id="folders" width="250">
        <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['folders']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
            <div class="folders">
                <a href="?module=<?php echo $_GET['module']; ?>
&action=mod&status=4&date=<?php echo $_GET['date']; ?>
&folder=<?php echo $this->_tpl_vars['folders'][$this->_sections['i']['index']]['id']; ?>
"
                   <?php if ($this->_tpl_vars['folders'][$this->_sections['i']['index']]['id'] == $_GET['folder']): ?>style="color:orange"<?php endif; ?>>
                    <strong>����� (<?php echo $this->_tpl_vars['folders'][$this->_sections['i']['index']]['folder_count']; ?>
)</strong>
                </a>
            </div>
        <?php endfor; endif; ?>
    </td>


    <td align="left" valign="top" width="170" style="padding-left:20px">
        <?php if ($this->_tpl_vars['folder_count'] == '0' && $_GET['status'] == '4'): ?>
            <div><a style="color:red;"
                    href="?worker=<?php echo $_GET['module']; ?>
&action=delete_folder&status=<?php echo $_GET['status']; ?>
&date=<?php echo $_GET['date']; ?>
&folder=<?php echo $_GET['folder']; ?>
">&laquo;&nbsp;�������
                    ������ �����</a></div>
        <?php endif; ?>

        <div>
            <a href="?module=<?php echo $_GET['module']; ?>
&action=mod&status=5&date=<?php echo $_GET['date']; ?>
&folder=<?php echo $_GET['folder']; ?>
"
               <?php if ($_GET['status'] == '5'): ?>style="color:orange"<?php endif; ?>>&laquo;&nbsp;����������� ������</a></div>
        <div>
            <a href="?module=<?php echo $_GET['module']; ?>
&action=mod&status=6&date=<?php echo $_GET['date']; ?>
&folder=<?php echo $_GET['folder']; ?>
"
               <?php if ($_GET['status'] == '6'): ?>style="color:orange"<?php endif; ?>>&laquo;&nbsp;�������� ������</a></div>
    </td>

    </tr>
</table>


<a class="button" onclick="toggleAromat();return false;" href="">����� �������</a><br>


<div style="display:none;" class="ui-tabs-panel" id="aromat_finder">

    <table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add">

        <tr class="table_bg">
            <td align="left" colspan="2" class="brend_liter">
                <div class="s_letters">
                    <a>A</a><a>B</a><a>C</a><a>D</a>
                    <a>E</a><a>F</a><a>G</a><a>H</a>
                    <a>I</a><a>J</a><a>K</a><a>L</a>
                    <a>M</a><a>N</a><a>O</a><a>P</a>
                    <a>Q</a><a>R</a><a>S</a><a>T</a>
                    <a>U</a><a>V</a><a>W</a><a>X</a>
                    <a>Y</a><a>Z</a>
                </div>
            </td>
        </tr>

        <tr>
            <td align="center" width="30%" bgcolor="White">
                <div id="msg_order_img">image</div>
            </td>
            <td align="left" width="70%">
                <select size="10" name="sel_brend" id="sel_brend" class="sel_data">
                    <option value="0">-- �������� ����� --</option>
                    <?php echo $this->_tpl_vars['sel_brend']; ?>

                </select>
            </td>
        </tr>
        <tr>
            <td align="right"><h2 style="margin:0;">������:</h2></td>
            <td align="left">
                <span id="msg_order_title"></span> <span id="msg_order_pol"></span>
            </td>
        </tr>
    </table>

    <table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add" style="border-top:0px;">
        <tr>
            <td align="left">
                <a id="ar_comment_form_add" style="display:none;cursor:pointer;color:gray">-= <strong>��������</strong>
                    �������� / ������ =-</a>

                <div id="ar_comment_form" style="display:none;">
                    <textarea name="ar_comment_text" id="ar_comment_text" style="width:100%" rows=3></textarea>

                    <input type="button" name="ar_wiki" id="ar_wiki" value="��������" style="color:blue">
                    <input type="button" name="ar_error" id="ar_error" value="������" style="color:red">
                </div>
            </td>
        </tr>
    </table>

    <table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add" style="border-top:0px;">
        <tr>
            <td align="left">
                <div id="ar_comment"></div>
            </td>
        </tr>
    </table>

    <table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add" style="border-top:0px;">
        <tr>
            <td align="left">
                <div id="msg_order"></div>
            </td>
        </tr>
    </table>


    <div id="msg"></div>
    <div id="msg1"></div>
    <div id="msg2"></div>

</div>


<br/>
<div style="clear: both"></div>


<table id="grid_table" class="flexigrid" style="display:none;float:left"></table>


<?php echo '
    <script type="text/javascript">
        Calendar.setup({
            inputField: "date1",
            ifFormat: "%Y-%m-%d",
            timeFormat: "24",
            button: "date1",
            singleClick: true,
            step: 1,
            showsTime: false
        });
    </script>
'; ?>

<div class="modal modal__black fade" id="download__modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">���������� ��������</h4>
            </div>
            <div class="modal-body">
                <p class="hidden text-warning"></p>
                <ul class="list-unstyled" id="download__list" style="margin-bottom: 0;">
                    <li class="option"><span class="option-title" data-action="get_sklad" data-url="note_sklad.csv">CSV ���������</span></li>
                    <li class="option"><span class="option-title" data-action="sklad_print" data-url="print_all.html" data-addict="&status=4&date=<?php echo $_GET['date']; ?>
&folder=<?php echo $_GET['folder']; ?>
">�����������</span></li>
                    <li class="option"><span class="option-title" data-action="sklad_print_logistics" data-url="print_all_logistics.html" data-addict="&status=4&date=<?php echo $_GET['date']; ?>
&folder=<?php echo $_GET['folder']; ?>
">���������� ��� IM-Logistics</span></li>
                    <!--li class="option"><span class="option-title" data-action="sklad_print_post" data-url="print_all_post.html" data-addict="&status=4&date=<?php echo $_GET['date']; ?>
&folder=<?php echo $_GET['folder']; ?>
">���������� ��� ����� ������</span></li-->
                    <li class="divider option"></li>
                    <li class="option"><span class="option-title" data-action="get_sklad_logistics" data-url="enigme-logistics.csv">CSV ��� IM-Logistics</span></li>
                    <li class="option"><span class="option-title" data-action="get_post_service" data-url="enigme-postservice.csv">��������� ������</span></li>
                    <!--li class="option"><span class="option-title" data-action="get_sklad_courier" data-url="enigme-ru-courier.xml">XML ��� Ru-Courier</span></li-->
                    <li class="option"><span class="option-title" data-action="get_sklad_courier_2" data-url="enigme-ru-courier-2.xml">XML ��� Ru-Courier</span></li>
                    <li class="option"><span class="option-title" data-action="get_sklad_post" data-url="enigme-post.csv">CSV ��� ����� ������</span></li>
                    <li class="option"><span class="option-title" data-action="get_sklad_bxb" data-url="enigme-boxberry.xml">XML ��� Boxberry</span></li>
                </ul>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>