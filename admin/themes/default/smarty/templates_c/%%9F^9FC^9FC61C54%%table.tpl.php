<?php /* Smarty version 2.6.18, created on 2016-11-19 13:12:34
         compiled from modules/mag_orders/table.tpl */ ?>
<?php echo '

<style type="text/css">
    #grid_table tr {height: 55px;}
</style>

<script type="text/javascript">

$(document).ready(function(){
    
    $("#cause_text").bind(\'input propertychange\', function(){
        if(this.value.length){
            $("#cause_0").attr(\'checked\', \'checked\');
        } 
    });
   
    $("#cause_0").change(function(){
       if($("#cause_text").val().length){
            $("#cause_0").attr(\'checked\', \'checked\');
            return false;
       } 
    });

	$("#sel_status").change(function () {
		var sel = $("#sel_status option:selected");
		window.location = \'?main=mag&module=mag_orders&action=mod&status=\' + sel.val();
	});

	/*$("#filter_ext").click(function () {
		var sel = $("#sel_status option:selected");
		var date1 = $("#date1").val();
		var date2 = $("#date2").val();
		window.location = \'?main=mag&module=mag_orders&action=mod&status=\' + sel.val() + \'&date1=\' + date1 + \'&date2=\' + date2;
	});*/


	$("#grid_table").flexigrid({
		url: \''; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=mod&status=<?php echo $_GET['status']; ?>
&is_sale=<?php echo $_GET['is_sale']; ?>
<?php echo '\',
		dataType: \'json\',
		colModel : [
            {display: \'ID\', 		    name : \'id\', 		width : 70, 	sortable : false, 	align: \'center\'},
            {display: \'���� ������\',    name : \'DateAdd\', 	width : 150, 	sortable : true, 	align: \'center\'},
            {display: \'���� ��������\',  name : \'DateEnd\',   width : 100, 	sortable : true, 	align: \'center\'},
            {display: \'��������\', 	    name : \'action\', 	width : 100, 	sortable : false, 	align: \'center\'},
            {display: \'������\', 	    name : \'title\', 	width : 200, 	sortable : false, 	align: \'left\'},
            {display: \'PRICE\', 		    name : \'price\', 	width : 100, 	sortable : false, 	align: \'right\'},
            {display: \'SUM\', 		    name : \'sum\', 		width : 40, 	sortable : false, 	align: \'left\'},
            {display: \'������� ������\', name : \'cause\', 	width : 200, 	sortable : false, 	align: \'left\'}
		],
		buttons : [
		'; ?>
<?php if ($_GET['status'] == '1'): ?><?php echo '
			{name: \'�������� ���\', bclass: \'check\', onpress : test},{separator: true},
			{name: \'����� ���������\', bclass: \'uncheck\', onpress : test},{separator: true},{separator: true},{separator: true},{name: \'�������� � ����������� �������� <strong>&#9658;</strong>\', bclass: \'\'},{separator: true},
			{name: \'�����������\', bclass: \'\', onpress : test},{separator: true},
			{name: \'���������\', bclass: \'\', onpress : test},{separator: true},
		'; ?>
<?php endif; ?><?php echo '
		
		'; ?>
<?php if ($_GET['status'] == '2'): ?><?php echo '
			{name: \'�������� ���\', bclass: \'check\', onpress : test},{separator: true},
			{name: \'����� ���������\', bclass: \'uncheck\', onpress : test},{separator: true},{separator: true},{separator: true},{name: \'�������� � ����������� �������� <strong>&#9658;</strong>\', bclass: \'\'},{separator: true},
			'; ?>
<?php if ($_SESSION['admin_user_login'] == 'admin'): ?><?php echo '{name: \'���������\', bclass: \'\', onpress : test},{separator: true},'; ?>
<?php endif; ?><?php echo '
			'; ?>
<?php if ($_SESSION['admin_user_login'] == 'truth4oll'): ?><?php echo '{name: \'���������\', bclass: \'\', onpress : test},{separator: true},'; ?>
<?php endif; ?><?php echo '
			{name: \'���������\', bclass: \'\', onpress : test},{separator: true},
			{separator: true},{separator: true},{name: \'� �������\', bclass: \'\', onpress : test},{separator: true},
		'; ?>
<?php endif; ?><?php echo '
		
		'; ?>
<?php if ($_GET['status'] == '5' || $_GET['status'] == '6'): ?><?php echo '
			{name: \'�������� ���\', bclass: \'check\', onpress : test},{separator: true},
			{name: \'����� ���������\', bclass: \'uncheck\', onpress : test},{separator: true},{separator: true},{separator: true},{name: \'�������� � ����������� �������� <strong>&#9658;</strong>\', bclass: \'\'},{separator: true},
			{name: \'���������\', bclass: \'\', onpress : test},{separator: true},
		'; ?>
<?php endif; ?><?php echo '
            {name: \'�������� � ��������\', bclass: \'\', onpress : orderActions},{separator: true},{separator: true}
		/*'; ?>
<?php if ($_GET['status'] == '-1'): ?><?php echo '{name: \'������� � �����\', bclass: \'\', onpress : test},{separator: true},'; ?>
<?php endif; ?><?php echo '*/
		],
		searchitems : [
		    {display: \'ID ������\', name : \'id\', isdefault: true},
		    {display: \'ID �����\', name : \'id_user\', isdefault: false}
		],
		sortname: "DateAdd",
		sortorder: "desc",
		usepager: true,
		showTableToggleBtn: true,
		width: \'auto\',
		height: \'auto\',
		useRp: true,
		rpOptions: [5,30,50,100],
		rp: '; ?>
<?php if ($_GET['status'] == '0' || $_GET['status'] == '3'): ?>5<?php else: ?>30<?php endif; ?><?php echo ',
		pagestat: \'�������� � {from} �� {to} �� {total} �������\',
		procmsg: \'���������, ����������, ��������� ...\',
		nomsg: \'��� ���������\'
	});
});

function getCause(grid){
        $.ajax({
				type: "GET",
				dataType: "json",
				url: "?ajax=mag_cancel_cause&action=get",
				success: function(data){
                    $.each(data, function(id,title){
                        $(\'#cause_list\').append(\'<input class="ccause" type="checkbox" name="cause_\'+id+\'" id="cause_\'+id+\'" />&nbsp;<label>\'+title+\'</label><br />\');
                    })
				}});
                tb_show("HAI","#TB_inline?height=400&amp;width=600&amp;inlineId=dialog-form&modal=true",null);
                
}

function checkCause(){
    var ret = false;
    $(\'.ccause\').each(function(){
        if(this.checked){
            ret = true;  
        }                
    });
    return ret;
}

function pushCause(){
            if(!checkCause()){
                alert(\'���������� ������� ���� �� ���� �������!\');
                return false;
            }
            
            grid = $("#grid_table");
            var items = $(\'.trSelected\',grid);
			var itemlist =\'\';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
            $("#cancel_list").val(itemlist);
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=set_status&status=0<?php echo '",
				data: $("#cform").serialize(),
				success: function(data){
				    clearCause();
					$("#grid_table").flexReload();
					document.location.reload();
				}});
} 

function clearCause(){
    tb_remove();
    $(\'#cause_list\').html(\'\');
    $(\'#cause_text\').val(\'\');
    $(\'#cause_0\').removeAttr(\'checked\');
    $("#cancel_list").val(\'\');
}

function test(com,grid){
	if (com==\'���������\'){
		if($(\'.trSelected\',grid).length>0){
		    url_cause = getCause();
		}else{
			return false;
		}

	}else if (com==\'�����������\'){
		if($(\'.trSelected\',grid).length>0){
			var items = $(\'.trSelected\',grid);
			var itemlist =\'\';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=set_status&status=2<?php echo '",
				data: "items="+itemlist,
				success: function(data){
					$("#grid_table").flexReload();
				}});
		}else{
			return false;
		}
		
	}else if (com==\'���������\'){
		if($(\'.trSelected\',grid).length>0){
			var items = $(\'.trSelected\',grid);
			var itemlist =\'\';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=set_status&status=3<?php echo '",
				data: "items="+itemlist,
				success: function(data){
					$("#grid_table").flexReload();
				}});
		}else{
			return false;
		}

	}else if (com==\'������� � �����\'){
		if($(\'.trSelected\',grid).length>0){
			var items = $(\'.trSelected\',grid);
			var itemlist =\'\';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=set_status&status=1<?php echo '",
				data: "items="+itemlist,
				success: function(data){
					$("#grid_table").flexReload();
				}});
		}else{
			return false;
		}

	}else if (com==\'� �������\'){
		if($(\'.trSelected\',grid).length>0){
			var items = $(\'.trSelected\',grid);
			var itemlist =\'\';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=note_add<?php echo '",
				data: "items="+itemlist,
				success: function(data){
					$("#grid_table").flexReload();
					$("#count_note").html(  parseInt($("#count_note").html()) + items.length );
				}});
		}else{
			return false;
		}


	}else if (com==\'�������� ���\'){
		$(\'.bDiv tbody tr\',grid).addClass(\'trSelected\');

	}else if (com==\'����� ���������\'){
		$(\'.bDiv tbody tr\',grid).removeClass(\'trSelected\');
	}
}
'; ?>

<?php $_from = $this->_tpl_vars['gaq']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>  
	<?php echo $this->_tpl_vars['item']; ?>

<?php endforeach; endif; unset($_from); ?>
</script>

<div id="dialog-form" style="display:none;" title="������� ������ ������">
  <p class="validateTips">������� ������� ���������� ������</p>
  <form id="cform">
  <fieldset id="cause_list" style="margin:0;padding:0;border:none;"></fieldset>
  <input type="checkbox" class="ccause" name="cause_0" id="cause_0" />&nbsp;<label>������ �������:</label><br />
  <textarea cols="75" rows="10" name="cause_text" id="cause_text"></textarea><br /><br />
  <input type="hidden" name="items" id="cancel_list" value="" />
  <input type="button" value="���������" onclick="pushCause()" />&nbsp;&nbsp;
  <input type="button" value="������" onclick="clearCause()" />
  </form>
</div>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="250">
	<table id="table_find" width="200" border="0" cellpadding="0" cellspacing="3" style="border: 1px #3e726e solid; background-color: #f4edc9;color:#3e726e;">
		<tr>
			<td width="100%">
				<select size="4" name="sel_status" id="sel_status" style="width:100%;color:#000;padding:5px;background-color: #fff;font-size:10pt;font-weight : bold;">
					<option value="1" <?php if ($_GET['status'] == '1'): ?>selected<?php endif; ?>>�����</option>
					<option value="2" <?php if ($_GET['status'] == '2'): ?>selected<?php endif; ?>>��������������</option>
					<option value="3" <?php if ($_GET['status'] == '3'): ?>selected<?php endif; ?>>������������</option>
					<option value="0" <?php if ($_GET['status'] == '0'): ?>selected<?php endif; ?>>�����������</option>
				</select>
			</td>
		</tr>
	</table>
</td>


<td style="padding-left:20px;border-right:1px solid grey;" id="folders" width="250">

	<h1>�����</h1>
	
	<div><a href="?module=<?php echo $_GET['module']; ?>
&action=mod&status=4&is_sale=1" <?php if ($_GET['status'] == '4'): ?>style="color:orange"<?php endif; ?>>&laquo;&nbsp;��������� ������</a></div>
	<div><a href="?module=<?php echo $_GET['module']; ?>
&action=mod&status=5" <?php if ($_GET['status'] == '5'): ?>style="color:orange"<?php endif; ?>>&laquo;&nbsp;����������� ������</a></div>
	<div><a href="?module=<?php echo $_GET['module']; ?>
&action=mod&status=6" <?php if ($_GET['status'] == '6'): ?>style="color:orange"<?php endif; ?>>&laquo;&nbsp;�������� ������</a></div>
</td>
<!--
<td align="left" valign="bottom">
			<a href="?main=mag&module=mag_orders&action=recal_disc">���������� ������</a>
</td>
-->
<td align="right" valign="bottom">
	<div><h1><a href="?main=mag&module=mag_note&action=mod">� �������� (<span id="count_note"><?php echo $this->_tpl_vars['order_note_count']; ?>
</span>)</a></h1></div>
</td>
</tr>
</table>


<br />

<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="display: block;">
                    ������ <span class="caret"></span>
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse">
            <div class="panel-body">
                <form id="orders_filter" method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="filter_id" class="col-xs-1 control-label">ID ������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_id-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_id-active" id="filter_id-active" data-type="id">
                                </span>
                                <input type="text" class="form-control" name="filter_id" id="filter_id" placeholder="ID ������� ������">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_userId" class="col-xs-1 control-label">ID �������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_id_user-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_id_user-active" id="filter_id_user-active" data-type="id_user">
                                </span>
                                <input type="text" class="form-control" name="filter_id_user" id="filter_id_user" placeholder="ID ������� �������">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_phone" class="col-xs-1 control-label">�������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_user__phone-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_user__phone-active" id="filter_user__phone-active" data-type="user__phone">
                                </span>
                                <input type="text" class="form-control" name="filter_user__phone" id="filter_user__phone" placeholder="������� �������">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_fio" class="col-xs-1 control-label">���</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_user__fio-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_user__fio-active" id="filter_user__fio-active" data-type="user__fio">
                                </span>
                                <input type="text" class="form-control" name="filter_user__fio" id="filter_user__fio" placeholder="��� �������">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_email" class="col-xs-1 control-label">Email</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_user__email-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_user__email-active" id="filter_user__email-active" data-type="user__email">
                                </span>
                                <input type="email" class="form-control" name="filter_user__email" id="filter_user__email" placeholder="Email �������">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_city" class="col-xs-1 control-label">�����</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_user__city-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_user__city-active" id="filter_user__city-active" data-type="user__city">
                                </span>
                                <input type="text" class="form-control" name="filter_user__city" id="filter_user__city" placeholder="����� �������">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_source" class="col-xs-1 control-label">�������� ������ �� ����������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_source-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_source-active" id="filter_source-active" data-type="source">
                                </span>
                                <select name="filter_source" id="filter_source" class="form-control">
                                    <option value="0">�� �����</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_manager" class="col-xs-1 control-label">�������� ������������� �� �������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_manager__login-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_manager__login-active" id="filter_manager__login-active" data-type="manager__login">
                                </span>
                                <select name="filter_manager__login" id="filter_manager__login" class="form-control">
                                    <option value="-1">�� �����</option>
                                    <option value="admin">admin</option>
                                    <option value="enCopy">enCopy</option>
                                    <option value="operator">operator</option>
                                    <option value="root">root</option>
                                    <option value="xander">xander</option>
                                    <option value="enCopy2">enCopy2</option>
                                    <option value="jannak">jannak</option>
                                    <option value="marinam">marinam</option>
                                    <option value="dianae">dianae</option>
                                    <option value="elenam">elenam</option>
                                    <option value="olesya">olesya</option>
                                    <option value="katarn">katarn</option>
                                    <option value="RaSeR">RaSeR</option>
                                    <option value="truth4oll">truth4oll</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_status" class="col-xs-1 control-label">������ ������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_status-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_status-active" id="filter_status-active" data-type="status">
                                </span>
                                <select name="filter_status" id="filter_status" class="form-control">
                                    <option value="-1">�� �����</option>
                                    <option value="0">�����������</option>
                                    <option value="1">�����</option>
                                    <option value="2">��������������</option>
                                    <option value="3">������������</option>
                                    <option value="4">� ���������/���������</option>
                                    <option value="5">�� ������ (�����������)</option>
                                    <option value="6">�� ������ (��������)</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_order-start" class="col-xs-1 control-label">���� ������</label>
                        <div class="col-xs-4">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_DateAdd-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_DateAdd-active" id="filter_DateAdd-active" data-type="DateAdd">
                                </span>
                                <input type="text" class="form-control form-datepicker" name="filter_DateAdd-start" id="filter_DateAdd-start" placeholder="���� ������ �">
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="text" class="form-control form-datepicker" name="filter_DateAdd-end" id="filter_DateAdd-end" placeholder="���� ������ ��">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_order-ks-start" class="col-xs-1 control-label">���� �������� � ��</label>
                        <div class="col-xs-4">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_DateSendCourier-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_DateSendCourier-active" id="filter_DateSendCourier-active" data-type="DateSendCourier">
                                </span>
                                <input type="text" class="form-control form-datepicker" name="filter_DateSendCourier-start" id="filter_DateSendCourier-start" placeholder="���� �������� � �� �">
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="text" class="form-control form-datepicker" name="filter_DateSendCourier-end" id="filter_DateSendCourier-end" placeholder="���� �������� � �� ��">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_order-delivery-start" class="col-xs-1 control-label">���� ��������</label>
                        <div class="col-xs-4">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_DateEnd-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_DateEnd-active" id="filter_DateEnd-active" data-type="DateEnd">
                                </span>
                                <input type="text" class="form-control form-datepicker" name="filter_DateEnd-start" id="filter_DateEnd-start" placeholder="���� �������� �">
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="text" class="form-control form-datepicker" name="filter_DateEnd-end" id="filter_DateEnd-end" placeholder="���� �������� ��">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_order-money-start" class="col-xs-1 control-label">���� ��������� �����</label>
                        <div class="col-xs-4">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_DateMoneyIncome-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_DateMoneyIncome-active" id="filter_DateMoneyIncome-active" data-type="DateMoneyIncome">
                                </span>
                                <input type="text" class="form-control form-datepicker" name="filter_DateMoneyIncome-start" id="filter_DateMoneyIncome-start" placeholder="���� ��������� ����� �">
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <input type="text" class="form-control form-datepicker" name="filter_DateMoneyIncome-end" id="filter_DateMoneyIncome-end" placeholder="���� ��������� ����� ��">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_subtotal" class="col-xs-1 control-label">��������� ��� ������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_Sum-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_Sum-active" id="filter_Sum-active" data-type="Sum">
                                </span>
                                <input type="text" class="form-control" name="filter_Sum" id="filter_Sum" placeholder="��������� ��� ������">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_total" class="col-xs-1 control-label">��������� �� �������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_dSum-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_dSum-active" id="filter_dSum-active" data-type="dSum">
                                </span>
                                <input type="text" class="form-control" name="filter_dSum" id="filter_dSum" placeholder="��������� �� �������">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_logistic" class="col-xs-1 control-label">������ ��������� � ������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_logistic-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_logistic-active" id="filter_logistic-active" data-type="logistic">
                                </span>
                                <select name="filter_logistic" id="filter_logistic" class="form-control">
                                    <option value="-1">�� �����</option>
                                    <option value="0">�� ��������</option>
                                    <option value="1">���������</option>
                                    <option value="2">�������</option>
                                    <option value="3">������ ��������</option>
                                    <option value="4">�������</option>
                                    <option value="5">������� �������</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_product-articul" class="col-xs-1 control-label">������� ������ � ������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_product__articul-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_product__articul-active" id="filter_product__articul-active" data-type="product__articul" disabled>
                                </span>
                                <input type="text" class="form-control" name="filter_product__articul" id="filter_product__articul" placeholder="������� ������ � ������">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_couriersAddress__delivery_type" class="col-xs-1 control-label">��� ��������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_couriersAddress__delivery_type-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_couriersAddress__delivery_type-active" id="filter_couriersAddress__delivery_type-active" data-type="couriersAddress__delivery_type">
                                </span>
                                <select name="filter_couriersAddress__delivery_type" id="filter_couriersAddress__delivery_type" class="form-control">
                                    <option value="-1">�� �����</option>
                                    <option value="��������">��������</option>
                                    <option value="���������">���������</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_couriers__couriersId" class="col-xs-1 control-label">���������� ������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_couriers__couriersId-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_couriers__couriersId-active" id="filter_couriers__couriersId-active" data-type="couriers__couriersId">
                                </span>
                                <select name="filter_couriers__couriersId" id="filter_couriers__couriersId" class="form-control">
                                    <option value="-1">�� �����</option>
                                    <option value="2">IM-Logistics</option>
                                    <option value="3">B2Bpl</option>
                                    <option value="4">DPD</option>
                                    <option value="5">����� ������</option>
                                    <option value="6">Enigme Express</option>
                                    <option value="7">Boxberry</option>
                                    <option value="8">Top-Delivery</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_delivery_cost_fact" class="col-xs-1 control-label">������ �� ��������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_delivery_cost_fact-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_delivery_cost_fact-active" id="filter_delivery_cost_fact-active" data-type="delivery_cost_fact">
                                </span>
                                <input type="text" class="form-control" name="filter_delivery_cost_fact" id="filter_delivery_cost_fact" placeholder="������ �� ��������">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_payment_type" class="col-xs-1 control-label">��� ������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_payment_type-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_payment_type-active" id="filter_payment_type-active" data-type="payment-type">
                                </span>
                                <select name="filter_payment_type" id="filter_payment_type" class="form-control">
                                    <option value="-1">�� �����</option>
                                    <option value="2">���������� �� ��</option>
                                    <option value="1">�������� ��� ���������</option>
                                    <option value="3">���������</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_productOrders__title" class="col-xs-1 control-label">�������� ������ � ������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_productOrders__title-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_productOrders__title-active" id="filter_productOrders__title-active" data-type="product__title" disabled>
                                </span>
                                <input type="text" class="form-control" name="filter_productOrders__title" id="filter_productOrders__title" placeholder="����� �� �������� ������ � ������">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_firm" class="col-xs-1 control-label">�����</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_firm-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_firm-active" id="filter_firm-active" data-type="firm">
                                </span>
                                <select name="filter_firm" id="filter_firm" class="form-control">
                                    <option value="-1">�� �����</option>
                                    <option value="plus">������</option>
                                    <option value="web">���-�������</option>
                                    <option value="yandex">������ ����� yandex</option>
                                    <option value="card">������ ������</option>
                                    <option value="kassa">������ ����� �����</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_track" class="col-xs-1 control-label">����� �����������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_track-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_track-active" id="filter_track-active" data-type="track">
                                </span>
                                <input type="text" name="filter_track" id="filter_track" class="form-control" placeholder="����� �� ������ �����������">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_deliveryTarif" class="col-xs-1 control-label">�����</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_deliveryTarif-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_deliveryTarif-active" id="filter_deliveryTarif-active" data-type="deliveryTarif">
                                </span>
                                <input type="text" name="filter_deliveryTarif" id="filter_deliveryTarif" class="form-control" placeholder="����� �� ������� ��������">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="filter_deliveryIns" class="col-xs-1 control-label">���������</label>
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <label for="filter_deliveryIns-active" class="sr-only"></label>
                                    <input type="checkbox" name="filter_deliveryIns-active" id="filter_deliveryIns-active" data-type="deliveryIns">
                                </span>
                                <input type="text" name="filter_deliveryIns" id="filter_deliveryIns" class="form-control" placeholder="����� �� ��������� ��������">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions text-right">
                        <button class="btn btn-default" type="button" id="make__download">��������</button>
                        <button class="btn btn-primary" id="do__filter" type="submit">���������</button>
                    </div>
                </form>
                <?php echo '
                <script>

                    $("#do__filter").on(\'click\', function(e) {
                        e.preventDefault();

                        var $this           = $(this),
                            f               = $this.parents(\'form\'),
                            addictParams    = [];

                        $.each(f.find("input, select"), function(i, item) {

                            var $item = $(item);

                            if($item.attr("type") == "checkbox") {
                                var checked = $item.is(":checked") ? 1 : 0;
                                addictParams.push($item.attr("name")+"="+checked);
                            } else {
                                addictParams.push($item.attr("name")+"="+encodeURIComponent($item.val()));
                            }
                        });

                        $.ajax({
                            type: \'get\',
                            url: '; ?>
'?ajax=<?php echo $_GET['module']; ?>
&action=mod&status=<?php echo $_GET['status']; ?>
&is_sale=<?php echo $_GET['is_sale']; ?>
&'+addictParams.join("&")<?php echo ',
                            data: {
                                get_only_keys: 1
                            },
                            success: function(response) {

                                if (response != null) {

                                    var $wrapper = $(\'#flex_wrapper\');
                                    $wrapper.empty();
                                    $wrapper.append("<table id=\'grid_table\' class=\'flexigrid\'></table>");

                                    $("#grid_table").flexigrid({
                                        url: '; ?>
'?ajax=<?php echo $_GET['module']; ?>
&action=mod&status=<?php echo $_GET['status']; ?>
&is_sale=<?php echo $_GET['is_sale']; ?>
&'+addictParams.join("&")<?php echo ',
                                        dataType: \'json\',
                                        colModel : response,
                                        buttons : [
                                            '; ?>
<?php if ($_GET['status'] == '1'): ?><?php echo '
                                            {name: \'�������� ���\', bclass: \'check\', onpress : test},{separator: true},
                                            {name: \'����� ���������\', bclass: \'uncheck\', onpress : test},{separator: true},{separator: true},{separator: true},{name: \'�������� � ����������� �������� <strong>&#9658;</strong>\', bclass: \'\'},{separator: true},
                                            {name: \'�����������\', bclass: \'\', onpress : test},{separator: true},
                                            {name: \'���������\', bclass: \'\', onpress : test},{separator: true},
                                            '; ?>
<?php endif; ?><?php echo '

                                            '; ?>
<?php if ($_GET['status'] == '2'): ?><?php echo '
                                            {name: \'�������� ���\', bclass: \'check\', onpress : test},{separator: true},
                                            {name: \'����� ���������\', bclass: \'uncheck\', onpress : test},{separator: true},{separator: true},{separator: true},{name: \'�������� � ����������� �������� <strong>&#9658;</strong>\', bclass: \'\'},{separator: true},
                                                '; ?>
<?php if ($_SESSION['admin_user_login'] == 'admin'): ?><?php echo '{name: \'���������\', bclass: \'\', onpress : test},{separator: true},'; ?>
<?php endif; ?><?php echo '
                                            {name: \'���������\', bclass: \'\', onpress : test},{separator: true},
                                            {separator: true},{separator: true},{name: \'� �������\', bclass: \'\', onpress : test},{separator: true},
                                            '; ?>
<?php endif; ?><?php echo '

                                            '; ?>
<?php if ($_GET['status'] == '5' || $_GET['status'] == '6'): ?><?php echo '
                                            {name: \'�������� ���\', bclass: \'check\', onpress : test},{separator: true},
                                            {name: \'����� ���������\', bclass: \'uncheck\', onpress : test},{separator: true},{separator: true},{separator: true},{name: \'�������� � ����������� �������� <strong>&#9658;</strong>\', bclass: \'\'},{separator: true},
                                            {name: \'���������\', bclass: \'\', onpress : test},{separator: true},
                                            '; ?>
<?php endif; ?><?php echo '
                                            {name: \'�������� � ��������\', bclass: \'\', onpress : orderActions},{separator: true},{separator: true}
                                        ],
                                        usepager: true,
                                        showTableToggleBtn: true,
                                        width: \'auto\',
                                        height: \'auto\',
                                        useRp: true,
                                        rpOptions: [5,30,50,100],
                                        rp: '; ?>
<?php if ($_GET['status'] == '0' || $_GET['status'] == '3'): ?>5<?php else: ?>30<?php endif; ?><?php echo ',
                                        pagestat: \'�������� � {from} �� {to} �� {total} �������\',
                                        procmsg: \'���������, ����������, ��������� ...\',
                                        nomsg: \'��� ���������\'
                                    });
                                } else {
                                    $("#grid_table").flexOptions({
                                        url: '; ?>
'?ajax=<?php echo $_GET['module']; ?>
&action=mod&status=<?php echo $_GET['status']; ?>
&is_sale=<?php echo $_GET['is_sale']; ?>
&'+addictParams.join("&")<?php echo '
                                    }).flexReload();
                                }
                            }
                        });
                    });
                </script>
                '; ?>

            </div>
        </div>
    </div>
</div>
<div id="flex_wrapper">
    <table id="grid_table" class="flexigrid" style="display:none"></table>
</div>
<div class="modal modal__black fade" id="download__modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">���������� ��������</h4>
            </div>
            <div class="modal-body">
                <p class="hidden text-warning"></p>
                <ul class="list-unstyled" id="download__list" style="margin-bottom: 0;">
                    <li class="option"><span class="option-title" data-action="get_sklad" data-url="note_sklad.csv">CSV ���������</span></li>
                    <li class="option"><span class="option-title" data-action="sklad_print" data-url="print_all.html" data-addict="&status=4&date=<?php echo $_GET['date']; ?>
&folder=<?php echo $_GET['folder']; ?>
">�����������</span></li>
                    <li class="option"><span class="option-title" data-action="sklad_print_logistics" data-url="print_all_logistics.html" data-addict="&status=4&date=<?php echo $_GET['date']; ?>
&folder=<?php echo $_GET['folder']; ?>
">���������� ��� IM-Logistics</span></li>
                    <!--li class="option"><span class="option-title" data-action="sklad_print_post" data-url="print_all_post.html" data-addict="&status=4&date=<?php echo $_GET['date']; ?>
&folder=<?php echo $_GET['folder']; ?>
">���������� ��� ����� ������</span></li-->
                    <li class="divider option"></li>
                    <li class="option"><span class="option-title" data-action="get_sklad_logistics" data-url="enigme-logistics.csv">CSV ��� IM-Logistics</span></li>
                    <li class="option"><span class="option-title" data-action="get_post_service" data-url="enigme-postservice.csv">��������� ������</span></li>
                    <!--li class="option"><span class="option-title" data-action="get_sklad_courier" data-url="enigme-ru-courier.xml">XML ��� Ru-Courier</span></li-->
                    <li class="option"><span class="option-title" data-action="get_sklad_courier_2" data-url="enigme-ru-courier-2.xml">XML ��� Ru-Courier</span></li>
                    <li class="option"><span class="option-title" data-action="get_sklad_post" data-url="enigme-post.csv">CSV ��� ����� ������</span></li>
                    <li class="option"><span class="option-title" data-action="get_sklad_bxb" data-url="enigme-boxberry.xml">XML ��� Boxberry</span></li>
                </ul>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>