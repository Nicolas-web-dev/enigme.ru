<?php /* Smarty version 2.6.18, created on 2016-03-11 14:58:53
         compiled from modules/mag_orders/edit_reg.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "calendar.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo ''; ?><?php echo '
<style type="text/css">
    #map {
        min-height: 400px;
        background: #f5f5f5;
        border: 1px solid #c8c6c7;
        padding: 15px;
        margin-bottom: 30px;
    }
* {
	font-family: Tahoma !important
}
.log_wrap {
	height: 233px;
	padding: 5px;
	font-size: 12pt;
	background: white;
	overflow-y: scroll;
	border: 1px solid silver; 
}
.form_add td {
	color: black !important
}
.s_letters {
	font-size: 13px
}
.fl_left {
	float: left;
}
.sel_data {
	font-size: 12pt;
}
.clear {
	clear: both;
}
.line {
	margin-bottom: 15px;
}
.dostavka_item {
	background: #f5f5f5;
	border: 1px solid #c8c6c7;
	padding: 15px;
	position: relative;
	margin-bottom: 15px; 
}
.dostavka_item .head {
	color: #969696
}
.dostavka_item .title {
	font-size: 16px;
	font-weight: bold;
	margin-bottom: 10px;
}
.dostavka_item__checkbox {
	padding-left: 48px;
	position: relative;
	cursor: pointer;
	height: 33px;
	line-height: 33px;
}
.dostavka_item__checkbox.checked:after {
	background: url(/images/cart_v2/checkbox.png) 0px 0 no-repeat;
}
.dostavka_item__checkbox.gray:hover:after {
	background: url(/images/cart_v2/checkbox.png) 100% 0 no-repeat;
}
.dostavka_item__checkbox.checked.gray:after {
	background: url(/images/cart_v2/checkbox.png) -49px 0 no-repeat;
}
.dostavka_item__checkbox.checked.gray:hover:after {
	background: url(/images/cart_v2/checkbox.png) -49px 0 no-repeat;
}
.dostavka_item__checkbox:hover:after {
	background: url(/images/cart_v2/checkbox.png) -49px 0 no-repeat;
}
.dostavka_item__checkbox.checked:hover:after {
	background: url(/images/cart_v2/checkbox.png) 0px 0 no-repeat;
}
.dostavka_item__checkbox:after {
	display: block;
	position: absolute;
	content: \'\';
	width: 33px;
	background: url(/images/cart_v2/checkbox.png) 100% 0 no-repeat;
	height: 33px;
	left: 0;
	top: 0;
}
.ui-tabs-nav .ui-tabs-selected a span {
	font-size: 14px;
}
.text td {
	font-size: 14px !important;
	line-height:20px;
}
	.abs_sel {
		font-size: 14px;
		
	}
	#msg3 {margin-bottom:20px;}
	#msg3 h4,#msg3 .abs_sel a {
		color: #f55d5c;
	}
	#msg4 h4, #msg4 .abs_sel a {
		color: #569ddb;
	}
	.brend_liter a {
		background-color:none !important;
		color:#000 !important;
		padding:5px;
		font-weight: bold;
		cursor:pointer;
	}
	.brend_liter a:hover {
		background-color:#000 !important;
		color:#fff !important;
		cursor:pointer;
	}

	.cat_item {
		width:80px;
		float:left;
		height:150px;
	}
	#sel_brend{font:12pt Arial;}

	.sel_data {width:100%;font-size:11px;color:#000;background:#fff;padding:2px;}
	.sel_catalog_data{cursor:pointer;}
	.sel_catalog_data_order{cursor:pointer;color:blue;}
	.sel_find_users{cursor:pointer;}
	.up {cursor:pointer;}
	.dn{cursor:pointer;}

	.mod_table td{background:#fff;}
	.mod_table_com td{background:#f7f7f7;}
.dostavka_tooltip {
	position: absolute;
	background: #00cbf6;
	color: white;
	text-align: center;
	padding: 15px;
	display: none;
	width: 100px;
	z-index: 1000000;
}
.dostavka_tooltip:after {
	display: block;
	content: \'\';
	position: absolute;
	left: 50%;
	top: 100%;
	margin-left: -9px;
	border-left: 9px solid transparent;
	border-right: 9px solid transparent;
	border-top: 9px solid #00cbf6;
}
.dostavka_tooltip.left:after {
	display: block;
	content: \'\';
	position: absolute;
	left: 100%;
	top: 0;
	margin-left: -5px;
	margin-top: 9px;
	border-left: 18px solid #00cbf6;
	border-bottom: 9px solid transparent;
	border-top: 9px solid transparent;
}

</style>
    '; ?><?php echo ''; ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "modules/mag_orders/edit_reg_js.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?><?php echo ''; ?><?php echo '
'; ?><?php echo '<form action="?main='; ?><?php echo $_GET['main']; ?><?php echo '&worker='; ?><?php echo $_GET['module']; ?><?php echo '&action=edit_reg&id_order='; ?><?php echo $_GET['id_order']; ?><?php echo '&status='; ?><?php echo $_GET['status']; ?><?php echo '" method="post" enctype="multipart/form-data" id="form_add_reg"><table width="100%" border="0" cellpadding="0" cellspacing="0" ><tr><td><div id="tabs" class="flora"><ul class="ui-tabs-nav"><li class="ui-tabs-selected"><a href="#tab-1"><span>����� ��������</span></a></li></ul></div><div style="display: block;" class="ui-tabs-panel" id="tab-1"><table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add"><tr class="table_bg" ><td align="left" colspan="2" class="brend_liter"><div class="s_letters"><a>A</a><a>B</a><a>C</a><a>D</a><a>E</a><a>F</a><a>G</a><a>H</a><a>I</a><a>J</a><a>K</a><a>L</a><a>M</a><a>N</a><a>O</a><a>P</a><a>Q</a><a>R</a><a>S</a><a>T</a><a>U</a><a>V</a><a>W</a><a>X</a><a>Y</a><a>Z</a></div></td></tr><tr ><td align="center" width="30%"  bgcolor="White"><div id="msg_order_img">image</div></td><td align="left" width="70%"><select size="10" name="sel_brend" id="sel_brend" class="sel_data"><option value="0" >-- �������� ����� --</option>'; ?><?php echo $this->_tpl_vars['sel_brend']; ?><?php echo '</select></td></tr><tr ><td align="right" ><h2 style="margin:0;">������:</h2></td><td align="left" ><span id="msg_order_title" ></span> <span id="msg_order_pol"></span></td></tr></table><table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add" style="border-top:0px;"><tr ><td align="left" ><a id="ar_comment_form_add" style="display:none;cursor:pointer;color:gray">-= <strong>��������</strong> �������� / ������ =-</a><div id="ar_comment_form" style="display:none;"><textarea name="ar_comment_text" id="ar_comment_text" style="width:100%" rows=3></textarea><input type="button" name="ar_wiki" id="ar_wiki" value="��������" style="color:blue"><input type="button" name="ar_error" id="ar_error" value="������" style="color:red"></div></td></tr></table><table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add" style="border-top:0px;"><tr ><td align="left" ><div id="ar_comment"></div></td></tr></table><table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add" style="border-top:0px;"><tr ><td align="left" ><div id="msg_order"></div></td></tr></table></div><br></td><td valign="top" style="padding-left:30px"><div id="tabs" class="flora"><ul class="ui-tabs-nav"><li class="ui-tabs-selected"><a href="#tab-1"><span>������ �����������</span></a></li></ul></div><table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add"><tr ><td align="left" width="70%"><div class="log_wrap"><table width="100%"><tr><td style="width: 80px;"><b>����</b></td><td><b>����</b></td><td><b>��������</b></td><td><b>��������</b></td></tr>'; ?><?php $_from = $this->_tpl_vars['aHistory']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['history']):
?><?php echo '<tr><td>'; ?><?php echo $this->_tpl_vars['history']['date']; ?><?php echo '<br>'; ?><?php echo $this->_tpl_vars['history']['time']; ?><?php echo '</td><td><span style="padding: 4px;color:rgb(201, 0, 0)">'; ?><?php echo $this->_tpl_vars['history']['user_name']; ?><?php echo '</span></td><td>'; ?><?php echo $this->_tpl_vars['history']['action']; ?><?php echo '</td><td>'; ?><?php echo $this->_tpl_vars['history']['text']; ?><?php echo '</td></tr>'; ?><?php endforeach; endif; unset($_from); ?><?php echo '</table></div></td></tr></table><br><div id="tabs" class="flora"><ul class="ui-tabs-nav"><li class="ui-tabs-selected"><a href="#tab-1"><span>����� �� ���� ������</span></a></li></ul></div><table width="100%" style="margin-bottom:25px"><tr><td valign="top"><input class="search_text"  type="text" style="width:100%;padding:9px 4px;"></td><td valign="top"><button class="btn btn-primary search">������</button></td></tr></table></td></tr><tr><td colspan="2"><table width="100%"><tr><td valign="top" width="100%"><div style="margin-top:30px" id="tabs" class="flora"><ul class="ui-tabs-nav"><li class="ui-tabs-selected"><a href="#tab-1"><span>�������</span></a></li></ul></div><div style="display: block;" class="ui-tabs-panel" id="tab-1"><table width="100%" border="0" cellpadding="5" cellspacing="2" class="form_add" id="table_add_orders" ><tr class="table_bg"><td align="center" width="25" ></td><td align="left" >��������</td><td align="center" width="60">����</td><td align="left" width="58">���-��</td><td align="left" width="60">���������</td><td align="left" width="90">������� ��</td></tr>'; ?><?php $_from = $this->_tpl_vars['c_order_sum']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['order_sum']):
?><?php echo '<tr class="tr_item_orders_clear" id="tr_item_orders_'; ?><?php echo $this->_tpl_vars['order_sum']['id_catalog_data_order']; ?><?php echo '"><td><input type="button" name="'; ?><?php echo $this->_tpl_vars['order_sum']['id_catalog_data_order']; ?><?php echo '" alt="'; ?><?php echo $this->_tpl_vars['order_sum']['price']; ?><?php echo '" class="delete_list" value="X" style="width:20px;color:red;font-weight: bold;border:0;background:none;"></td><td align="left" ><input class="id_orders" type="hidden" name="id_orders[]" value="'; ?><?php echo $this->_tpl_vars['order_sum']['id_catalog_data_order']; ?><?php echo '">'; ?><?php echo $this->_tpl_vars['order_sum']['title']; ?><?php echo '</td><td align="center" ><span id="price_orders_'; ?><?php echo $this->_tpl_vars['order_sum']['id_catalog_data_order']; ?><?php echo '">'; ?><?php echo $this->_tpl_vars['order_sum']['sum']; ?><?php echo '</span></td><td align="left" ><input style="width:25px;padding:1px;float:left;height:18px;" type="text" id="kol_orders_'; ?><?php echo $this->_tpl_vars['order_sum']['id_catalog_data_order']; ?><?php echo '" name="kol_orders_'; ?><?php echo $this->_tpl_vars['order_sum']['id_catalog_data_order']; ?><?php echo '" value="'; ?><?php echo $this->_tpl_vars['order_sum']['kol']; ?><?php echo '"  onkeydown="return false;"><input style="width:25px;padding:1px;float:left;height:18px;display: none" type="text" id="ordersSum_kol_'; ?><?php echo $this->_tpl_vars['order_sum']['articul_catalog_data_order']; ?><?php echo '" name="ordersSum_kol['; ?><?php echo $this->_tpl_vars['order_sum']['articul_catalog_data_order']; ?><?php echo ']" value="'; ?><?php echo $this->_tpl_vars['order_sum']['kol']; ?><?php echo '"  onkeydown="return false;"><a class="up" data-articul="'; ?><?php echo $this->_tpl_vars['order_sum']['articul_catalog_data_order']; ?><?php echo '" alt="'; ?><?php echo $this->_tpl_vars['order_sum']['id_catalog_data_order']; ?><?php echo '"><img src="images/nav/up.gif" ></a><br /><a alt="'; ?><?php echo $this->_tpl_vars['order_sum']['id_catalog_data_order']; ?><?php echo '" data-articul="'; ?><?php echo $this->_tpl_vars['order_sum']['articul_catalog_data_order']; ?><?php echo '" class="dn"><img src="images/nav/dn.gif" ></a></td><td align="left" ><span class="price_item_sum" id="price_orders_sum_'; ?><?php echo $this->_tpl_vars['order_sum']['id_catalog_data_order']; ?><?php echo '">'; ?><?php echo $this->_tpl_vars['order_sum']['price']; ?><?php echo '</span></td><td align="center" ><span style="color:blue;">'; ?><?php echo $this->_tpl_vars['order_sum']['gmargin']; ?><?php echo '</span></td></tr>'; ?><?php endforeach; endif; unset($_from); ?><?php echo '</table><table width="100%"><tr><td valign="top" width="60%"><table style="border-top:0" width="100%" border="0" cellpadding="3" cellspacing="1" class="form_add"  ><tr ><td width="250" style="padding-left:60px" align="left" >����������� � ������:</td><td ><textarea name="br_comment" rows="5" style="width:100%;">'; ?><?php echo $this->_tpl_vars['order']['pComment']; ?><?php echo '</textarea></td></tr><tr><td width="250" style="padding-left:60px" align="left">����������� ������:</td><td align="left"><textarea name="br_comment_sklad" style="width:100%;color:red" rows="3">'; ?><?php echo $this->_tpl_vars['order']['pCommentSklad']; ?><?php echo '</textarea></td></tr></table></td><td valign="top"><div style="float:right;width:400px"><table width="400" border="0" cellpadding="5" cellspacing="2" ><tr ><td align="right" width="220" >����� ������:</td><td align="right" width="70" style=""><span id="big_sum">'; ?><?php echo $this->_tpl_vars['order']['Sum']; ?><?php echo '</span> ���.</td></tr><tr ><td align="right" width="220" style="color:blue;" >������<span id="big_sum_percent_type">'; ?><?php if ($this->_tpl_vars['order']['dType'] == '1'): ?><?php echo '(���)'; ?><?php elseif ($this->_tpl_vars['order']['dType'] == '2'): ?><?php echo '(���)'; ?><?php endif; ?><?php echo '</span>:</td><td align="right" width="70" style="color:blue;"><span id="big_sum_percent">'; ?><?php echo $this->_tpl_vars['order']['dPercent']; ?><?php echo '</span> %</td></tr><tr><td align="right" width="220" style="color: green;">�������� (�������� - b2c, ����� �� ���):</td><td width="70" align="right" style="color: green;"><span id="delivery_cost-text">'; ?><?php echo $this->_tpl_vars['order']['delivery_cost']; ?><?php echo '</span> ���.</td></tr><tr><td align="right" width="220">����� �� ������� � ���������:</td><td width="70" align="right"><span id="delivery_discrount_cost-text">'; ?><?php echo $this->_tpl_vars['order']['dSum']+$this->_tpl_vars['order']['delivery_cost']; ?><?php echo '</span> ���.</td></tr><tr><td align="right" width="220" style="color: green;">��������� ��������� (�������):</td><td width="70" align="right" style="color: green;"><span id="insurance_cost-text">'; ?><?php echo $this->_tpl_vars['order']['insurance_cost']; ?><?php echo '</span> ���.</td></tr><tr ><td align="right" width="220" ><strong>�����:</strong></td><td align="right" width="70"><strong><span id="big_sum_all" data-sum="'; ?><?php echo $this->_tpl_vars['order']['dSum']; ?><?php echo '">'; ?><?php echo $this->_tpl_vars['order']['dSum']+$this->_tpl_vars['order']['delivery_cost']+$this->_tpl_vars['order']['insurance_cost']; ?><?php echo '</span></strong> ���.<input type="hidden" name="dSum" id="dSum" value="'; ?><?php echo $this->_tpl_vars['order']['dSum']; ?><?php echo '"/></td></tr></table></div></td></tr></table><div style="clear:both"></div><!--<table width="100%" border="0" cellpadding="5" cellspacing="0"><tr><td align="right">'; ?><?php if ($this->_tpl_vars['order']['status'] == '1'): ?><?php echo '<input id="submit_form" type="submit" name="save" value="��������� �����" style="color:red;font-size:16pt;">'; ?><?php elseif ($this->_tpl_vars['order']['status'] == '2'): ?><?php echo '<input id="submit_form" type="submit" name="save" value="��������� �����" style="color:red;font-size:16pt;">'; ?><?php elseif ($this->_tpl_vars['order']['status'] == '5'): ?><?php echo '<input id="submit_form" type="submit" name="save" value="��������� �����" style="color:red;font-size:16pt;">'; ?><?php elseif ($this->_tpl_vars['order']['status'] == '6'): ?><?php echo '<input id="submit_form" type="submit" name="save" value="��������� �����" style="color:red;font-size:16pt;">'; ?><?php elseif ($this->_tpl_vars['order']['status'] == '3'): ?><?php echo '<input id="submit_form" type="submit" name="save" value="����� ���������" style="color:grey;font-size:16pt;"  disabled>'; ?><?php elseif ($this->_tpl_vars['order']['status'] == '0'): ?><?php echo '<input id="submit_form" type="submit" name="save" value="����� ��������" style="color:grey;font-size:16pt;" disabled>'; ?><?php endif; ?><?php echo '</td></tr></table>--></div><br /></td></tr></table></td></tr><tr><td valign="top"><div id="msg"></div><div id="msg1" style="position:relative;"></div><div id="msg2" style="position:relative;"></div><div style="clear:both"></div></td><td valign="top"><div id="msg3"></div><div id="msg4"></div></td></tr><tr><td colspan="2"><div id="tabs" class="flora"><ul class="ui-tabs-nav"><li class="ui-tabs-selected"><a href="#tab-1"><span>������ '; ?><?php if ($this->_tpl_vars['user'] == false): ?><?php echo '�� ������'; ?><?php endif; ?><?php echo '</span></a></li></ul></div></td></tr><tr style="border:1px solid #789eab;background:#f5f5f5" id="table_info_users"><td width="650" valign="top" style="border-left:1px solid #789eab;border-bottom:1px solid #789eab;"><div style="display: block;" class="ui-tabs-panel" id="tab-1"><table width="100%"  border="0" cellpadding="5" cellspacing="2" class="form_add"><tr ><td align="left" width="30%"><strong>ID</strong></td><td align="left" width="70%"><input type="hidden" name="user_id" value="'; ?><?php echo $this->_tpl_vars['user']['id']; ?><?php echo '"><input type="hidden" id="user_percent" name="user_percent" value="'; ?><?php echo $this->_tpl_vars['order']['dPercent']; ?><?php echo '" ><input type="hidden" id="user_percent_type" name="user_percent_type" value="'; ?><?php echo $this->_tpl_vars['order']['dType']; ?><?php echo '" ><input type="hidden" id="user_big_sum_all" name="user_big_sum_all" value="'; ?><?php echo $this->_tpl_vars['order']['dSum']; ?><?php echo '" ><input type="hidden" id="user_big_sum" name="user_big_sum" value="'; ?><?php echo $this->_tpl_vars['order']['Sum']; ?><?php echo '" ><input type="hidden" name="user_region" value="'; ?><?php echo $this->_tpl_vars['user']['region']; ?><?php echo '" >'; ?><?php echo $this->_tpl_vars['user']['id']; ?><?php echo '</td></tr><tr ><td align="left" width="30%"><strong>E-mail</strong></td><td align="left" width="70%"><span id="email">'; ?><?php echo $this->_tpl_vars['user']['email']; ?><?php echo '</span><input name="email"  class="hide"  value="'; ?><?php echo $this->_tpl_vars['user']['email']; ?><?php echo '" style="width: 250px;"></a></td></tr><tr ><td align="left" width="30%"><strong>���</strong></td><td align="left" width="70%"><span id="fio">'; ?><?php echo $this->_tpl_vars['oUser']->getFio(); ?><?php echo '</span><input name="fio" class="hide"  value="'; ?><?php echo $this->_tpl_vars['user']['fio']; ?><?php echo '" style="width: 250px;"></td></tr><tr ><td align="left" width="30%"><strong>�������</strong></td><td align="left" width="70%"><span id="phone">'; ?><?php echo $this->_tpl_vars['user']['phone']; ?><?php echo '</span><input name="phone" class="hide"  value="'; ?><?php echo $this->_tpl_vars['user']['phone']; ?><?php echo '" style="width: 250px;"></td></tr>'; ?><?php if ($this->_tpl_vars['user']['phone2'] != null): ?><?php echo '<tr ><td align="left" width="30%"><strong>������� 2</strong></td><td align="left" width="70%"><span id="phone2">'; ?><?php echo $this->_tpl_vars['user']['phone2']; ?><?php echo '</span><input name="phone2" class="hide"  value="'; ?><?php echo $this->_tpl_vars['user']['phone2']; ?><?php echo '" style="width: 250px;"></td></tr>'; ?><?php endif; ?><?php echo '<tr ><td align="left" width="30%"><b>������</b></td><td align="left" width="70%"><span id="d_percent">'; ?><?php if ($this->_tpl_vars['user']): ?><?php echo ''; ?><?php echo $this->_tpl_vars['user']['d_percent']; ?><?php echo ''; ?><?php else: ?><?php echo '0'; ?><?php endif; ?><?php echo '</span>%</td></tr><tr ><td align="left" width="30%"><b>����������� �����</b></td><td align="left" width="70%"><span id="d_sum_all">'; ?><?php echo $this->_tpl_vars['user']['d_sum_all']; ?><?php echo '</span></td></tr><tr ><td align="left" width="30%"><b>���� ��������</b></td><td align="left" width="70%"><span id="date_birthday">'; ?><?php echo $this->_tpl_vars['user']['date_birthday']; ?><?php echo '</span><input name="date_birthday" id="date_birthday_input" class="hide"  value="'; ?><?php echo $this->_tpl_vars['user']['date_birthday']; ?><?php echo '"></td></tr><tr ><td align="left" width="30%"><b>���</b></td><td align="left" width="70%"><span id="pol">'; ?><?php echo $this->_tpl_vars['user']['pol']; ?><?php echo '</span><select class="hide" name="pol"><option  '; ?><?php if ($this->_tpl_vars['user']['pol'] == 'M'): ?><?php echo 'SELECTED=""'; ?><?php endif; ?><?php echo ' value="M">M<option '; ?><?php if ($this->_tpl_vars['user']['pol'] == 'F'): ?><?php echo 'SELECTED=""'; ?><?php endif; ?><?php echo ' value="F">F</select>   </td></tr></table></div></td><td style="border-right:1px solid #789eab;border-bottom:1px solid #789eab;" valign="top">'; ?><?php if ($this->_tpl_vars['order']['pAddress'] != ''): ?><?php echo '<span style="color:red">��������� �� ������� ������: </span>'; ?><?php echo $this->_tpl_vars['order']['pAddress']; ?><?php echo ''; ?><?php endif; ?><?php echo '<table width="100%"  border="0" cellpadding="5" cellspacing="2" class="form_add "><tr><td colspan="2"><table class="address_edit" style="width: 70%;display: none">'; ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "address.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?><?php echo '</table></td></tr><tr><td colspan="2"><table class="address" style="width: 100%"><tr><td align="left" width="30%"><strong>������� ������</strong></td><td align="left" width="70%"><span id="region">'; ?><?php echo $this->_tpl_vars['user']['address_region']; ?><?php echo '</span></td></tr><tr><td align="left" width="30%"><strong>�����/�����</strong></td><td align="left" width="70%"><span id="city">'; ?><?php echo $this->_tpl_vars['user']['address_city']; ?><?php echo '</span></td></tr><tr><td align="left" width="30%"><strong>����� (���, �����)</strong></td><td align="left" width="70%"><span id="part_name">'; ?><?php echo $this->_tpl_vars['list_capital_districts'][$this->_tpl_vars['user']['address_part']]; ?><?php echo '</span></td></tr><tr><td align="left" width="30%"><strong>�����</strong></td><td align="left" width="70%"><span id="metro">'; ?><?php echo $this->_tpl_vars['aMetro'][$this->_tpl_vars['user']['metro']]['name']; ?><?php echo '</span></td></tr><tr><td align="left" width="30%"><strong>���������� �����</strong></td><td align="left" width="70%"><span id="settlement">'; ?><?php echo $this->_tpl_vars['user']['address_settlement']; ?><?php echo '</span></td></tr><tr><td align="left" width="30%"><strong>�����</strong></td><td align="left" width="70%"><span id="street">'; ?><?php echo $this->_tpl_vars['user']['address_street']; ?><?php echo '</span></td></tr><tr><td valign="top" class="form_add" style="padding:0;border-bottom:0" colspan="2"><table style="" cellpadding="4" cellspacing="2" width="100%"><tr><td align="left" width="16%" ><strong>���:</strong></td><td align="left" width="16%" ><span id="house">'; ?><?php echo $this->_tpl_vars['user']['address_house']; ?><?php echo '</span></td><td align="left" width="16%" ><strong>������:</strong></td><td align="left" width="16%" ><span id="corpus">'; ?><?php echo $this->_tpl_vars['user']['address_corpus']; ?><?php echo '</span></td><td align="left" width="16%" ><strong>��������:</strong></td><td align="left" width="16%" ><span id="stroenie">'; ?><?php echo $this->_tpl_vars['user']['address_stroenie']; ?><?php echo '</span></td></tr></table></td></tr><tr><td valign="top" class="form_add" style="padding:0;border-bottom:0" colspan="2"><table style="" cellpadding="4" cellspacing="2" width="100%"><tr><td align="left" width="16%" ><strong>��������:</strong></td><td align="left" width="16%" ><span id="flat">'; ?><?php echo $this->_tpl_vars['user']['address_flat']; ?><?php echo '</span></td><td align="left" width="16%" ><strong>�������:</strong></td><td align="left" width="16%" ><span id="domophone">'; ?><?php echo $this->_tpl_vars['user']['address_domophone']; ?><?php echo '</span></td><td align="left" width="16%" ><strong>����:</strong></td><td align="left" width="16%" ><span id="floor">'; ?><?php echo $this->_tpl_vars['user']['address_floor']; ?><?php echo '</span></td></tr></table></td></tr><tr><td align="left" ><strong>������</strong></td><td align="left" ><span id="postcode">'; ?><?php echo $this->_tpl_vars['user']['postcode']; ?><?php echo '</span></td></tr></table></td></tr><tr><td><a style="color:white" href="" class="btn btn-warning edit">�������������</a></td><td><a style="color:white" href="" class="btn btn-primary save">���������</a></td></tr></table><br></td></tr><tr><td colspan="2"><div style="margin-top:30px" id="tabs" class="flora"><ul class="ui-tabs-nav"><li class="ui-tabs-selected"><a href="#tab-1"><span>�������� ������</span></a></li></ul></div><table width="100%" class="checkbox_group_pay"><tr><td valign="top" ><div class="dostavka_item"><div data-value="1" data-width="250" data-toggle="tooltip" data-content="���� ����� ������ ��������� 10000�, �� ������ ��� ������ �� ��������" class="dostavka_item__checkbox '; ?><?php if ($this->_tpl_vars['order']['payment_type'] == 1): ?><?php echo 'checked'; ?><?php endif; ?><?php echo '">��������� ��� ���������</div></div></td><td><div class="dostavka_item"><div data-value="2" data-width="250" data-toggle="tooltip" data-content="��� ���� ������� ������ �������� ����� ������ ������� �������" class="dostavka_item__checkbox '; ?><?php if ($this->_tpl_vars['order']['payment_type'] == 2): ?><?php echo 'checked'; ?><?php endif; ?><?php echo '">���������� �� ���������</div></div></td><td><div class="dostavka_item"><div data-value="3" data-width="250" data-toggle="tooltip" data-content="QIWI, Webmoney, ������.������, Rapida Online, MoneyMail, ������@mail.ru, ������ �������, EasyPay, LiqPay, Handy Bank" class="dostavka_item__checkbox  '; ?><?php if ($this->_tpl_vars['order']['payment_type'] == 3): ?><?php echo 'checked'; ?><?php endif; ?><?php echo '">���������� Robokassa</div></div></td><td><input class="payment_type" type="hidden" name="payment_type" value="'; ?><?php echo $this->_tpl_vars['order']['payment_type']; ?><?php echo '"></td></tr></table></td></tr><tr><td colspan="2"><div style="margin-top:30px" id="tabs" class="flora"><ul class="ui-tabs-nav"><li class="ui-tabs-selected"><a href="#tab-1"><span>��������</span></a></li></ul></div><table width="100%" class=""><tr id="couriers__wrapper"><td valign="top" width="350"><div class="dostavka_item" id="courier" data-type="��������" data-modal="courier"><div class="title">�������� ��������</div><table width="100%"><thead class="courier__table-header head"><tr><th>������ ��������</th><th>����</th><th>����</th></tr></thead><tbody class="courier__table-body"></tbody></table></div><div class="dostavka_item" id="post" data-type="�����" data-modal="post"><div class="title">����� ������</div><table width="100%"><thead class="courier__table-header head"><tr><th>������ ��������</th><th>����</th><th>����</th></tr></thead><tbody class="courier__table-body"><tr class="courier__row" data-courier="0"><td style="position:relative;"><div data-width="250" data-placement="left" data-toggle="tooltip" data-content="������ ���������� � ������� 1 ��� ���������� �������� 1 ������. �������� � �������� ����� �� ������� �� ����� �������� ���������. � ������ �������� ������� �� �������� ��������� �� ����� ������. ��� ������ ���������� �������� ����� ������ ������� �� 2 �� 5% �������� �� ������� ����� �� ��� ����." class="dostavka_item__checkbox">����� ������</div></td><td>4 ��.</td><td>265 �.</td></tr></tbody></table></div><div class="dostavka_item" id="self" data-type="���������" data-modal="byself"><div class="title">���������</div><table width="100%"><thead class="courier__table-header head"><tr><th>������ ��������</th><th>����</th><th>����</th></tr></thead><tbody class="courier__table-body"></tbody></table><div id="self_selected"></div></div></td><td valign="top" style="padding-left:20px"></td></tr><tr><td colspan="2"><div id="map"></div></td></tr><tr><td style="padding-left:60px" align="left">���� ��������:</td><td align="left"><input style="border:1px solid red;" type="text" name="date1" id="date1" value="'; ?><?php echo $this->_tpl_vars['order']['DateEnd']; ?><?php echo '" />&nbsp;<input style="border:1px solid grey;" type="button" id="trigger1" value="�������� ����"><input type="hidden" id="courier_address_id" value="'; ?><?php echo $this->_tpl_vars['order']['courier_address_id']; ?><?php echo '" name="courier_address_id"/><input type="hidden" id="delivery_cost" value="'; ?><?php echo $this->_tpl_vars['order']['delivery_cost']; ?><?php echo '" name="delivery_cost"/><input type="hidden" id="insurance_cost" value="'; ?><?php echo $this->_tpl_vars['order']['insurance_cost']; ?><?php echo '" name="insurance_cost"/></td></tr>'; ?><?php if ($this->_tpl_vars['user']['region'] == 'region'): ?><?php echo ''; ?><?php if ($this->_tpl_vars['order']['is_send_rect'] == '0'): ?><?php echo '<tr><td style="padding-left:60px"align="left">��������� ���������:</td><td align="left"><input type="checkbox" name="is_sending"></td></tr>'; ?><?php elseif ($this->_tpl_vars['order']['is_send_rect'] == '1'): ?><?php echo '<tr ><td align="left" colspan=2 bgcolor="white"></td></tr><tr ><td align="left" colspan=2 bgcolor="white"></td></tr><tr><td align="left" colspan="2"><strong>��������� ���������� ('; ?><?php echo $this->_tpl_vars['order']['DateSendRect']; ?><?php echo '):</strong>&nbsp;<input type="checkbox" name="is_sending" id="is_sending"><label for="is_sending">��������� ��������?:</label>&nbsp;</td><input type="hidden" name="is_sending_d" value="1"></td></tr><tr><td align="left"></td><td align="left"></td></tr>'; ?><?php endif; ?><?php echo ''; ?><?php endif; ?><?php echo ''; ?><?php if ($this->_tpl_vars['user']['region'] == 'region'): ?><?php echo ''; ?><?php if ($this->_tpl_vars['order']['is_send_post'] == '1'): ?><?php echo '<tr ><td align="left" colspan=2 bgcolor="white"></td></tr><tr ><td align="left" colspan=2 bgcolor="white"></td></tr><tr><td align="left" colspan="2">������������� <strong>�'; ?><?php echo $this->_tpl_vars['order']['pPostNumber']; ?><?php echo '</strong> ��������� - '; ?><?php echo $this->_tpl_vars['order']['DateSendPost']; ?><?php echo ':</td></tr><input type="hidden" name="is_sending_post" value="1"><input type="hidden" name="courier_address_id" id="courier_address_id" value="'; ?><?php echo $this->_tpl_vars['order']['courier_address_id']; ?><?php echo '">'; ?><?php endif; ?><?php echo '<tr><td style="padding-left:60px" align="left">����. �������������:</td><td align="left"><input style="border:1px solid grey;width:190px;" maxlength="40" type="text" name="track" id="track" value="'; ?><?php echo $this->_tpl_vars['order']['track']; ?><?php echo '" />&nbsp;����:&nbsp;<input style="border:1px solid grey;width:65px;" type="text" name="track_date" id="track_date" value="'; ?><?php echo $this->_tpl_vars['order']['track_date']; ?><?php echo '" /></td></tr>'; ?><?php endif; ?><?php echo '</table><table width="100%" border="0" cellpadding="5" cellspacing="0"><tr><td align="right">'; ?><?php if ($this->_tpl_vars['order']['status'] == '1'): ?><?php echo '<input id="submit_form" type="submit" name="save" value="��������� �����" style="color:red;font-size:16pt;">'; ?><?php elseif ($this->_tpl_vars['order']['status'] == '2'): ?><?php echo '<input id="submit_form" type="submit" name="save" value="��������� �����" style="color:red;font-size:16pt;">'; ?><?php elseif ($this->_tpl_vars['order']['status'] == '5'): ?><?php echo '<input id="submit_form" type="submit" name="save" value="��������� �����" style="color:red;font-size:16pt;">'; ?><?php elseif ($this->_tpl_vars['order']['status'] == '6'): ?><?php echo '<input id="submit_form" type="submit" name="save" value="��������� �����" style="color:red;font-size:16pt;">'; ?><?php elseif ($this->_tpl_vars['order']['status'] == '3'): ?><?php echo '<input id="submit_form" type="submit" name="save" value="����� ���������" style="color:grey;font-size:16pt;"  disabled>'; ?><?php elseif ($this->_tpl_vars['order']['status'] == '0'): ?><?php echo '<input id="submit_form" type="submit" name="save" value="����� ��������" style="color:grey;font-size:16pt;" disabled>'; ?><?php endif; ?><?php echo '<p class="error_delivery">�� �� ������� ������ ��������</p></td></tr></table></td></tr></form><tr><td colspan="2"><div style="margin-top:30px" id="tabs" class="flora"><ul class="ui-tabs-nav"><li class="ui-tabs-selected"><a href="#tab-1"><span>��������� � ������</span></a></li></ul></div><table class="form_add checkbox_logistic" width="100%" ><tr><td style="display: none"><input class="logistic allow" name="logistic" value="'; ?><?php echo $this->_tpl_vars['order']['logistic_value']; ?><?php echo '"></td><td style="padding:10px 0 10px 10px"><div data-value="1" class="dostavka_item__checkbox '; ?><?php if (in_array ( 1 , $this->_tpl_vars['order']['logistic'] )): ?><?php echo 'checked'; ?><?php endif; ?><?php echo ' '; ?><?php if ($this->_tpl_vars['aAllows']['LOGISTIC_SEND']): ?><?php echo 'allow'; ?><?php endif; ?><?php echo '">���������</div></td><td style="padding:10px 0"><div data-value="2" class="dostavka_item__checkbox '; ?><?php if (in_array ( 2 , $this->_tpl_vars['order']['logistic'] )): ?><?php echo 'checked'; ?><?php endif; ?><?php echo ' '; ?><?php if ($this->_tpl_vars['aAllows']['LOGISTIC_PAY']): ?><?php echo 'allow'; ?><?php endif; ?><?php echo '">�������</div></td><td style="padding:10px 0"><div data-value="3" class="dostavka_item__checkbox '; ?><?php if (in_array ( 3 , $this->_tpl_vars['order']['logistic'] )): ?><?php echo 'checked'; ?><?php endif; ?><?php echo ' '; ?><?php if ($this->_tpl_vars['aAllows']['LOGISTIC_MONEY']): ?><?php echo 'allow'; ?><?php endif; ?><?php echo '">������ ��������</div></td><td style="padding:10px 0"><div data-value="4" class="dostavka_item__checkbox '; ?><?php if (in_array ( 4 , $this->_tpl_vars['order']['logistic'] )): ?><?php echo 'checked'; ?><?php endif; ?><?php echo ' '; ?><?php if ($this->_tpl_vars['aAllows']['LOGISTIC_REFOUND']): ?><?php echo 'allow'; ?><?php endif; ?><?php echo '">�������</div></td><td style="padding:10px 10px 10px 0"><div data-value="5" class="dostavka_item__checkbox '; ?><?php if (in_array ( 5 , $this->_tpl_vars['order']['logistic'] )): ?><?php echo 'checked'; ?><?php endif; ?><?php echo ' '; ?><?php if ($this->_tpl_vars['aAllows']['LOGISTIC_REFOUND_COMPLETE']): ?><?php echo 'allow'; ?><?php endif; ?><?php echo '">������� �������</div></td></tr><tr><td style="border:0" colspan="5"><table width="100%"><tr><td  style="padding:10px 0;border:0" width="50%"><table width="100%"><tr><td style="border:0" width="170" align="right">������ ������:</td><td style="border:0"><span class="payment_type" style="font-weight: bold"">��������� ��� ���������</span></td></tr></table></td><td style="border:0"><table width="100%"><tr><td style="border:0" width="170" align="right">��� ��������:</td><td style="border:0"><span class="payment_type" style="font-weight: bold">�������� (b2c)</span></td></tr></table></td></tr><tr><td  style="padding:10px 0;border:0" width="50%"><table width="100%"><tr><td style="border:0" width="170" align="right">���� ������</td><td style="border:0"><input  disabled  id="payment_date" name="payment_date"  value="'; ?><?php echo $this->_tpl_vars['order']['payment_date']; ?><?php echo '" style="padding:6px" type="text" class="'; ?><?php if ($this->_tpl_vars['aAllows']['LOGISTIC_PAYMENT_DATE']): ?><?php echo 'allow'; ?><?php endif; ?><?php echo '"></td></tr></table></td><td style="padding:10px 0;border:0" ><table width="100%"><tr><td style="border:0" width="170" align="right">���������� ��������</td><td style="border:0"><input disabled name="TotalMoneyIncome" value="'; ?><?php echo $this->_tpl_vars['order']['TotalMoneyIncome']; ?><?php echo '" style="padding:6px" type="text" class="'; ?><?php if ($this->_tpl_vars['aAllows']['LOGISTIC_TOTALMONEYINCOME']): ?><?php echo 'allow'; ?><?php endif; ?><?php echo '"></td></tr></table></td></tr><tr><td style="padding:10px 0;border:0" ><table width="100%"><tr><td style="border:0" width="170" align="right">���� ����������� �����</td><td style="border:0"><input disabled id="DateMoneyIncome" value="'; ?><?php echo $this->_tpl_vars['order']['DateMoneyIncome']; ?><?php echo '" name="DateMoneyIncome" style="padding:6px" type="text" class="'; ?><?php if ($this->_tpl_vars['aAllows']['LOGISTIC_DATEMONEYINCOME']): ?><?php echo 'allow'; ?><?php endif; ?><?php echo '"></td></tr></table></td><td style="padding:10px 0;border:0" ><table width="100%"><tr><td style="border:0" width="170" align="right">������ �� �������� (������)</td><td style="border:0"><input disabled name="delivery_cost_fact" value="'; ?><?php echo $this->_tpl_vars['order']['delivery_cost_fact']; ?><?php echo '" style="padding:6px" type="text" class="'; ?><?php if ($this->_tpl_vars['aAllows']['LOGISTIC_DELIVERYCOST']): ?><?php echo 'allow'; ?><?php endif; ?><?php echo '"></td></tr></table></td></tr><tr style="display: none"><td><input  class="allow" type="hidden" name="order_id" value="'; ?><?php echo $this->_tpl_vars['order']['id']; ?><?php echo '"> </td></tr></table></td></tr>'; ?><?php if ($this->_tpl_vars['aAllows']['LOGISTIC']): ?><?php echo '<tr><td colspan="5"><table width="100%"><tr><td style="border:0" width="50%"><table width="100%"><tr><td style="border:0" width="170" align="right">�����</td><td style="border:0"><select name="firm" id="firm"><option '; ?><?php if ($this->_tpl_vars['order']['firm'] == ""): ?><?php echo 'selected'; ?><?php endif; ?><?php echo ' value="">������ �� �������</option><option '; ?><?php if ($this->_tpl_vars['order']['firm'] == 'web'): ?><?php echo 'selected'; ?><?php endif; ?><?php echo ' value="web">��� �������</option><option '; ?><?php if ($this->_tpl_vars['order']['firm'] == 'plus'): ?><?php echo 'selected'; ?><?php endif; ?><?php echo ' value="plus">������</option><option '; ?><?php if ($this->_tpl_vars['order']['firm'] == 'card'): ?><?php echo 'selected'; ?><?php endif; ?><?php echo ' value="card">������ ������</option><option '; ?><?php if ($this->_tpl_vars['order']['firm'] == 'yandex'): ?><?php echo 'selected'; ?><?php endif; ?><?php echo ' value="yandex">������ ����� yandex</option><option '; ?><?php if ($this->_tpl_vars['order']['firm'] == 'kassa'): ?><?php echo 'selected'; ?><?php endif; ?><?php echo ' value="kassa">������ ����� �����</option></select></td></tr></table></td><td style="border:0" width="50%"></td></tr></table></td></tr>'; ?><?php endif; ?><?php echo '<tr><td colspan="5"><table width="100%"><tr><td style="border:0" width="50%"></td><td style="border:0" width="50%">'; ?><?php if ($this->_tpl_vars['aAllows']['LOGISTIC']): ?><?php echo '<table width="100%"><tr><td style="border:0" width="170">&nbsp;</td><td style="border:0"><a style="color:white" href="" class="btn btn-warning edit">�������������</a><span style="padding:0 20px"></span><a style="color:white;display: none" href="" class="btn btn-primary save">���������</a></td></tr></table>'; ?><?php endif; ?><?php echo '</td></tr></table></td></tr></table></td></tr></td><td width="20">&nbsp;</td><!-- ������� , ������ ���� --><td valign="top"></td></tr></table>'; ?>


<?php echo '
<script type="text/javascript">
Calendar.setup({
	inputField     	: "date1",
	ifFormat       	: "%Y-%m-%d %H:%M:%S",
	timeFormat     	: "24",
	button         	: "trigger1",
	singleClick    	: true,
	step           	: 1,
	showsTime		: true
});
</script>
'; ?>


<?php echo '
<script type="text/javascript">
$(function(){
    if ($(\'#track_date\').length>0){
        Calendar.setup({
            inputField     	: "track_date",
            ifFormat       	: "%Y-%m-%d",
            timeFormat     	: "24",
            button         	: "date2",
            singleClick    	: true,
            step           	: 1,
            showsTime		: false
        });
    }

    if ($(\'#date_birthday_input\').length>0){

        Calendar.setup({
            inputField     	: "date_birthday_input",
            ifFormat       	: "%Y-%m-%d",
            timeFormat     	: "24",
            showsTime		: false
        });
    }

    if ($(\'#DateMoneyIncome\').length>0){

        Calendar.setup({
            inputField     	: "DateMoneyIncome",
            ifFormat       	: "%Y-%m-%d",
            timeFormat     	: "24",
            showsTime		: false
        });
    }

    if ($(\'#payment_date\').length>0){
        Calendar.setup({
            inputField     	: "payment_date",
            ifFormat       	: "%Y-%m-%d",
            timeFormat     	: "24",
            showsTime		: false
        });
    }
})
</script>
'; ?>


<?php echo '
<script>
    var yMap,
        ymapsDef = $.Deferred(),
        ymapsResolved = false,
        // ���������� ����������, ��������� � ��������� true, �����
        // �� ��������������, ��������, ��������� ����� �������� �� ������� ������, ����� api
        // � ��� ���� � ��� ��� ������������� ���������� ���������
        isDeliveryCostInited = false;

    /**
     * ������������� ������ ���� ��� ����� ��������
     */
    function initYmaps() {

        yMap = new ymaps.Map(\'map\', {
            center: [55.753994, 37.622093],
            zoom: 8,
            controls: [\'zoomControl\', \'typeSelector\']
        });

        ymapsDef.resolve();
    }

    ymaps.ready(initYmaps);

    /**
     * ������� ���������� ����� �������� �� ���������� ������ ������������
     * ��� �� �� ��� ��������� ������, ������� ��������� ��� ��������� �������� �� ������
     * ������ �������� ������, ���� ������� ��� ������ �� ���������� ������
     *
     * @param {Number} kladr_id - ������������� ������������� ������
     * @param {String} city - �������� ������
     * @param {Object} resp - ������ XHR ������� �� ��������� "������������� ��������"
     */
    function fillCouriers(kladr_id, city, resp) {

        $.when(ymapsDef).then(function() {
            AddressSuggestions.getLogistic(kladr_id, city).then(function() {

                var $wrapper = $(\'#couriers__wrapper\'),
                    $logistic = $(\'#self_selected\'),
                    arr = [];

                if (resp.delivery_type != "���������") {
                    $wrapper.find(\'.courier__row[data-courier="\'+resp.id+\'"]\').find(\'.dostavka_item__checkbox\').trigger(\'click\');//addClass(\'checked\');
                } else {

                    arr.push("<span>������������ ������: <b>"+resp.courier_name+"</b></span>");
                    arr.push("<span>����� ������ ����������: <b>"+resp.delivery_address+"</b></span>");
                    arr.push("<span>����� �������� �� "+resp.delivery_days+" ��.</span>");
                    arr.push("<span>��������� �������� �� "+resp.delivery_cost+" �.</span>");
                    $logistic.html(arr.join(""));

                    $("#submit_form").removeAttr(\'disabled\');

                    isDeliveryCostInited = true;
                }

                $("#courier_address_id").val(resp.id);
            });
        });
    }

    $.ajax({
        url: "/api/logistic_get/",
        data: {
            k: $("#courier_address_id").val(),
            t: $(\'input[name="payment_type"]\').val()
        },
        success: function(resp) {

            // ���� ����� �������, �� ���� ��� ������ ������ ��������
            if (resp.id != null && resp.id != 0) {

                fillCouriers(resp.kladr_id_city, resp.name_city, resp);

            // ���� �� ������ �������� �� ��� ������ ������ ������ �� ������ �����
            } else {

                var addr = [],
                        result,
                        answer;

                // ���������� ������ ������ �� ����� ������������

                //��������� ������������ ������ ������
                if ($(\'#part_name\').text() != "") {

                    fillCouriers($(\'#select_part\').val(), resp.name_city, resp);
                } else {

                    addr.push($(\'#region\').val());
                    // ���������� ������ ������ �� ����� ������������
                    if ($(\'#region\').val() != $(\'#city\').val()){
                        addr.push($(\'#city\').val());
                    }
                    addr.push($(\'#step2_settlement\').val());

                    addr = addr.join(" ");

                    answer = DadataApi.clean(addr);
                    answer.then(function(msg) {
                        if (typeof msg != \'undefined\') {
                            result = AddressSuggestions.getCleanData(msg);
                            fillCouriers(result.kladr_id, addr, resp);
                        }
                    });
                }
            }
        }
    });
</script>
'; ?>