<?php /* Smarty version 2.6.18, created on 2015-02-02 09:56:22
         compiled from modules/catalog/catalog_data_order_table.tpl */ ?>
<?php echo '
<script type="text/javascript">
$(document).ready(function(){
	$("#grid_table").flexigrid({
		url: \''; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=mod&id_cat=<?php echo $_GET['id_cat']; ?>
&id_catalog_data=<?php echo $_GET['id_catalog_data']; ?>
<?php echo '\',
		dataType: \'json\',
		colModel : [
		{display: \'ID\', name : \'id\', width : 40, sortable : false, align: \'center\'},
		{display: \'SALE\', name : \'is_block\', width : 25, sortable : false, align: \'center\'},
		{display: \'ARTICLE-1\', name : \'article\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-2\', name : \'article2\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-3\', name : \'article3\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-4\', name : \'article4\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-5\', name : \'article5\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-6\', name : \'article6\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-7\', name : \'article7\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-8\', name : \'article8\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-9\', name : \'article9\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-10\', name : \'article10\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-11\', name : \'article11\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-12\', name : \'article12\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-13\', name : \'article13\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-14\', name : \'article14\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-15\', name : \'article15\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-16\', name : \'article16\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-17\', name : \'article17\', width : 70, sortable : false, align: \'center\'},
		{display: \'ARTICLE-18\', name : \'article18\', width : 70, sortable : false, align: \'center\'},
		{display: \'���� (RUB)\', name : \'price\', width : 60, sortable : false, align: \'center\'},
		{display: \'�����\', name : \'sklad\', width : 30, sortable : false, align: \'center\'},
		{display: \'���������\', name : \'title\', width : \'auto\', sortable : false, align: \'left\'},
		],
		buttons : [
		{name: \'�������� ���\', bclass: \'check\', onpress : test},{separator: true},
		{name: \'����� ���������\', bclass: \'uncheck\', onpress : test},{separator: true},{separator: true},
		{name: \'�������\', bclass: \'delete\', onpress : test},{separator: true},
		{name: \'������������\', bclass: \'active\', onpress : test},{separator: true},
		{name: \'�������������\', bclass: \'deactive\', onpress : test},{separator: true}
		],
		striped: false,
		sortname: "id",
		sortorder: "desc",
		usepager: false,
		title: \'\',
		useRp: false,
		rp: 10,
		showTableToggleBtn: true,
		width: \'auto\',
		height: \'auto\'
	});
});

function test(com,grid){
	if (com==\'�������\'){
		if($(\'.trSelected\',grid).length>0){
			var items = $(\'.trSelected\',grid);
			var itemlist =\'\';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=delete<?php echo '",
				data: "items="+itemlist,
				success: function(data){
					$("#grid_table").flexReload();
				}});
		}else{
			return false;
		}

	}else if (com==\'�������� ���\'){
		$(\'.bDiv tbody tr\',grid).addClass(\'trSelected\');

	}else if (com==\'����� ���������\'){
		$(\'.bDiv tbody tr\',grid).removeClass(\'trSelected\');

	}else if (com==\'������������\'){
		if($(\'.trSelected\',grid).length>0){
			var items = $(\'.trSelected\',grid);
			var itemlist =\'\';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=active<?php echo '",
				data: "items="+itemlist,
				success: function(data){
					$("#grid_table").flexReload();
				}});
		}else{
			return false;
		}

	}else if (com==\'�������������\'){
		if($(\'.trSelected\',grid).length>0){
			var items = $(\'.trSelected\',grid);
			var itemlist =\'\';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=deactive<?php echo '",
				data: "items="+itemlist,
				success: function(data){
					$("#grid_table").flexReload();
				}});
		}else{
			return false;
		}
	}
}
</script>
'; ?>


<table id="grid_table" class="flexigrid" style="display:none"></table>