<?php /* Smarty version 2.6.18, created on 2015-02-06 14:40:57
         compiled from modules/mag_client/table.tpl */ ?>
<?php echo '

<script type="text/javascript">

$(document).ready(function(){

	$("#client_table").flexigrid({
		url: \''; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=mod<?php echo '\',
		dataType: \'json\',
		colModel : [
		{display: \'���� �����������\', name : \'date_register\', width : 100, sortable : true, align: \'left\'},
		{display: \'ID\', name : \'id\', width : 40, sortable : false, align: \'center\' },
		{display: \'EMAIL\', name : \'email\', width : 200, sortable : false, align: \'left\'},
		{display: \'���\', name : \'fio\', width : 200, sortable : false, align: \'left\'},
		{display: \'����������� �����\', name : \'d_sum_all\', width : 100, sortable : true, align: \'left\'},
		{display: \'������ (%)\', name : \'d_percent\', width : 100, sortable : true, align: \'left\'},
		{display: \'��������\', name : \'d_sum_eco\', width : 100, sortable : true, align: \'left\'},
		{display: \'���������\', name : \'note\', width : 60, sortable : false, align: \'center\'},
		{display: \'���� ����. ������\', name : \'date_last_visit\', width : 100, sortable : false, align: \'left\'},
		{display: \'A\', name : \'is_block\', width : 20, sortable : false, align: \'center\'},
		],
		buttons : [
		{name: \'�������� ���\', bclass: \'check\', onpress : action},{separator: true},
		{name: \'����� ���������\', bclass: \'uncheck\', onpress : action},{separator: true},{separator: true},
		{name: \'������������\', bclass: \'active\', onpress : action},{separator: true},
		{name: \'�������������\', bclass: \'deactive\', onpress : action},{separator: true},
		{name: \'�������\', bclass: \'delete\', onpress : action},{separator: true},
		],
		searchitems : [
		{display: \'ID\', name : \'id\', isdefault: true},
		{display: \'EMAIL\', name : \'email\'},
		],
		sortname: "date_register",
		sortorder: "desc",
		usepager: true,
		showTableToggleBtn: true,
		width: \'auto\',
		height: \'auto\',
		useRp: true,
		rpOptions: [15,20,25,40],
		rp: 15,
		pagestat: \'�������� � {from} �� {to} �� {total} ��������\',
		procmsg: \'���������, ����������, ��������� ...\',
		nomsg: \'��� ���������\',
	});
});

function action(com,grid){
	if (com==\'������������\'){
		if($(\'.trSelected\',grid).length>0){
			var items = $(\'.trSelected\',grid);
			var itemlist =\'\';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=active<?php echo '",
				data: "items="+itemlist,
				success: function(data){
					$("#client_table").flexReload();
				}});
		}else{
			return false;
		}

	}else if (com==\'�������������\'){
		if($(\'.trSelected\',grid).length>0){
			var items = $(\'.trSelected\',grid);
			var itemlist =\'\';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=deactive<?php echo '",
				data: "items="+itemlist,
				success: function(data){
					$("#client_table").flexReload();
				}});
		}else{
			return false;
		}

	}else if (com==\'�������\'){
		if($(\'.trSelected\',grid).length>0){
			var items = $(\'.trSelected\',grid);
			var itemlist =\'\';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "'; ?>
?ajax=<?php echo $_GET['module']; ?>
&action=delete<?php echo '",
				data: "items="+itemlist,
				success: function(data){
					$("#client_table").flexReload();
				}});
		}else{
			return false;
		}

	}else if (com==\'�������� ���\'){
		$(\'.bDiv tbody tr\',grid).addClass(\'trSelected\');

	}else if (com==\'����� ���������\'){
		$(\'.bDiv tbody tr\',grid).removeClass(\'trSelected\');
	}
}

</script>
'; ?>


<table id="client_table" class="flexigrid" style="display:none"></table>