<?php
$path_root = $_SERVER[DOCUMENT_ROOT];
$str_all = '';
$site_host = 'http://enigme.ru';
if ($_SESSION['admin'] == 'allow'){
	$cat_module = $_GET['worker'];
	$cat_main 	= $_GET['main'];
	// �������� ���� �������
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $cat_module)) 
	err_message("� ��� ��� ���� �� ��������.");

	if ($_GET['action'] == "generate_sitemap"){
		$str_all = '<?xml version="1.0" encoding="utf-8"?>'."\n";
		$str_all.='<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">'."\n";
		$str_all.= '<url>'."\n";
		$str_all.= '   <loc>'.$site_host.'</loc>'."\n";
		$str_all.= '   <lastmod>'.date("Y-m-d").'</lastmod>'."\n";
		$str_all.= '   <changefreq>daily</changefreq>'."\n";
		$str_all.= '   <priority>0.9</priority>'."\n";
		$str_all.= '</url>'."\n";
		
		$sql_m_catalog_block = ' AND id != 106 AND id != 83 AND id != 176 AND id != 460';
		$select = "SELECT "
		."url "
		."FROM m_catalog "
		."WHERE is_block=0{$sql_m_catalog_block} "
		."ORDER BY id";
		$stmt = $db->query($select);
		while ($content = $stmt->fetch()) {
			$str_all.= '<url>'."\n";
			$str_all.= '   <loc>'.$site_host.'/catalog/'.$content['url'].'/</loc>'."\n";
			$str_all.= '   <lastmod>'.date("Y-m-d").'</lastmod>'."\n";
			$str_all.= '   <changefreq>daily</changefreq>'."\n";
			$str_all.= '   <priority>0.8</priority>'."\n";
			$str_all.= '</url>'."\n";
		}
		
		$sql_m_catalog_block = ' AND a.id != 106 AND a.id != 83 AND a.id != 176 AND a.id != 460';
		$select = "SELECT "
		."a.url AS brend_url, b.url AS product_url "
		."FROM m_catalog AS a LEFT JOIN m_catalog_data AS b ON a.id = b.id_catalog "
		."WHERE a.is_block=0 AND b.is_block=0{$sql_m_catalog_block} "
		."ORDER BY a.id";
		$stmt = $db->query($select);
		while ($content = $stmt->fetch()) {
			$str_all.= '<url>'."\n";
			$str_all.= '   <loc>'.$site_host.'/catalog/'.$content['brend_url'].'/'.$content['product_url'].'/</loc>'."\n";
			$str_all.= '   <lastmod>'.date("Y-m-d").'</lastmod>'."\n";
			$str_all.= '   <changefreq>daily</changefreq>'."\n";
			$str_all.= '   <priority>0.8</priority>'."\n";
			$str_all.= '</url>'."\n";
		}

		$c_cat =
		"SELECT canon_name "
		."FROM base_cat "
		."WHERE is_block = 0 AND id IN ('86','82','84','85','87','95','98','99') "
		."ORDER BY id ";
		$stmt = $db->query($c_cat);
		while ($content = $stmt->fetch()) {
			$str_all.= '<url>'."\n";
			$str_all.= '   <loc>'.$site_host.'/'.$content['canon_name'].'/</loc>'."\n";
			$str_all.= '   <lastmod>'.date("Y-m-d").'</lastmod>'."\n";
			$str_all.= '   <changefreq>daily</changefreq>'."\n";
			$str_all.= '   <priority>0.8</priority>'."\n";
			$str_all.= '</url>'."\n";
		}
		
		$m_have =
		"SELECT id, url "
		."FROM m_news "
		."WHERE is_block = 0 AND id_cat = 84 "
		."ORDER BY id ";
		$stmt = $db->query($m_have);
		while ($content = $stmt->fetch()) {
			$str_all.= '<url>'."\n";
			$str_all.= '   <loc>'.$site_host.'/must-have/'.$content['url'].'/</loc>'."\n";
			$str_all.= '   <lastmod>'.date("Y-m-d").'</lastmod>'."\n";
			$str_all.= '   <changefreq>daily</changefreq>'."\n";
			$str_all.= '   <priority>0.6</priority>'."\n";
			$str_all.= '</url>'."\n";
		}
		
		$m_discount =
		"SELECT id, url "
		."FROM m_news "
		."WHERE is_block = 0 AND id_cat = 87 "
		."ORDER BY id ";
		$stmt = $db->query($m_discount);
		while ($content = $stmt->fetch()) {
			$str_all.= '<url>'."\n";
			$str_all.= '   <loc>'.$site_host.'/discount/'.$content['url'].'/</loc>'."\n";
			$str_all.= '   <lastmod>'.date("Y-m-d").'</lastmod>'."\n";
			$str_all.= '   <changefreq>daily</changefreq>'."\n";
			$str_all.= '   <priority>0.6</priority>'."\n";
			$str_all.= '</url>'."\n";
		}
		
		$str_all.= '<url>'."\n";
		$str_all.= '   <loc>'.$site_host.'/sale/'.'</loc>'."\n";
		$str_all.= '   <lastmod>'.date("Y-m-d").'</lastmod>'."\n";
		$str_all.= '   <changefreq>daily</changefreq>'."\n";
		$str_all.= '   <priority>0.6</priority>'."\n";
		$str_all.= '</url>'."\n";
		$str_all.= '<url>'."\n";
		$str_all.= '   <loc>'.$site_host.'/sale/page/2/'.'</loc>'."\n";
		$str_all.= '   <lastmod>'.date("Y-m-d").'</lastmod>'."\n";
		$str_all.= '   <changefreq>daily</changefreq>'."\n";
		$str_all.= '   <priority>0.6</priority>'."\n";
		$str_all.= '</url>'."\n";
		$str_all.= '<url>'."\n";
		$str_all.= '   <loc>'.$site_host.'/sale/page/3/'.'</loc>'."\n";
		$str_all.= '   <lastmod>'.date("Y-m-d").'</lastmod>'."\n";
		$str_all.= '   <changefreq>daily</changefreq>'."\n";
		$str_all.= '   <priority>0.6</priority>'."\n";
		$str_all.= '</url>'."\n";
		
		$str_all.= '</urlset>';
		if ($fp = @fopen($_SERVER["DOCUMENT_ROOT"]."/tmp/sitemap.xml", 'wb')) {
			@fwrite($fp, $str_all);
			@fclose($fp);
			$status = 'sitemap_status_ok';
		} else {
			$status = 'sitemap_status_err';
		}
		go("?main=$cat_main&module=$cat_module&action=mod&$status");
		break;
	}
}
?>