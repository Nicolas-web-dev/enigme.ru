<?php
/**
 * Created by PhpStorm.
 * User: loram
 * Date: 23.10.14
 * Time: 17:28
 */
interface Order_Delivery_Uploads_Interface {

    /**
     * @param null $file
     *
     * @return mixed
     */
    public function parse($file = null);

    /**
     * @return mixed
     */
    public function show();

    /**
     * @param bool $isConfirm
     *
     * @return mixed
     */
    public function confirm($isConfirm = false);

    /**
     * @return mixed
     */
    public function redir();
}