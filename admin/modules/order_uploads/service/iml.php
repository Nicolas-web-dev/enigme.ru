<?php

require_once "uploads_interface.php";

/**
 * Created by PhpStorm.
 * User: loram
 * Date: 02.10.14
 * Time: 17:45
 */
class Order_Delivery_Uploads_Iml extends Order_Delivery_Uploads implements Order_Delivery_Uploads_Interface {

    /**
     * �������� ���������� ������� ��� �������������
     * @var string
     */
    private $_imlTable = "m_mag_Orders_Iml";

    public function __construct()
    {

    }

    /**
     * @param null $file
     *
     * @return $this
     * @throws Exception
     */
    public function parse($file = null)
    {
        if ($file == null) {
            throw new Exception("���� �� ��� ��������");
        }

        $pattern = "/".$this->_sitePrefix."-(\d+)/";

        $csv = new CCSVData();
        $csv->LoadFile($file);

        // ������� ������� ������
        $this->clearTable();

        while ($line = $csv->Fetch()) {

            // �������� ������������ �������� � ������
            if (!preg_match($pattern, $line[0], $matches)) {
                continue;
            }

            $sql = mysql_query("SELECT * FROM " . $this->_orderTable . " WHERE id = " . $matches[1] . " LIMIT 1");
            $result = mysql_fetch_assoc($sql);

            if ($result != null) {

                // �������� ������ ����
                $date_payment = DateTime::createFromFormat("j.m.Y", $line[4]);
                $date_payment = $date_payment->format("Y-m-d");

                $date_money_income = DateTime::createFromFormat("j.m.Y", $line[8]);
                $date_money_income = $date_money_income->format("Y-m-d");

                $delivery_cost = (float) str_replace(",", ".", $line[9]) + (float) str_replace(",", ".", $line[10]);

                $firm = $firm = $line[1] == "������" ? "plus" : "web";

                mysql_query("INSERT DELAYED INTO " . $this->_imlTable . "
                (
                    order_id,
                    date_payment,
                    date_money_income,
                    money_fact,
                    delivery_cost,
                    firm
                ) VALUES (
                    " . (int) $matches[1] . ",
                    \"" . $date_payment . "\",
                    \"" . $date_money_income . "\",
                    " . (float) str_replace(",", ".", $line[7]) . ",
                    " . (float) $delivery_cost . ",
                    \"" . mysql_real_escape_string($firm) . "\"
                )");
            }
        }

        return $this;
    }

    public function redir()
    {
        go("?main=order_uploads&module=order_uploads&action=index&type=iml");
    }

    public function show()
    {
        $sql = mysql_query("SELECT * FROM " . $this->_imlTable);

        if (mysql_num_rows($sql) > 0) {

            echo "<table><thead><tr>";
            echo "<th>����� ������</th><th>���� ������</th><th>���� ����������� �����</th><th>���������� �����</th><th>������������ �����</th><th>������ �� ��������</th><th>�����</th>";
            echo "</tr></thead><tbody>";

            $totalMoneyIncome = 0;
            $totalDeliveryCost = 0;

            while ($row = mysql_fetch_assoc($sql)) {

                $is_correct = "<span style='color: red;'>����� �����������!</span>";

                $sql1 = mysql_query("SELECT * FROM m_mag_Orders WHERE id = " . $row['order_id']. " LIMIT 1");
                if ($item = mysql_fetch_assoc($sql1)) {
                    if (($item['dSum']+$item['delivery_cost']) == $row["money_fact"]) {
                        $is_correct = "<span style='color: green;'>����� ���������</span>";
                    }
                }

                $arr = array(
                    $row["order_id"],
                    $row["date_payment"],
                    $row["date_money_income"],
                    $row["money_fact"],
                    $is_correct,
                    $row["delivery_cost"],
                    $row["firm"]
                );

                $totalMoneyIncome += $row["money_fact"];
                $totalDeliveryCost += $row["delivery_cost"];

                echo "<tr><td>";
                echo implode("</td><td>", $arr);
                echo "</td></tr>";
            }

            echo "<tr><td>";
            echo "<strong>����� ��������: </strong></td><td> {$totalMoneyIncome}�.</td><td><strong>����� �� ��������: </strong></td><td> {$totalDeliveryCost}�.</td><td></td><td>";
            echo "</td></tr>";

            echo "</tr></tbody></table>";

            echo "<a class='btn' href='?main=order_uploads&module=order_uploads&action=index&type=iml&confirm=0'>���������</a>";
            echo "<a class='btn btn-primary' href='?main=order_uploads&module=order_uploads&action=index&type=iml&confirm=1'>�����������</a>";
        } else {

            echo "������������ ����������. ������� ������������� �����.";
        }

        return $this;
    }

    public function confirm($isConfirm = false)
    {
        if (!$isConfirm) {
            mysql_query("TRUNCATE " . $this->_imlTable);
            return true;
        }

        $sql = mysql_query("SELECT * FROM " . $this->_imlTable);
        if (mysql_num_rows($sql) > 0) {
            while ($row = mysql_fetch_assoc($sql)) {

                $logistic = "";

                if($row["date_payment"] != "0000-00-00 00:00:00") {
                    $logistic = ", logistic = '2'";
                }

                if ($row["date_money_income"] != "0000-00-00 00:00:00") {
                    $logistic = ", logistic = '3'";
                }

                if ($row["money_fact"] == 0) {
                    $logistic = ", logistic = '4'";
                }

                mysql_query("UPDATE ". $this->_orderTable ."
                    SET
                     delivery_cost_fact = $row[delivery_cost],
                     TotalMoneyIncome = $row[money_fact],
                     DateMoneyIncome = '$row[date_money_income]',
                     payment_date = '$row[date_payment]',
                     firm = '$row[firm]'$logistic
                    WHERE
                     id = $row[order_id]"
                );
            }
        }

        return $this->confirm(false);
    }
}