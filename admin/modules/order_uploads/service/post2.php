<?php

require_once "uploads_interface.php";

/**
 * Created by PhpStorm.
 * User: loram
 * Date: 07.11.14
 * Time: 16:29
 */
class Order_Delivery_Uploads_Post2 extends Order_Delivery_Uploads implements Order_Delivery_Uploads_Interface
{

    /**
     * �������� ���������� ������� ��� �������������
     * @var string
     */
    private $_postTable = "m_mag_Orders_Post2";

    public function __construct()
    {

    }

    /**
     * @param null $file
     *
     * @return $this
     * @throws Exception
     */
    public function parse($file = null)
    {
        if ($file == null) {
            throw new Exception("���� �� ��� ��������");
        }

        $csv = new CCSVData();
        $csv->LoadFile($file);

        // ������� ������� ������
        $this->clearTable();

        while ($line = $csv->Fetch()) {

            // �������� ������������ ������� �������
            if (!is_numeric($line[0])) {
                continue;
            }

            $sql = mysql_query("SELECT * FROM " . $this->_orderTable . " WHERE track = " . $line[0] . " LIMIT 1");
            $result = mysql_fetch_assoc($sql);

            if ($result != null) {

                // �������� ������ ����
                $date = DateTime::createFromFormat("j.m.Y", $line[5]);
                $date = $date->format("Y-m-d");

                $firm = $firm = preg_match("/������/", $line[12]) ? "plus" : "web";

                mysql_query("INSERT INTO " . $this->_postTable . "
                (
                    date_money_income,
                    money_fact,
                    track_id,
                    firm
                ) VALUES (
                    \"" . $date . "\",
                    " . (float) str_replace(",", ".", $line[2]) . ",
                    " . (int)$line[0] . ",
                    \"" . mysql_real_escape_string($firm) . "\"
                )");
            }
        }

        return $this;
    }

    public function redir()
    {
        go("?main=order_uploads&module=order_uploads&action=index&type=post2");
    }

    public function show()
    {
        $sql = mysql_query("SELECT * FROM " . $this->_postTable);

        if (mysql_num_rows($sql) > 0) {

            echo "<table><thead><tr>";
            echo "<th>����� �����������</th><th>���� ����������� �����</th><th>���������� ��������</th><th>������������ �����</th><th>�����</th>";
            echo "</tr></thead><tbody>";

            while ($row = mysql_fetch_assoc($sql)) {

                $is_correct = "<span style='color: red;'>����� �����������!</span>";

                $sql1 = mysql_query("SELECT * FROM m_mag_Orders WHERE track = " . $row['track_id']. " LIMIT 1");
                if ($item = mysql_fetch_assoc($sql1)) {
                    if (($item['dSum']+$item['delivery_cost']) == $row["money_fact"]) {
                        $is_correct = "<span style='color: green;'>����� ���������</span>";
                    }
                }

                $arr = array(
                    $row["track_id"],
                    $row["date_money_income"],
                    $row["money_fact"],
                    $is_correct,
                    $row["firm"]
                );

                echo "<tr><td>";
                echo implode("</td><td>", $arr);
                echo "</td></tr>";
            }

            echo "</tr></tbody></table>";

            echo "<a class='btn' href='?main=order_uploads&module=order_uploads&action=index&type=post2&confirm=0'>���������</a>";
            echo "<a class='btn btn-primary' href='?main=order_uploads&module=order_uploads&action=index&type=post2&confirm=1'>�����������</a>";
        } else {

            echo "������������ ����������. ������� ������������� �����.";
        }

        return $this;
    }

    public function confirm($isConfirm = false)
    {
        if (!$isConfirm) {
            mysql_query("TRUNCATE " . $this->_postTable);
            return true;
        }

        $sql = mysql_query("SELECT * FROM " . $this->_postTable);
        if (mysql_num_rows($sql) > 0) {
            while ($row = mysql_fetch_assoc($sql)) {

                mysql_query("UPDATE ". $this->_orderTable ."
                    SET
                     TotalMoneyIncome = $row[money_fact],
                     DateMoneyIncome = '$row[date_money_income]',
                     firm = '$row[firm]'
                    WHERE
                     track = $row[track_id]"
                );
            }
        }

        return $this->clearTable();
    }
}