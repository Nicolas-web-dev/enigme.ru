<?php
/**
 * Created by PhpStorm.
 * User: loram
 * Date: 17.11.14
 * Time: 18:01
 */

require_once "uploads_interface.php";

class Order_Delivery_Uploads_Status extends Order_Delivery_Uploads implements Order_Delivery_Uploads_Interface
{

    /**
     * �������� ���������� ������� ��� �������������
     * @var string
     */
    private $_postTable = "m_mag_Orders_Status";

    public function __construct()
    {

    }

    /**
     * @param null $file
     *
     * @return $this
     * @throws Exception
     */
    public function parse($file = null)
    {
        if ($file == null) {
            throw new Exception("���� �� ��� ��������");
        }

        $csv = new CCSVData();
        //$csv->SetDelimiter(",");
        $csv->LoadFile($file);

        // ������� ������� ������
        $this->clearTable();

        while ($line = $csv->Fetch()) {

            // �������� ������������ ������� �������
            if (!preg_match("/enigme/", $line[0])) {
                continue;
            }

            $pattern = "/".$this->_sitePrefix."-(\d+)/";
            $order_id = $line[1];

            if (!is_numeric($order_id)) {
                if (!preg_match($pattern, $order_id, $matches)) {
                    continue;
                } else {
                    $order_id = $matches[1];
                }
            }


            $sql = mysql_query("SELECT * FROM " . $this->_orderTable . " WHERE id = " . $order_id . " LIMIT 1");
            $result = mysql_fetch_assoc($sql);

            if ($result != null) {

                // �������� ������ ����
                $date = DateTime::createFromFormat("j.m.Y", $line[3]);
                //$date->add(new DateInterval('P2000Y'));
                $date = $date->format("Y-m-d");

                $firm = $firm = preg_match("/��/", $line[4]) ? "plus" : "web";

                mysql_query("INSERT INTO " . $this->_postTable . "
                (
                    DateSendCourier,
                    order_id,
                    firm
                ) VALUES (
                    '$date',
                    $order_id,
                    '$firm'
                )");
            }
        }

        return $this;
    }

    public function redir()
    {
        go("?main=order_uploads&module=order_uploads&action=index&type=status");
    }

    public function show()
    {
        $sql = mysql_query("SELECT * FROM " . $this->_postTable);

        if (mysql_num_rows($sql) > 0) {

            echo "<table><thead><tr>";
            echo "<th>����� ������</th><th>���� ����������� � ��</th><th>�����</th>";
            echo "</tr></thead><tbody>";

            while ($row = mysql_fetch_assoc($sql)) {

                $arr = array(
                    $row["order_id"],
                    $row["DateSendCourier"],
                    $row["firm"],
                    "<br>\r\n"
                );

                echo "<tr><td>";
                echo implode("</td><td>", $arr);
                echo "</td></tr>";
            }

            echo "</tr></tbody></table>";

            echo "<a class='btn' href='?main=order_uploads&module=order_uploads&action=index&type=status&confirm=0'>���������</a>";
            echo "<a class='btn btn-primary' href='?main=order_uploads&module=order_uploads&action=index&type=status&confirm=1'>�����������</a>";
        } else {

            echo "������������ ����������. ������� ������������� �����.";
        }

        return $this;
    }

    public function confirm($isConfirm = false)
    {
        if (!$isConfirm) {
            mysql_query("TRUNCATE " . $this->_postTable);
            return true;
        }

        $sql = mysql_query("SELECT * FROM " . $this->_postTable);
        if (mysql_num_rows($sql) > 0) {
            while ($row = mysql_fetch_assoc($sql)) {

                mysql_query("UPDATE ". $this->_orderTable ."
                    SET
                     DateSendCourier = $row[DateSendCourier],
                     firm = '$row[firm]',
                     logistic = '1'
                    WHERE
                     id = $row[order_id]"
                );
            }
        }

        return $this->clearTable();
    }
}