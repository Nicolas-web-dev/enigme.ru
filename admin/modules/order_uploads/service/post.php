<?php

require_once "uploads_interface.php";

/**
 * Created by PhpStorm.
 * User: loram
 * Date: 02.10.14
 * Time: 17:44
 */
class Order_Delivery_Uploads_Post extends Order_Delivery_Uploads implements Order_Delivery_Uploads_Interface
{

    private $_parseDataNames = array(
        "KodBan",
        "DtMesh",
        "VesTarif",
        "StrTarif",
        "firm",
        "Barkod"
    );

    private $_parsedNamesCols = array();

    /**
     * �������� ���������� ������� ��� �������������
     * @var string
     */
    private $_postTable = "m_mag_Orders_Post";

    public function __construct()
    {
        $this->phone = mysql_result(mysql_query("SELECT text FROM m_text WHERE id=5"),0,0);
    }

    /**
     * @param null $file
     *
     * @return $this
     * @throws Exception
     */
    public function parse($file = null)
    {
        if ($file == null) {
            throw new Exception("���� �� ��� ��������");
        }

        $csv = new CCSVData();
        $csv->LoadFile($file);
        $f = 0;

        // ������� ������� ������
        $this->clearTable();

        while ($line = $csv->Fetch()) {

            if ($f === 0) {

                foreach ($line as $key => $name) {
                    if (in_array($name, $this->_parseDataNames)) {
                        $this->_parsedNamesCols[$name] = $key;
                    }
                }

                ++$f;
                continue;
            }

            $sql = mysql_query("SELECT * FROM " . $this->_orderTable . " WHERE id = " . $line[$this->_parsedNamesCols["KodBan"]] . " LIMIT 1");
            $result = mysql_fetch_assoc($sql);

            if ($result != null) {

                // �������� ������ ����
                $date = DateTime::createFromFormat("j.m.Y", $line[$this->_parsedNamesCols["DtMesh"]]);
                $date->add(new DateInterval('P2000Y'));
                $date = $date->format("Y-m-d");

                $firm = $line[$this->_parsedNamesCols["firm"]] == "��" ? "plus" : "web";

                mysql_query("INSERT DELAYED INTO " . $this->_postTable . "
                (
                    KodBan,
                    DtMesh,
                    VesTarif,
                    StrTarif,
                    order_id,
                    firm
                ) VALUES (
                    " . (int)$line[$this->_parsedNamesCols["Barkod"]] . ",
                    \"" . $date . "\",
                    " . (float) str_replace(",", ".", $line[$this->_parsedNamesCols["VesTarif"]]) . ",
                    " . (float) str_replace(",", ".", $line[$this->_parsedNamesCols["StrTarif"]]) . ",
                    " . (int)$line[$this->_parsedNamesCols["KodBan"]] . ",
                    \"" . mysql_real_escape_string($firm) . "\"
                )");
            }
        }

        return $this;
    }

    public function redir()
    {
        go("?main=order_uploads&module=order_uploads&action=index&type=post");
    }

    public function show()
    {
        $sql = mysql_query("SELECT * FROM " . $this->_postTable);

        if (mysql_num_rows($sql) > 0) {

            echo "<table><thead><tr>";
            echo "<th>����� ������</th><th>Kodbar</th><th>���� ����������� �����</th><th>������ �� ��������</th><th>�����</th>";
            echo "</tr></thead><tbody>";

            $totalDeliveryCost = 0;

            while ($row = mysql_fetch_assoc($sql)) {
                $arr = array(
                    $row["order_id"],
                    $row["KodBan"],
                    $row["DtMesh"],
                    $row["VesTarif"] + $row["StrTarif"],
                    $row["firm"]
                );

                echo "<tr><td>";
                echo implode("</td><td>", $arr);
                echo "</td></tr>";

                $totalDeliveryCost += $row["VesTarif"] + $row["StrTarif"];
            }

            echo "<tr><td></td><td></td><td><strong>����� �� ��������: </strong></td><td> {$totalDeliveryCost}�.</td><td></td><td>";
            echo "</td></tr>";

            echo "</tr></tbody></table>";

            echo "<a class='btn' href='?main=order_uploads&module=order_uploads&action=index&type=post&confirm=0'>���������</a>";
            echo "<a class='btn btn-primary' href='?main=order_uploads&module=order_uploads&action=index&type=post&confirm=1'>�����������</a>";
        } else {

            echo "������������ ����������. ������� ������������� �����.";
        }

        return $this;
    }

    /**
     * @param bool $isConfirm
     *
     * @return bool
     */
    public function confirm($isConfirm = false)
    {
        if (!$isConfirm) {
            mysql_query("TRUNCATE " . $this->_postTable);
            return true;
        }

        $sql = mysql_query("SELECT * FROM " . $this->_postTable);
        if (mysql_num_rows($sql) > 0) {
            while ($row = mysql_fetch_assoc($sql)) {

                mysql_query("UPDATE ". $this->_orderTable ."
                    SET
                     track = $row[KodBan],
                     track_date = '$row[DtMesh]',
                     delivery_cost_fact = $row[VesTarif] + $row[StrTarif],
                     deliveryTarif = $row[VesTarif],
                     deliveryIns = $row[StrTarif],
                     firm = '$row[firm]'
                    WHERE
                     id = $row[order_id]"
                );

                // �������� ����������� ������ ������
                $sql = mysql_query("SELECT * FROM " . $this->_orderTable . " WHERE id = ". $row['order_id']);
                $order = mysql_fetch_assoc($sql);

                // �������� ������������ � ���������� ��� email � �������
                $sql = mysql_query("SELECT * FROM " . $this->_userTable . " WHERE id = ". $order['id_user']);
                $client = mysql_fetch_assoc($sql);

                $this->sendEmail($client,
                    array(
                        'order_id' => $row['order_id'],
                        'track_date' => $row['DtMesh'],
                        'track' => $row['KodBan']
                    )
                );
            }
        }

        return $this->clearTable();
    }

    /**
     * �������� ������ �������
     * @param $client
     * @param $order
     * @todo �������� email
     *
     * @throws Zend_Mail_Exception
     */
    private function sendEmail($client, $order)
    {
        $data = array(
            'order_id' => $order['order_id'],
            'fio' => $client['fio'],
            'date2' => $order['track_date'],
            'post_code' => $order['track'],
            'global_phone' => $this->phone
        );

        $message_body = parser('mail_message_order_post.tpl', $data, 0);

        $mail = new Zend_Mail('windows-1251');
        $mail->setBodyHtml($message_body);
        $mail->setFrom('office@enigme.ru', 'enigme');
        //$mail->addTo($client['email'], $client['fio']);
        $mail->addTo('mr.nobody.lip@gmail.com', '��');
        $mail->setSubject("Enigme.ru - ��� �������� �������������");
        $mail->send();
    }
}