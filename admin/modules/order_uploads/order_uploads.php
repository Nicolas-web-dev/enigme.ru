<?php
/**
 * Created by PhpStorm.
 * User: loram
 * Date: 02.10.14
 * Time: 17:04
 */

error_reporting(E_ALL);
ignore_user_abort(1);
set_time_limit(0);
date_default_timezone_set("Europe/Moscow");

ini_set('include_path', ini_get('include_path') . ':includes/library');
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();

require_once "obj.php";

if ($_SESSION['admin'] == 'allow') {

    $uploads = new Order_Delivery_Uploads;
    $deliveryTypeObj = $uploads->start($_GET['type']);

    $deliveryTypeObj->parse($_FILES['file']['tmp_name'])
        ->redir();
}