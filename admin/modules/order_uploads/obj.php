<?php

require_once "service/post.php";
require_once "service/post2.php";
require_once "service/iml.php";
require_once "service/status.php";
require_once "service/bxb.php";
require_once 'Zend/Mail.php';

/**
 * Created by PhpStorm.
 * User: loram
 * Date: 06.11.14
 * Time: 16:01
 */
class Order_Delivery_Uploads {

    /**
     * ��� ������ �������, ����������� ������ � ����� �����
     * @var string
     */
    protected $_sitePrefix = "[RE|ME]";

    /**
     * �������� ������� �������
     * @var string
     */
    protected $_orderTable = "m_mag_Orders";

    /**
     * �������� ������� �������
     * @var string
     */
    protected $_userTable = "m_mag_Users";

    /**
     * ����������� ���� ����������� ��������
     * @var array
     */
    private $_allowedTypes = array(
        'iml',
        'post',
        'post2',
        'status',
        'bxb'
    );

    /**
     * ���������� ������� ������
     * @var null|string
     */
    public $phone = null;

    /**
     * ��� �������
     * @var null|string
     */
    private $_type = null;

    public function __construct() {

    }

    /**
     * @param $type
     *
     * @return Order_Delivery_Uploads_Interface|string
     * @throws Exception
     */
    public function start($type) {

        if (in_array($type, $this->_allowedTypes)) {
            $this->_type = $type;
        } else {
            throw new Exception("����������� ��� �������");
        }

        $obj = "Order_Delivery_Uploads_".ucfirst($this->_type);
        /** @var $obj Order_Delivery_Uploads_Interface */
        $obj = new $obj;

        return $obj;
    }

    public function confirm($isConfirm = false)
    {
        return true;
    }

    public function clearTable()
    {
        return $this->confirm(false);
    }
}