<?php
/**
 * Created by PhpStorm.
 * User: loram
 * Date: 05.11.14
 * Time: 16:07
 */

error_reporting(E_ALL);
ignore_user_abort(1);
set_time_limit(0);
date_default_timezone_set("Europe/Moscow");

ini_set('include_path', ini_get('include_path') . ':includes/library');
require_once __DIR__.'./../obj.php';

$uploads = new Order_Delivery_Uploads;
$deliveryTypeObj = $uploads->start($_GET['type']);

if (isset($_GET['confirm'])) {
    $deliveryTypeObj->confirm($_GET['confirm']);
}

$deliveryTypeObj->show();

$sm = new Smarty_Admin($valinorConfig['interface.themes']);
$sm->display("modules/order_uploads/index.tpl");