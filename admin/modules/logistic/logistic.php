<?php
/**
 * Created by PhpStorm.
 * User: loram
 * Date: 08.04.14
 * Time: 16:20
 */
if ($_SESSION['admin'] == 'allow'){

    error_reporting(E_ALL);
    ignore_user_abort(1);
    set_time_limit(0);
    date_default_timezone_set("Europe/Moscow");

    // ����������� ����������
    $module 	= "logistic";
    $main 		= "logistic";

    $table_c 	= "couriers";
    $table      = "couriers_addresses";

    switch ($_GET['action']){
        case "csv":

            // ������ �� ����������
            $sql = "UPDATE $table SET is_active=0";
            mysql_query($sql);

            $csv = new CCSVData();
            $csv->LoadFile($_FILES['file']['tmp_name']);
            $f = 0;

            while ( $line = $csv->Fetch() ){

                // ���������� ������ ������
                if ($f < 1) {
                    ++$f;
                    continue;
                }

                $data = array();

                $data['kladr_id_region']    = !isset($line[0]) ? null : $line[0];
                $data['kladr_id_city']      = !isset($line[1]) ? null : $line[1];
                $data['name_region']        = !isset($line[2]) ? null : $line[2];
                $data['name_city']          = !isset($line[3]) ? null : $line[3];

                for($j = 1; $j < 9; $j++) {

                    $data['code_region'] = !isset($line[$j*17-13]) ? null : $line[$j*17-13];
                    $data['code_city'] = !isset($line[$j*17-12]) ? null : $line[$j*17-12];
                    $data['is_active'] = !isset($line[$j*17-11]) ? null : (int) $line[$j*17-11];
                    $data['delivery_days'] = !isset($line[$j*17-10]) ? null : (int) $line[$j*17-10];
                    $data['delivery_type'] = !isset($line[$j*17-9]) ? null : $line[$j*17-9];
                    $data['delivery_code'] = !isset($line[$j*17-8]) ? null : $line[$j*17-8];
                    $data['delivery_metro'] = !isset($line[$j*17-7]) ? null : $line[$j*17-7];
                    $data['delivery_address'] = !isset($line[$j*17-6]) ? null : $line[$j*17-6];
                    $data['delivery_how_to'] = !isset($line[$j*17-5]) ? null : $line[$j*17-5];
					$data['delivery_payment'] = !isset($line[$j*17-4]) ? null : $line[$j*17-4];
                    $data['delivery_cost_1'] = !isset($line[$j*17-3]) ? null : (int) $line[$j*17-3];
                    $data['delivery_cost_3'] = !isset($line[$j*17-2]) ? null : (int) $line[$j*17-2];
                    $data['delivery_news_to_operator'] = !isset($line[$j*17-1]) ? null : $line[$j*17-1];
                    $data['delivery_about_to_operator'] = !isset($line[$j*17]) ? null : $line[$j*17];
                    $data['delivery_our_code'] = !isset($line[$j*17+1]) ? null : $line[$j*17+1];
                    $data['percentage_kass'] = !isset($line[$j*17+2]) ? null : preg_replace('/,/', '.', $line[$j*17+2]);
                    $data['percentage_insurance'] = !isset($line[$j*17+3]) ? null : preg_replace('/,/', '.', $line[$j*17+3]);

                    $data['courier_id'] = $j;

                    // ���������� ������ ������
                    if ($data['delivery_our_code'] == null || $data['delivery_our_code'] == "")
                        continue;

                    // ��������� ������ ��� ����������
                    $keys = implode(',', array_keys($data));
                    $values = implode('\', \'', $data);

                    // ���� �� � ��
                    $sql = "SELECT * FROM $table WHERE
                                courier_id = '$data[courier_id]'
                                AND kladr_id_region = '$data[kladr_id_region]'
                                AND kladr_id_city = '$data[kladr_id_city]'
                                AND name_region = '$data[name_region]'
                                AND name_city = '$data[name_city]'
                                AND delivery_code = '$data[delivery_code]'
                                LIMIT 1";

                    $result = mysql_fetch_array(mysql_query($sql));

                    // ���� ����� ������� ��, ���������
                    if ($result != false) {

                        $dataToUpdate = array();
                        foreach($data as $k => $v) {
                            $dataToUpdate[] = "$k = '$v'";
                        }
                        $dataToUpdate = implode(', ', $dataToUpdate);

                        $sql = "UPDATE $table SET $dataToUpdate WHERE id = $result[id]";
                    } else {
                        $sql = "INSERT $table ($keys) VALUES ('$values')";
                    }

                    mysql_query($sql);
                }

                ++$f;
            }

            go("?main=$main&module=$module&action=upload");
            break;
        case "save":


            $postData = $_POST;
            unset($postData['save']);
            unset($postData['save_list']);

            $id = (int) $postData['id'];

            if ($id == null || $postData['kladr_id_region'] == null ||
                $postData['kladr_id_city'] == null || $postData['name_region'] == null ||
                $postData['name_city'] == null || $postData['courier_id'] == null) {

                err_message("�� ������� ������������ ������");
            }

            // ��������� ������
            $postData['percentage_insurance'] = preg_replace('/,/', '.', $postData['percentage_insurance']);
            $postData['percentage_kass'] = preg_replace('/,/', '.', $postData['percentage_kass']);

            $postData['delivery_cost_1'] = (int) $postData['delivery_cost_1'];
            $postData['delivery_cost_3'] = (int) $postData['delivery_cost_3'];

            $postData['is_active'] = strtolower($postData['is_active']);
            $postData['delivery_days'] = (int) $postData['delivery_days'];
            $postData['delivery_code'] = (int) $postData['delivery_code'];

            $postData['is_active'] = 1;

            $dataToUpdate = array();
            foreach($postData as $k => $v) {
                $dataToUpdate[] = "$k = '$v'";
            }
            $dataToUpdate = implode(', ', $dataToUpdate);

            $sql = "UPDATE $table SET $dataToUpdate WHERE id = $id";
            mysql_query($sql);

            if (isset($_POST['save']))
                go("?main=$main&module=$module&action=view&id=$id");
            elseif (isset($_POST['save_list']))
                go("?main=$main&module=$module&action=index");

            break;
    }
}