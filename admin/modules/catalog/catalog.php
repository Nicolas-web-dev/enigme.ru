<?php
function generateCsvOut($filename = 'catalog_data')
{
    @unlink("../images/uploads/$filename.csv");
    $csv = new CCSVData();

    $q_catalog = mysql_query("SELECT id,title,articul FROM m_catalog  ORDER BY id");
    while ($c_catalog = mysql_fetch_array($q_catalog)) {
        $item = array('', '', '', '', '');
        $csv->SaveFile("../images/uploads/$filename.csv", $item);

        $item = array('1', $c_catalog['id'], $c_catalog['articul'], format_text_out($c_catalog['title']));
        $csv->SaveFile("../images/uploads/$filename.csv", $item);

        $query = mysql_query("SELECT id, title, articul, text, tag, pol FROM m_catalog_data WHERE id_catalog='{$c_catalog['id']}' ORDER BY id");
        while ($content = mysql_fetch_array($query)) {

            $item = array('2', $content['id'], $content['articul'], format_text_out($content['title']), $content['pol']);
            $csv->SaveFile("../images/uploads/$filename.csv", $item);


            $q_mod = mysql_query("SELECT * FROM m_catalog_data_order WHERE id_catalog_data='{$content['id']}'  ");
            while ($c_mod = mysql_fetch_array($q_mod)) {

                $item = array('3', $c_mod['id'], $c_mod['articul'], $c_mod['type'], $c_mod['v'], $c_mod['article'], $c_mod['article2'], $c_mod['article3'], $c_mod['article4'], $c_mod['article5'], $c_mod['article6'], $c_mod['article7'], $c_mod['article8'], $c_mod['article9'], $c_mod['article10'], $c_mod['article11'],$c_mod['article12'],$c_mod['article13'],$c_mod['article14'],$c_mod['article15'],$c_mod['article16'],$c_mod['article17'],$c_mod['article18']);
                $csv->SaveFile("../images/uploads/$filename.csv", $item);
            }
        }
    }
    return $filename;
}


if ($_GET['ajax_check_cat'] == 1) {
	session_start();
	if (isset($_POST['id_cat_check']) && isset($_POST['check_cat'])) {
		$_SESSION['cat_id_check'][(int)$_POST['id_cat_check']] = (int)$_POST['check_cat'];
	}
}
else {


if ($_SESSION['admin'] == 'allow') {
    $id_cat = intval($_GET['id_cat']);
    $cat_module = $_GET['worker'];
    $cat_main = $_GET['main'];
    $cat_name = mysql_result(mysql_query("SELECT name FROM base_cat WHERE id='$id_cat'"), 0, 0);

    switch ($_GET['action']) {

        //==========================================================+
        // ���������� �������� ��������
        //==========================================================+
        case "add":

            // �������� �� �������
            if ($_POST['sel_catalog'] == '0'
                or $_POST['title'] == ''
                or $_POST['url'] == ''
            ) {
                err_message("�� ��������� ������������ ����.");
            }

            //�������� �� ������������ ���
            $sql = "SELECT * FROM m_catalog_data WHERE url='{$_POST['url']}' and id_catalog={$_POST['sel_catalog']}";

            $result = mysql_query($sql);

            if ($row = mysql_fetch_assoc($result)){
                err_message("����� url ��� ����������.");
            }

            // �������� ������
            $sel_catalog = $_POST['sel_catalog'];
            $title = format_text(trim($_POST['title']));
            $url = translit($_POST['url']);
            $tag = trim($_POST['tag']);
            $tagl = translit_tag($tag);
            $pol = $_POST['pol'];

            // META TAG
            if (isset($_POST['is_meta_title'])) $is_meta_title = 1; else $is_meta_title = 0;
            if (isset($_POST['is_meta_description'])) $is_meta_description = 1; else $is_meta_description = 0;
            if ($is_meta_title) $meta_title = trim($_POST['meta_title']); else $meta_title = $title;
            //if ($is_meta_description) $meta_description = trim($_POST['meta_description']); else $meta_description = HTMLToTxt( maxsite_str_word($_POST['anot'], 50) );
            $meta_description = trim($_POST['meta_description']);
            $meta_title_1 = trim($_POST['meta_title_1']);

            // ��������� ����������
            $sort = trim($_POST['sort']);
            if (isset($_POST['is_block'])) {
                $is_block = 1;
            } else {
                $is_block = 0;
            }
            if (isset($_POST['is_main'])) {
                $is_main = 1;
            } else {
                $is_main = 0;
            }
            if (isset($_POST['is_subs'])) {
                $is_subs = 1;
            } else {
                $is_subs = 0;
            }
            if (isset($_POST['is_sale'])) {
                $is_sale = 1;
            } else {
                $is_sale = 0;
            }

            //����������� �������
            $sql = "(SELECT `articul` FROM m_catalog) UNION ALL
                    (SELECT `articul` FROM m_catalog_data) UNION ALL
                    (SELECT `articul` FROM m_catalog_data_order) ORDER BY articul DESC LIMIT 1";
            $articul = mysql_fetch_assoc(mysql_query($sql));
            $articul_number = str_replace('PF', '', $articul['articul']);

            $new_articul = 'PF' . str_pad($articul_number + 1, 7, '0', STR_PAD_LEFT);


            // ��������� ������
            $text = ($_POST['text']);
            $text2 = ($_POST['text2']);
            $alt = trim($_POST['alt']);

            $sql = "INSERT INTO m_catalog_data
			(id_cat, id_catalog, articul,
			title, text, text2, img, alt,
			registerDate, 
			is_block, is_main, is_subs, is_sale,
			sort, url, tag, tagl,
			pol,
			meta_title, meta_title_1, meta_description)
			VALUES (
			'$id_cat', '$sel_catalog', '$new_articul',
			'$title', '$text','$text2', '$img', '$alt',
			NOW(),
			'$is_block', '$is_main', '$is_subs', '$is_sale',
			'$sort', '$url', '$tag', '$tagl',
			'$pol',
			'$meta_title', '$meta_title_1', '$meta_description')";

            mysql_query($sql);
            $id_catalog = DGetLast();

            // ������
            if ($_FILES['img']['name'] != '') {

                // ������������ ����������
                $path = '../images/';
                $mkdir = 'uploads/catalog/' . $id_catalog;
                $mkdir_trumbs = $mkdir . '/trumbs/';
                $mkdir_small = $mkdir . '/small/';
                $mkdir_big = $mkdir . '/big/';

                if (!file_exists($path . $mkdir)) {
                    mkdir($path . $mkdir);
                    mkdir($path . $mkdir_trumbs);
                    mkdir($path . $mkdir_small);
                    mkdir($path . $mkdir_big);
                }

                $time = time();

                $img = upload_image($_FILES['img']['tmp_name'], $time, $path . $mkdir_small, $valinorConfig['catalog.small.w'], $valinorConfig['catalog.small.w']);
                upload_image($_FILES['img']['tmp_name'], $time, $path . $mkdir_big, $valinorConfig['catalog.big.w'], $valinorConfig['catalog.big.w']);

            } else {
                $img = '';
            }

            // �������
            if (isset($_POST['save']))
                go("?main=$cat_main&module=$cat_module&action=edit&id_cat=$id_cat&id=$id_catalog");
            elseif (isset($_POST['save_add']))
                go("?main=$cat_main&module=$cat_module&action=add&id_cat=$id_cat");
            elseif (isset($_POST['save_list']))
                go("?main=$cat_main&module=$cat_module&action=mod&id_cat=$id_cat");


            break;

        //==========================================================+
        // �������������� �������� ������
        //==========================================================+
        case "edit":

            error_reporting(E_ALL);
            ini_set("display_errors", 1);


            // �������� �� �������
            if ($_POST['sel_catalog'] == '0'
                or $_POST['title'] == ''
            ) {
                err_message("�� ��������� ������������ ����.");
            }






            $id_catalog = $_GET['id'];

            // �������� ������
            $sel_catalog = $_POST['sel_catalog'];
            $title = mysql_real_escape_string(format_text(trim($_POST['title'])));
            //$url		= translit($_POST['url']);
            $tag = trim($_POST['tag']);
            $tagl = translit_tag($tag);
            $pol = $_POST['pol'];

            // Meta
            $meta_title = mysql_real_escape_string(trim($_POST['meta_title']));
            $meta_title_1 = mysql_real_escape_string(trim($_POST['meta_title_1']));
            $meta_description = mysql_real_escape_string($_POST['meta_description']);

            // ��������� ����������
            $sort = trim($_POST['sort']);
            if (isset($_POST['is_block'])) {
                $is_block = 1;
            } else {
                $is_block = 0;
            }
            if (isset($_POST['is_main'])) {
                $is_main = 1;
            } else {
                $is_main = 0;
            }
            if (isset($_POST['is_subs'])) {
                $is_subs = 1;
            } else {
                $is_subs = 0;
            }
            if (isset($_POST['is_sale'])) {
                $is_sale = 1;
            } else {
                $is_sale = 0;
            }

            if (isset($_POST['is_holiday'])) {
                $is_holiday = 1;
            } else {
                $is_holiday = 0;
            }

            if (isset($_POST['is_holiday_23'])) {
                $is_holiday_23 = 1;
            } else {
                $is_holiday_23 = 0;
            }

            if (isset($_POST['is_holiday_8'])) {
                $is_holiday_8 = 1;
            } else {
                $is_holiday_8 = 0;
            }

            // ��������� ������
            $text = ($_POST['text']);
            $text2 = ($_POST['text2']);
            $alt = trim($_POST['alt']);

            if ($_POST['image_path'] != '') {
				$image_path = iconv("CP1251", "UTF-8", $_POST['image_path']);
                // ������������ ����������
                $path = $_SERVER['DOCUMENT_ROOT'] . '/images/';
                $mkdir = 'uploads/catalog/' . $id_catalog;
                $mkdir_trumbs = $mkdir . '/trumbs/';
                $mkdir_small = $mkdir . '/small/';
                $mkdir_big = $mkdir . '/big/';

                if (!file_exists($path . $mkdir)) {
                    mkdir($path . $mkdir,0775);
                    mkdir($path . $mkdir_trumbs,0775);
                    mkdir($path . $mkdir_small,0775);
                    mkdir($path . $mkdir_big,0775);
                }

                $time = time();
                $img = upload_image($_SERVER['DOCUMENT_ROOT'] . $image_path, $time, $path . $mkdir_small, $valinorConfig['catalog.small.w'], $valinorConfig['catalog.small.w']);
                upload_image($_SERVER['DOCUMENT_ROOT'] . $image_path, $time, $path . $mkdir_big, $valinorConfig['catalog.big.w'], $valinorConfig['catalog.big.w']);

                $time = date('Y-m-d H:i:s');
                $sql = "UPDATE m_catalog_data SET `img`='{$img}', `img_update`=NOW() WHERE `id`={$id_catalog}";
                mysql_query($sql);

                //�����������

                $user_id = $_SESSION['user_id'];
                $target_id = $id_catalog;
                $id_cat = $id_cat;
                $log_text = '��������� ����������';
                $data['action'] = '��������� ����������';

                //�������� �������
                $log_text = mysql_real_escape_string($log_text);
                $sql = "INSERT INTO `m_catalog_history` (`user_id`,`target_id`,`text`,`id_cat`,`action`) VALUES ($user_id,$target_id,'{$log_text}',$id_cat,{$data['action']}";

                mysql_query($sql);
            }


            $db1251->update('m_catalog_data',[
                'id_catalog'=>$sel_catalog,
                'title'=>$title,
                'text'=>$text,
                'text2'=>$text2,
                'alt'=>$alt,
                'is_block'=>$is_block,
                'is_main'=>$is_main,
                'is_subs'=>$is_subs,
                'is_sale'=>$is_sale,
                'is_holiday'=>$is_holiday,
                'is_holiday_23'=>$is_holiday_23,
                'is_holiday_8'=>$is_holiday_8,
                'sort'=>$sort,
                'tag'=>$tag,
                'tagl'=>$tagl,
                'pol'=>$pol,
                'meta_title'=>$meta_title,
                'meta_title_1'=>$meta_title_1,
                'meta_description'=>$meta_description
            ],'id='.$id_catalog );

			
			if (isset($_SESSION['cat_id_check'])){
				foreach ($_SESSION['cat_id_check'] as $id_cat=>$ch_cat) {
					$sql = "SELECT id_category FROM m_category_product WHERE (id_product='{$id_catalog}' AND id_category='{$id_cat}')";
					$result = mysql_query($sql);
					if ($row = mysql_fetch_assoc($result)) {
						
							if ((int)$ch_cat == 0) {
								@mysql_query("DELETE FROM m_category_product WHERE (id_product='$id_catalog' AND id_category='$id_cat')");
							}
					
					}
					else {
						if ((int)$ch_cat == 1) {
							$sql = "INSERT INTO m_category_product (id_category, id_product) VALUES ('$id_cat', '$id_catalog')";
							mysql_query($sql);
						}
					}
				}
			}

            // �������
            if (isset($_POST['save']))
                go("?main=$cat_main&module=$cat_module&action=edit&id_cat=$id_cat&id=$id_catalog");
            elseif (isset($_POST['save_add']))
                go("?main=$cat_main&module=$cat_module&action=add&id_cat=$id_cat");
            elseif (isset($_POST['save_list']))
                go("?main=$cat_main&module=$cat_module&action=mod&id_cat=$id_cat");

            break;

        //==========================================================+
        // ���������� ������� ��������
        //==========================================================+
        case "catalog_add":
            die();

            if ($_POST['title'] == ''
                or $_POST['sort'] == ''
                or $_POST['url'] == ''
            ) {
                err_message("�� ��������� ������������ ����.");
            }

            //  �������� ����
            $url = translit($_POST['url']);
            $is_url = mysql_result(mysql_query("SELECT count(id) FROM m_catalog WHERE url='$url' AND id_cat='$id_cat' "), 0, 0);
            if ($is_url != 0) err_message("$url - ����� ������������ ��� ��� ���������� � ����. �������� ������ ��� ��������������� �������");

            // �������� ����������
            $title = format_text($_POST['title']);
            $anot = ($_POST['anot']);
            $sort = trim($_POST['sort']);
            $pol = trim($_POST['pol']);
            if (isset($_POST['is_block'])) {
                $is_block = 1;
            } else {
                $is_block = 0;
            }
            if (isset($_POST['is_top'])) {
                $is_top = 1;
            } else {
                $is_top = 0;
            }

            // META TAG
            if (isset($_POST['is_meta_title'])) $is_meta_title = 1; else $is_meta_title = 0;
            if (isset($_POST['is_meta_description'])) $is_meta_description = 1; else $is_meta_description = 0;
            if ($is_meta_title) $meta_title = trim($_POST['meta_title']); else $meta_title = $title;
            //if ($is_meta_description) $meta_description = trim($_POST['meta_description']); else $meta_description = HTMLToTxt( maxsite_str_word($_POST['anot'], 50) );
            $meta_description = trim($_POST['meta_description']);
            $meta_title_1 = trim($_POST['meta_title_1']);

            // ���������� ��������������
            $sel_catalog = intval($_POST['sel_catalog']);
            if ($sel_catalog != 0) {
                $is_level = mysql_result(mysql_query("SELECT id_parent FROM m_catalog WHERE id='$sel_catalog'"), 0, 0);
                if ($is_level != 0) err_message("�� ����������� ����������� ������ ������� ������.");
            }

            $sql = "INSERT INTO m_catalog (id, id_cat, id_parent, title, anot, url, sort, pol, is_block, is_top,
			meta_title, meta_title_1, meta_description)
			VALUES ('','$id_cat', '$sel_catalog', '$title', '$anot','$url', '$sort', '$pol', '$is_block', '$is_top',
			'$meta_title', '$meta_title_1', '$meta_description')";

            mysql_query($sql);
            $id_catalog = DGetLast();

            // �������
            if (isset($_POST['save']))
                go("?main=$cat_main&module=$cat_module&action=catalog_edit&id_cat=$id_cat&id_catalog=$id_catalog");
            elseif (isset($_POST['save_add']))
                go("?main=$cat_main&module=$cat_module&action=catalog_add&id_cat=$id_cat");
            elseif (isset($_POST['save_list']))
                go("?main=$cat_main&module=$cat_module&action=catalog&id_cat=$id_cat");
            break;

        //==========================================================+
        // �������������� ������� �������
        //==========================================================+
        case "catalog_edit":
            if ($_POST['title'] == ''
                or $_POST['sort'] == ''
                or $_POST['url'] == ''
            ) {
                err_message("�� ��������� ������������ ����.");
            }
            $id_catalog = intval($_GET['id_catalog']);

            // �������� ����
            $url = translit($_POST['url']);
            $old_url = mysql_result(mysql_query("SELECT url FROM m_catalog WHERE id='$id_catalog'  "), 0, 0);
            if ($old_url != $url) {
                $is_url = mysql_result(mysql_query("SELECT count(id) FROM m_catalog WHERE url='$url' AND id_cat='$id_cat' "), 0, 0);
                if ($is_url != 0) err_message("$url - ����� ������������ ��� ��� ���������� � ����.");
            }

            // ��������� �����
            $title = format_text($_POST['title']);
            $anot = ($_POST['anot']);
            $text2 = ($_POST['text2']);
            $sort = trim($_POST['sort']);
            $pol = trim($_POST['pol']);
            if (isset($_POST['is_block'])) $is_block = 1; else$is_block = 0;
            if (isset($_POST['is_lux'])) $is_lux = 1; else $is_lux = 0;
            if (isset($_POST['is_top'])) $is_top = 1; else $is_top = 0;


            // Meta
            $meta_title = trim($_POST['meta_title']);
            $meta_title_1 = trim($_POST['meta_title_1']);
            $meta_description = $_POST['meta_description'];

            // ���������� ��������������
            $sel_catalog = intval($_POST['sel_catalog']);
            if ($sel_catalog != 0) {
                $is_level = mysql_result(mysql_query("SELECT id_parent FROM m_catalog WHERE id='$sel_catalog'"), 0, 0);
                if ($is_level != 0) err_message("�� ����������� ����������� ������ ������� ������.");
            }



            $db1251->update('m_catalog',[
                'id_parent'=>$sel_catalog,
                'title'=>$title,
                'anot'=>$anot,
                'text2'=>$text2,
                'url'=>$url,
                'sort'=>$sort,
                'pol'=>$pol,
                'is_block'=>$is_block,
                'is_top'=>$is_top,
                'is_lux'=>$is_lux,
                'meta_title'=>$meta_title,
                'meta_title_1'=>$meta_title_1,
                'meta_description'=>$meta_description
            ],'id='.$id_catalog );


            // �������
            if (isset($_POST['save']))
                go("?main=$cat_main&module=$cat_module&action=catalog_edit&id_cat=$id_cat&id_catalog=$id_catalog");
            elseif (isset($_POST['save_add']))
                go("?main=$cat_main&module=$cat_module&action=catalog_add&id_cat=$id_cat");
            elseif (isset($_POST['save_list']))
                go("?main=$cat_main&module=$cat_module&action=catalog&id_cat=$id_cat");
            break;
        case "recovery_catalog":

            @mysql_query("TRUNCATE m_catalog;");
            @mysql_query("TRUNCATE m_catalog_data;");
            @mysql_query("TRUNCATE m_catalog_data_order;");


            @mysql_query("INSERT m_catalog SELECT * FROM recovery_m_catalog;");
            @mysql_query("INSERT m_catalog_data SELECT * FROM recovery_m_catalog_data;");
            @mysql_query("INSERT m_catalog_data_order SELECT * FROM recovery_m_catalog_data_order;");
            go("?main=$cat_main&module=$cat_module&action=csv_price&id_cat=$id_cat");
            break;
        case "get_csv_price":
            //==========================================================+
            // �������� ���� CSV ��� ���������� ����
            //==========================================================+
            $filename = generateCsvOut();

            go("?main=$cat_main&module=$cat_module&action=csv_price&id_cat=$id_cat&file=$filename");
            break;

        //==========================================================+
        // �������� ���� CSV ��� ���������� ����
        //==========================================================+
        case "csv_price_out":

            //==========================================================+
            // �������� ���� CSV ��� 1c
            //==========================================================+
            /*$filename = 'catalog_data_1c';
            @unlink("../images/uploads/$filename.csv");
            $csv = new CCSVData();

            $q_mod = mysql_query("SELECT * FROM m_catalog_data_order");
            while ($c_mod = mysql_fetch_array($q_mod)){

                $c_catalog_data = mysql_fetch_assoc(mysql_query("SELECT id, title, id_catalog, pol FROM m_catalog_data WHERE id='{$c_mod['id_catalog_data']}' LIMIT 1"));
                $c_brend = mysql_fetch_assoc(mysql_query("SELECT id,title FROM m_catalog WHERE id='{$c_catalog_data['id_catalog']}' LIMIT 1 "));

                $title = "[{$c_catalog_data['pol']}] {$c_brend['title']} {$c_catalog_data['title']} {$aroma_type[$c_mod['type']]} {$c_mod['v']}ml";

                $item = array(
                    $c_mod['articul'],
                    $title
                );

                $csv->SaveFile("../images/uploads/$filename.csv", $item);

            }
            go("?main=$cat_main&module=$cat_module&action=csv_price&id_cat=$id_cat&file=$filename");*/

            //==========================================================+
            // ��������� ������ ��� ����� _m
            //==========================================================+
            /*$q_data = mysql_query("SELECT id, id_catalog, url, pol, RegisterDate FROM x_catalog_data WHERE pol='M' ");
            while ($c_data = mysql_fetch_array($q_data)){

                $id = $c_data['id'];
                $url = $c_data['url'];
                $pol = $c_data['pol'];
                $id_catalog = $c_data['id_catalog'];

                $is = mysql_result(mysql_query("SELECT count(*) FROM x_catalog_data WHERE pol='F' and url='$url' and id_catalog='$id_catalog' "),0,0);
                if ($is!=0) {
                    $new_url = $url . '_m';
                    echo $c_data['RegisterDate'] . ' - ' . $new_url.'<br />';
                    //mysql_query("UPDATE x_catalog_data SET url='$new_url' WHERE id='$id' ");
                }
            }die();*/


            /* -=========================================
            ���������� � ���������� ����� ��������
            */

            if ($_FILES['file']['name'] != '') {

                $filename = $_FILES['file']['tmp_name'];
                $csv = new CCSVData();
                $csv->LoadFile($filename);
                $max = 0;

                while ($line = $csv->Fetch()) {

                    $art = (int)str_replace('PF', '', $line[2]);
                    if ($max < $art) {
                        $max = $art;
                    }

                }
            }
            echo 'Max Art ' . $max . '<br />';


            if ($_FILES['file']['name'] != '') {

                $filename = $_FILES['file']['tmp_name'];
                $csv = new CCSVData();
                $csv->LoadFile($filename);

                @mysql_query("DROP TABLE IF EXISTS recovery_m_catalog");
                @mysql_query("DROP TABLE IF EXISTS recovery_m_catalog_data");
                @mysql_query("DROP TABLE IF EXISTS recovery_m_catalog_data_order");

                @mysql_query("CREATE TABLE recovery_m_catalog LIKE m_catalog;");
                @mysql_query("CREATE TABLE recovery_m_catalog_data LIKE m_catalog_data;");
                @mysql_query("CREATE TABLE recovery_m_catalog_data_order LIKE m_catalog_data_order;");

                @mysql_query("INSERT recovery_m_catalog SELECT * FROM m_catalog;");
                @mysql_query("INSERT recovery_m_catalog_data SELECT * FROM m_catalog_data;");
                @mysql_query("INSERT recovery_m_catalog_data_order SELECT * FROM m_catalog_data_order;");

                $filename = generateCsvOut('recovery');


                while ($line = $csv->Fetch()) {

                    $line_type = intval($line[0]); // type
                    $line_p1 = trim($line[1]); // action
                    $articul = trim($line[2]); // art
                    $line_p2 = $line[3]; // name/mod
                    $line_p3 = $line[4]; // pol/ml
                    $line_p4 = $line[5]; // a1
                    $line_p5 = $line[6]; // a2
                    $line_p6 = $line[7]; // a3
                    $line_p7 = $line[8]; // a4
                    $line_p8 = $line[9]; // a5
                    $line_p9 = $line[10]; // a6
                    $line_p10 = $line[11]; // a7
                    $line_p11 = $line[12]; // a8
                    $line_p12 = $line[13]; // a9
                    $line_p13 = $line[14]; // a10
                    $line_p14 = $line[15]; // a11
                    $line_p15 = $line[16]; // a12
                    $line_p16 = $line[17]; // a13
                    $line_p17 = $line[18]; // a14
                    $line_p18 = $line[19]; // a15
                    $line_p19 = $line[20]; // a16
                    $line_p20 = $line[21]; // a17
                    $line_p21 = $line[22]; // a18



                    // �����
                    if ($line_type == 1) {

                        if ($line_p1 == '+') {

                            $max = $max + 1;
                            $newarticle = 'PF00' . $max;

                            $title = trim($line_p2);
                            $url = translit($line_p2);

                            $sql = "INSERT INTO m_catalog "
                                . "(id_cat, title, url, meta_title, articul )"
                                . "VALUES ('$id_cat', '$title', '$url', '$title', '$newarticle'  )";
                            mysql_query($sql);

                            $count_1++;
                            $id_brend = DGetLast();


                        } elseif ($line_p1 == '-') {

                            $newarticle = $articul;
                            if ($newarticle == '') continue;
                            $c_brend = mysql_fetch_assoc(mysql_query("
								SELECT id,title 
								FROM m_catalog
								WHERE articul='$newarticle' 
								LIMIT 1 
							"));


                            $query = mysql_query("SELECT id FROM m_catalog_data WHERE id_catalog='{$c_brend['id']}'");
                            while ($content = mysql_fetch_array($query)) {

                                $id_catalog = $content['id'];

                                @mysql_query("DELETE FROM m_catalog_data WHERE id='$id_catalog' ");
                                @mysql_query("DELETE FROM m_catalog_data_order WHERE id_catalog_data='$id_catalog' ");

                            }
                            @mysql_query("DELETE FROM m_catalog WHERE id='{$c_brend['id']}'");


                        } else {
                            //change isset records (brand title)
                            $newarticle = $articul;
                            if ($newarticle == '') continue;
                            $c_brend = mysql_fetch_assoc(mysql_query("
								SELECT id
								FROM m_catalog
								WHERE articul='$newarticle' 
								LIMIT 1 
							"));
                            $id_brend = $c_brend['id'];

                            $title = trim(format_text($line_p2));

                            $sql = "
								UPDATE
									m_catalog
								SET
									title='$title'
								WHERE
									id='$id_brend'
							";
                            mysql_query($sql);


                        }

                        // ������
                    } elseif ($line_type == 2) {

                        if ($line_p1 == '+') {

                            $max = $max + 1;
                            $newarticle = 'PF00' . $max;

                            $title = trim(format_text($line_p2));
                            $url = translit(trim($line_p2));
                            $pol = strtoupper(trim($line_p3));

                            $sql = "INSERT INTO m_catalog_data "
                                . "(id_cat, id_catalog, title, url, registerDate,  pol, meta_title, articul )"
                                . "VALUES ('$id_cat', '$id_brend', '$title', '$url', NOW(), '$pol', '$title', '$newarticle' )";

                            if ($id_brend != 0) {
                                mysql_query($sql);
                                $id_catalog_data = DGetLast();
                            } else {
                                continue;
                            }
                            $count_2++;


                        } elseif ($line_p1 == '-') {

                            $newarticle = $articul;
                            if ($newarticle == '') continue;
                            $c_catalog_data = mysql_fetch_assoc(mysql_query("
								SELECT id
								FROM m_catalog_data
								WHERE articul='$newarticle' 
								LIMIT 1 
							"));

                            @mysql_query("DELETE FROM m_catalog_data_order WHERE id_catalog_data='{$c_catalog_data['id']}' ");
                            @mysql_query("DELETE FROM m_catalog_data WHERE id='{$c_catalog_data['id']}' ");


                        } else {
                            //change isset records (title, pol)

                            $newarticle = $articul;
                            if ($newarticle == '') continue;
                            $c_catalog_data = mysql_fetch_assoc(mysql_query("
								SELECT id
								FROM m_catalog_data
								WHERE articul='$newarticle' 
								LIMIT 1 
							"));
                            $id_catalog_data = $c_catalog_data['id'];

                            $title = trim(format_text($line_p2));
                            $pol = strtoupper(trim($line_p3));

                            $sql = "
								UPDATE
									m_catalog_data
								SET
									title='$title',
									pol='$pol'
								WHERE
									id='$id_catalog_data'
								";
                            $result = mysql_query($sql);


                        }

                        // ����������
                    } elseif ($line_type == 3) {

                        if ($line_p1 == '+') {

                            $max = $max + 1;
                            $newarticle = 'PF00' . $max;

                            $sel_type = strtoupper(trim($line_p2));
                            $v = trim($line_p3);
                            $article = strtoupper(trim($line_p4));
                            $article2 = strtoupper(trim($line_p5));
                            $article3 = strtoupper(trim($line_p6));
                            $article4 = strtoupper(trim($line_p7));
                            $article5 = strtoupper(trim($line_p8));
                            $article6 = strtoupper(trim($line_p9));
                            $article7 = strtoupper(trim($line_p10));
                            $article8 = strtoupper(trim($line_p11));
                            $article9 = strtoupper(trim($line_p12));
                            $article10 = strtoupper(trim($line_p13));
                            $article11 = strtoupper(trim($line_p14));

                            $article12 = strtoupper(trim($line_p15));
                            $article13 = strtoupper(trim($line_p16));
                            $article14 = strtoupper(trim($line_p17));
                            $article15 = strtoupper(trim($line_p18));

                            $article16 = strtoupper(trim($line_p19));
                            $article17 = strtoupper(trim($line_p20));
                            $article18 = strtoupper(trim($line_p21));


                            echo $sql = "INSERT INTO m_catalog_data_order "
                                . "(id_catalog_data, article, article2, article3, article4, article5, article6, article7, article8, article9, article10, article11,article12,article13,article14,article15,article16,article17,article18 type, v, articul )"
                                . "VALUES ('$id_catalog_data', '$article', '$article2', '$article3', '$article4', '$article5', '$article6', '$article7', '$article8', '$article9', '$article10','$article11','$article12','$article13','$article14','$article15','$article16','$article17','$article18', '$sel_type', '$v', '$newarticle' )";

                            $count_3++;


                            // �������� �� ������� � ������������
                            if ($id_catalog_data != 0) {
                                mysql_query($sql);
                            }


                        } elseif ($line_p1 == '-') {

                            $newarticle = $articul;
                            if ($newarticle == '') continue;
                            $c_catalog_data_order = mysql_fetch_assoc(mysql_query("
								SELECT id
								FROM m_catalog_data_order
								WHERE articul='$newarticle' 
								LIMIT 1 
							"));

                            @mysql_query("DELETE FROM m_catalog_data_order WHERE id='{$c_catalog_data_order['id']}' ");


                        } else {

                            $newarticle = $articul;
                            if ($newarticle == '') continue;
                            $c_catalog_data_order = mysql_fetch_assoc(mysql_query("
								SELECT id
								FROM m_catalog_data_order
								WHERE articul='$newarticle' 
								LIMIT 1 
							"));
                            $id_order = $c_catalog_data_order['id'];


                            $sel_type = strtoupper(trim($line_p2));
                            $v = trim($line_p3);
                            $article = strtoupper(trim($line_p4));
                            $article2 = strtoupper(trim($line_p5));
                            $article3 = strtoupper(trim($line_p6));
                            $article4 = strtoupper(trim($line_p7));
                            $article5 = strtoupper(trim($line_p8));
                            $article6 = strtoupper(trim($line_p9));
                            $article7 = strtoupper(trim($line_p10));
                            $article8 = strtoupper(trim($line_p11));
                            $article9 = strtoupper(trim($line_p12));
                            $article10 = strtoupper(trim($line_p13));
                            $article11 = strtoupper(trim($line_p14));
                            $article12 = strtoupper(trim($line_p15));
                            $article13 = strtoupper(trim($line_p16));
                            $article14 = strtoupper(trim($line_p17));
                            $article15 = strtoupper(trim($line_p18));
                            $article16 = strtoupper(trim($line_p19));
                            $article17 = strtoupper(trim($line_p20));
                            $article18 = strtoupper(trim($line_p21));

                            $sql = "
								UPDATE
									m_catalog_data_order
								SET
									article='$article',
									article2='$article2',
									article3='$article3',
									article4='$article4',
									article5='$article5',
									article6='$article6',
									article7='$article7',
									article8='$article8',
									article9='$article9',
									article10='$article10',
									article11='$article11',
									article12='$article12',
									article13='$article13',
									article14='$article14',
									article15='$article15',
									article16='$article16',
									article17='$article17',
									article18='$article18',
									type='$sel_type',
									v='$v'
								WHERE
									id='$id_order'
								";
                            mysql_query($sql);

                        }
                    }
                }

                echo "����� ������� - $count_1<br />";
                echo "����� �������� - $count_2<br />";
                echo "����� ����������� - $count_3<br />";


                die();

                go("?main=$cat_main&module=$cat_module&action=csv_price&id_cat=82");

            } else {
                err_message('�� ������ ���� ��� ��������');
            }


            break;

        //==========================================================+
        // ������ CSV ��� ���������� ���
        //==========================================================+
        case "csv":


            //==========================================================+
            // ������� ������� � ��������
            //==========================================================+
            /*$query = mysql_query("SELECT id, article FROM m_catalog_data_order ");
            while ($content = mysql_fetch_array($query)){
            $id = $content['id'];
            $article = eregi_replace('[^a-z0-9]', '', $content['article']);
            mysql_query("UPDATE m_catalog_data_order SET article='$article' WHERE id='$id' ");
            }*/


            //==========================================================+
            // ���������� ������� � �����
            //==========================================================+
            /*if ($_FILES['file']['name']!=''){

            $filename = $_FILES['file']['tmp_name'];
            $csv = new CCSVData();
            $csv->LoadFile($filename);

            while ( $line = $csv->Fetch() ){
            $id =  intval($line[0]);
            $title1 =  format_text(trim($line[2]));
            $title2 =  format_text(trim($line[3]));

            if ($id != 0) {
            mysql_query("UPDATE m_catalog SET title_1='$title1', title_2='$title2' WHERE id='$id' ");
            }
            }

            go("?main=$cat_main&module=$cat_module&action=csv&id_cat=$id_cat&load=1");

            }else {
            err_message('�� ������ ���� ��� ��������');
            }
            */

            //==========================================================+
            // ���������� ���� - �����
            //==========================================================+
            /*$q_data = mysql_query("SELECT id, title FROM m_catalog ");
            while ($c_data = mysql_fetch_array($q_data)){
            mysql_query("UPDATE m_catalog SET meta_title='{$c_data['title']}' WHERE id='{$c_data['id']}' ");
            }*/


            break;

        //==========================================================+
        // ���������� �������� ����
        //==========================================================+
        case "catalog_data_order_add":

            if ($_POST['v'] == ''
            ) {
                err_message("�� ��������� ������������ ����.");
            }

            $id_catalog_data = $_GET['id_catalog_data'];
            $sel_type = $_POST['sel_type'];
            $v = $_POST['v'];
            if (isset($_POST['is_block'])) {
                $is_block = 1;
            } else {
                $is_block = 0;
            }

            $article = $_POST['article'];
            $article2 = $_POST['article2'];
            $article3 = $_POST['article3'];
            $article4 = $_POST['article4'];
            $article5 = $_POST['article5'];
            $article6 = $_POST['article6'];
            $article7 = $_POST['article7'];
            $article8 = $_POST['article8'];
            $article9 = $_POST['article9'];
            $article10 = $_POST['article10'];
            $article11 = $_POST['article11'];
            $article12 = $_POST['article12'];
            $article13 = $_POST['article13'];
            $article14 = $_POST['article14'];
            $article15 = $_POST['article15'];
            $article16 = $_POST['article16'];
            $article17 = $_POST['article17'];
            $article18 = $_POST['article18'];

            $price_usd1 = (str_replace(",", ".", $_POST['price_usd1']));
            $price_usd2 = (str_replace(",", ".", $_POST['price_usd2']));
            $price_usd3 = (str_replace(",", ".", $_POST['price_usd3']));
            $price_usd4 = (str_replace(",", ".", $_POST['price_usd4']));
            $price_usd5 = (str_replace(",", ".", $_POST['price_usd5']));
            $price_usd6 = (str_replace(",", ".", $_POST['price_usd6']));
            $price_usd7 = (str_replace(",", ".", $_POST['price_usd7']));
            $price_usd8 = (str_replace(",", ".", $_POST['price_usd8']));
            $price_usd9 = (str_replace(",", ".", $_POST['price_usd9']));
            $price_usd10 = (str_replace(",", ".", $_POST['price_usd10']));
            $price_usd11 = (str_replace(",", ".", $_POST['price_usd11']));
            $price_usd12 = (str_replace(",", ".", $_POST['price_usd12']));
            $price_usd13 = (str_replace(",", ".", $_POST['price_usd13']));
            $price_usd14 = (str_replace(",", ".", $_POST['price_usd14']));
            $price_usd15 = (str_replace(",", ".", $_POST['price_usd15']));
            $price_usd16 = (str_replace(",", ".", $_POST['price_usd16']));
            $price_usd17 = (str_replace(",", ".", $_POST['price_usd17']));
            $price_usd18 = (str_replace(",", ".", $_POST['price_usd18']));

            // ����������� ���������
            $priority = catalog_priority($price_usd1, $price_usd2, $price_usd3, $price_usd4, $price_usd5, $price_usd6, $price_usd7, $price_usd8, $price_usd9, $price_usd10, $price_usd11, $price_usd12, $price_usd13, $price_usd14, $price_usd15,$price_usd16,$price_usd17,$price_usd18);


            if ($is_block == 0) {
                $price = catalog_creat_price($priority['price']);
            } else {
                $price = intval($_POST['price']);
                $price_old = intval($_POST['price_old']);
            }

            //����������� �������
            $sql = "(SELECT `articul` FROM m_catalog) UNION ALL
                    (SELECT `articul` FROM m_catalog_data) UNION ALL
                    (SELECT `articul` FROM m_catalog_data_order) ORDER BY articul DESC LIMIT 1";
            $articul = mysql_fetch_assoc(mysql_query($sql));
            $articul_number = str_replace('PF', '', $articul['articul']);

            $new_articul = 'PF' . str_pad($articul_number + 1, 7, '0', STR_PAD_LEFT);



            $sql = "
				INSERT INTO m_catalog_data_order 
				(
					id_catalog_data,
					articul,
					article,
					article2,
					article3,
					article4,
					article5,
					article6,
					article7, 
					article8, 
					article9, 
					article10, 
					article11, 
					article12,
					article13,
					article14,
					article15,
					article16,
					article17,
					article18,
					type,
					v, 
					price, 
					price_old,
					price_usd1,
					price_usd2,
					price_usd3,
					price_usd4,
					price_usd5,
					price_usd6,
					price_usd7, 
					price_usd8, 
					price_usd9, 
					price_usd10, 
					price_usd11, 
					price_usd12,
					price_usd13,
					price_usd14,
					price_usd15,
					price_usd16,
					price_usd17,
					price_usd18,
					is_block,
					sklad
				)
				VALUES 
				(
					'$id_catalog_data',
					'$new_articul',
					'$article', 
					'$article2', 
					'$article3', 
					'$article4', 
					'$article5', 
					'$article6', 
					'$article7', 
					'$article8', 
					'$article9', 
					'$article10', 
					'$article11', 
					'$article12',
					'$article13',
					'$article14',
					'$article15',
					'$article16',
					'$article17',
					'$article18',
					'$sel_type',
					'$v', 
					'$price', 
					'$price_old', 
					'$price_usd1', 
					'$price_usd2', 
					'$price_usd3', 
					'$price_usd4', 
					'$price_usd5', 
					'$price_usd6', 
					'$price_usd7', 
					'$price_usd8', 
					'$price_usd9', 
					'$price_usd10', 
					'$price_usd11', 
					'$price_usd12',
					'$price_usd13',
					'$price_usd14',
					'$price_usd15',
					'$price_usd16',
					'$price_usd17',
					'$price_usd18',
					'$is_block',
					'{$priority['sklad']}'
				)
			";
            $return = mysql_query($sql);
            $id_order = DGetLast();

            // ������ ��������
            $pr = mysql_fetch_array(mysql_query("SELECT MAX(price) as max_price, MIN(price) as min_price FROM m_catalog_data_order WHERE id_catalog_data='$id_catalog_data' AND price!=0 "));
            mysql_query("UPDATE m_catalog_data SET price_s='{$pr['min_price']}', price_e='{$pr['max_price']}' WHERE id='$id_catalog_data' ");


            // �������
            if (isset($_POST['save']))
                go("?main=$cat_main&module=$cat_module&action=catalog_data_order_edit&id_cat=$id_cat&id_catalog_data=$id_catalog_data&id_order=$id_order");
            elseif (isset($_POST['save_add']))
                go("?main=$cat_main&module=$cat_module&action=catalog_data_order_add&id_cat=$id_cat&id_catalog_data=$id_catalog_data");
            elseif (isset($_POST['save_list']))
                go("?main=$cat_main&module=$cat_module&action=catalog_data_order&id_cat=$id_cat&id_catalog_data=$id_catalog_data");
            break;


        //==========================================================+
        // �������������� �������� ����
        //==========================================================+
        case "catalog_data_order_edit":
            if ($_POST['v'] == ''
            ) {
                err_message("�� ��������� ������������ ����.");
            }

            $id_catalog_data = $_GET['id_catalog_data'];
            $id_order = $_GET['id_order'];
            $sel_type = $_POST['sel_type'];
            $v = $_POST['v'];
            if (isset($_POST['is_block'])) {
                $is_block = 1;
            } else {
                $is_block = 0;
            }

            $article = $_POST['article'];
            $article2 = $_POST['article2'];
            $article3 = $_POST['article3'];
            $article4 = $_POST['article4'];
            $article5 = $_POST['article5'];
            $article6 = $_POST['article6'];
            $article7 = $_POST['article7'];
            $article8 = $_POST['article8'];
            $article9 = $_POST['article9'];
            $article10 = $_POST['article10'];
            $article11 = $_POST['article11'];
            $article12 = $_POST['article12'];
            $article13 = $_POST['article13'];
            $article14 = $_POST['article14'];
            $article15 = $_POST['article15'];
            $article16 = $_POST['article16'];
            $article17 = $_POST['article17'];
            $article18 = $_POST['article18'];

            $price_usd1 = (str_replace(",", ".", $_POST['price_usd1']));
            $price_usd2 = (str_replace(",", ".", $_POST['price_usd2']));
            $price_usd3 = (str_replace(",", ".", $_POST['price_usd3']));
            $price_usd4 = (str_replace(",", ".", $_POST['price_usd4']));
            $price_usd5 = (str_replace(",", ".", $_POST['price_usd5']));
            $price_usd6 = (str_replace(",", ".", $_POST['price_usd6']));
            $price_usd7 = (str_replace(",", ".", $_POST['price_usd7']));
            $price_usd8 = (str_replace(",", ".", $_POST['price_usd8']));
            $price_usd9 = (str_replace(",", ".", $_POST['price_usd9']));
            $price_usd10 = (str_replace(",", ".", $_POST['price_usd10']));
            $price_usd11 = (str_replace(",", ".", $_POST['price_usd11']));
            $price_usd12 = (str_replace(",", ".", $_POST['price_usd12']));
            $price_usd13 = (str_replace(",", ".", $_POST['price_usd13']));
            $price_usd14 = (str_replace(",", ".", $_POST['price_usd14']));
            $price_usd15 = (str_replace(",", ".", $_POST['price_usd15']));
            $price_usd16 = (str_replace(",", ".", $_POST['price_usd16']));
            $price_usd17 = (str_replace(",", ".", $_POST['price_usd17']));
            $price_usd18 = (str_replace(",", ".", $_POST['price_usd18']));


            $priority = catalog_priority($price_usd1, $price_usd2, $price_usd3, $price_usd4, $price_usd5, $price_usd6, $price_usd7, $price_usd8, $price_usd9, $price_usd10, $price_usd11,$price_usd12,$price_usd13,$price_usd14,$price_usd15,$price_usd16,$price_usd17,$price_usd18);

            if ($is_block == 0) {
                $price = catalog_creat_price($priority['price']);
            } else {
                $price = intval($_POST['price']);
                $price_old = intval($_POST['price_old']);
            }

            $price_com = intval($_POST['price_com']);

            $sql = "UPDATE m_catalog_data_order SET
			type='$sel_type', 
			article='$article', 
			article2='$article2', 
			article3='$article3', 
			article4='$article4', 
			article5='$article5', 
			article6='$article6', 
			article7='$article7', 
			article8='$article8', 
			article9='$article9', 
			article10='$article10', 
			article11='$article11', 
			article12='$article12',
			article13='$article13',
			article14='$article14',
			article15='$article15',
			article16='$article16',
			article17='$article17',
			article18='$article18',
			v='$v',
			price='$price', 
			price_old='$price_old', 
			price_com='$price_com', 
			price_usd1='$price_usd1', 
			price_usd2='$price_usd2', 
			price_usd3='$price_usd3', 
			price_usd4='$price_usd4', 
			price_usd5='$price_usd5', 
			price_usd6='$price_usd6', 
			price_usd7='$price_usd7', 
			price_usd8='$price_usd8', 
			price_usd9='$price_usd9', 
			price_usd10='$price_usd10', 
			price_usd11='$price_usd11', 
			price_usd12='$price_usd12',
			price_usd13='$price_usd13',
			price_usd14='$price_usd14',
			price_usd15='$price_usd15',
			price_usd16='$price_usd16',
			price_usd17='$price_usd17',
			price_usd18='$price_usd18',
			is_block='$is_block',
			sklad='{$priority['sklad']}'
			WHERE id='$id_order'";
            mysql_query($sql);

            // ������ ��������
            $pr = mysql_fetch_array(mysql_query("SELECT MAX(price) as max_price, MIN(price) as min_price FROM m_catalog_data_order WHERE id_catalog_data='$id_catalog_data' AND price!=0 "));
            mysql_query("UPDATE m_catalog_data SET price_s='{$pr['min_price']}', price_e='{$pr['max_price']}' WHERE id='$id_catalog_data' ");

            // �������
            if (isset($_POST['save']))
                go("?main=$cat_main&module=$cat_module&action=catalog_data_order_edit&id_cat=$id_cat&id_catalog_data=$id_catalog_data&id_order=$id_order");
            elseif (isset($_POST['save_add']))
                go("?main=$cat_main&module=$cat_module&action=catalog_data_order_add&id_cat=$id_cat&id_catalog_data=$id_catalog_data");
            elseif (isset($_POST['save_list']))
                go("?main=$cat_main&module=$cat_module&action=catalog_data_order&id_cat=$id_cat&id_catalog_data=$id_catalog_data");
            break;

        case "galarey_foto":
            //==========================================================+
            // ���������� ����
            //==========================================================+
            if (isset($_POST['foto_add'])) {
                if ($_POST['sort'] == ''
                    or $_POST['title'] == ''
                ) {
                    err_message("�� ��������� ������������ ����.");
                }

                $id_catalog_data = intval($_GET['id_catalog_data']);

                $title = format_text(trim($_POST['title']));
                $anot = htmlentities($_POST['anot']);
                $sort = trim($_POST['sort']);
                if (trim($_POST['alt']) == '') $alt = format_text(trim($_POST['title']));
                else $alt = format_text(trim($_POST['alt']));

                if ($_FILES['img']['name'] != '') {
                    // ������������ ����������
                    $path = '../images/';
                    $mkdir = 'uploads/catalog/' . $id_catalog_data;
                    $mkdir_trumbs = $mkdir . '/trumbs/';
                    $mkdir_small = $mkdir . '/small/';
                    $mkdir_big = $mkdir . '/big/';

                    if (!file_exists($path . $mkdir)) {
                        mkdir($path . $mkdir);
                        mkdir($path . $mkdir_trumbs);
                        mkdir($path . $mkdir_small);
                        mkdir($path . $mkdir_big);
                    }

                    // ������
                    $time = time();
                    $img = upload_image($_FILES['img']['tmp_name'], $time, $path . $mkdir_small, $valinorConfig['galarey.small.w'], $valinorConfig['galarey.small.w']);
                    upload_image($_FILES['img']['tmp_name'], $time, $path . $mkdir_big, $valinorConfig['galarey.big.w'], $valinorConfig['galarey.big.w']);
                } else {
                    err_message("�� ��������� ������������ ����.");
                }

                $sql = "INSERT INTO m_catalog_galarey_data
				(id_catalog_data, title, alt, anot, img, sort)
				VALUES ('$id_catalog_data', '$title', '$alt', '$anot', '$img', '$sort')";
                mysql_query($sql);

                go("?main=$cat_main&module=$cat_module&action=galarey_foto&id_cat=$id_cat&id_catalog_data=$id_catalog_data");


                //==========================================================+
                //  �������������� ����
                //==========================================================+
            } elseif (isset($_POST['foto_edit'])) {
                if ($_POST['sort'] == ''
                    or $_POST['title'] == ''
                ) {
                    err_message("�� ��������� ������������ ����.");
                }

                $id_catalog_data = intval($_GET['id_catalog_data']);
                $id_foto = intval($_GET['id_foto']);

                $title = format_text(trim($_POST['title']));
                $alt = format_text(trim($_POST['alt']));
                $anot = format_text(trim($_POST['anot']));
                $sort = trim($_POST['sort']);

                if ($_FILES['img']['name'] != '') {
                    // ������������ ����������
                    $path = '../images/';
                    $mkdir = 'uploads/catalog/' . $id_catalog_data;
                    $mkdir_trumbs = $mkdir . '/trumbs/';
                    $mkdir_small = $mkdir . '/small/';
                    $mkdir_big = $mkdir . '/big/';

                    if (!file_exists($path . $mkdir)) {
                        mkdir($path . $mkdir);
                        mkdir($path . $mkdir_trumbs);
                        mkdir($path . $mkdir_small);
                        mkdir($path . $mkdir_big);
                    }

                    // �������� ������ ����
                    @unlink($path . $mkdir_trumbs . $_POST['img_name']);
                    @unlink($path . $mkdir_small . $_POST['img_name']);
                    @unlink($path . $mkdir_big . $_POST['img_name']);

                    $time = time();
                    $img = upload_image($_FILES['img']['tmp_name'], $time, $path . $mkdir_small, $valinorConfig['galarey.small.w'], $valinorConfig['galarey.small.w']);
                    upload_image($_FILES['img']['tmp_name'], $time, $path . $mkdir_big, $valinorConfig['galarey.big.w'], $valinorConfig['galarey.big.w']);

                } else {
                    $img = $_POST['img_name'];
                }

                $sql = "UPDATE m_catalog_galarey_data SET
				img='$img', 
				sort='$sort', 
				title='$title', 
				alt='$alt', 
				anot='$anot' 
				WHERE id='$id_foto'";

                mysql_query($sql);
                go("?main=$cat_main&module=$cat_module&action=galarey_foto&id_cat=$id_cat&id_catalog_data=$id_catalog_data");
            }
            break;

        case "delete_comment":
            $id = $_GET['id'];
            @mysql_query("DELETE FROM m_catalog_com WHERE id='$id'");
            go("index.php");
            break;

        case "order":
            // �������� ���� �������
            if (!MyIsValidUsers($_SESSION['user_id'], 'DELETE', $id_cat)) err_message("� ��� ��� ���� �� �������� � ���������� �������.");

            if ($_POST['actions'] == 'null') {
                err_message("�� ������� ��������.");
            }
            //if (!isset($_POST['id']) and !isset($_POST['sel_total'])){err_message("�� ������� ��������.");}

            // �������� �������
            if ($_POST['actions'] == 'delete_item') {
                foreach ($_POST['id'] as $key => $value) {

                    // ��������� �������� � �������
                    delete_directory("../images/uploads/catalog/" . $value);
                    @mysql_query("DELETE FROM m_catalog_galarey WHERE id_catalog_data='$value'");
                    @mysql_query("DELETE FROM m_catalog_galarey_data WHERE id_catalog_data='$value'");
                    @mysql_query("DELETE FROM m_catalog_data_order WHERE id_catalog_data='$value'");

                    // ������� ������ ��������
                    @mysql_query("DELETE FROM m_catalog_data WHERE id='$value'");
                }
                go("?main=$cat_main&module=$cat_module&action=mod&id_cat=$id_cat");


                // �������� ������� ��������
            } elseif ($_POST['actions'] == 'delete_item_catalog') {
                foreach ($_POST['id'] as $key => $value) {
                    $is_parent = mysql_result(mysql_query("SELECT count(id) FROM m_catalog WHERE id_parent='$value'"), 0, 0);
                    if ($is_parent != 0) {
                        err_message("������ �������� ��������� ��������, ������� ������� ��.");
                    } else {
                        //delete_directory("../images/uploads/catalog/".$value);
                        @mysql_query("DELETE FROM m_catalog WHERE id='$value'");
                        //@mysql_query("DELETE FROM m_catalog_data WHERE id_catalog='$value'");
                        //@mysql_query("DELETE FROM m_catalog_galarey WHERE id_catalog='$value'");
                    }
                }
                go("?main=$cat_main&module=$cat_module&action=catalog&id_cat=$id_cat");

                // �������� ���� � ������
            } elseif ($_POST['actions'] == 'delete_item_galarey_foto') {
                $id_galarey = $_GET['id'];
                $id_catalog_data = $_GET['id_catalog_data'];

                foreach ($_POST['id'] as $key => $value) {
                    $del_img = mysql_result(mysql_query("SELECT img FROM m_catalog_galarey_data WHERE id='$value'"), 0, 0);

                    @unlink("../images/uploads/catalog/" . $id_catalog_data . "/trumbs/" . $del_img);
                    @unlink("../images/uploads/catalog/" . $id_catalog_data . "/small/" . $del_img);
                    @unlink("../images/uploads/catalog/" . $id_catalog_data . "/big/" . $del_img);

                    mysql_query("DELETE FROM m_catalog_galarey_data WHERE id='$value'");
                }
                go("?main=$cat_main&module=$cat_module&action=galarey_foto&id_cat=$id_cat&id_catalog_data=$id_catalog_data&id=$id_galarey");

                // ��������� ��������
            } elseif ($_POST['actions'] == 'active_catalog') {
                foreach ($_POST['id'] as $key => $value) {
                    $is = mysql_result(mysql_query("SELECT is_block FROM m_catalog WHERE id='$value'"), 0, 0);
                    if ($is == 0) {
                        mysql_query("UPDATE m_catalog SET is_block=1 WHERE id='$value'");
                    } else {
                        mysql_query("UPDATE m_catalog SET is_block=0 WHERE id='$value'");
                    }
                }
                go("?main=$cat_main&module=$cat_module&action=catalog&id_cat=$id_cat");

                // ��������� �������� TOP
            } elseif ($_POST['actions'] == 'active_top') {
                foreach ($_POST['id'] as $key => $value) {
                    $is = mysql_result(mysql_query("SELECT is_top FROM m_catalog WHERE id='$value'"), 0, 0);
                    if ($is == 0) {
                        mysql_query("UPDATE m_catalog SET is_top=1 WHERE id='$value'");
                    } else {
                        mysql_query("UPDATE m_catalog SET is_top=0 WHERE id='$value'");
                    }
                }
                go("?main=$cat_main&module=$cat_module&action=catalog&id_cat=$id_cat");

            } elseif ($_POST['actions'] == 'active_item_catalog') {
                foreach ($_POST['id'] as $key => $value) {
                    $is = mysql_result(mysql_query("SELECT is_block FROM m_catalog_data WHERE id='$value'"), 0, 0);
                    if ($is == 0) {
                        mysql_query("UPDATE m_catalog_data SET is_block=1 WHERE id='$value'");
                    } else {
                        mysql_query("UPDATE m_catalog_data SET is_block=0 WHERE id='$value'");
                    }
                }
                go("?main=$cat_main&module=$cat_module&action=mod&id_cat=$id_cat");

            } elseif ($_POST['actions'] == 'main_item_catalog') {
                foreach ($_POST['id'] as $key => $value) {
                    $is = mysql_result(mysql_query("SELECT is_main FROM m_catalog_data WHERE id='$value'"), 0, 0);
                    if ($is == 0) {
                        mysql_query("UPDATE m_catalog_data SET is_main=1 WHERE id='$value'");
                    } else {
                        mysql_query("UPDATE m_catalog_data SET is_main=0 WHERE id='$value'");
                    }
                }
                go("?main=$cat_main&module=$cat_module&action=mod&id_cat=$id_cat");


            } elseif ($_POST['actions'] == 'subs_item_catalog') {
                foreach ($_POST['id'] as $key => $value) {
                    $is = mysql_result(mysql_query("SELECT is_subs FROM m_catalog_data WHERE id='$value'"), 0, 0);
                    if ($is == 0) {
                        mysql_query("UPDATE m_catalog_data SET is_subs=1 WHERE id='$value'");
                    } else {
                        mysql_query("UPDATE m_catalog_data SET is_subs=0 WHERE id='$value'");
                    }
                }
                go("?main=$cat_main&module=$cat_module&action=mod&id_cat=$id_cat");


            } elseif ($_POST['actions'] == 'sale_item_catalog') {
                foreach ($_POST['id'] as $key => $value) {
                    $is = mysql_result(mysql_query("SELECT is_sale FROM m_catalog_data WHERE id='$value'"), 0, 0);
                    if ($is == 0) {
                        mysql_query("UPDATE m_catalog_data SET is_sale=1 WHERE id='$value'");
                    } else {
                        mysql_query("UPDATE m_catalog_data SET is_sale=0 WHERE id='$value'");
                    }
                }
                go("?main=$cat_main&module=$cat_module&action=mod&id_cat=$id_cat");
            }

            break;
    }


}
}
?>