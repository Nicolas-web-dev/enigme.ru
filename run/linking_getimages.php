<?php
/**
 * Created by PhpStorm.
 * User: truth4oll
 * Date: 03.12.13
 * Time: 14:20
 */

set_time_limit(300);

ini_set('include_path', ini_get('include_path') . ':../includes/library');
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();
include_once('../includes/config.php');
include_once('../includes/output.php');
include_once('../includes/csv.php');

$db_config['charset'] = 'cp1251';

$db = Zend_Db::factory('MySqli', $db_config);
$db->setFetchMode(Zend_Db::FETCH_ASSOC);
$i=0;
while($links = $db->fetchAll($db->select()->from('m_linking3')->where('preview=""')->limit(20))){
    $i++;


    $aUrls=[];
    foreach ($links as $link) {
        $aUrls[$link['id']] = $link['to'];
    }

    $aPages = getByCurl($aUrls);


    foreach ($links as $link) {
        $preview = getPreview($aPages[$link['to']]);
        $data['preview'] = $preview;

        $db->update('m_linking3',$data  ,'id='.(int)$link['id']);

        echo $link['to'].' '.$preview;
        echo "\n";
    }

}




/*foreach ($links as $link) {
    //echo $image =  getPreview($link);
    $image =  getByCurl([$link['to'],$link['to']]);
    //$data['preview'] = $image;
    //$db->update('m_linking3',$data  ,'id='.(int)$link['id']);

}*/


function getPreview($page)
{

    $html = $page;

    libxml_use_internal_errors(true); // Yeah if you are so worried about using @ with warnings
    $doc = new DomDocument();
    $doc->loadHTML($html);
    $xpath = new DOMXPath($doc);
    $query = '//*/meta[starts-with(@property, \'og:\')]';
    $metas = $xpath->query($query);
    foreach ($metas as $meta) {
        $property = $meta->getAttribute('property');
        $content = $meta->getAttribute('content');
        $rmetas[$property] = $content;
    }


    if (isset($rmetas['og:image'])){
        $rmetas['og:image'] = str_replace('/big/','/small/',$rmetas['og:image']);
        return $rmetas['og:image'];
    }else{
        return '';
    }

    //return $this->hasOne(Files::className(), ['id' => 'file_id']);


}


function getByCurl($aUrls){
    $cmh = curl_multi_init();
    foreach ($aUrls as $url) {
        // инициализируем отдельное соединение (поток)
        $ch = curl_init($url);
        // если будет редирект - перейти по нему
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // не возвращать http-заголовок
        curl_setopt($ch, CURLOPT_HEADER, 0);
        // таймаут соединения
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        // таймаут ожидания
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        // добавляем дескриптор потока в массив заданий
        $tasks[$url] = $ch;
        // добавляем дескриптор потока в мультикурл
        curl_multi_add_handle($cmh, $ch);
    }

    $active = null;
    do {
        $mrc = curl_multi_exec($cmh, $active);
    }
    while ($mrc == CURLM_CALL_MULTI_PERFORM);
    // выполняем, пока есть активные потоки
    while ($active && ($mrc == CURLM_OK)) {
        // если какой-либо поток готов к действиям
        if (curl_multi_select($cmh) != -1) {
            // ждем, пока что-нибудь изменится
            do {
                $mrc = curl_multi_exec($cmh, $active);
                // получаем информацию о потоке
                $info = curl_multi_info_read($cmh);
                // если поток завершился
                if ($info['msg'] == CURLMSG_DONE) {
                    $ch = $info['handle'];
                    // ищем урл страницы по дескриптору потока в массиве заданий
                    $url = array_search($ch, $tasks);
                    // забираем содержимое
                    $tasks[$url] = curl_multi_getcontent($ch);
                    // удаляем поток из мультикурла
                    curl_multi_remove_handle($cmh, $ch);
                    // закрываем отдельное соединение (поток)
                    curl_close($ch);
                }
            }
            while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }
    }

    // закрываем мультикурл
    curl_multi_close($cmh);

    return $tasks;


}