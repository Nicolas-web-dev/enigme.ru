<?php
/**
 * Created by PhpStorm.
 * User: truth4oll
 * Date: 03.12.13
 * Time: 14:20
 */

set_time_limit(300);
ini_set("memory_limit", "256M");

ini_set('include_path', ini_get('include_path') . ':../includes/library');
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();
include_once('../includes/config.php');
include_once('../includes/output.php');
include_once('../includes/csv.php');

$db_config['charset'] = 'cp1251';

$db = Zend_Db::factory('MySqli', $db_config);
$db->setFetchMode(Zend_Db::FETCH_ASSOC);

$price_col = array(
    'price_usd1',
    'price_usd2',
    'price_usd3',
    'price_usd4',
    'price_usd5',
    'price_usd6',
    'price_usd7',
    'price_usd8',
    'price_usd9',
    'price_usd10',
    'price_usd11',
    'price_usd12',
    'price_usd13',
    'price_usd14',
    'price_usd15',
    'price_usd16',
    'price_usd17',
    'price_usd18',
);

$doc = new DOMDocument('1.0');
// we want a nice output
$doc->formatOutput = true;
$doc->encoding = 'UTF-8';

$root = $doc->createElement('data');
$doc->appendChild($root);

$brands = $doc->createElement('brands');
$brands = $root->appendChild($brands);

$select = $db->select()->from('m_catalog', array('id', 'articul', 'title', 'title_1', 'title_2', 'url', 'date_edit'));
$aBrands = $db->fetchAll($select);

foreach ($aBrands as $item) {

    foreach ($item as $key => $val) {
        $item[$key] = iconv('cp1251', 'utf-8', $val);
    }

    $brand = $doc->createElement('brand');
    $brand->setAttribute('id', $item['id']);
    $brand->setAttribute('articul', $item['articul']);
    $brand->setAttribute('title', htmlentities($item['title']));
    $brand->setAttribute('title_1', htmlentities($item['title_1']));
    $brand->setAttribute('title_2', htmlentities($item['title_2']));
    $brand->setAttribute('url', $item['url']);
    $brand->setAttribute('date_edit', $item['date_edit']);
    $brands->appendChild($brand);
}

$select = $db->select()->from('m_catalog_data')->joinLeft('m_catalog', 'm_catalog.id=m_catalog_data.id_catalog', array('catalog_articul' => 'articul'));
$aProducts = $db->fetchAll($select);

$products = $doc->createElement('products');
$products = $root->appendChild($products);

foreach ($aProducts as $item) {
    foreach ($item as $key => $val) {
        $item[$key] = iconv('cp1251', 'utf-8', $val);
    }

    $product = $doc->createElement('product');
    $product->setAttribute('id', $item['id']);
    $product->setAttribute('articul', $item['articul']);
    $product->setAttribute('title', htmlentities($item['title']));
    $product->setAttribute('url', $item['url']);
    $product->setAttribute('id_catalog', $item['id_catalog']);
    $product->setAttribute('catalog_articul', $item['catalog_articul']);
    $product->setAttribute('pol', $item['pol']);
    $product->setAttribute('img', $item['img']);
    $product->setAttribute('img_path', 'http://enigme.ru/images/uploads/catalog/' . $item['id'] . '/big/' . $item['img']);
    $product->setAttribute('img_update', $item['img_update']);

    $select = $db->select()->from('m_catalog_data_order',
        array(
            'articul',
            'article',
            'article2',
            'article3',
            'article4',
            'article5',
            'article6',
            'article7',
            'article8',
            'article9',
            'article10',
            'article11',
            'article12',
            'article13',
            'article14',
            'article15',
            'article16',
            'article17',
            'article18',
            'type',
            'v',
            'price_usd1',
            'price_usd2',
            'price_usd3',
            'price_usd4',
            'price_usd5',
            'price_usd6',
            'price_usd7',
            'price_usd8',
            'price_usd9',
            'price_usd10',
            'price_usd11',
            'price_usd12',
            'price_usd13',
            'price_usd14',
            'price_usd15',
            'price_usd16',
            'price_usd17',
            'price_usd18'
        ))->where('id_catalog_data=?', $item['id']);

    $aMod = $db->fetchAll($select);

    foreach ($aMod as $item) {
        $mod = $doc->createElement('mod');

        foreach ($item as $key => $val) {
            $item[$key] = iconv('cp1251', 'utf-8', $val);
        }

        foreach ($item as $key => $row) {
            if ($row == '') {
                $row = null;
            }

            if (in_array($key, $price_col)) {
                $row = str_replace(',', '.', $row);
            }

            $mod->setAttribute($key, $row);
        }
        $mod = $product->appendChild($mod);
    }

    $products->appendChild($product);
}

echo $doc->saveXML() . "\n";

