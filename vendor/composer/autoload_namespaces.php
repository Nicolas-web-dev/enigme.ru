<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Zend_' => array($vendorDir . '/zendframework/zendframework1/library'),
    'Krizon' => array($vendorDir . '/krizon/php-ga-measurement-protocol/src'),
    'Guzzle\\Tests' => array($vendorDir . '/guzzle/guzzle/tests'),
    'Guzzle' => array($vendorDir . '/guzzle/guzzle/src'),
);
