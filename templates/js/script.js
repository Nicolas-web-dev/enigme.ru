$(document).ready(function(){
	if (window.location.pathname == '/guarantees/') {
		$(".certificate_item").fancybox();
	}

	$('#catalog_sort_price').click(function(event){
		 event.preventDefault();
		$('.sorting__button').removeClass('sorting__button--active');
		$('#catalog_sort_price').addClass('sorting__button--active');
		$.ajax({
            method: "POST",
			dataType: 'json',
			url: "/catalog/?ajax_catalog_filter=1",
			data: "sort=price&id_catalog=" + $('#sorting_catalog_block').data('id-catalog') + "&brend_title=" + $('#sorting_catalog_block').data('brend-title') + "&brend_url=" + $('#sorting_catalog_block').data('brend-url')
        })
		.done(function (data) {
			var arstr = JSON.stringify(data);
			var dat_parse = JSON.parse(arstr);
			var html_str = '';
			$('#catalog_prod_items').empty();
			$.each(dat_parse, function(i, b) {
				html_str = "";
				if ($.isPlainObject(b)) {
					if (b['pol'] == 'F') {
						$('#catalog_prod_items').append(content_catalog_item(b,'f'));
					} else {
						$('#catalog_prod_items').append(content_catalog_item(b,'m'));
					}
				}
			});
		});
	});
	
	$('#catalog_sort_rate').click(function(event){
		 event.preventDefault();
		$('.sorting__button').removeClass('sorting__button--active');
		$('#catalog_sort_rate').addClass('sorting__button--active');
		$.ajax({
            method: "POST",
			dataType: 'json',
			url: "/catalog/?ajax_catalog_filter=1",
			data: "sort=rate&id_catalog=" + $('#sorting_catalog_block').data('id-catalog') + "&brend_title=" + $('#sorting_catalog_block').data('brend-title') + "&brend_url=" + $('#sorting_catalog_block').data('brend-url')
        })
		.done(function (data) {
			var arstr = JSON.stringify(data);
			var dat_parse = JSON.parse(arstr);
			var html_str = '';
			$('#catalog_prod_items').empty();
			$.each(dat_parse, function(i, b) {
				html_str = "";
				if ($.isPlainObject(b)) {
					if (b['pol'] == 'F') {
						$('#catalog_prod_items').append(content_catalog_item(b,'f'));
					} else {
						$('#catalog_prod_items').append(content_catalog_item(b,'m'));
					}
				}
			});
		});
	});
	
	$('#catalog_sort_abc').click(function(event){
		 event.preventDefault();
		$('.sorting__button').removeClass('sorting__button--active');
		$('#catalog_sort_abc').addClass('sorting__button--active');
		$.ajax({
            method: "POST",
			dataType: 'json',
			url: "/catalog/?ajax_catalog_filter=1",
			data: "sort=abc&id_catalog=" + $('#sorting_catalog_block').data('id-catalog') + "&brend_title=" + $('#sorting_catalog_block').data('brend-title') + "&brend_url=" + $('#sorting_catalog_block').data('brend-url')
        })
		.done(function (data) {
			var arstr = JSON.stringify(data);
			var dat_parse = JSON.parse(arstr);
			var html_str = '';
			$('#catalog_prod_items').empty();
			$.each(dat_parse, function(i, b) {
				html_str = "";
				if ($.isPlainObject(b)) {
					if (b['pol'] == 'F') {
						$('#catalog_prod_items').append(content_catalog_item(b,'f'));
					} else {
						$('#catalog_prod_items').append(content_catalog_item(b,'m'));
					}
				}
			});
		});
	});
	
	$('#catalog_sort_new').click(function(event){
		 event.preventDefault();
		$('.sorting__button').removeClass('sorting__button--active');
		$('#catalog_sort_new').addClass('sorting__button--active');
		$.ajax({
            method: "POST",
			dataType: 'json',
			url: "/catalog/?ajax_catalog_filter=1",
			data: "sort=new&id_catalog=" + $('#sorting_catalog_block').data('id-catalog') + "&brend_title=" + $('#sorting_catalog_block').data('brend-title') + "&brend_url=" + $('#sorting_catalog_block').data('brend-url')
        })
		.done(function (data) {
			var arstr = JSON.stringify(data);
			var dat_parse = JSON.parse(arstr);
			var html_str = '';
			$('#catalog_prod_items').empty();
			$.each(dat_parse, function(i, b) {
				html_str = "";
				if ($.isPlainObject(b)) {
					if (b['pol'] == 'F') {
						$('#catalog_prod_items').append(content_catalog_item(b,'f'));
					} else {
						$('#catalog_prod_items').append(content_catalog_item(b,'m'));
					}
				}
			});
		});
	});
	
	function content_catalog_item(arr_cat_item, str_pol) {
		var html_str_t = '';
		if (str_pol == 'f') {
			if ($('#catalog-controls_m').hasClass('catalog-controls__category-item--active')) {
				html_str_t += '<div class="item_prod catalog_prod_item_f" style="display: none;">';
			} else {
				html_str_t += '<div class="item_prod catalog_prod_item_f">';
			}
		} else {
			if ($('#catalog-controls_wm').hasClass('catalog-controls__category-item--active')) {
				html_str_t += '<div class="item_prod catalog_prod_item_m" style="display: none;">';
			} else {
				html_str_t += '<div class="item_prod catalog_prod_item_m">';
			}
		}
		html_str_t += '	<a class="item_prod__picture-link" href="' + arr_cat_item["url"] + '#aromat">';
		html_str_t += '<img class="item_prod__picture" src="' + arr_cat_item["img"] + '" title="' + arr_cat_item["brend_title"] + arr_cat_item["title"] + '" alt="' + arr_cat_item["brend_title"] + arr_cat_item["title"] + '">';
		html_str_t += '	</a>';
		html_str_t += '<a class="item_prod__title" href="'+ arr_cat_item["url"] +'#aromat">';
		html_str_t += '<div class="item_prod__brand">' + arr_cat_item["brend_title"] + '</div>';
		html_str_t += '<div class="item_prod__name">' + arr_cat_item["title"] + '</div>';
		html_str_t += '</a>';
		html_str_t += '<div class="item_prod__price">';
		if (arr_cat_item["price_s"] > 0) {
			html_str_t += '<div class="item_prod__price-from">�� ' + arr_cat_item["price_s"] + '</div>';
			html_str_t += '<div class="item_prod__price-to">&nbsp;�� ' + arr_cat_item["price_e"] + ' ���.</div>';
		} else {
			html_str_t += '<div class="item_prod__price-to">��� � �������</div>';
		}
		html_str_t += '</div>';
		if (arr_cat_item["price_s"] > 0) {
			html_str_t += '<div class="item_prod__footer">';
			html_str_t += '<a onclick="quick_buy_click(' + arr_cat_item["id"] + ');return false;" class="item__buy_prod button_prod">������ � 1 ����</a>';
			html_str_t += '<a href="' + arr_cat_item["url"] + '#aromat" class="add-bask_prod button_prod">';
			html_str_t += '  	� �������';
			html_str_t += '</a>';
			html_str_t += '</div>';
		}
		html_str_t += '</div>';
		return html_str_t;
	}
	
	
	$("input[name='phone']").mask("7(999) 999-99-99");


	$('#tab_price_first').click(function(){
		$('.price_second').hide();
		$('.price_third').hide();
		$('.price_fourth').hide();
		$('.price_first').show();

		$('#tab_price_first').addClass('tab--active');
		$('#tab_price_second').removeClass('tab--active');
		$('#tab_price_third').removeClass('tab--active');
		$('#tab_price_fourth').removeClass('tab--active');
		return false;
	});

	$('#tab_price_second').click(function(){
		$('.price_first').hide();
		$('.price_third').hide();
		$('.price_fourth').hide();
		$('.price_second').show();

		$('#tab_price_second').addClass('tab--active');
		$('#tab_price_first').removeClass('tab--active');
		$('#tab_price_third').removeClass('tab--active');
		$('#tab_price_fourth').removeClass('tab--active');
		return false;
	});

	$('#tab_price_third').click(function(){
		$('.price_second').hide();
		$('.price_first').hide();
		$('.price_fourth').hide();
		$('.price_third').show();

		$('#tab_price_third').addClass('tab--active');
		$('#tab_price_second').removeClass('tab--active');
		$('#tab_price_first').removeClass('tab--active');
		$('#tab_price_fourth').removeClass('tab--active');
		return false;
	});

	$('#tab_price_fourth').click(function(){
		$('.price_second').hide();
		$('.price_third').hide();
		$('.price_first').hide();
		$('.price_fourth').show();

		$('#tab_price_fourth').addClass('tab--active');
		$('#tab_price_second').removeClass('tab--active');
		$('#tab_price_third').removeClass('tab--active');
		$('#tab_price_first').removeClass('tab--active');
		return false;
	});

	$('#tab_modal_rest_email').click(function(){
		$('#modal_rest_telephone').hide();
		$('#modal_rest_email').show();

		$('#tab_modal_rest_email').addClass('tab--active');
		$('#tab_rest_telephone').removeClass('tab--active');
		return false;
	});

	$('#tab_rest_telephone').click(function(){
		$('#modal_rest_telephone').show();
		$('#modal_rest_email').hide();

		$('#tab_modal_rest_email').removeClass('tab--active');
		$('#tab_rest_telephone').addClass('tab--active');
		return false;
	});

	$('#catalog-controls_wm').click(function(){
		$('.catalog_prod_item_f').show();
		$('.catalog_prod_item_m').hide();

		$('#catalog-controls_m').removeClass('catalog-controls__category-item--active');
		$('#catalog-controls_all').removeClass('catalog-controls__category-item--active');
		$('#catalog-controls_wm').addClass('catalog-controls__category-item--active');
		return false;
	});
	
	$('#catalog-controls_m').click(function(){
		$('.catalog_prod_item_f').hide();
		$('.catalog_prod_item_m').show();

		$('#catalog-controls_wm').removeClass('catalog-controls__category-item--active');
		$('#catalog-controls_all').removeClass('catalog-controls__category-item--active');
		$('#catalog-controls_m').addClass('catalog-controls__category-item--active');
		return false;
	});

	$('#catalog-controls_all').click(function(){
		$('.catalog_prod_item_f').show();
		$('.catalog_prod_item_m').show();

		$('#catalog-controls_wm').removeClass('catalog-controls__category-item--active');
		$('#catalog-controls_m').removeClass('catalog-controls__category-item--active');
		$('#catalog-controls_all').addClass('catalog-controls__category-item--active');
		return false;
	});
	
	
	$('#tab_modal_orders_login_email').click(function(){
		$('#modal_orders_login_telephone').hide();
		$('#modal_orders_login_email').show();

		$('#tab_modal_orders_login_email').addClass('tab--active');
		$('#tab_modal_orders_login_telephone').removeClass('tab--active');
		return false;
	});

	$('#tab_modal_orders_login_telephone').click(function(){
		$('#modal_orders_login_telephone').show();
		$('#modal_orders_login_email').hide();

		$('#tab_modal_orders_login_email').removeClass('tab--active');
		$('#tab_modal_orders_login_telephone').addClass('tab--active');
		return false;
	});
	


	$(".unique-item__like").click(function(){

		var id = $(this).attr('id');
		var obj = $(this);

		$.post("/protect/note.php",{
			type: 'add',
			id: id
		}, function(xml){
			$("message",xml).each(function(id){
				message = $("message",xml).get(id);
				//$( obj ).addClass('active');
			});
		});
		return false;
	});

	$(".unique-item__remove").click(function(){
		
		var id = $(this).attr('id');
		var note_item = '.unique-item-' + id;
		
		$.post("/protect/note.php",{
			type: 'del',
			id: id
		}, function(xml){
			$("message",xml).each(function(id){
				message = $("message",xml).get(id);
				$( note_item ).animate({ opacity: "hide" }, "slow");
			});
		});
		return false;
	});

	
	$('#tab_settings_form-first').click(function(){
		$('#settings_form-first').show();
		$('#settings_form-second').hide();
		$('#settings_form-third').hide();
		
		$('#tab_settings_form-first').addClass('tab--active');
		$('#tab_settings_form-second').removeClass('tab--active');
		$('#tab_settings_form-third').removeClass('tab--active');
		return false;
	});

	
	$('#tab_settings_form-second').click(function(){
		$('#settings_form-first').hide();
		$('#settings_form-second').show();
		$('#settings_form-third').hide();
		
		$('#tab_settings_form-first').removeClass('tab--active');
		$('#tab_settings_form-second').addClass('tab--active');
		$('#tab_settings_form-third').removeClass('tab--active');
		return false;
	});

	$('#tab_settings_form-third').click(function(){
		$('#settings_form-first').hide();
		$('#settings_form-second').hide();
		$('#settings_form-third').show();
		
		$('#tab_settings_form-first').removeClass('tab--active');
		$('#tab_settings_form-second').removeClass('tab--active');
		$('#tab_settings_form-third').addClass('tab--active');
		return false;
	});



	
	$('#pass_manual').click(function() {
		$('#fields_pass_manual').show();
	});
	
	$('#pass_auto').click(function() {
			$('#fields_pass_manual').hide();
	});

	
	
	 $('#korz_kol-vo a').on('click',function(e){
        e.preventDefault();
        var action  = $(this).data('action');
        var uid  = $(this).data('uid');
        $.post('/api/cart/',{action:action,uid:uid},function(result){
            $.each(result.items,function(i,v){
                $('.tr_' + v.uid).find('.cart-table__amount-content .cart-table__amount-number').text(v.kol);
                $('.tr_' + v.uid).find('.sum').text(v.sum);
            });

            $('.val_kol').text(result.kol);
            $('.val_subTotal').text(result.subTotal);
            $('.val_total').text(result.total);
          
        });
    });

	
	$("#nakop_sk").mouseenter(function() {
		var sk = $("#nakop_sk");
		var offset = sk.offset();
		$(".nakop_id").css('top', offset.top-200);
		$(".nakop_id").show();
	});
	
	$("#nakop_sk").mouseleave(function() {
		$(".nakop_id").hide();
	});

	

$('.modal-link').magnificPopup({type:'inline'});




// ========================
	// suv: NOTE
	//
	
	// add
	$(".bloknot").click(function(){

		var id = $(this).attr('id');
		var obj = $(this);

		$.post("/protect/note.php",{
			type: 'add',
			id: id
		}, function(xml){
			$("message",xml).each(function(id){
				message = $("message",xml).get(id);
				$( obj ).parent().addClass('active');
				$( obj ).text('� ���������');
			});
		});
	});
	
	// delete
	$(".bloknot_del").click(function(){

		var id = $(this).attr('id');
		var note_item = '.note-' + id;
		
		$.post("/protect/note.php",{
			type: 'del',
			id: id
		}, function(xml){
			$("message",xml).each(function(id){
				message = $("message",xml).get(id);
				$( note_item ).animate({ opacity: "hide" }, "slow");
			});
		});
	});







	$('#login_form').on('submit',function(e){
            e.preventDefault();
            $.ajax({ url: '/users/login-ajax/',data:$('#login_form').serialize(),method:'post'}).done(function(data){
                var data = $.parseJSON(data);
                if (data.login==true){
                    document.location.href = '/orders/thankyou_basket_force1/';
                }else{
					/*
                    $('#login_form_register').show();

                    $('#login_form_popup .error').show();

                    var email = $('#login_form').find('[name=log_email]').val();
                    $('#login_form_register').find('[name=log_email]').val(email);
					*/
                }
            });
        });

        $('#login_form_phone').on('submit',function(e){
            e.preventDefault();
            $.ajax({ url: '/users/login-ajax/',data:$('#login_form_phone').serialize(),method:'post'}).done(function(data){
                var data = $.parseJSON(data);
                if (data.login==true){
                    document.location.href = '/orders/thankyou_basket_force1/';
                }else{
					/*
                    $('#login_form_register').show();
                    $('#login_form_popup .error').show();
                    var phone = $('#login_form_phone').find('[name=phone]').val();
                    $('#login_form_register').find('[name=phone]').val(phone);
					*/
                }
            });
        });

		/*
        $('#login_form_register').on('submit',function(e){
            e.preventDefault();
            $.ajax({ url: '/users/register-ajax/',data:$('#login_form_register').serialize(),method:'post'}).done(function(data){
                var data = $.parseJSON(data);

                if (data.login==true){
                    document.location.href = '/orders/force_purchase/';
                }else{

                    if (data.errors){
                        $('#login_form_register').find('input').css('border','1px solid #cbcbcb');
                        $.each(data.errors,function(i,v){
                            $('#login_form_register').find('[name=' + i + ']').css('border-color','red');
                        });
                    }


                }
            });
        });
		*/




$('.alphabet a').click(function(e) {
	var t = $(this);
		l = t.attr('href');
	
	resetDropdown();
	t.addClass('active');
	t.parent().append('<div class="alphabet__list fade"></div>');
	
	$('.alphabet__list').load(l + ' .catalog__brands', function() {
		$(this).children('.catalog__brands').removeAttr('class');
		$(this).append('<a href="/catalog/" class="alphabet__link">��� ������ A-Z</a><hr><a href="/women/" class="alphabet__link--sex">������� ���������� <span>&raquo;</span></a><a href="/men/" class="alphabet__link--sex">������� ���������� <span>&raquo;</span></a>');
		$(this).addClass('in');
	});
	
	e.preventDefault();
});

$(document).mousedown(function(e) {
	var l = $('.alphabet__letter'),
		d = $('.alphabet__list');
	
	if (!l.is(e.target) && !d.is(e.target) && d.has(e.target).length === 0) {
		resetDropdown();
	}
});

function resetDropdown() {
	$('.alphabet a').removeClass('active');
	$('.alphabet__list').remove();
}



if ($(window).width() < 1140) {
  $(function () {
    $(window).bind('resize', function () {
        resizeMe();
    }).trigger('resize');
  });
};



var resizeMe = function () {
    //Standard width, for which the body font size is correct
    var preferredWidth = 1230;
    //Base font size for the page
    var fontsize = 10;

    var displayWidth = $(window).width();
    var percentage = displayWidth / preferredWidth;
    var newFontSize = fontsize * percentage;
    $("html").css("font-size", newFontSize);
}})

