$(function () {
    $('#form_orders').submit(function (event) {
        var action = $(this).attr('action');


        $.ajax({
            method: "POST",
            url: action,
            data: $(this).serialize() + "&security=202cb962ac59075b964b07152d234b70&com_submit=1"
        }).done(function (msg) {
            if (msg=='ok'){
                location.reload();
            }
        });


        event.preventDefault();
    });
});

