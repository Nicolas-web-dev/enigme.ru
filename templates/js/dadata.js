(function() {
    "use strict";

    /**
     * API �������������� ������ dadata.ru
     * * ��. https://dadata.ru/api/
     * @type {{DADATA_API_URL: string, TOKEN: string, clean: Function}}
     */
    var DadataApi = {

        /**
         * URL ���������� ���������� dadata.ru
         */
        DADATA_API_URL: "http://suggestions.dadata.ru/suggestions/api/4_1/rs/",

        /**
         * API-���� ��� ��������� � DaData
         */
        TOKEN: "a4ad873506c4c01d447a363f53494903f0c19d53",

        /**
         * ������������� ������ ����� API
         * @param req ������ ��� ��������������
         * @returns {*} ����������������� ������
         */
            /*
        clean: function(req) {
            return $.ajax({
                type: "POST",
                url: this.DADATA_API_URL + "/clean",
                headers: { "Authorization": "Token " + this.TOKEN },
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify(req)
            });
        }
        */
        clean: function(q) {
            return $.ajax({
                type: "POST",
                url: this.DADATA_API_URL + "suggest/address",
                headers: { "Authorization": "Token " + this.TOKEN },
                contentType: "application/json",
                dataType: "json",
                data:JSON.stringify({"query":q})
            });
        },
        cleanName: function(q) {
            return $.ajax({
                type: "POST",
                url: this.DADATA_API_URL + "suggest/fio",
                headers: { "Authorization": "Token " + this.TOKEN },
                contentType: "application/json",
                dataType: "json",
                data:JSON.stringify({"query":q})
            });
        }


    };

    /**
     * ������� ��� ���������� ����, ������� ������ �������������� ����� Dadata.
     * @param el       DOM-������� ���������� ����
     * @param type     ��� ������ ����
     * @param getValue �������, ������������ ����������������� �������� �� ����������� ������
     */
    var ValidatedField = function(el, type, getValue) {
        var self = this;
        self.$el = $(el);
        self.$parent = self.$el.parent();
        self.type = type;
        self.getValue = getValue;

        /**
         * @constructor
         */
        self.init = function() {
            self.$el.change(function() {
                // ��� �������� - ������ ������� ������ ����
                if (!self.$el.val()) {
                    self.clearState();
                    // ����� ������������� ��� � ���������� ���������
                } else {
                    /*
                    DadataApi
                        .clean(self.$el.val())
                        .done(function(resp) {
                            //self.validate(resp.data['suggestions'][0]['data']);
                        });
                        */
                }
            });
        };

        /**
         * �������� ��������� ����, ����������� �� ����������������� ������ �� Dadata
         * @param validatedObj ����� �� Dadata
         */
        self.validate = function (validatedObj) {
            // ���� ��� �������� "����������", ���������� ������ ��
            if (validatedObj.qc == 0) {
                self.$el.val(
                    self.getValue(validatedObj)
                );
                self.clearState();
                self.setOK();
                // ����� ���������� ������ ������
            } else {
                self.setError();
            }
        };

        /**
         * ������������� ������ ������
         */
        self.setError = function () {
            self.$parent.addClass("has-error has-feedback");
            self.$parent.find("span").remove();
            self.$parent.append("<span class=\"glyphicon glyphicon-remove form-control-feedback\"></span>")
        };

        /**
         * ������������� ������ ��
         */
        self.setOK = function () {
            self.$parent.addClass("has-success has-feedback");
            self.$parent.append("<span class=\"glyphicon glyphicon-ok form-control-feedback\"></span>")
        };

        /**
         * ������� ������
         */
        self.clearState = function () {
            self.$parent.removeClass("has-error has-success has-feedback");
            self.$parent.find("span").remove();
        };

        self.init();

    };

    window.DadataApi = DadataApi;
    window.ValidatedField = ValidatedField;

})();