var dadata_clear_def = $.Deferred(),
    metro = {
        3: [
            '���������� ����� �. �. ������',
            '��������� ����',
            '�������������',
            '��������������',
            '������� ������',
            '�������������',
            '�������',
            '������� ���',
            '���� ��������',
            '�������������� �������',
            '�������� �����������',
            '����������',
            '����������',
            '����� ������������',
            '�����������',
            '�����������',
            '������������',
            '������ �����',
            '���-��������'
        ],
        5: [
            '�������������',
            '������������',
            '������������',
            '�������',
            '������������ ���',
            '����',
            '����������� �����',
            '���������',
            '�����-�����',
            '��������',
            '��������� ��������',
            '����������',
            '��������������',
            '����� ���������',
            '�����������',
            '�������� ����',
            '�����������',
            '�������',
            '��������',
            '�����������',
            '������ ����',
            '������������',
            '�����������',
            '�������'
        ],
        1: [
            '������������',
            '������������',
            '�����������',
            '������',
            '������� ������',
            '�������������',
            '����� �����������'
        ],
        10: [
            '����������',
            '���������',
            '���������'
        ],
        7: [
            '���������',
            '������',
            '��������',
            '����������',
            '������� ������� ��������',
            '���������',
            '�����������',
            '�������������',
            '�����������',
            '��������',
            '����������� ��������',
            '��������',
            '���������-�����������',
            '�������',
            '��������',
            '�����������',
            '���������������',
            '������������',
            '�������������',
            '��������',
            '����� ��������� ������',
            '����� ���������',
            '������� �������',
            '������������',
            '���������',
            '�����'
        ],
        2: [
            '�������������',
            '��������',
            '������ �������',
            '����������',
            '������',
            '�������������',
            '��������������',
            '�����������',
            '�����������������',
            '����������',
            '�������������',
            '�������',
            '������ ������',
            '�����',
            '��������',
            '�����������',
            '��������'
        ],
        8: [
            '�����������',
            '�������',
            '������������� ��������',
            '������',
            '�����-�����',
            '��������� ����',
            '���������',
            '����������� ����',
            '���������',
            '������������',
            '������������',
            '����������',
            '��������� ��������',
            '�����������',
            '���������',
            '������������',
            '���������',
            '����� 1905 ����',
            '���������'
        ],
        4: [
            '��������������� ���',
            '��������� (���������)',
            '���������������',
            '�����������',
            '��������',
            '����������',
            '�����������',
            '�������������',
            '����������',
            '����������',
            '������������',
            '��������� ����',
            '����'
        ],
        9: [
            '��������� (����������)',
            '����������',
            '�������������',
            '������������',
            '��������',
            '����������',
            '������',
            '����������',
            '��������',
            '���� ������',
            '������������',
            '������������',
            '������� ���������',
            '�����������',
            '���������� �������',
            '����������',
            '��������',
            '����������',
            '����������������'
        ],
        6: [
            '�������������',
            '��������',
            '�����������',
            '��������',
            '�����������',
            '������������ �������',
            '�������',
            '������� ����',
            '�������',
            '���������',
            '�������',
            '���������� �������',
            '�������',
            '����������'
        ],
        11: [
            '�����������',
            '������������',
            '��������',
            '�������������',
            '�����������������',
            '�������',
            '��������������',
            '�����������',
            '����������',
            '���� ��������',
            '�������� ����',
            '���������'
        ]
    };

// require dadata.js
(function () {
    "use strict";

    /**
     * ���������� �������� ������� ����� �����������. ��� ���� ���������� ������ ��������.
     * @param arr ������
     * @param separator �����������. �������������� ��������, �� ��������� - �������
     * @returns {string}
     */
    function join(arr /*, separator */) {
        var separator = arguments.length > 1 ? arguments[1] : ", ";
        return arr.filter(function (n) {
            return n
        }).join(separator);
    }



    /**
     * ������� ������ ���������
     * �������������� ��������� ����������� ����� � ����� "����� �������"
     */
    var Suggestions = {
        /**
         * �������������� ��������� �� ��������� ��������
         * @param $el   jQuery-������� ����� ����� �������
         * @param parts ������ jQuery-��������� ��� ����������� ������
         * @param separator �����������, ����� ������� ����� ���������� ����������� �����
         * @constructor
         */
        init: function ($el, parts, separator) {
            parts.forEach(function ($part) {
                $part.change(function () {
                    var partialValues = parts.map(
                        function ($el) {
                            return $el.val()
                        }
                    );
                    $el.val(
                        join(partialValues, separator)
                    );
                });
            });

            //set options
        }
    };

    /**
     * �������� �� ������
     */
    var AddressSuggestions = {


        initName: function(){
            /**
             * ���� ���� ���� ��� ������ � ���� ���������
             */
            if ($('input[name="fio"]').length>0){
                //���������
                $("input[name=fio]").suggestions({
                    serviceUrl: DadataApi.DADATA_API_URL + "suggest/fio",
                    selectOnSpace: true,
                    token: DadataApi.TOKEN,
                    /* ����������, ����� ������������ �������� ���� �� ��������� */
                    onSelect: function(suggestion) {
                        console.log(suggestion)
                        suggestion = suggestion;
                        $('input[name=fio_data]').val(JSON.stringify(suggestion));
                    }
                });
            }

        },

        initForm: function () {
            $('body').addClass('loading');



            $.when(dadata_clear_def).then(function(){
                $('body').removeClass('loading');
            },function(){
                $('body').removeClass('loading');
                $('body').addClass('error');
            });

            // ��������� ������ �� ��� ������ ������ ���� � ��� � ������
            $('#select_part').on('change', function(e) {

                var addr = $("#region").val();
                addr += ", "+$(this).find("option[value='"+$(this).val()+"']").text();

                AddressSuggestions.getLogistic($(this).val(), addr);
            });

            //bind inputs

            var region_name = window.region_name;
            var city_name = window.city_name;
            //��������� �������� ������ �� ��������� �����
            AddressSuggestions.init($("[name=region]"), [], 'region');
            AddressSuggestions.init($("[name=city]"), [], 'city');
            AddressSuggestions.init($("[name=settlement]"), [], 'settlement');
            AddressSuggestions.init($("[name=street]"), [], 'street');

            if (typeof region_name != 'undefined') {
                if ($('[name=region]').val() == '') {
                    $('[name=region]').val(region_name)
                    $('[name=city]').val(city_name);

                }else{
                    region_name  = $('[name=region]').val();
                }



                this.correctValues();
                if (region_name == '������' || region_name=='� ������' || region_name == '�����-���������' || region_name == '� �����-���������') {
                    this.initParts(region_name);
                    $('#city').hide();
                } else {
                    $('.part').hide();
                    $('.other').hide();
                    $('#select_part').attr('disabled','disabled')
                }
            }else{
                console.log('region_name is not recognized');
                var region_name = $('[name=region]').val();
                if (region_name == '� ������' || region_name == '� �����-���������') {
                    this.initParts(region_name);
                    $('#city').hide();
                } else {
                    $('.part').hide();
                    $('.other').hide();
                    $('#select_part').attr('disabled','disabled')
                }
            }

            if ($('#street').val()==''){
                $('.house').hide();
                $('.house input').val('');
                //fix
                $('[name="house"]').val('');
                $('[name="corpus"]').val('');
                $('[name="stroenie"]').val('');
                $('[name="flat"]').val('');
                $('[name="domophone"]').val('');
                $('[name="regs_postcode"]').val('');


                $('.house').show();
            }else{
                $('.house').show();
            }

            if ($('[name=settlement]').val()!=''){
                $('.settlement').show();
            }





            $('[name=city]').on('click', function () {
                $('[name=city]').change();
            });


            $('[name=part]').change(function () {
                if ($('[name=part]').val() == '0') {
                    $('.other').show();
                } else {
                    $('.other').hide();
                    $('#other').val('');
                }
            })


            $('[name=house]').on('focusout', function () {
                AddressSuggestions.clean();
            })

            $('[name=corpus]').on('focusout', function () {
                AddressSuggestions.clean();
            })

            $('[name=stroenie]').on('focusout', function () {
                AddressSuggestions.clean();
            })






            /*
            $('form[name="reg_form_new"]').find('input[name="register_submit"]').on('click', function () {

                var q = $('[name=region]').val() + ' ' + $('[name=city]').val() + ' ' +  $('[name=settlement]').val() + ' ' + $('[name=street]').val() + ' ' + $('[name=house]').val()
                var request = DadataApi.clean(q)

                request.done(function (msg) {
                    var postal_code = msg.data[0][0].postal_code
                    $('[name=regs_postcode]').val(postal_code)
                    $('[name=address_data]').val(JSON.stringify(msg.data[0][0]));
                    $('form[name="reg_form_new"]').submit();
                    return false;
                });
                return false;
            })
            */


        },

        /**
         * �������������� ��������� �� ������ �� ��������� ��������
         * @param $el   jQuery-������� ����� ������ ����� �������
         * @param parts ������ jQuery-��������� ��� ����������� ������ ������
         * @constructor
         */
        init: function ($el, parts, type) {
            var self = this;
            Suggestions.init.call(self, $el, parts, ", ");


            $el.suggestions({
                serviceUrl: DadataApi.DADATA_API_URL + "suggest/address",
                token: DadataApi.TOKEN,
                selectOnSpace: true,
                maxHeight: 310,
                count: 40,
                onSearchStart: function (params) {
                    return self.forceRegion(params, type)
                },
                transformResult: function (response) {
                    var query = this.params.query;
                    return self.trimResults(response, type, query)
                },
                formatResult: function (suggestion, currentValue) {
                    return self.formatResult(suggestion, currentValue, type);
                },
                onSelect: function (suggestion) {
                    if (suggestion.data) {
                        this.value = self.formatSelected(suggestion, type);
                        self.showSelected(suggestion, type);
                    }
                }
            });
        },

        /**
         * ������������ ����� ��������
         * @param params ��������� ajax-�������
         */
        forceRegion: function (params, type) {
            var query = params["query"];

            var region_name = $('[name=region]').val()

            if (type == 'postal_code' || type == 'region') {

            } else if (type == 'street') {
                var city_name = $('[name=city]').val()
                if ($('[name=other]').val() != '') {
                    city_name = $('[name=other]').val();
                }

                //������� ����� ������ ������ � �������
                if (region_name==city_name){
                    city_name='';
                }

                if ($('[name=settlement]').val() != ''){
                    city_name = city_name + ' ' + $('[name=settlement]').val()
                }



                query = region_name + " " + city_name + " " + query;
            } else if (type == 'settlement') {
                var city_name = $('[name=city]').val()
                query = region_name + " " + city_name + " " + query;
            } else {
                query = region_name + " " + query;
            }
            params["query"] = query;

        },

        /**
         * ��������� ������ ���������
         * @param response ����� �� ������� ���������
         */
        trimResults: function (response, type, query) {
            var indexDelta = 0;

            $(response.suggestions).each(function (index, value) {
                if (value.data.region_type == '�' && (value.data.city == null && value.data.settlement == null)) {
                    if (typeof (response.suggestions[index]) !== 'undefined') {
                        response.suggestions[index].data.city_type = value.data.region_type
                        response.suggestions[index].data.city = value.data.region
                    }
                }
                var skip = false;

                if (type == 'city') {
                    var currentValue = $('[name=city]').val();
                    //��������� ���������� � �������
                    if (value.data.street != null) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }

                    //��������� ���������� � ���������
                    if (!skip && (value.data.settlement != null)) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }

                    //
                    if (!skip && !(value.data.city != null||value.data.area != null)) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }
                } else if (type == 'settlement') {
                    if (value.data.street != null) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }
                    if (!skip && (value.data.settlement == null && value.data.city == null)) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }
                } else if (type == 'region') {
                    //����������� �������
                    //���������� ������� ����: ���� �����, �������, �����
                    if (value.data.street != null || value.data.area!=null || (value.data.city!=null && value.data.region!=value.data.city )) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }

                } else if (type == 'street') {
                    //��������� �� ���������� ������ ���� �������

                }

            })

            if (type == 'street') {
                var newArray = new Array();
                $.each(response.suggestions, function (i, v) {
                    if (v.data.house == null) {
                        newArray.push(response.suggestions[i])
                    }
                });
                response.suggestions = newArray
            }


            return response;
        },

        /**
         * �������������� �������� ������ ��������� � ��� ������.
         * ��� ��������� ������ ��������� ���������� ��� ������� �������� ������.
         * @param suggestion   ���������
         * @param currentValue ��������� ������������� �����
         * @returns {string} HTML ��� �������� ������ ���������
         */
        formatResult: function (suggestion, currentValue, type) {
            if (type == 'postal_code') {
                var address = suggestion.data;
                // ������ ������
                var part1 = join([
                    address.postal_code
                ]);
                // ������ ������ - ���������� �����, ����� � ���
                var part2 = join([
                    join([address.region_type, address.region], " "),
                    join([address.area_type, address.area], " "),
                    join([address.city_type, address.city], " "),
                    join([address.settlement_type, address.settlement], " "),
                    join([address.street_type, address.street], " "),
                    join([address.house_type, address.house], " ")
                ]);
                suggestion.value = part1;
            } else if (type == 'region') {
                var address = suggestion.data;
                // ������ ������
                var part1 = join([
                    join([address.region_type, address.region], " ")
                ]);
                // ������ ������
                var part2 = '';
                suggestion.value = part1

            } else if (type == 'city') {
                var address = suggestion.data;
                // ������ ������
                if (address.city!=null){
                    var part1 = join([
                        join([address.city_type, address.city], " ")
                    ]);
                }else{
                    var part1 = join([
                        join([address.area_type, address.area], " ")
                    ]);
                }

                // ������ ������
                var part2 = join([
                    join([address.region_type,address.region], " "),
                    join([address.area_type, address.area], " "),
                    join([address.city_type, address.city], " "),
                    join([address.settlement_type, address.settlement], " "),
                    join([address.street_type, address.street], " "),
                ]);
                suggestion.value = part1;
            } else if (type == 'settlement') {
                console.log('settlement')

                var address = suggestion.data;
                if (address.settlement==null){
                    var part1 = join([
                        join([address.city_type, address.city], " ")
                    ]);
                }else{
                    var part1 = join([
                        join([address.settlement_type, address.settlement], " ")
                    ]);
                }


                // ������ ������
                var part2 = join([
                    join([address.region_type, address.region], " "),
                    join([address.area_type, address.area], " "),
                    join([address.city_type, address.city], " "),
                    join([address.settlement_type, address.settlement], " "),
                    join([address.street_type, address.street], " "),
                ]);
                suggestion.value = part1;

            } else if (type == 'street') {
                var address = suggestion.data;
                // ������ ������
                var part1 = join([
                    join([address.street_type, address.street], " "),
                ]);
                // ������ ������
                var part2 = join([
                    join([address.region_type, address.region], " "),
                    join([address.area_type, address.area], " "),
                    join([address.city_type, address.city], " "),
                    join([address.settlement_type, address.settlement], " "),
                    join([address.street_type, address.street], " ")
                ]);
                suggestion.value = part1;

            } else {
                suggestion.value = suggestion.value.replace("������, ", "");
                var address = suggestion.data;
                // ������ ������
                var part1 = join([
                    address.postal_code,
                    address.region,
                    join([address.area_type, address.area], " "),
                    join([address.city_type, address.city], " ")
                ]);
                // ������ ������
                var part2 = join([
                    join([address.region_type, address.region], " "),
                    join([address.settlement_type, address.settlement], " "),
                    join([address.street_type, address.street], " "),
                    join([address.house_type, address.house], " ")
                ]);
            }


            // ��������� ���������� ������������� ������
            var pattern = '(^|\\s+)(' + $.Suggestions.utils.escapeRegExChars(currentValue) + ')';
            part2 = part2.replace(new RegExp(pattern, 'gi'), '$1<strong>$2<\/strong>')
            var suggestedValue = part2 ?
                "<span class=\"autocomplete-suggestion-result\">" + part1 + "</span>" + "<br><span class=\"autocomplete-info\">&nbsp;&nbsp;" + part2 + "</span>"
                : part1;
            return suggestedValue;
        },

        /**
         * ��������� ��������� ������������� ���������, ����� ������������ �������� �� �� ������
         * ���������� ���, ����� ������ � �������.
         * @param suggestion
         * @returns {string}
         */
        formatSelected: function (suggestion, type) {
            if (type == 'postal_code') {
                var address = suggestion.data;
                return address.postal_code;
            }


            var address = suggestion.data;
            return join([
                join([address.region_type, address.region], " "),
                join([address.area_type, address.area], " "),
                join([address.city_type, address.city], " "),
                join([address.settlement_type, address.settlement], " "),
                join([address.street_type, address.street], " "),
                join([address.house_type, address.house], " ")
            ]);
        },

        /**
         * ��������� ���� ����� ������������ ������ ������ �� ��������� ���������
         * @param suggestion ��������� ���������
         */
        showSelected: function (suggestion, type) {
            var address = suggestion.data;





            $('[name=region]').val('');
            $('[name=city]').val('');
            $('[name=settlement]').val('');
            $('[name=regs_postcode]').val('');



            if ((address.region == '� �����-���������' || address.region == '�����-���������'|| address.region == '� ������' || address.region == '������') && (address.city == null && address.settlement == null)) {
                address.city = address.region
                address.city_type = address.region_type
            }



            //dadata clean
            $("[name=regs_postcode]").val(address.postal_code);
            $("[name=region]").val(address.region_type + ' ' + address.region);
            $("[name=city]").val(address.city_type + ' ' +address.city);

            // ����� ���������� ������ ����������� ������ ��������
            // ���� �� ��� ��� �����
            if ((address.city != null || address.settlement != null) && address.street == null) {

                var addr;
                if (address.city != null) {
                    addr = address.city_type + ' ' + address.city;
                } else {
                    addr = address.settlement;
                }

                //������ ����� � �����
                if ( $('.city_name').length>0){
                    $('.city_name').html(addr);
                    $.post('/api/geo/',{city_name:addr,region_name:address.region_type+ ' ' + address.region, action: 'set_location'})
                }


                AddressSuggestions.getLogistic(address.kladr_id, addr);
            }





            //�����
            if (address.settlement==null){
                //Lipetsk
                if (address.area==null){
                    $("[name=city]").val(join([
                        join([address.city_type, address.city], " ")
                    ]));
                    $('.settlement').hide();
                    //Elec
                }else{
                    $("[name=city]").val(join([
                        join([address.area_type, address.area], " ")
                    ]));

                    $("[name=settlement]").val(join([
                        join([address.city_type, address.city], " ")
                    ]));
                    $('.settlement').show();
                }
            }else{
                //���������� ����, � �����������, ���� ������ ��������
                if (address.area==null && address.city!=null){
                    $("[name=city]").val(join([
                        join([address.city_type, address.city], " ")
                    ]));
                    //�������� ���, �������������� �-�, ���� �����������
                }else{
                    $("[name=city]").val(join([
                        join([address.area_type, address.area], " ")
                    ]));
                }

                $("[name=settlement]").val(join([
                    join([address.settlement_type, address.settlement], " ")
                ]));
                $('.settlement').show();

            }

            $("[name=street]").val(join([
                join([address.street_type, address.street], " "),
            ]));






            if (address.house!=null){
                if (address.house_type=='�'){
                    var match = address.house.match(/(\d+)( � \d+)?( ��� \d+)?/)
                    if (typeof (match[1])!='undefined'){
                        $("[name=house]").val(match[1].match(/\d+/)[0])
                        console.log('���:' + match[1].match(/\d+/)[0])
                    }
                    if (typeof (match[2])!='undefined'){
                        $("[name=corpus]").val(match[2].match(/\d+/)[0])
                        console.log('������:' + match[2].match(/\d+/)[0])
                    }

                    if (typeof (match[3])!='undefined'){
                        $("[name=stroenie]").val(match[3].match(/\d+/)[0])
                        console.log('��������:' + match[3].match(/\d+/)[0])
                    }
                }

                if (address.house_type=='���'){
                    $("[name=stroenie]").val(address.house)
                    $("[name=house]").val('')
                    $("[name=corpus]").val('')
                    console.log('��������:' + address.house)
                }


                if (address.house_type=='�'){
                    $("[name=stroenie]").val('')
                    $("[name=house]").val('')
                    $("[name=corpus]").val(address.house)
                    console.log('������:' + address.house)
                }
            }


            /* Zelenograd, korpus 100 %(
            if ($('[name=street]').val() != '') {
                $('.house').show();
            } else {
                $('.house').hide();
                //$('.house input').val('');
                $('[name="house"]').val('');
                $('[name="corpus"]').val('');
                $('[name="stroenie"]').val('');
                $('[name="flat"]').val('');
                $('[name="domophone"]').val('');
                $('[name="regs_postcode"]').val('');

            }
            */

            //if (type == 'region') {
            if (address.region == '������' || address.region == '�����-���������' || address.region == '� ������' || address.region == '� �����-���������') {
                this.initParts(address.region);
                $('#city').show();
            } else {
                $('.part').hide();
                $('.other').hide();
                $('#select_part').attr('disabled','disabled')
                $('#city').show();
            }
            //}


            // ������������� ������� ����� ��� ������
            var $metroHolder = $('.metro'),
                $metroItems = $metroHolder.find("option");

            $metroItems.removeClass('metro-1');
            $.each($metroItems, function(i, item) {

                var $item = $(item);

                for(var m in metro) {
                    $.each(metro[m], function(h, metroItem) {

                        if ($.trim(metroItem) == $.trim($item.text())) {
                            $item.addClass("metro-"+m);
                        }
                    });
                }
            });

            if (address.region == '������'){
                $metroHolder.show();
            }else{
                $metroHolder.hide();
            }

            $('select[name=metro]').selectpicker({
                size: 8
            });
        },
        /**
         * ������������� ������� ������ ������ ������
         * @param {Number} kladr_id - ������������� ������������� ������
         * @param {String} city - �������� ������
         * @returns {Object} - ������������ ������ ajax �������
         */
        initParts: function (region_name) {
            if (region_name == '������' || region_name=='� ������') {
                if ($('[name="part"] option:selected').attr('data-location')!='m'){
                    $('[name="part"] option').hide().removeAttr("selected");
                    $('[name="part"] option[data-location="m"]').show();
                    $('[name="part"] option[data-location="m"]:first').attr("selected", "selected");
                }else{
                    $('[name="part"] option[data-location="m"]').show();
                }
            } else if (region_name == '�����-���������' || region_name == '� �����-���������') {
                if ($('[name="part"] option:selected').attr('data-location')!='s'){
                    $('[name="part"] option').hide().removeAttr("selected");
                    $('[name="part"] option[data-location="s"]').show();
                    $('[name="part"] option[data-location="s"]:first').attr("selected", "selected");
                }else{
                    $('[name="part"] option[data-location="s"]').show();
                }
            }

            $('#city').hide();
            $('#city input').val('');
            $('.other').hide();
            $('.part').show();
            $('#select_part').removeAttr('disabled')

            if ($('[name=part]').val() == '0') {
                $('.other').show();
            }
        },
        /**
         * ������� ���������� �������� �� ����� ����� ��������
         * ��� ���������� ������ ���������� ��� ��������� ������ ��������
         * � ������������ �� ���� �������� � ������ ��������
         * ��� ���������� - ����������� ����� �� �����
         *
         * @param {Number} kladr_id - ������������� ������������� ������
         * @param {String} city - �������� ������
         * @returns {Object} - ������������ ������ ajax �������
         */
        getLogistic: function(kladr_id, city) {


            var $wrapper = $('#couriers__wrapper'),
                aColors = {
                    1: 'blue',
                    2: 'grey',
                    3: 'violet',
                    4: 'orange',
                    5: 'yellow',
                    6: 'black',
                    7: 'red',
                    8: 'green'
                },
                currentCity = city;

            $wrapper.find('.dostavka_item').removeClass('active');
            $wrapper.find('.dostavka_item__checkbox').removeClass('checked');

            $(document).off('click', '.courier__select');
            $(document).off('click', '#couriers__wrapper .dostavka_item__checkbox');

            $(document).on('click', '.courier__select', function(e) {

                e.preventDefault();

                var $this = $(this),
                    id = $this.data('id'),
                    $logistic = $('#self_selected'),
                    $courier_wrapper = $('#couriers__wrapper'),
                    $parent = $this.parents('.ymaps-balloon__content'),
                    arr = [];

                $courier_wrapper.find('.dostavka_item').removeClass('active');
                $courier_wrapper.find('.dostavka_item__checkbox').removeClass('checked');
                $('#courier_address_id').val(id);

                // ����������� ��������� ��������
                $("#delivery_cost-text").text($parent.find('.cost').text());
                $("#delivery_cost").val($parent.find('.cost').text());

                // ��������� ����� ���������
                $('#big_sum_all').text(parseInt($('#dSum').val()) + parseInt($("#delivery_cost").val()));
                $('.val_total').text(parseInt($('#dSum').val()) + parseInt($("#delivery_cost").val()));
                //$("#user_big_sum_all").val(parseInt($('#dSum').val()) + parseInt($("#delivery_cost").val()));


                arr.push("<div class='wrr'>");
                arr.push("<span>�� �������: <b>"+$parent.find('.ymaps-balloon-content__header').text()+"</b></span>");
                arr.push("<span>����� ������ ����������: <b>"+$parent.find('.address').text()+"</b></span>");
                arr.push("<span>����� �������� �� "+$parent.find('.days').text()+" ��.</span>");
                arr.push("<span>��������� �������� �� "+$parent.find('.cost').text()+" �.</span>");
                arr.push("</div>");
                $logistic.html(arr.join(""));

                //$("#orders").find('.cart_btn1').removeAttr('disabled');
                $("#orders").data('validate',true);

                //$(".header_delivery span").html($("#delivery_cost-text"));



                $('.order_tooltip').hide();

            });

            $('.dop_info input').on('change',function(){
                $('.dop_info input[name="'+ $(this).attr('name') + '"').val($(this).val());
            });

            $(document).on('click', '#couriers__wrapper .dostavka_item__checkbox', function(e) {

                e.preventDefault();

                var $this = $(this),
                    $parent = $this.parents('.courier__row'),
                    id = $parent.data('courier'),
                    $next = $parent.next('.courier__row-desc'),
                    $wrapper = $('#couriers__wrapper');

                $wrapper.find('.dostavka_item__checkbox').not($this).removeClass('checked');
                $('.dostavka_item').not($this.parents('.dostavka_item')).removeClass('active');
                $this.toggleClass('checked');

                $('#self_selected').empty();
                $wrapper.find('.courier__row').removeClass("active");

                if ($this.hasClass('checked')) {

                    // ���������� ���� � �������
                    $this.parents('.dostavka_item').addClass('active');

                    // ����������� ��������� ��������
                    $("#delivery_cost-text").text($parent.find('.cost').text());
                    $("#delivery_cost").val($parent.find('.cost').text());



                    $('#courier_address_id').val(id);

                    //$("#orders").find('.cart_btn1').removeAttr('disabled');
                    $("#orders").data('validate',true);

                    $(".header_delivery span").html('��������: <br>' + $('.dostavka_item.active .text').html() + ' -  ' + $("#delivery_cost-text").html() + ' ���.');
                    $(".header_delivery a").hide();
                    $('.select_delivery').removeClass('highlight');

                    $('.order_tooltip').hide();

                    if($next.length) {
                        $wrapper.find('.courier__row-desc').not($next).slideUp(0, function() {
                            $next.slideDown(0).addClass("active");
                            $parent.addClass("active");
                        });
                    }
                } else {

                    // �������� ���� � �������
                    $this.parents('.dostavka_item').removeClass('active');

                    // ���������� ��������� ��������
                    $("#delivery_cost-text").text(0);
                    $("#delivery_cost").val(0);

                    $('#courier_address_id').val(0);

                    //$("#orders").find('.cart_btn1').attr('disabled', 'disabled');
                    $("#orders").data('validate',false);
                    $(".header_delivery span").html($(".header_delivery span").data('default'));
                    $(".header_delivery a").show();
                    //$('.order_tooltip').show();

                    $wrapper.find('.courier__row-desc').slideUp(0).removeClass("active");
                }

                //����������� ����� �� ������� � ��������:
                $("#total_discount_delivery_cost-text").text(parseInt($('#dSum').val()) + parseInt($("#delivery_cost").val()));
                $("#total_discount_delivery_cost").val(parseInt($('#dSum').val()) + parseInt($("#delivery_cost").val()));


                //������������ ��������� ��� ��������
                var region = $('[name=region]').val();
                if (region=='� ������' || region=='��� ����������'){
                    var insurance = 0;
                }else{
                    var insurance = parseInt((parseInt($('#dSum').val()) + parseInt($("#delivery_cost").val()))*0.04);
                }
                var insurance = 0;
                $("#insurance_cost-text").text(insurance);
                $("#insurance_cost").val(parseInt(insurance));

                console.log(region)



                // ��������� ����� ��������� �������+��������+���������
                $('#big_sum_all').text(parseInt($('#dSum').val()) + parseInt($("#delivery_cost").val())+insurance);
                $('.val_total').text(parseInt($('#dSum').val()) + parseInt($("#delivery_cost").val())+insurance);
                //$("#user_big_sum_all").val(parseInt($('#dSum').val()) + parseInt($("#delivery_cost").val()));
            });

            return $.ajax({
                url: "/api/logistic/",
                data: {
                    k: kladr_id
                },
                success: function(response) {

                    var arData = [],
                        selfData = [],
                        $container = "",
                        hasCourier = false,
                        hasPost = false,
                        hasSelf = false,
                        count = 0,
                        dSelf = $.Deferred(),
                        resultsCount = 0,
                        geoAllAddedDef = $.Deferred();

                    // ���� ��� �����, ��������� ���
                    if (response != null) {

                        // ��������� ��� �� ��������� �� ����
                        for(var k in response) {

                            arData = [];

                            if (k == '��������') {

                                hasCourier = true;
                                $container = $('#courier').show(0).find('.courier__table-body');

                                // ������ ��� ������� ���������, ���� ���������� ��
                                $container.html("");

                                var city = "";

                                // ��������� ��� ������
                                $.each(response[k], function(i, item) {

                                    // ��������� ��� ������ ��� �������� � ������� ��
                                    $.each(item.addresses, function(j, addr) {

                                        city = addr.name_city;

                                        arData.push('<tr class="courier__row" data-courier="'+addr.id+'">');

                                        arData.push('<td><div data-toggle="tooltip" class="dostavka_item__checkbox" data-content="'+addr.delivery_news_to_operator+'" class="dostavka_item__checkbox">'+item.name+'</div></td>');
                                        arData.push('<td><span class="days">'+addr.delivery_days+'</span> ��.</td>');
                                        arData.push('<td><span class="cost">'+addr.delivery_cost+'</span> �.</td>');

                                        arData.push('</tr>');
                                    });
                                });

                                // ��������� ��������� � ��������� � ���������
                                $container.html(arData.join(""));

                            } else if (k == '���������') {

                                hasSelf = true;
                                $container = $('#self').show(0).find('.courier__table-body');

                                // ������ ��� ������� ���������, ���� ���������� ��
                                $container.html("");

                                var city = "",
                                    metroClass = "";

                                // ��������� ��� ������
                                $.each(response[k], function(i, item) {

                                    // ��������� ��� ������ ��� �������� � ������� ��
                                    $.each(item.addresses, function(j, addr) {

                                        city = addr.name_city;

                                        for(var m in metro) {
                                            $.each(metro[m], function(h, metroItem) {

                                                var mTemp = addr.delivery_metro.replace(/�\./, "");
                                                if ($.trim(metroItem) == $.trim(mTemp)) {
                                                    metroClass = "metro-"+m;
                                                }
                                            });
                                        }

                                        arData.push('<tr class="courier__row" data-courier="'+addr.id+'">');

                                        if (addr.delivery_metro==''){
                                            arData.push('<td><div data-toggle="tooltip" class="dostavka_item__checkbox" data-content="'+addr.delivery_news_to_operator+'">' +
                                                '<div class="dostavka_item__bottom">'+addr.delivery_address+'</div>' +
                                                '</div>' +
                                                '</td>');
                                        }else{
                                            arData.push('<td><div data-toggle="tooltip" class="dostavka_item__checkbox" data-content="'+addr.delivery_news_to_operator+'">' +
                                                '<div class="dostavka_item__metro_bottom">'+addr.delivery_address+'</div>' +
                                                '</div>' +
                                                '</td>');
                                        }



                                        arData.push('<td><span class="days">'+addr.delivery_days+'</span> ��.</td>');
                                        arData.push('<td><span class="cost">'+addr.delivery_cost+'</span> �.</td>');

                                        arData.push('</tr>');

                                        arData.push('<tr class="courier__row-desc" style="display: none;">');
                                        arData.push('<td colspan="3"><p style="margin-top: 10px;">'+addr.delivery_how_to+'</p></td>');
                                        arData.push('</tr>');
                                    });
                                });

                                // ��������� ��������� � ��������� � ���������
                                $container.html(arData.join(""));

                            } else if (k == '�����') {

                                hasPost = true;

                                $container = $('#post').show(0).find('.courier__table-body');

                                // ������ ��� ������� ���������, ���� ���������� ��
                                $container.html("");

                                var city = "";

                                // ��������� ��� ������
                                $.each(response[k], function(i, item) {

                                    // ��������� ��� ������ ��� �������� � ������� ��
                                    $.each(item.addresses, function(j, addr) {

                                        //��������� ����� ��� ����������������
                                        city = addr.name_city;

                                        arData.push('<tr class="courier__row" data-courier="'+addr.id+'">');

                                        arData.push('<td><div data-toggle="tooltip" class="dostavka_item__checkbox" data-content="'+addr.delivery_news_to_operator+'" class="dostavka_item__checkbox">'+item.name+'</div></td>');
                                        arData.push('<td><span class="days">'+addr.delivery_days+'</span> ��.</td>');
                                        arData.push('<td><span class="cost">'+addr.delivery_cost+'</span> �.</td>');

                                        arData.push('</tr>');
                                    });
                                });

                                // ��������� ��������� � ��������� � ���������
                                $container.html(arData.join(""));
                            }
                        }

                        // ���� �� ������������ ����� ��
                        if (!hasSelf) {

                            if (city == null || city == "") {
                                city = currentCity;
                            }

                            $.when(ymapsDef).then(function() {
                                ymaps.geocode(city, {
                                    results: 1
                                }).then(function(res) {

                                    // �������� ������ ��������� ��������������
                                    var firstGeoObject = res.geoObjects.get(0),
                                    // ���������� ����������
                                        coords = firstGeoObject.geometry.getCoordinates(),
                                    // ������� ��������� ����������
                                        bounds = firstGeoObject.properties.get('boundedBy');

                                    // ������������ ����� �� ������� ��������� ����������.
                                    yMap.setBounds(bounds, {
                                        checkZoomRange: true // ��������� ������� ������ �� ������ ��������.
                                    });
                                });
                            });
                        }

                        // ������� ��, ��� ���� �� ����������
                        if (!hasCourier) {
                            $('#courier').hide(0).find('.courier__table-body').html("");
                        }

                        if (!hasPost) {
                            $('#post').hide(0).find('.courier__table-body').html("");
                        }

                        if (!hasSelf) {
                            $('#self').hide(0).find('.courier__table-body').html("");

                            $.when(ymapsDef).then(function() {
                                // ������� �����
                                yMap.geoObjects.removeAll();
                            });
                        }

                        var $cart = $('#cart_step2');

                        if ($cart.length) {
                            var scroll_top = $cart.find('.rCol').offset().top,
                            scroll_block = $cart.find('.slideBlock'),
                            content_h = parseInt($cart.find('.lCol').height()) + $cart.find('.lCol').offset().top - $cart.find('.slideBlock').height() - 60;

                            cart_sb_scroll(scroll_top,scroll_block,content_h);
                        }
                    }

                    $('.dostavka_item [data-toggle=tooltip]').hover(function() {
                        var elem = $(this),
                            content = elem.data('content'),
                            width = 200,
                            elem_l = 0;

                        if (content == "")
                            return;

                        if (elem.data('width') != undefined) {
                            width = elem.data('width');
                            elem_l = - 145;
                        }

                        console.log(234)

                        elem.parent().append('<div style="left:-70px;bottom:'+95+'%;margin-left:'+elem_l+'px;width:'+width+'px" class="dostavka_tooltip">'+content+'</div>');
                        $('.dostavka_tooltip').fadeIn(150)
                    }, function() {

                        var elem = $('.dostavka_tooltip');
                        elem.fadeOut(150);
                        setTimeout(function(){
                            elem.remove();
                        },151);
                    });
                }
            })
        },
        correctValues: function(){

            if ($('[name=region]').val()==$('[name=city]').val()){
                var q = $('[name=region]').val() + ' ' +  $('[name=settlement]').val() + ' ' + $('[name=street]').val() + ' ' + $('[name=house]').val()
            }else{
                var q = $('[name=region]').val() + ' ' + $('[name=city]').val() + ' ' +  $('[name=settlement]').val() + ' ' + $('[name=street]').val() + ' ' + $('[name=house]').val()
            }


            if ($('[name=corpus]').val()!=''){
                q = q + ' ���� ' + $('[name=corpus]').val();
            }
            if ($('[name=stroenie]').val()!=''){
                q = q + ' ��� ' + $('[name=stroenie]').val();
            }

            var request = DadataApi.clean(q);
            var self = this;
            request.done(function (msg) {
                if (typeof msg !='undefined'){
                    dadata_clear_def.resolve();
                    var data = new Array();
                    data['data']= self.getCleanData(msg);
                    self.showSelected(data,'all');


                }else{
                    dadata_clear_def.reject();
                }

            });
            return request;
        },
        /**
         * �������� ������ �� ������ ���������
         * @param msg
         * @returns {*}
         */
        getCleanData:function(msg){
            if (typeof (msg.suggestions[0])!='undefined'){
                return msg.suggestions[0]['data'];
            }
        },

        clean:function(){
            if ($('[name=region]').val()==$('[name=city]').val()){
                var q = $('[name=region]').val() + ' ' +  $('[name=settlement]').val() + ' ' + $('[name=street]').val() + ' ' + $('[name=house]').val()
            }else{
                var q = $('[name=region]').val() + ' ' + $('[name=city]').val() + ' ' +  $('[name=settlement]').val() + ' ' + $('[name=street]').val() + ' ' + $('[name=house]').val()
            }

            if ($('[name=corpus]').val()!=''){
                q = q + ' ���� ' + $('[name=corpus]').val();
            }
            if ($('[name=stroenie]').val()!=''){
                q = q + ' ��� ' + $('[name=stroenie]').val();
            }
            var request = DadataApi.clean(q);
            request.done(function (msg) {
                if (typeof (msg.suggestions[0])!='undefined'){
                    var data =  msg.suggestions[0]['data'];
                }
                if (data!=''){
                    var postal_code = data.postal_code;
                    console.log(postal_code)
                    $('[name=regs_postcode]').val(postal_code)
                    $('[name=address_data]').val(JSON.stringify(data));
                    $('[name=regs_postcode]').val(postal_code)
                }
            });
        },
        cleanName:function(input){
            var q = $(input).val();


            var request = DadataApi.cleanName(q);
            request.done(function (msg) {

                console.log(msg)
                /*
                if (typeof (msg.suggestions[0])!='undefined'){
                    var data =  msg.suggestions[0]['data'];
                }
                if (data!=''){
                    var postal_code = data.postal_code;
                    console.log(postal_code)
                    $('[name=regs_postcode]').val(postal_code)
                    $('[name=address_data]').val(JSON.stringify(data));
                    $('[name=regs_postcode]').val(postal_code)
                }
                */
            });
        }
    };
    window.AddressSuggestions = AddressSuggestions;
})()