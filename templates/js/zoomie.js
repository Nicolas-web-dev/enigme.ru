$(function(){
	$('.zoomme img').mouseover(function(){

		var div_wrap = $(this).parent().next();
		$('.vspliv').hide();
		var this_width = $(this).width()/2;
		var div_w = div_wrap.width()/2;
		var left = div_w - this_width;
		left = "-" + left;
		
		var this_height = $(this).height()/2;
		var div_h = div_wrap.height()/2;
		var top = div_h - this_height + 55;
		top = "-" + top;
		
		div_wrap.css("position","absolute").css("z-index","10").css("left", "0px").css("top", top + "px").fadeIn();
	});
	$('.vspliv').mouseleave(function(){
			$(this).fadeOut();
		});
	
});