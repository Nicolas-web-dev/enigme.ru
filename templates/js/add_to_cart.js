$(document).ready(function(){
	$(".add_to_cart").click(function (event) {
	event.preventDefault();
	var id_product_add = Number($(this).data('id_product'));
	var id_catalog = Number($(this).data('id_catalog'));
	if (id_product_add != 0) {
		
			$("#product_cat_" + id_catalog)
            .clone()
            .css({'position' : 'absolute', 'z-index' : '11100', 'top': $(this).offset().top, 'left': $(this).offset().left-100})
            .appendTo("body")
            .animate({opacity: 0.05,
                left: $(".cart-block").offset()['left'],
                top: $(".cart-block").offset()['top'],
                width: 20}, 1000, function() {
                $(this).remove();
            });
		
		
		$.ajax({
			type: "POST",
			url: "/ajax-requests/?ajax_add_to_cart=1",
			//dataType: "json",
			data: "id_product_add=" + id_product_add
		}).done(function(data) {
			var res_data = eval("(" + data + ")");
			if (Number(res_data.products_quantity) > 0) {
				$(".cart-block__icon").addClass("in_basket");
				$(".cart-block__items").text("���������� �������: " + res_data.products_quantity + " ��");
				$(".cart-block__price").text("�� ����� " + res_data.products_sum + " �");
				$(".cart-block2__header_hide").show();
				if ($('#global_geo_segment').data('geo_segment') == '1') {
					yaCounter21951622.reachGoal('add_to_cart_tovar_success1');
				} else {
					yaCounter42164999.reachGoal('add_to_cart_tovar_success1');
				}
			}
			
		});
	}

});
	
});