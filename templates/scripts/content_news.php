<div class="section__header">
    <div class="breadcrumbs">
		<a class="breadcrumbs__item" href="/">�������</a>
		<a class="breadcrumbs__item breadcrumbs__item--active" href="#"><?php echo $this->cat_name; ?></a>
	</div>
    <h1 class="section__title"><?php echo $this->cat_name; ?></h1>
</div>
<div class="specials">
	<?php foreach ($this->news_item as $news_item) { ?>
    <div class="specials__item">
        <div class="specials__item-picture">
			<?php if($news_item['img']!='') { ?>
				<img class="specials__item-img" src="/images/uploads/news/<?php echo $news_item['id']; ?>/trumbs/<?php echo $news_item['img']; ?>" alt="<?php echo $news_item['title']; ?>" title="<?php echo $news_item['title']; ?>">
			<?php } ?>
		</div>
            <div class="specials__item-content">
                <p class="specials__item-header"><?php echo $news_item['title']; ?></p>
                <div class="specials__item-text"><?php echo $news_item['anot']; ?></div>
				<a class="button specials__button" href="/<?php echo $this->canon_name1; ?>/<?php echo $news_item['url']; ?>/">���������</a>
            </div>
    </div>
    <?php } ?>          
</div>