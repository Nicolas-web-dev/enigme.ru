<div class="section__header">
    <div class="breadcrumbs">
		<a class="breadcrumbs__item" href="/">�������</a>
		<a class="breadcrumbs__item" href="/catalog/">������ ����������</a>
		<a class="breadcrumbs__item breadcrumbs__item--active" href="#"><?php echo $this->brend_title; ?></a>
	</div>
	<?php //if ( isset($this->catalog_item_f) || isset($this->catalog_item_m) ) { ?>
	<?php if (isset($this->catalog_prod_items)) { ?>
            <div class="catalog-controls">
                <h1 class="catalog-controls__header">������� <span class="catalog-controls__header-brand"><?php echo $this->brend_title; ?>: </span>���� � ��������� ����</h1>
                <div class="catalog-controls__categories">
					<a id="catalog-controls_wm" class="catalog-controls__category-item" href="#cat_wm">������� ����</a>
					<a id="catalog-controls_m" class="catalog-controls__category-item" href="#cat_m">������� ����</a>
					<a id="catalog-controls_all" class="catalog-controls__category-item catalog-controls__category-item--active" href="#cat_m">��� ����</a>
				</div>
				<?php if ($this->brend['text2']) { ?>
					<div style="padding: 10px 30px;">
						<?php echo $this->brend['text2']; ?>
					</div>
				<?php } ?>
				<div id="sorting_catalog_block" class="sorting" data-id-catalog="<?php echo $this->brend['id']; ?>" data-brend-title="<?php echo $this->brend_title; ?>" data-brend-url="<?php echo $this->brend['url']; ?>">
					<div class="sorting__header">����������� ��:</div>
					<div class="sorting__content">
						<a id="catalog_sort_rate" class="sorting__button <?=(!$_GET['sort'] || $_GET['sort']=='rate')?'sorting__button--active':''?>" href="<?=$_SERVER['REQUEST_URI']?>?sort=rate">��������</a>
						<a id="catalog_sort_price" class="sorting__button <?=($_GET['sort']=='price')?'sorting__button--active':''?>" href="#">����</a>
						<a id="catalog_sort_abc" class="sorting__button <?=($_GET['sort']=='abc')?'sorting__button--active':''?>" href="<?=$_SERVER['REQUEST_URI']?>?sort=abc">��������</a>
						<a id="catalog_sort_new" class="sorting__button <?=($_GET['sort']=='new')?'sorting__button--active':''?>" href="<?=$_SERVER['REQUEST_URI']?>?sort=new">��������</a>
					</div>
				</div>
            </div>
	<?php } ?>		  
</div>
<div id="catalog_prod_items" class="section-wrapper catalog">

	<?php if (isset($this->catalog_prod_items)) { ?>
		<?php foreach ($this->catalog_prod_items as $catalog_item) { ?>
			<?php if ($catalog_item['pol'] == 'F') {
				include("content_catalog_item_f.php");
			} else {
				include("content_catalog_item_m.php");
			}
			?>
		<?php } ?>
	<?php } ?>
</div>		  
<?php if($this->brend['anot']) { ?>
	<div class="block_description_info">
		<div class="description_info_name">
			<?php echo $this->brend_title; ?>
		</div>
		<div class="description_info_text">
			<?php echo $this->brend['anot']; ?>
		</div>
	</div>
<?php } ?>