<div class="section__header">
    <div class="breadcrumbs">
		<a class="breadcrumbs__item" href="/">�������</a>
		<a class="breadcrumbs__item" href="/users/">������ �������</a>
		<a class="breadcrumbs__item breadcrumbs__item--active" href="#">������� �������</a>
	</div>
    <h1 class="section__title">������� �������</h1>
</div>

<div class="cabinet section-wrapper">
	<div class="tabs history__tab">
		<a href="/users/history/status/1/" class="tab<?=($this->status == 1)?' tab--active':''?>">�� ��������������</a>
		<a href="/users/history/status/2/" class="tab<?=($this->status == 2)?' tab--active':''?>">��������������</a>
		<a href="/users/history/status/4/" class="tab<?=($this->status == 4)?' tab--active':''?>">� ���������</a>
		<a href="/users/history/status/3/" class="tab<?=($this->status == 3)?' tab--active':''?>">������������</a>
	</div>
	
	<?php foreach ($this->orders as $orders) { ?>
	<div class="cart-table order-table">
		<div class="cart-table__meta">
			<div class="cart-table__meta-date">��������: <?php echo $orders['DateAdd']; ?></div>
			<div class="cart-table__meta-code">��� ������ [<?php echo $orders['id']; ?>]</div>
		</div>
		<table class="cart-table__content">
			<tr class="cart-table__header">
				<th class="cart-table__header-item" colspan="2">�������� ������</th>
				<th class="cart-table__header-item">����</th>
				<th class="cart-table__header-item">���-��</th>
				<th class="cart-table__header-item">���������</th>
			</tr>
			<?php foreach ($orders['order'] as $order) { ?>
			<tr class="cart-table__item">
				<td class="cart-table__item-picture">
					<img class="cart-table__img" src="/images/uploads/catalog/<?php echo $order['id_catalog_data']; ?>/small/<?php echo $order['img']; ?>" alt="">
				</td>
				<td class="cart-table__item-header"><?php echo substr($order['title'], 5); ?></td>
				<td class="cart-table__item-price price cart-table__item-price--wide">
					<?php if ($order['is_sale'] == 1) { ?>
						<div class="hot-price"></div>
					<?php } ?>
					<?php echo parsePrice($order['sum']); ?>
				</td>
				<td class="cart-table__item-amount cart-table__item-amount--wide">
					<?php echo $order['kol']; ?>
				</td>
				<td class="cart-table__item-price cart-table__item-price--wide sum"><?php echo parsePrice($order['sum'] * $order['kol']); ?></td>
			</tr>
			<?php } ?>
		</table>
		<div class="cart-table__overall">
			<div class="cart-table__overall-item">
				<div class="cart-table__overall-header">
					<div class="cart-table__overall-header-content">����� ������:</div>
				</div>
				<div class="cart-table__overall-content"><span class="val_subTotal" id="big_sum"><?php echo parsePrice($orders['Sum']); ?></span> ���</div>
			</div>
			<div class="cart-table__overall-item">
				<div class="cart-table__overall-header">
					<div class="cart-table__overall-header-content"><? if ($orders['dType'] == 1) echo '�������������'; else echo '�������'; ?> ������:</div>
				</div>
				<div class="cart-table__overall-content"><?php echo $orders['dPercent']; ?>%</div>
			</div>
			<div class="cart-table__overall-item">
				<div class="cart-table__overall-header">
					<div class="cart-table__overall-header-content">����� �����: </div>
				</div>
				<div class="cart-table__overall-content cart-table__final-price"><span class="val_total" id="big_sum_all"><?php echo parsePrice($orders['dSum']); ?></span> ���</div>
			</div>
		</div>
	</div>
	<?php } ?>
</div>