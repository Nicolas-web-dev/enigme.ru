 <div class="section__header">
    <div class="breadcrumbs">
		<a class="breadcrumbs__item" href="/">�������</a>
		<a class="breadcrumbs__item breadcrumbs__item--active" href="#">������ �������</a>
	</div>
    <h1 class="section__title">������ �������</h1>
</div>
<?php
	switch ($this->user['d_percent']){
		case 0:
			$status = 'Enigme CLUB';
			$perm = '����������� ��������� ������� �� "��������" ��� ���������� ������� �� ����� ����� 3 000 ���. <br />
            ����� ���� ������ �� ��� ����������� ������� ���������� �������� - 3%';
			break;
		case 3:
			$status = '��������';
			$perm = '����������� ��������� ������� �� "������������" ��� ���������� ������� �� ����� ����� 10 000 ���.<br />
            ����� ���� ������ �� ��� ����������� ������� ���������� �������� - 5%';
			break;
		case 5:
			$status = '������������';
			$perm = '����������� ��������� ������� �� "��������" ��� ���������� ������� �� ����� ����� 20 000 ���. <br />
            ����� ���� ������ �� ��� ����������� ������� ���������� �������� - 7%';
			break;
		case 7:
			$status = '"������"';
			$perm = '����������� ��������� ������� �� "VIP" ��� ���������� ������� �� ����� ����� 70 000 ���.<br />
            ����� ���� ������ �� ��� ����������� ������� ���������� �������� - 10%';
			break;
		case 10:
			$status = 'VIP';
			$perm = '����������� ��������� ������� �� "VIP" ��� ���������� ������� �� ����� ����� 140 000 ���.<br />
            ����� ���� ������ �� ��� ����������� ������� ���������� �������� - 15%';
			break;
		case 15:
			$status = 'Premium';
			$perm = '���� ������ �� ��� ������� ���������� ���������� - 15%';
			break;
		default:
			break;
	}
?>
 <div class="cabinet">
    <div class="cabinet__top section-wrapper section-wrapper--light">
        <div class="cabinet__header">
            <div class="cabinet__header-item"><?php echo $this->user['fio']; ?></div>
            <div class="cabinet__header-item">��� ID ����� - <?php echo $this->user['id']; ?></div>
        </div>
        <div class="cabinet__stats">
            <div class="cabinet__stats-item">���� ������: <span class="cabinet__stats-item-content"><?php echo $this->user['d_percent']; ?>%</span></div>
            <div class="cabinet__stats-item">��� ������: <span class="cabinet__stats-item-content"><?php echo $status; ?></span></div>
        </div>
        <div class="cabinet__privilege">
            <div class="cabinet__privilege-header">���� ����������:</div>
            <div class="cabinet__privilege-content"><?php echo $perm; ?></div>
        </div>
    </div>
    <div class="cabinet__menu">
		<a class="cabinet__menu-item" href="/users/sale/">����������</a>
		<a class="cabinet__menu-item" href="/users/discounts/">������ � �����</a>
		<a class="cabinet__menu-item" href="/users/favorite/">���������</a>
		<a class="cabinet__menu-item" href="/users/history/status/1/">������� �������</a>
		<a class="cabinet__menu-item" href="/users/comments/">��� �����������</a>
	</div>
    <div class="section-wrapper section-wrapper--light">
        <div class="sales">
            <h2 class="sales__header">���������� ������:</h2>
				<?php foreach ($this->catalog_item_top as $catalog_item) { ?>
					<div class="unique-item">
						<div class="unique-item__picture"><img class="unique-item__img" width="160" height="160" src="<?php echo $catalog_item['img']; ?>" alt=""></div>
							<div class="unique-item__content">
								<div class="unique-item__header">
									<div class="unique-item__brand"><?php echo $catalog_item['brend_title']; ?> </div>
									<div class="unique-item__name"><?php echo $catalog_item['title']; ?></div>
								</div>
								<div class="unique-item__collection"><?php echo $catalog_item['pol']; ?></div>
								<?php if ( $catalog_item['tag']!='' ) { ?>
									<div class="notes unique-item__notes">
										<?php foreach ($catalog_item['tag'] as $key => $catalog_item_tag) { ?>
											<a href="/tag/<?php echo $catalog_item_tag['url']; ?>/"><?php echo $catalog_item_tag['title']; ?></a><?php if (count($catalog_item['tag']) != ((int)$key + 1)) { ?>,&nbsp;<?php } ?>
										<?php } ?>
									</div>
								<?php } ?>
								<div class="unique-item__dscr"><?php echo $catalog_item['anot']; ?></div>
								<div class="unique-item__controls">
									<a id="<?php echo $catalog_item['id']; ?>" class="unique-item__like <?php if ($catalog_item['is_note']) { echo 'active'; } ?>" href="#favorites"><?php if ($catalog_item['is_note']) { echo '�&nbsp;���������'; } else {echo '�&nbsp;���������';} ?></a>
									<a class="button" href="<?php echo $catalog_item['url']; ?>">���������</a>
								</div>
							</div>
					</div>
				<?php } ?>
        </div>
    </div>
</div>
<script>
$(function() {
	$('[href="#favorites"]').click(function(e) {
		$(this).addClass('active');
		$(this).text('� ���������');

		e.preventDefault();
	});
});
</script>