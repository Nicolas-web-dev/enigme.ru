<div class="section__header">
    <div class="breadcrumbs">
		<a class="breadcrumbs__item" href="/">�������</a>
		<a class="breadcrumbs__item breadcrumbs__item--active" href="#">�����������</a>
	</div>
    <h1 class="section__title">�����������</h1>
</div>
<form class="registration" action="/users/register/" method="post" enctype="multipart/form-data" name="reg_form_new" autocomplete="off">
    <div class="section-wrapper">
        <div class="registration__row">
            <div class="input-item">
                <div class="input-item__header input-item__header--required">���:</div>
                <input class="input registration__input" type="text" value="<?php echo $this->data['fio']; ?>" name="regs_fio" id="log_fio2" required>
            </div>
            <div class="input-item">
                <div class="input-item__header input-item__header--required">E-mail:</div>
                <input class="input registration__input" type="text" value="<?php echo $this->data['email']; ?>" name="regs_email" id="login2" required>
            </div>
            <div class="input-item">
                <div class="input-item__header input-item__header--required">�������:</div>
                <input class="input registration__input" type="text" value="<?php echo $this->data['phone']; ?>" name="regs_phone" id="phone2" required>
            </div>
        </div>
        <div class="registration__row">
            <div class="input-item region">
                <div class="input-item__header input-item__header--required">������/�������:</div>
                <input class="input registration__input" type="text" value="<?php echo $this->data['address-region']; ?>" name="region" id="region" required>
            </div>
            <div class="input-item part">
                <div class="input-item__header input-item__header--required">�������� �����:</div>
                <select class="input select registration__input" name="part" id="select_part" required>
					<?php foreach ($this->capital_districts as $letter => $capital) { ?>
                            <? foreach ($capital as $key => $item) { ?>
                                <option  <?=($this->data['address-part']==$key)?'SELECTED':''?> style="display: none" data-location="<?=$letter; ?>"
                                        value="<?=$key; ?>"><?=$item; ?></option>
                            <?php } ?>
                    <?php } ?>
				</select>
            </div>
            <div class="input-item city">
                <div class="input-item__header input-item__header--required">�����/�����:</div>
                <input class="input registration__input" type="text" name="city" value="<?php echo $this->data['address-city']; ?>" id="city" required>
            </div>
        </div>
        <div class="registration__row street">
            <div class="input-item">
                <div class="input-item__header input-item__header--required">�����:</div>
                <input class="input registration__street" type="text" name="street" value="<?php echo $this->data['address-street']; ?>" id="street" required>
            </div>
        </div>
        <div class="registration__row">
            <div class="input-item">
                <div class="input-item__header">���:</div>
                <input class="input registration__input--short" name="house" type="text" value="<?php echo $this->data['address-house']; ?>" id="house">
            </div>
            <div class="input-item">
                <div class="input-item__header">������:</div>
                <input class="input registration__input--short" type="text" name="corpus" value="<?php echo $this->data['address-corpus']; ?>" id="corpus">
            </div>
            <div class="input-item">
                <div class="input-item__header">��������:</div>
                <input class="input registration__input--short" type="text" name="stroenie" value="<?php echo $this->data['address-stroenie']; ?>" id="stroenie">
            </div>
            <div class="input-item">
                <div class="input-item__header">��������:</div>
                <input class="input registration__input--short" type="text" name="flat" value="<?php echo $this->data['address-flat']; ?>" id="flat">
            </div>
            <div class="input-item">
                <div class="input-item__header">����:</div>
                <input class="input registration__input--short" type="text" name="floor" value="<?php echo $this->data['address-floor']; ?>" id="floor">
            </div>
            <div class="input-item">
                <div class="input-item__header">����� ��������:</div>
                <input class="input registration__intercom" type="text" name="domophone" value="<?php echo $this->data['address-domophone']; ?>" id="domophone">
            </div>
        </div>
        <div class="registration__password-options">
            <div class="roundbutton">
                <input class="roundbutton__hidden" type="radio" name="regs_p" id="pass_auto" value="1" <?php if ($this->data['p'] == 1 || !isset($this->data['p'])) echo 'checked'; ?>>
                <label class="roundbutton__fake" for="pass_auto"></label>
                <label class="roundbutton__label" for="pass_auto">������������� ������ �������������</label>
            </div>
            <div class="roundbutton">
                <input class="roundbutton__hidden" type="radio" id="pass_manual" name="regs_p" value="2" <?php if ($this->data['p'] == 2) echo 'checked'; ?>>
                <label class="roundbutton__fake" for="pass_manual"></label>
                <label class="roundbutton__label" for="pass_manual">������ ������</label>
            </div>
        </div>
        <div id="fields_pass_manual" class="registration__row" style="<?php if ($this->data['p'] == 1 || !isset($this->data['p'])) echo 'display:none;'; ?>">
            <div class="input-item">
                    <div class="input-item__header input-item__header--required">������:</div>
                    <input class="input registration__password" type="password" name="regs_passw" required>
            </div>
            <div class="input-item">
                <div class="input-item__header input-item__header--required">������ ������:</div>
                <input class="input registration__password" type="password" name="regs_repass" required>
            </div>
        </div>
        <div class="checkbox">
            <input class="checkbox__hidden" type="checkbox" id="subscribe" name="regs_maillist" <?php if ($this->data['is_maillist'] == 1 || !isset($this->data['is_maillist'])) echo 'checked'; ?>>
            <label class="checkbox__fake" for="subscribe"></label>
            <label class="checkbox__label" for="subscribe">�������� �� ������� � �����</label>
        </div>
		<input hidden name="regs_postcode" value="11111111" >
		<input hidden name="fio_data" value="<?php echo htmlentities($this->data['fio_data'], ENT_COMPAT, 'cp1251'); ?>">
        <input hidden name="address_data" value="<?php echo htmlentities($this->data['address_data'], ENT_COMPAT, 'cp1251'); ?>">
        <button class="button button--big registration__button" type="submit" name="register_submit">������������������</button>
    </div>
</form>

<script>
        $(function () {
            //suggetions fio
            $("input[name=regs_fio]").suggestions({
                serviceUrl: DadataApi.DADATA_API_URL + "suggest/fio",
                selectOnSpace: true,
                token: "15309822a7c43aacc9f604670c10489fc4b19a0b",
                /* ����������, ����� ������������ �������� ���� �� ��������� */
                onSelect: function (suggestion) {
                    console.log(suggestion)
                    suggestion = suggestion;
                    $('input[name=fio_data]').val(JSON.stringify(suggestion));
                }
            });

            console.log(region_name)
            //index suggest
            AddressSuggestions.initForm();
			
			
			$('form[name="reg_form_new"]').validate({
				rules: {
				  regs_fio: 'required',
				  regs_email: {
					required: true,
					email: true
				  },
				  regs_phone: 'required',
				  region: 'required',
				  part: 'required',
				  city: 'required',
				  street: 'required',
				  regs_passw: 'required',
				  regs_repass: 'required'
				},
				messages: {
				  regs_fio: '����������, ������� ���� ���',
				  regs_email: {
					required: '����������, ������� e-mail',
					email: '������ ������������ e-mail �����'
				  },
				  regs_phone: '����������, ������� ��� �����',
				  region: '����������, ������� ������',
				  part: '����������, ������� �����',
				  city: '����������, ������� �����',
				  street: '����������, ������� �����',
				  regs_passw: '����������, ������� ������',
				  regs_repass: '����������, ��������� ������'
				},
				submitHandler: function(form) {
				  form.submit();
				}
			});

        })
</script>
