<div class="section__header">
    <div class="breadcrumbs">
		<a class="breadcrumbs__item" href="/">�������</a>
		<a class="breadcrumbs__item" href="/users/">������ �������</a>
		<a class="breadcrumbs__item breadcrumbs__item--active" href="#">���������</a>
	</div>
    <h1 class="section__title">���������</h1>
</div>
<div class="cabinet">
    <div class="favorites section-wrapper--light">
		<?php if (count($this->catalog_item)!=0){ ?>
			<?php foreach ($this->catalog_item as $catalog_item) { ?>
					<div class="unique-item unique-item-<?php echo $catalog_item['id']; ?>">
						<div class="unique-item__picture"><img class="unique-item__img" width="160" height="160" src="<?php echo $catalog_item['img']; ?>" alt=""></div>
							<div class="unique-item__content">
								<div class="unique-item__header">
									<div class="unique-item__brand"><?php echo $catalog_item['brend_title']; ?> </div>
									<div class="unique-item__name"><?php echo $catalog_item['title']; ?></div>
								</div>
								<div class="unique-item__collection"><?php echo $catalog_item['pol']; ?></div>
								<?php if ( $catalog_item['tag']!='' ) { ?>
									<div class="notes unique-item__notes">
										<?php foreach ($catalog_item['tag'] as $key => $catalog_item_tag) { ?>
											<a href="/tag/<?php echo $catalog_item_tag['url']; ?>/"><?php echo $catalog_item_tag['title']; ?></a><?php if (count($catalog_item['tag']) != ((int)$key + 1)) { ?>,&nbsp;<?php } ?>
										<?php } ?>
									</div>
								<?php } ?>
								<div class="unique-item__dscr"><?php echo $catalog_item['anot']; ?></div>
								<div class="unique-item__controls">
									<a class="button" href="<?php echo $catalog_item['url']; ?>" style="margin-right: 30%;">���������</a>
									<a class="unique-item__cart" href="<?php echo $catalog_item['url']; ?>">� �������</a>
									<a id="<?php echo $catalog_item['id']; ?>" class="unique-item__remove" href="#">�������</a>
								</div>
							</div>
					</div>
			<?php } ?>  
		<?php } ?>
    </div>
</div>