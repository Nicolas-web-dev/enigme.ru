<div class="section__header">
    <div class="breadcrumbs">
		<a class="breadcrumbs__item" href="/">�������</a>
		<a class="breadcrumbs__item" href="/users/">������ �������</a>
		<a class="breadcrumbs__item breadcrumbs__item--active" href="#">���������</a>
	</div>
    <h1 class="section__title">��������� ��������</h1>
</div>
<form class="settings" action="/users/options/" method="post" enctype="multipart/form-data" name="reg_form" autocomplete="off">
    <div class="section-wrapper">
        <div class="tabs">
			<a id="tab_settings_form-first" class="tab tab--active" href="#">������</a>
			<!--
			<a id="tab_settings_form-second" class="tab" href="#">����� ������</a>
			<a id="tab_settings_form-third" class="tab" href="#">���������</a>
			-->
        </div>
        <div class="settings__form">
			<div id="settings_form-first" style="display: block;">
				<div class="settings__form-item">
					<div class="settings__form-header">��� ID:</div>
					<div class="settings__form-content">
						<div class="settings__form-text"><?php echo $this->users_options['id']; ?></div>
					</div>
				</div>
				<div class="settings__form-item">
					<div class="settings__form-header">E-mail:</div>
					<div class="settings__form-content">
						<div class="settings__form-text"><?php echo $this->users_options['email']; ?></div>
					</div>
				</div>
				<div class="settings__form-item">
					<div class="settings__form-header">���:</div>
					<div class="settings__form-content">
						<input class="input settings__input" type="text" value="<?php echo $this->users_options['fio']; ?>" name="fio">
					</div>
				</div>
				<div class="settings__form-item">
					<div class="settings__form-header">���:</div>
					<div class="settings__form-content">
						<select class="input select settings__input" name="pol">
							<option value="0" selected>��� �� ������</option>
							<option value="1" <?php if ($this->users_options['pol'] == 'M') { ?> selected <?php } ?> >��</option>
							<option value="2" <?php if ($this->users_options['pol'] == 'F') { ?> selected <?php } ?> >���</option>
						</select>
					</div>
				</div>
				<div class="settings__form-item">
					<div class="settings__form-header">���� ��������:</div>
					<div class="settings__form-content">
						<select class="input select settings__select" name="day">
							<option value="0">����</option>
							<?php for ($i = 1; $i <= 31; $i++) { ?>
								<option <?php if ($this->users_options_birthday[2] == $i) { ?> selected <?php } ?>
								value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php } ?>
						</select>
						<select class="input select settings__select" name="month">
							<option value="0">�����</option>
							<option value="1" <?php if ($this->users_options_birthday[1] == 1) { ?> selected <?php } ?> >������</option>
							<option value="2" <?php if ($this->users_options_birthday[1] == 2) { ?> selected <?php } ?>>�������</option>
							<option value="3" <?php if ($this->users_options_birthday[1] == 3) { ?> selected <?php } ?>>����</option>
							<option value="4" <?php if ($this->users_options_birthday[1] == 4) { ?> selected <?php } ?>>������</option>
							<option value="5" <?php if ($this->users_options_birthday[1] == 5) { ?> selected <?php } ?>>���</option>
							<option value="6" <?php if ($this->users_options_birthday[1] == 6) { ?> selected <?php } ?>>����</option>
							<option value="7" <?php if ($this->users_options_birthday[1] == 7) { ?> selected <?php } ?>>����</option>
							<option value="8" <?php if ($this->users_options_birthday[1] == 8) { ?> selected <?php } ?>>������</option>
							<option value="9" <?php if ($this->users_options_birthday[1] == 9) { ?> selected <?php } ?>>��������</option>
							<option value="10" <?php if ($this->users_options_birthday[1] == 10) { ?> selected <?php } ?>>�������</option>
							<option value="11" <?php if ($this->users_options_birthday[1] == 11) { ?> selected <?php } ?>>������</option>
							<option value="12" <?php if ($this->users_options_birthday[1] == 12) { ?> selected <?php } ?>>�������</option>
						</select>
						<select class="input select settings__select" name="year">
							<option value="0">���</option>
							<?php for ($i = 2005; $i >= 1900; $i--) { ?>
								<option <?php if ($this->users_options_birthday[0] == $i) { ?> selected <?php } ?>
								value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="settings__form-item">
					<div class="settings__form-header">�������:</div>
					<div class="settings__form-content">
						<input class="input settings__input" type="text" value="<?php echo $this->users_options['phone']; ?>" name="phone">
					</div>
				</div>
				<button class="button button--big settings__button" type="submit" name="save_options">���������</button>
			</div>
			<div id="settings_form-second" style="display: none;">
			
			</div>
			<div id="settings_form-third" style="display: none;">
			
			</div>
            
        </div>
    </div>
</form>