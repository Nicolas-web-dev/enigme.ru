<div class="section__header">
    <div class="breadcrumbs">
		<a class="breadcrumbs__item" href="/">�������</a>
		<a class="breadcrumbs__item breadcrumbs__item--active" href="/catalog/">������ ����������</a>
	</div>
    <h1 class="section__title"><?=$this->h1?></h1>
</div>

<div class="catalog">
<?php foreach ($this->brend_liter as $brend_liter) { ?>
	<?php if($brend_liter=='a' or $brend_liter=='f' or $brend_liter=='m') {?><ul class="catalog_list"><?php } ?>
	
	<li class="catalog__box"><span class="catalog__litera"><?php echo strtoupper($brend_liter); ?></span>
	<ul class="catalog__brands">
	<?php foreach ($this->brend_item as $brend_item) { ?>
		<?php if ($brend_liter == $brend_item['liter']) { ?>
			<li><a href="/catalog/<?php echo $brend_item['url']; ?>/">
			<?php if($brend_item['is_top']==1){ ?>
				<strong><?php echo $brend_item['title']; ?></strong>
			<?php }else{ ?>
				<?php echo $brend_item['title']; ?>
			<?php } ?>
			</a></li>
		<?php } ?>
	<?php } ?>
	</ul>
	</li>
	
	<?php if($brend_liter=='e' or $brend_liter=='l' or $brend_liter=='z') {?></ul><?php } ?>
<?php } ?>
</div>

<div class="clear"></div>
<br>

<div class="inf" style="font-size:12px;font-family: 'Rubik', 'Arial', sans-serif;margin-left:28px;padding: 20px 0 30px;">
    <p>
        <?=$this->meta_body?>
    </p>
</div>