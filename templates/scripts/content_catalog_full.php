<script type="text/javascript" src="/templates/js/add_to_cart.js"></script>
<div class="section__header">
    <div class="breadcrumbs">
		<a class="breadcrumbs__item" href="/">�������</a>
		<a class="breadcrumbs__item" href="/catalog/">������ ����������</a>
		<a class="breadcrumbs__item" href="/catalog/<?php echo $this->brend_url; ?>/"><?php echo $this->brend_title; ?></a>
		<a class="breadcrumbs__item breadcrumbs__item--active" href="#"><?php echo $this->catalog_full['title']; ?></a>
	</div>
</div>
<div class="product">
    <div class="section-wrapper">
        <div class="product__content">
            <div class="product__picture">
				<figure>
					<img id="product_cat_<?php echo $this->catalog_id; ?>" class="product__picture-img" src="<?php echo $this->catalog_full['img']; ?>" alt="<?php echo $this->brend_title; ?> <?php echo $this->catalog_full['title']; ?>" title="<?php echo $this->brend_title; ?> <?php echo $this->catalog_full['title']; ?>">
					<figcaption><?php echo $this->brend_title." "; ?><?php echo $this->catalog_full['title']; ?></figcaption>
				</figure>
				<p class="product__guarantee">100% �������� <span class="product__guarantee-comment">����������� ��������</span></p>
				<div class="product__code">��� ������: <span class="product__code-number"><?php echo $this->catalog_full['id']; ?></span></div>
            </div>
            <div class="product__info">
                <div class="product__top">
                    <div class="product__top-left">
						<?php $t_brend = $this->brend_title;
							if (strtolower($t_brend{0}) == 'd' || strtolower($t_brend{0}) == 'o') { ?>
								<h1 class="product__brand"><?php echo $this->brend_title; ?> <?php echo $this->catalog_full['title']; ?></h1>
							<?php }elseif(strtolower($t_brend{0}) == 'f' || strtolower($t_brend{0}) == 'h') { 
								if ($this->catalog_full['pol'] == '�������') {?>
									<h1 class="product__brand"><?php echo $this->catalog_full['title']; ?> ���� �������</h1>
								<?php } else { ?>
									<h1 class="product__brand"><?php echo $this->catalog_full['title']; ?> ���� �������</h1>
								<?php } ?>
							<?php }else { ?>
								<h1 class="product__brand"><?php echo $this->catalog_full['title']; ?></h1>
							<?php } ?>
                    </div>
					<?php if ($this->catalog_full['price_s'] > 0) { ?>
						<div class="product__cart"><a class="link_product_prices" href="#prices">������� � �������</a></div>
					<?php } else { ?>
						<div class="not_available_prod_card ">��� � �������</div>
					<?php } ?>
					<?php if (isset($this->user)) { ?>
						<div class="product__fav <?php if ($this->is_note) { echo 'active'; } ?>">
							<a id="<?php echo $this->catalog_full['id']; ?>" class="link_product_fav bloknot" href="#favorites"><?php if ($this->is_note) { echo '�&nbsp;���������'; } else {echo '�&nbsp;���������';} ?></a>
						</div>
					<?php } ?>
                </div>
                <div class="product__middle">
                    <div class="product__middle-left">
						<div class="product__category"><?php echo $this->catalog_full['pol']; ?> ������</div>
						<?php require_once('partials/sort_rate.php'); ?>
                    </div>
					<?php if (isset($this->catalog_item_tag)) { ?>
						<div class="notes product__notes">
							<?php foreach ($this->catalog_item_tag as $key => $catalog_item_tag) { ?>
								<a href="/tag/<?php echo $catalog_item_tag['url']; ?>/"><?php echo $catalog_item_tag['title']; ?></a><?php if (count($this->catalog_item_tag) != ((int)$key + 1)) { ?>,&nbsp;<?php } ?>
							<?php } ?>
						</div>
					<?php } ?>
                </div>
				<section>
				<?php if (strtolower($t_brend{0}) == 'd' || strtolower($t_brend{0}) == 'o') { ?>
					<h2 class="product__title prod_title_top"><?php echo $this->catalog_full['title']; ?></h2>	
				<?php }elseif(strtolower($t_brend{0}) == 'f' || strtolower($t_brend{0}) == 'h') { ?>
					<h2 class="product__title prod_title_top"><?php echo $this->catalog_full['title']; ?> <?php echo $this->brend_title; ?></h2>
				<?php }else { ?>
					<h2 class="product__title prod_title_top"><?php echo $this->brend_title; ?> <?php echo $this->catalog_full['title']; ?></h2>
				<?php } ?>
				<?php if ($this->catalog_full['text'] != '') { ?>
					<p class="product__dscr"><?php echo $this->catalog_full['text']; ?></p>
				<?php } ?>
				</section>
            </div>
        </div>
		<?php if (isset($this->catalog_item_order)) { ?>
			<div class="product__options">
				<?php foreach ($this->catalog_item_order as $catalog_item_order) { ?>
					<div class="product-option">
						<div class="product-option__name"> 
							<div class="product-option__name-content"><?php echo $catalog_item_order['type']; ?>
							<?php if ($catalog_item_order['t'] == 'TTW' OR $catalog_item_order['t'] == 'TT' OR $catalog_item_order['t'] == 'DT' OR $catalog_item_order['t'] == 'OT' OR $catalog_item_order['t'] == 'PT' OR $catalog_item_order['t'] == 'BLT') { ?>
								<div class="info_tester">
									<span>i</span>
									<div class="popup_tester">
										�������� ������� �������. �������� � ������� ����� ������� �� �� ���������� �� ����������� �����. ������������� � ������� ������������ ��������� ��������.
									</div>
								</div>
							<? } ?>
							</div>
						</div>
						<div class="product-option__right">
							<div class="product-option__price">
								<?php if ($catalog_item_order['is_spec']) { ?>
									<div class="hot-price"></div>
								<?php } ?>
								<?php if ($this->user['d_percent'] != 0 AND $catalog_item_order['is_block'] == 0) { ?>		
									<span class="product-option__price--old">
										<?php echo $catalog_item_order['price']; ?> ���
									</span>
									<span class="product-option__price--space">/</span> 
									<?php echo show_users_percent($catalog_item_order['price'], $this->user['d_percent']); ?> ���
								<?php } else { ?>
									<?php if ($catalog_item_order['is_block'] == 1) { ?>
										<span class="product-option__price--old">
											<?php echo $catalog_item_order['price_old']; ?> ���
										</span>
										<span class="product-option__price--space">/</span>
										<?php echo $catalog_item_order['price']; ?> ���
									<?php } else { ?>
												<?php if ($this->sell) { ?>
													<span class="product-option__price--old">
														<?php echo round($catalog_item_order['price'] + ($catalog_item_order['price'] * $this->sell)); ?> ���
													</span>
												<?php } ?>
												<?php echo $catalog_item_order['price']; ?> ���
											<?php } ?>
								<?php } ?>	
							</div>
							<a class="add-bask_prod button_prod add_to_cart" data-id_product="<?php echo $catalog_item_order['id']; ?>" data-id_catalog="<?php echo $this->catalog_id; ?>" href="">� �������</a>
							<a onclick="quick_buy_click_product(<?php echo $catalog_item_order['id']; ?>);return false;" class="item__buy_prod button_prod">������ � 1 ����</a>
						</div>
					</div>
                <?php } ?>
                 
			</div>
		<?php } ?>
		<div style="float: right; padding: 1rem;">
            <script type="text/javascript" src="//yastatic.net/share2/share.js" async="async" charset="utf-8"></script>
            <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter"
                 data-counter></div>

        </div>
		<section class="note">
			<?php echo $this->meta_body; ?>
		</section>
    </div>
</div>
<div class="delivery-method">
    <div class="section-wrapper">
            <p class="delivery-method__header">��������</p>
        <div class="delivery-method__content">
            <div class="delivery-method__item delivery-method__item--moscow">
                <div class="delivery-method__item-header">������</div>
                <div class="delivery-method__item-price">���������</div><a class="delivery-method__item-link" href="/dostavka/">���������</a>
            </div>
            <div class="delivery-method__item delivery-method__item">
                <div class="delivery-method__item-header">���������� �������</div>
                <div class="delivery-method__item-price">�� 250 ���</div><a class="delivery-method__item-link" href="/dostavka/">���������� ���������</a>
            </div>
            <div class="delivery-method__item delivery-method__item">
                <div class="delivery-method__item-header">�����-���������</div>
                <div class="delivery-method__item-price">�� 118 ���</div><a class="delivery-method__item-link" href="/dostavka/">���������� ���������</a>
            </div>
            <div class="delivery-method__item delivery-method__item">
                <div class="delivery-method__item-header">������</div>
                <div class="delivery-method__item-price">�� 118 ���</div><a class="delivery-method__item-link" href="/dostavka/">���������� ���������</a>
            </div>
        </div>
    </div>
</div>
<div class="discounts">
    <div class="discounts__header">
        <p class="discounts__title">������</p>
        <div class="discounts__title-comment">���������� ��������</div>
        <p class="discounts__dscr">��������� ����������, ��� ������ ���������� �� ����� Enigme.ru, �� ��������� ������!</p>
    </div>
    <div class="discounts__content">
        <p class="discounts__content-header">������ ��� ���������� ���� �������</p>
        <div class="discounts__item">
            <div class="discounts__item-percent">3%</div>
            <div class="discounts__item-price">3000 - 10000 ���</div>
        </div>
        <div class="discounts__item">
            <div class="discounts__item-percent">5%</div>
            <div class="discounts__item-price">10001 - 20000 ���</div>
        </div>
        <div class="discounts__item">
            <div class="discounts__item-percent">7%</div>
            <div class="discounts__item-price">20001 - 70000 ���</div>
        </div>
        <div class="discounts__item">
            <div class="discounts__item-percent">10%</div>
            <div class="discounts__item-price">70 001 - 139999 ���</div>
        </div>
        <div class="discounts__item">
            <div class="discounts__item-percent">15%</div>
            <div class="discounts__item-price">�� 140000 ���</div>
        </div>
    </div>
</div>
<div class="product__catalog section-wrapper">
    <p class="product__catalog-header">������ ������� ������</p>
    <div class="catalog">
		<?php foreach ($this->catalog_item_br as $catalog_item) { 
			$catalog_item['price_s'] = $catalog_item['price'];
			$catalog_item['brend_title'] = $this->brend_title;
			include("content_catalog_item.php");
		} ?>
    </div>
</div>
<input id="global_geo_segment" type="hidden" data-geo_segment="<?php echo $GLOBALS["user_segment"]; ?>">