<div class="item_prod">
	<a class="item_prod__picture-link" href="<?php echo $catalog_item['url']; ?>#aromat">
		<img class="item_prod__picture" src="<?php echo $catalog_item['img']; ?>" title="<?php echo $catalog_item['brend_title']; ?> <?php echo $catalog_item['title']; ?>" alt="<?php echo $catalog_item['brend_title']; ?> <?php echo $catalog_item['title']; ?>">
		<?php if($catalog_item['is_spec']) { ?>
			<div class="hot-price"></div>
		<?php } ?>
	</a>
	<a class="item_prod__title" href="<?php echo $catalog_item['url']; ?>#aromat">
        <div class="item_prod__brand"><?php echo $catalog_item['brend_title']; ?></div>
        <div class="item_prod__name"><?php echo $catalog_item['title']; ?></div>
	</a>
	<div class="item_prod__price">
		<?php if ($catalog_item['price_s'] > 0) { ?>
			<div class="item_prod__price-from">�� <?php echo $catalog_item['price_s']; ?></div>
			<div class="item_prod__price-to">&nbsp;�� <?php echo $catalog_item['price_e']; ?> ���.</div>
		<?php } else { ?>
			<div class="item_prod__price-to">��� � �������</div>
		<?php } ?>
	</div>
	<?php if ($catalog_item['price_s'] > 0) { ?>
	<div class="item_prod__footer">
		<a onclick="quick_buy_click(<?php echo $catalog_item['id']; ?>);return false;" class="item__buy_prod button_prod">������ � 1 ����</a>
		<a href="<?php echo $catalog_item['url']; ?>#aromat" class="add-bask_prod button_prod">
            	� �������
        </a>
	</div>
	<?php } ?>
</div>