<div id="quick_order" class="one-click-modal" data-id_product="" style="display: none;">
    <div class="close-modal"></div>
    <div class="one-click-thumb">
        <img src="/images/uploads/catalog/" alt="���� ������">
    </div>
    <div class="one-click-descr">
        <p>������� � ���� ����</p>
        <div class="one-click-info">
            ��� �������� �������� � ���� ��� �� �������� � �������� ��� �����
        </div>
        <div class="one-click-made">
            <b id="prod_brend_title">Christian Dior Homme Sport</b>
            <span id="prod_aroma">������� ������</span>
        </div>
        <div class="one-click-list">
            <ul>
                <li id="prod_aroma_type">���: ��������� ����</li>
                <li id="prod_v">�����: 50 ��</li>
                <li id="prod_id">��� ������: 250</li>
            </ul>
        </div>
        <div class="one-click-form">
            <form>
                <div class="one-click-input">
                    <input type="text" name="quick_buy_phone" placeholder="������� ��� ����� ��������">
                </div>
                <div class="one-click-bt">
                    <input id="quick_order_button" type="button" value="������ � 1 ����">
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
function quick_buy_click(id_catalog) {
        $.ajax({
            method: "POST",
			url: "/ajax-requests/?ajax_quick_buy=1",
			data: "quick_buy_id_catalog=" + id_catalog
        })
		.done(function (msg) {
			var res_data = eval("(" + msg + ")");
			if (Number(res_data.id_product) > 0) {
				//$('#fast_view').hide();
                //$('#overlay').remove();
				var fv_wrap = $('#quick_order');
				var fv_close = fv_wrap.find('.close-modal');
				var fv_input_phone = fv_wrap.find("input[name='quick_buy_phone']");
				fv_input_phone.val('');
				$('#prod_brend_title').text(res_data.brend_title + " " + res_data.title);
				$('#quick_order').attr("data-id_product", res_data.id_product);
				$('#prod_aroma').text(res_data.aroma);
				$('#prod_aroma_type').text("���: " + res_data.aroma_type);
				$('#prod_v').text("�����: " + res_data.v);
				$('#prod_id').text("��� ������: " + res_data.id_product);
				$('.one-click-thumb img').attr("src", res_data.img);
			
				fv_wrap.show();
				$('body').append($('<div/>').attr('id', 'overlay'));
				fv_wrap.css('margin-top', 0);
				fv_close.click(function () {
					$('#quick_order').hide();
					$('#overlay').remove();
				});
				$('#overlay').click(function () {
					$('#quick_order').hide();
					$(this).remove();
				});
			}
            //yaCounter21451555.reachGoal('onclick_katalog_er');
		});
}

function quick_buy_click_product(id_product) {
	if (Number(id_product) > 0) {
		$.ajax({
            method: "POST",
			url: "/ajax-requests/?ajax_quick_buy_product=1",
			data: "quick_buy_id_product=" + id_product
        })
		.done(function (msg) {
			var res_data = eval("(" + msg + ")");
		
				//$('#fast_view').hide();
                //$('#overlay').remove();
				var fv_wrap = $('#quick_order');
				var fv_close = fv_wrap.find('.close-modal');
				var fv_input_phone = fv_wrap.find("input[name='quick_buy_phone']");
				fv_input_phone.val('');
				$('#prod_brend_title').text(res_data.brend_title + " " + res_data.title);
				$('#quick_order').attr("data-id_product", id_product);
				$('#prod_aroma').text(res_data.aroma);
				$('#prod_aroma_type').text("���: " + res_data.aroma_type);
				$('#prod_v').text("�����: " + res_data.v);
				$('#prod_id').text("��� ������: " + id_product);
				$('.one-click-thumb img').attr("src", res_data.img);
			
				fv_wrap.show();
				$('body').append($('<div/>').attr('id', 'overlay'));
				fv_wrap.css('margin-top', 0);
				fv_close.click(function () {
					$('#quick_order').hide();
					$('#overlay').remove();
				});
				$('#overlay').click(function () {
					$('#quick_order').hide();
					$(this).remove();
				});
			
            //yaCounter21451555.reachGoal('onclick_katalog_er');
		});
		/*
		var fv_wrap = $('#quick_order');
				var fv_close = fv_wrap.find('.close-modal');
				var fv_input_phone = fv_wrap.find("input[name='quick_buy_phone']");
				fv_input_phone.val('');
				str = $(this).text();
				alert(str);
				$('#prod_brend_title').text($('.product__brand').text() + " " + $('.product__title').text());
				$('#quick_order').attr("data-id_product", id_product);
				$('#prod_aroma').text($('.product__category').text());
				$('#prod_aroma_type').text("���: " + str.substr(1, str.indexOf('1 2 3 4 5 6 7 8 9 0')));
				$('#prod_v').text("�����: " );
				$('#prod_id').text("��� ������: " + id_product);
				$('.one-click-thumb img').attr("src", $('.product__picture-img').attr("src"));
			
				fv_wrap.show();
				$('body').append($('<div/>').attr('id', 'overlay'));
				fv_wrap.css('margin-top', 0);
				fv_close.click(function () {
					$('#quick_order').hide();
					$('#overlay').remove();
				});
				$('#overlay').click(function () {
					$('#quick_order').hide();
					$(this).remove();
				});
				*/
	}
}

$(document).ready(function() {
	$('#quick_order_button').click(function () {
		if ($("input[name='quick_buy_phone']").val() != '') {
			$.ajax({
				method: "POST",
				url: "/ajax-requests/?ajax_quick_buy_submit=1",
				data: "quick_buy_id_product=" + Number($('#quick_order').attr("data-id_product")) + "&quick_buy_phone=" + $("input[name='quick_buy_phone']").val()
			})
			.done(function (msg) {
				$('#quick_order').hide();
				$('#overlay').remove();
				location.replace("/thankyou_quickorder1/");
				
				//yaCounter21451555.reachGoal('quick_buy_tovar_success');
			});
		}
	});
	$("input[name='quick_buy_phone']").mask("7 (999) 999-99-99");
});
</script>