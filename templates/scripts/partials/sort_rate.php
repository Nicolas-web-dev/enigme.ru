<?php
$rate = $this->catalog_full['rate'];
for($i=1;$i<=5;$i++){
    if ($rate>=$i){
        $stars[$i] = 'stars__item--full';
    }elseif (ceil($rate)==$i){
        $stars[$i] = 'stars__item--half';
    }else{
        $stars[$i] = 'stars__item--empty';
    }
}
?>

<div class="product__rating">
	<div class="stars">
		<?php foreach ($stars as $key=>$class) { ?>
			<div class="stars__item <?php echo $class; ?>"></div>
		<?php } ?>						
	</div>
</div>