<div class="recommendations">
    <p class="recommendations__header">�����������</p>
    <div class="recommendations__list">
		<?php if ($this->linking) { ?>
			<?php foreach($this->linking as $key=>$link) { ?>
				<a class="recommendations__item" href="<?php echo $link['to']; ?>">
					<div class="recommendations__item-picture">
						<img class="recommendations__item-img" src="<?php echo $link['preview']; ?>" title=" <?php echo $link['ancor']; ?>" alt="<?php echo $link['ancor']; ?>">
					</div>
					<div class="recommendations__item-content">
					<!--
						<div class="recommendations__item-brand">NO DATA</div>
					-->
						<div class="recommendations__item-name"><?php echo $link['ancor']; ?></div>
					</div>
				</a>
			<?php } ?>
		<?php } ?>
	</div>
</div>