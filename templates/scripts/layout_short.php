<?php require_once(__DIR__ . '/../../includes/geo_domain.php'); ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<script>
		_udn = ".enigme.ru";
		</script>
		<meta charset="WINDOWS-1251">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title><?php echo geo_replace($city, $this->main['meta_title']); ?></title>
		<meta name="description" content="<?php echo geo_replace($city, $this->main['meta_description']); ?>" />
		<meta name="keywords" content="<?php echo geo_replace($city, $this->main['meta_keywords']); ?>" />
		<?php if ($this->main['meta_noindex']==true) echo '<meta name="robots" content="noindex"/>'; ?>
		<?php if ($this->main['og:title']!='') echo '<meta property="og:title" content="'.geo_replace($city, $this->main['og:title']).'" /> '; ?>
		<?php if ($this->main['og:image']!='') echo '<meta property="og:image" content="'.$this->main['og:image'].'" /> '; ?>
		<?php if ($this->main['og:description']!='') echo '<meta property="og:description" content="'.geo_replace($city, $this->main['og:description']).'" /> '; ?>
		<?php if ($this->main['og:url']!='') echo '<meta property="og:url" content="http://' . $host . $this->main['og:url'].'" /> '; ?>

		<meta name='yandex-verification' content='6e903acfbfccb637' />
		<meta name='e96a84d9564629a79ad5072edf2d4b6f' content=''>
		<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
		<meta name="google-site-verification" content="L4HRi6k9b9NEITAlU4iI4tYExUfLLY9RR5ZRbVURHsw" />
		<meta name="robots" content="noyaca"/>
		<meta name='wmail-verification' content='72d530550fbb2ed9' />
		<meta name="mailru-verification" content="c59e8c8032401dea" />
		<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" href="/templates/css/style.css">
		<link href="/templates/css/suggestions.css" type="text/css" rel="stylesheet" />
		<link rel="stylesheet" href="/templates/js/jquery.scrollbar/jquery.scrollbar.css" />
		<meta name="viewport" content="width=device-width, target-densitydpi=device-dpi, initial-scale=1.0, minimum-scale=0.75, maximum-scale=4.0, user-scalable=yes">
		<meta name="format-detection" content="telephone=no">
		<script type="text/javascript" src="/js/jquery/jquery-1.11.0.min.js?v=1"></script>
		<script>
			$(function(){
			$('.brands-list_scroll').scrollbar({
				'ignoreMobile': false,
				'ignoreOverlay': false
				});
			})
		</script>
		<script>
			var region_name = '<?=convert_region($city['region']);?>';
			var city_name = '<?=convert_city($city['name']);?>';
		</script>
		<script type="text/javascript">
			VK.init({apiId: 2024589, onlyWidgets: true});
		</script>
		<script type="text/javascript" src="http://apis.google.com/js/plusone.js" async defer></script>
		<script type="text/javascript">
			function ct(w,d,e,c){
			var a='all',b='tou',src=b+'c'+'h';src='m'+'o'+'d.c'+a+src;
			var jsHost="https://"+src,s=d.createElement(e),p=d.getElementsByTagName(e)[0];
			s.async=1;s.src=jsHost+"."+"r"+"u/d_client.js?param;"+(c?"client_id"+c+";":"")+"ref"+escape(d.referrer)+";url"+escape(d.URL)+";cook"+escape(d.cookie)+";";
			if(!w.jQuery){var jq=d.createElement(e);
			jq.src=jsHost+"."+"r"+'u/js/jquery-1.7.min.js';
			jq.onload=function(){
			p.parentNode.insertBefore(s,p);};
			p.parentNode.insertBefore(jq,p);}else{
			p.parentNode.insertBefore(s,p);}}
			if(!!window.GoogleAnalyticsObject){window[window.GoogleAnalyticsObject](function(tracker){ 
			if (!!window[window.GoogleAnalyticsObject].getAll()[0])
			{ct(window,document,'script', window[window.GoogleAnalyticsObject].getAll()[0].get('clientId'))}
			else{ct(window,document,'script', null);}});
			}else{ct(window,document,'script', null);}
		</script>
		<script type="text/javascript" src="/templates/js/content.js?v=2"></script>
		<script type="text/javascript" src="/templates/js/dadata.js?v=2"></script>
		<script type="text/javascript" src="/templates/js/comments.js?v=2"></script>
		<script type="text/javascript" src="/templates/js/suggest.js?v=2"></script>
		<script type="text/javascript" src="/templates/js/swfobject.js?v=2"></script>
		<script type="text/javascript" src="/templates/js/combo.js?v=2"></script>
		<script type="text/javascript" src="/templates/lib/star-rating/jquery.rating.pack.js?v=2"></script>
		<script src="/templates/js/bootstrap-select/bootstrap-select.min.js"></script>
		<script src="/templates/css/bootstrap/bootstrap.min.js"></script>
	</head>

	<body id="rebrand" style="overflow-x:hidden;">
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TR7T5K"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TR7T5K');</script>
	<!-- End Google Tag Manager -->
	<div class="wrapper">
		<header class="header">
				<a class="logo header__logo" href="/">
					<div class="logo__image"></div>
					<div class="logo__content">
						<div class="logo__title">Enigme</div>
						<div class="logo__dscr">��������-������� ������� ����������</div>
					</div>
				</a>
				<div class="phones header__phones">
					<!--
					<div class="phones__content">
						<a class="phones__content-item" href="tel:8(499)390-88-76">8 (499) 390-88-76</a>
						<a class="phones__content-item" href="tel:8(499)390-88-76">8 (812) 407-12-05</a>
						<a class="phones__content-item" href="tel:8(499)390-88-76">8 (800) 555-86-17</a>
					</div>
					<div class="phones__headers">
						<div class="phones__header-item">������� � ������</div>
						<div class="phones__header-item">������� � �����-����������</div>
						<div class="phones__header-item">��������� �� ������</div>
					</div>
					-->
					<div class="phones__content">
						<a class="phones__content-item" href="tel:88005558617"><?=$city['phone']; ?></a>
					</div>
					<div class="phones__headers">
						<div class="phones__header-item">������� � <?php echo $city['soname']; ?></div>
						<div class="phones__header-item">��������� �� ������</div>
					</div>
				</div>
				<?php if (!$this->is_login) { ?>
				<div class="authorization__header_block">
					<div class="authorization header__authorization">
						<div class="authorization__top">
							<a class="authorization__top-link modal-link" href="#login">����</a>
							<a class="authorization__top-link" href="/users/register/">�����������</a>
						</div>
						<form action="/users/login/" method="post" enctype="multipart/form-data" name="reg_form_top" autocomplete="off">
							<input class="authorization__input" type="text" name="log_email">
							<input class="authorization__input" type="password" name="log_passw">
							<div class="authorization__bottom">
								<a class="authorization__lost modal-link" href="#restore">������ ������?</a>
								<button class="button button--small" type="submit" name="register">�����</button>
							</div>
						</form>
					</div>
				</div>
				<?php } else { ?>
				<div class="authorization__header_block">
					<div class="authorization header__authorization top_menu_personal">
						<span>����� ����������!</span>
						<span>
							<?php echo $this->user['email']; ?>
						</span>
						<div style="height:0.3rem"></div>
						<a href="/orders/">���� ������� (<?=$this->cart->getCount()?>)</a><br />
						<a href="/users/options/">���������</a><br />
						<a href="/users/exit/">�����</a>
						<a href="/users/" class="more_top">���...</a>
					</div>
				</div>
				<?php } ?>
			</header>
			<nav class="menu">
				<a class="menu__item" href="/discount/">�����</a>
				<a class="menu__item" href="/guarantees/">�������� � �����������</a>
				<a class="menu__item" href="/refund/">�������</a>
				<a class="menu__item" href="/dostavka/">��������</a>
				<a class="menu__item" href="/about/">��������</a>
				<a class="menu__item" href="/faq/">������-�����</a>
			</nav>
			<div class="content">
				<div class="section <?php if($this->canon_name1 == 'orders') { echo 'section--full'; }?>">
					<div class="section__top">
						<div class="alphabet">
							<div class="alphabet__header">�������� ���� �� ��������</div>
							<div class="alphabet__content">
								<a class="alphabet__letter" href="/catalog/A/">A</a>
								<a class="alphabet__letter" href="/catalog/B/">B</a>
								<a class="alphabet__letter" href="/catalog/C/">C</a>
								<a class="alphabet__letter" href="/catalog/D/">D</a>
								<a class="alphabet__letter" href="/catalog/E/">E</a>
								<a class="alphabet__letter" href="/catalog/F/">F</a>
								<a class="alphabet__letter" href="/catalog/G/">G</a>
								<a class="alphabet__letter" href="/catalog/H/">H</a>
								<a class="alphabet__letter" href="/catalog/I/">I</a>
								<a class="alphabet__letter" href="/catalog/J/">J</a>
								<a class="alphabet__letter" href="/catalog/K/">K</a>
								<a class="alphabet__letter" href="/catalog/L/">L</a>
								<a class="alphabet__letter" href="/catalog/M/">M</a>
								<a class="alphabet__letter" href="/catalog/N/">N</a>
								<a class="alphabet__letter" href="/catalog/O/">O</a>
								<a class="alphabet__letter" href="/catalog/P/">P</a>
								<a class="alphabet__letter" href="/catalog/Q/">Q</a>
								<a class="alphabet__letter" href="/catalog/R/">R</a>
								<a class="alphabet__letter" href="/catalog/S/">S</a>
								<a class="alphabet__letter" href="/catalog/T/">T</a>
								<a class="alphabet__letter" href="/catalog/U/">U</a>
								<a class="alphabet__letter" href="/catalog/V/">V</a>
								<a class="alphabet__letter" href="/catalog/W/">W</a>
								<a class="alphabet__letter" href="/catalog/X/">X</a>
								<a class="alphabet__letter" href="/catalog/Y/">Y</a>
								<a class="alphabet__letter" href="/catalog/Z/">Z</a>
							</div>
						</div>
						<form class="search" name="form" enctype="multipart/form-data" action="/find/" method="post">
							<div class="search__header">����� �������</div>
							<div class="search__content">
								<input class="search__input" type="text" name="find">
								<button class="button button--small search__button" name="find_ok">������</button>
							</div>
						</form>
					</div>
					<?= $this->main['content']; ?>
				</div>
			</div>
		<?php if (($this->canon_name1 != 'discount') && ($this->canon_name1 != 'users') && ($this->canon_name1 != 'orders')) { ?>
			<article class="note">
				<section>
				<?php if (($this->canon_name1 == 'catalog') && ($this->canon_name2 != '') && ($this->canon_name3 != '')) { ?>
					����� ������ �� ������ ������� ����� ������� ��� �� �������� � <?php echo $city['soname']; ?> <?php echo $city['phone']; ?>
				<?php } else { ?> 
					��������-������� ���������� Enigme.ru ����� 9 ��� ����� ����������� �������! �� �������� ������� ������� �������, ������� ������ �� ������� � ������� ����������� � �����������!
				<?php } ?>
				</section>
			</article>
		<?php } ?>
		<?php if (($city['domain'] === "msk") && ($this->canon_name1 != 'orders')) { ?>
			<div class="delivery-block">
				<p class="delivery-block__header">������� �������� � ������ ������:</p>
				<div class="delivery-block__content">������, �����������, ���������, �������, ��������, ������������, ������, ������� ��������, �����������, ��������, ���������, �������, �������, �����-�������, ������������, �������, ������, ������-���, �������, ������, �����������, ������, ��������, �����, ��������, ���������, ����������, ������, �����, �������-���������, ������, ������������, ������, ���������� �����, �������������, ������ ��������, ������ �����, �����������, ������������, �����������, ��������, ����, ���, ��������, �����, �����, ������������, ������-��-����, ������, ������, �����-���������, �������, �������, ��������, , ����, ����������, ������ �����, �����������, ������, ���������, ������, �����, ��������, �����, ����, ������, ����-���, ���������, ���, ���������, ���������, ���������, ����, ������, ���������.</div>
			</div>
		<?php } ?>
		<div class="sitemap">
				<div class="sitemap__item">
					<h3 class="sitemap__header">�������� �� ����</h3>
					<div class="sitemap__links">
						<a class="sitemap__link" rel="nofollow" href="http://vk.com/enigme_parfum" target="_blank">���������</a>
						<a class="sitemap__link" rel="nofollow" href="http://www.facebook.com/enigme.store" target="_blank">Facebook</a>
						<a class="sitemap__link" rel="nofollow" href="https://twitter.com/enigme_ru" target="_blank">Twitter</a>
						<a class="sitemap__link" rel="nofollow publisher" href="https://plus.google.com/100419777799689206628" target="_blank">Google+</a>
					</div>
				</div>
				<div class="sitemap__item">
					<h3 class="sitemap__header">������</h3>
					<div class="sitemap__links">
						<a class="sitemap__link" href="/dostavka/">��������</a>
						<a class="sitemap__link" href="/about/">��������</a>
						<a class="sitemap__link" href="/oferta/">������� ������</a>
						<a class="sitemap__link" href="/guarantees/">�������� � �����������</a>
						<a class="sitemap__link" href="/refund/">�������</a>
					</div>
				</div>
				<div class="sitemap__item">
					<h3 class="sitemap__header">�������</h3>
					<div class="sitemap__links">
						<a class="sitemap__link" href="/">�������</a>
						<a class="sitemap__link" href="/catalog/">��� ������</a>
						<a class="sitemap__link" href="/sale/">����������</a>
					</div>
				</div>
				<div class="sitemap__item">
					<h3 class="sitemap__header">����������</h3>
					<div class="sitemap__links">
						<a class="sitemap__link" href="/discount/">�����</a>
						<a class="sitemap__link" href="/must-have/">Must Have</a>
					</div>
				</div>
		</div>
		<footer class="footer">
				<div class="footer__left">
					<div class="siteinfo">
						<div class="siteinfo__logo"></div>
						<div class="siteinfo__content">� 2014-2016<br>ENIGME.RU<br>��������-������� ����������</div>
					</div>
					<div class="copyright">��� ����� �� ��������� �������������� �� ����� ����������� enigme.ru. ����������� ���������� ���������. (������.��)</div>
				</div>
				<!--
				<a class="developer" href="#">
					<div class="developer__content">���������� ����� <br>KAN AGENCY</div>
					<div class="developer__icon"></div>
				</a>
				-->
				<div class="page-buttons"><a class="callback-button" href="tel:88005558617"></a><a class="go-top page-buttons__go-top" rel="nofollow" href="#top"></a>
				</div>
				<form class="login modal mfp-hide" id="login" action="/users/login/" enctype="multipart/form-data" name="reg_form_top" method="post">
					<div class="mfp-close"></div>
					<p class="modal__header">�����������</p>
					<div class="input-item modal__input-item">
						<div class="input-item__header input-item__header--required">Email:</div>
						<input class="input modal__input" type="text" name="log_email">
					</div>
					<div class="input-item modal__input-item">
						<div class="input-item__header input-item__header--required">������:</div>
						<input class="input modal__input" type="password" name="log_passw">
					</div><a class="login__lost modal-link" href="#restore">������ ������?</a>
					<button class="button button--big modal__button" type="submit" name="register">�����</button>
				</form>
				<form class="restore modal mfp-hide" id="restore" action="/users/forgot-password/" enctype="multipart/form-data" method="post" name="forgot_form">
					<div class="mfp-close"></div>
					<p class="modal__header">�������������� ������</p>
					<div class="tabs restore__tabs">
						<a id="tab_modal_rest_email" class="tab restore__tab tab--active" href="#rest_email">������ email</a>
						<!--
						<a id="tab_rest_telephone" class="tab restore__tab" href="#rest_tel">������ �������</a>
						-->
					</div>
					<div id="modal_rest_email" class="input-item modal__input-item" style="display: block;">
						<div class="input-item__header input-item__header--required">Email:</div>
						<input class="input modal__input" type="text" name="forgot_email">
					</div>
					<button class="button button--big modal__button" name="send_passw" type="submit">�������</button>
				</form>
				<div class="orders-login modal mfp-hide" id="orders_login">
					<div class="mfp-close"></div>
					<p class="modal__header">���������� ����� ������ �������</p>
					<div class="tabs restore__tabs">
						<a id="tab_modal_orders_login_email" class="tab restore__tab tab--active" href="#rest_email">������ email</a>
						<a id="tab_modal_orders_login_telephone" class="tab restore__tab" href="#rest_tel">������ �������</a>
					</div>
					<div id="modal_orders_login_email" class="input-item modal__input-item" style="display: block;">
						<form id="login_form" action="/users/login/" method="post" name="reg_form">
							<div class="input-item__header input-item__header--required">��� ���������� ������ ������� ��� E-mail:</div>
							<input class="input modal__input" type="text" name="log_email">
							<button class="button button--big modal__button" name="register" type="submit">�������� ����� ����� ������ �������</button>
						</form>
					</div>
					<div id="modal_orders_login_telephone" class="input-item modal__input-item" style="display: none;">
						<form id="login_form_phone" action="/users/login/" method="post" name="reg_form_phone">
							<div class="input-item__header input-item__header--required">��� ���������� ������ ������� ��� �������:</div>
							<input class="input modal__input" type="text" name="phone">
							<button class="button button--big modal__button" name="register" type="submit">�������� ����� ����� ������ �������</button>
						</form>
					</div>
				</div>
		</footer>
		
	</div>
	<script type="text/javascript" src="/templates/js/jquery.min.js"></script>
	<script type="text/javascript" src="/templates/js/plugins.js"></script>
	<script type="text/javascript" src="/templates/js/jquery.maskedinput.min.js"></script>
	<script type="text/javascript" src="/templates/js/script.js"></script>
	<script src="/templates/js/jquery.scrollbar/jquery.scrollbar.js"></script>
	<script type="text/javascript" src="/templates/js/dadata/suggestions-jquery-4.1.min.js?v=1"></script>
	<script type="text/javascript" src="/templates/js/dadata.js?v=2"></script>
	<?php if($this->canon_name2 != 'thankyou_basket1') { ?>
		<script type="text/javascript" src="/templates/js/suggest_delivery.js"></script>
	<?php } ?>
	<!--
	<script type="text/javascript" src="/templates/js/suggest.js?v=2"></script>
	-->
	<!--BEGIN JIVOSITE CODE {literal} -->
	<script type='text/javascript'>
	if (screen.width > 480) {
	(function(){
		var widget_id = 'Rp0o7gSVp3';
		var s =
		document.createElement('script'); s.type = 'text/javascript'; s.async = true;
		s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss =
		document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s,
		ss);})();
	}
	</script>
	<!-- {/literal} END JIVOSITE CODE -->
	<!--LiveInternet counter--><script type="text/javascript">
					document.write("<a href='http://www.liveinternet.ru/click' "+
						"target=_blank><img src='http://counter.yadro.ru/hit?t26.2;r"+
						escape(document.referrer)+((typeof(screen)=="undefined")?"":
						";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
							screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
						";"+Math.random()+
						"' alt='' title='LiveInternet: �������� ����� ����������� ��"+
						" �������' "+
						"border='0' width='88' height='15'><\/a>")
				</script><!--/LiveInternet-->
	
<?php if ($city['domain'] === 'msk') { ?> 

<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter21951622 = new Ya.Metrika({ id:21951622, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/21951622" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

<?php } else { ?>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter42164999 = new Ya.Metrika({
                    id:42164999,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/42164999" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<?php } ?>
	<?php include('modals.php'); ?>
	<?php include('_counters.php'); ?>
	</body>
</html>