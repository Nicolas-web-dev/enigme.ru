<?php require_once(__DIR__ . '/../../includes/geo_domain.php'); ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<script>
        _udn = ".enigme.ru";
		</script>
		<meta charset="WINDOWS-1251">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title>404</title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name='yandex-verification' content='6e903acfbfccb637' />
		<meta name='e96a84d9564629a79ad5072edf2d4b6f' content=''>
		<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
		<meta name="google-site-verification" content="L4HRi6k9b9NEITAlU4iI4tYExUfLLY9RR5ZRbVURHsw" />
		<meta name="robots" content="noyaca"/>
		<meta name='wmail-verification' content='72d530550fbb2ed9' />
		<meta name="mailru-verification" content="c59e8c8032401dea" />
		<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" href="/templates/css/style.css">
		<meta name="viewport" content="width=device-width, target-densitydpi=device-dpi, initial-scale=1.0, minimum-scale=0.75, maximum-scale=4.0, user-scalable=yes">
		<meta name="format-detection" content="telephone=no">
		<script type="text/javascript" src="/js/jquery/jquery-1.11.0.min.js?v=1"></script>
		<script type="text/javascript">
			function ct(w,d,e,c){
			var a='all',b='tou',src=b+'c'+'h';src='m'+'o'+'d.c'+a+src;
			var jsHost="https://"+src,s=d.createElement(e),p=d.getElementsByTagName(e)[0];
			s.async=1;s.src=jsHost+"."+"r"+"u/d_client.js?param;"+(c?"client_id"+c+";":"")+"ref"+escape(d.referrer)+";url"+escape(d.URL)+";cook"+escape(d.cookie)+";";
			if(!w.jQuery){var jq=d.createElement(e);
			jq.src=jsHost+"."+"r"+'u/js/jquery-1.7.min.js';
			jq.onload=function(){
			p.parentNode.insertBefore(s,p);};
			p.parentNode.insertBefore(jq,p);}else{
			p.parentNode.insertBefore(s,p);}}
			if(!!window.GoogleAnalyticsObject){window[window.GoogleAnalyticsObject](function(tracker){ 
			if (!!window[window.GoogleAnalyticsObject].getAll()[0])
			{ct(window,document,'script', window[window.GoogleAnalyticsObject].getAll()[0].get('clientId'))}
			else{ct(window,document,'script', null);}});
			}else{ct(window,document,'script', null);}
		</script>
	</head>
	<body>
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TR7T5K"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TR7T5K');</script>
	<!-- End Google Tag Manager -->
		<div class="wrapper">
			<header class="header">
				<a class="logo header__logo" href="/">
					<div class="logo__image"></div>
					<div class="logo__content">
						<div class="logo__title">Enigme</div>
						<div class="logo__dscr">��������-������� ������� ����������</div>
					</div>
				</a>
				<div class="phones header__phones">
					<div class="phones__content">
						<a class="phones__content-item" href="tel:88005558617"><?=$city['phone']; ?></a>
					</div>
					<div class="phones__headers">
						<div class="phones__header-item">������� � <?php echo $city['soname']; ?></div>
						<div class="phones__header-item">��������� �� ������</div>
					</div>
				</div>
				<?php if (!$GLOBALS['is_login']) { ?>
				<div class="authorization__header_block">
					<div class="authorization header__authorization">
						<div class="authorization__top">
							<a class="authorization__top-link modal-link" href="#login">����</a>
							<a class="authorization__top-link" href="/users/register/">�����������</a>
						</div>
						<form action="/users/login/" method="post" enctype="multipart/form-data" name="reg_form_top" autocomplete="off">
							<input class="authorization__input" type="text" name="log_email">
							<input class="authorization__input" type="password" name="log_passw">
							<div class="authorization__bottom">
								<a class="authorization__lost modal-link" href="#restore">������ ������?</a>
								<button class="button button--small" type="submit" name="register">�����</button>
							</div>
						</form>
					</div>
				</div>
				<?php } else { ?>
				<div class="authorization__header_block">
					<div class="authorization header__authorization top_menu_personal">
						<span>����� ����������!</span>
						<span>
							<?php echo $GLOBALS['user_email']; ?>
						</span>
						<div style="height:0.3rem"></div>
						<a href="/orders/">���� ������� (<?=$GLOBALS['count_prod']?>)</a><br />
						<a href="/users/options/">���������</a><br />
						<a href="/users/exit/">�����</a>
						<a href="/users/" class="more_top">���...</a>
					</div>
				</div>
				<?php } ?>
			</header>
			<div class="header__menu-top">
				<nav class="menu menu--main">
					<a class="menu__item" href="/discount/">�����</a>
					<a class="menu__item" href="/guarantees/">�������� � �����������</a>
					<a class="menu__item" href="/refund/">�������</a>
					<a class="menu__item" href="/dostavka/">��������</a>
					<a class="menu__item" href="/about/">��������</a>
					<a class="menu__item" href="/faq/">������-�����</a>
				</nav>
				<div class="banner_free_ship">
					<img class="banner_free_ship_img" src="/templates/images/free-delivery.png" alt="">
				</div>
			</div>
			<div class="content">
				<div class="section">
					<div class="section__top">
						<div class="alphabet">
							<div class="alphabet__header">�������� ���� �� ��������</div>
							<div class="alphabet__content">
								<a class="alphabet__letter" href="/catalog/A/">A</a>
								<a class="alphabet__letter" href="/catalog/B/">B</a>
								<a class="alphabet__letter" href="/catalog/C/">C</a>
								<a class="alphabet__letter" href="/catalog/D/">D</a>
								<a class="alphabet__letter" href="/catalog/E/">E</a>
								<a class="alphabet__letter" href="/catalog/F/">F</a>
								<a class="alphabet__letter" href="/catalog/G/">G</a>
								<a class="alphabet__letter" href="/catalog/H/">H</a>
								<a class="alphabet__letter" href="/catalog/I/">I</a>
								<a class="alphabet__letter" href="/catalog/J/">J</a>
								<a class="alphabet__letter" href="/catalog/K/">K</a>
								<a class="alphabet__letter" href="/catalog/L/">L</a>
								<a class="alphabet__letter" href="/catalog/M/">M</a>
								<a class="alphabet__letter" href="/catalog/N/">N</a>
								<a class="alphabet__letter" href="/catalog/O/">O</a>
								<a class="alphabet__letter" href="/catalog/P/">P</a>
								<a class="alphabet__letter" href="/catalog/Q/">Q</a>
								<a class="alphabet__letter" href="/catalog/R/">R</a>
								<a class="alphabet__letter" href="/catalog/S/">S</a>
								<a class="alphabet__letter" href="/catalog/T/">T</a>
								<a class="alphabet__letter" href="/catalog/U/">U</a>
								<a class="alphabet__letter" href="/catalog/V/">V</a>
								<a class="alphabet__letter" href="/catalog/W/">W</a>
								<a class="alphabet__letter" href="/catalog/X/">X</a>
								<a class="alphabet__letter" href="/catalog/Y/">Y</a>
								<a class="alphabet__letter" href="/catalog/Z/">Z</a>
							</div>
						</div>
						<form class="search" name="form" enctype="multipart/form-data" action="/find/" method="post">
							<div class="search__header">����� �������</div>
							<div class="search__content">
								<input class="search__input" type="text" name="find">
								<button class="button button--small search__button" name="find_ok">������</button>
							</div>
						</form>
					</div>
					<div class="section-wrapper wrapper_err">
						<div class="container_err">
							<a href="/"><img src="/images/error_404.jpg" alt="������ 404"></a>
						</div>
					</div>
				</div>
				<div class="sidebar">
					<a class="cart-block" href="/orders/">
						<div class="cart-block__icon<?php if ($GLOBALS['count_prod'] > 0) { echo " in_basket"; } ?>"></div>
						<div class="cart-block__content">
							<div class="cart-block__header">�������</div>
							<div class="cart-block__text cart-block__items">���������� ������� <?php echo $GLOBALS['count_prod']; ?> ��</div>
							<div class="cart-block__text cart-block__price">�� ����� <?=$GLOBALS['prod_subtotal'];?> �</div>
						</div>
					</a>
					<div class="cart-block2__header_hide" style="display:<?php if ($GLOBALS['count_prod'] > 0) { echo "block"; } else { echo "none"; }?>;">
							<div>
								<a class="" href="/orders/">�������� �����</a>
							</div>
							<div>
								<a class="" href="/">���������� �������</a>
							</div>
					</div>
					<?php if ($GLOBALS['is_login']) { ?>
					<div class="cabinet-block sidebar__cabinet-block">
						<a href="/users/"><p class="cabinet-block__header">������ �������</p></a>
						<div class="cabinet-block__links">
							<a class="cabinet-block__link " href="/users/sale/">����������</a>
							<a class="cabinet-block__link " href="/discount/">������ � �����</a>
							<a class="cabinet-block__link " href="/users/favorite/">���������</a>
							<a class="cabinet-block__link " href="/users/history/status/1/">������� �������</a>
							<a class="cabinet-block__link " href="/users/comments/">��� �����������</a>
						</div>
						<div class="cabinet-block__controls">
							<a class="cabinet-block__link" href="/users/exit/">�����</a>
							<a class="cabinet-block__link" href="/users/options/">���������</a>
						</div>
					</div>
					<?php } ?>
					<div class="side-catalog">
						<p class="side-catalog__header">��� 100 �������</p><a class="side-catalog__link" href="/catalog/">��� ������</a>
						<div class="brands-list">
							<aside>
							<?php global $brend_item_arr; ?>
							<?php foreach ($brend_item_arr as $brend_item) {?>
                                <?php if ($brend_item['is_top'] == 1) { ?>
                                    <a class="brands-list__item" href="/catalog/<?php echo $brend_item['url']; ?>/"><?php echo $brend_item['title']; ?></a>
                                <?php } ?>
                            <?php } ?>
							</aside>
						</div>
					</div>
				</div>
			</div>
			<article class="note"><section>��������-������� ���������� Enigme.ru ����� 9 ��� ����� ����������� �������! �� �������� ������� ������� �������, ������� ������ �� ������� � ������� ����������� � �����������!</section></article>
			<div class="delivery-block">
				<?php if ($city['domain'] === "msk") { ?>
					<p class="delivery-block__header">������� �������� � ������ ������:</p>
					<div class="delivery-block__content">������, �����������, ���������, �������, ��������, ������������, ������, ������� ��������, �����������, ��������, ���������, �������, �������, �����-�������, ������������, �������, ������, ������-���, �������, ������, �����������, ������, ��������, �����, ��������, ���������, ����������, ������, �����, �������-���������, ������, ������������, ������, ���������� �����, �������������, ������ ��������, ������ �����, �����������, ������������, �����������, ��������, ����, ���, ��������, �����, �����, ������������, ������-��-����, ������, ������, �����-���������, �������, �������, ��������, , ����, ����������, ������ �����, �����������, ������, ���������, ������, �����, ��������, �����, ����, ������, ����-���, ���������, ���, ���������, ���������, ���������, ����, ������, ���������.</div>
				<?php } ?>
			</div>
			<div class="sitemap">
					<div class="sitemap__item">
						<h3 class="sitemap__header">�������� �� ����</h3>
						<div class="sitemap__links">
							<a class="sitemap__link" rel="nofollow" href="http://vk.com/enigme_parfum" target="_blank">���������</a>
							<a class="sitemap__link" rel="nofollow" href="http://www.facebook.com/enigme.store" target="_blank">Facebook</a>
							<a class="sitemap__link" rel="nofollow" href="https://twitter.com/enigme_ru" target="_blank">Twitter</a>
							<a class="sitemap__link" rel="nofollow publisher" href="https://plus.google.com/100419777799689206628" target="_blank">Google+</a>
						</div>
					</div>
					<div class="sitemap__item">
						<h3 class="sitemap__header">������</h3>
						<div class="sitemap__links">
							<a class="sitemap__link" href="/dostavka/">��������</a>
							<a class="sitemap__link" href="/about/">��������</a>
							<a class="sitemap__link" href="/oferta/">������� ������</a>
							<a class="sitemap__link" href="/guarantees/">�������� � �����������</a>
							<a class="sitemap__link" href="/refund/">�������</a>
						</div>
					</div>
					<div class="sitemap__item">
						<h3 class="sitemap__header">�������</h3>
						<div class="sitemap__links">
							<a class="sitemap__link" href="/">�������</a>
							<a class="sitemap__link" href="/catalog/">��� ������</a>
							<a class="sitemap__link" href="/sale/">����������</a>
						</div>
					</div>
					<div class="sitemap__item">
						<h3 class="sitemap__header">����������</h3>
						<div class="sitemap__links">
							<a class="sitemap__link" href="/discount/">�����</a>
							<a class="sitemap__link" href="/must-have/">Must Have</a>
						</div>
					</div>
			</div>
			<footer class="footer">
					<div class="footer__left">
						<div class="siteinfo">
							<div class="siteinfo__logo"></div>
							<div class="siteinfo__content">� 2014-2016<br>ENIGME.RU<br>��������-������� ����������</div>
						</div>
						<div class="copyright">��� ����� �� ��������� �������������� �� ����� ����������� enigme.ru. ����������� ���������� ���������. (������.��)</div>
					</div>
					
					<div class="page-buttons"><a class="callback-button" href="tel:88005558617"></a><a class="go-top page-buttons__go-top" rel="nofollow" href="#top"></a>
					</div>
					<form class="login modal mfp-hide" id="login" action="/users/login/" enctype="multipart/form-data" name="reg_form_top" method="post">
						<div class="mfp-close"></div>
						<p class="modal__header">�����������</p>
						<div class="input-item modal__input-item">
							<div class="input-item__header input-item__header--required">Email:</div>
							<input class="input modal__input" type="text" name="log_email">
						</div>
						<div class="input-item modal__input-item">
							<div class="input-item__header input-item__header--required">������:</div>
							<input class="input modal__input" type="password" name="log_passw">
						</div><a class="login__lost modal-link" href="#restore">������ ������?</a>
						<button class="button button--big modal__button" type="submit" name="register">�����</button>
					</form>
					<form class="restore modal mfp-hide" id="restore" action="/users/forgot-password/" enctype="multipart/form-data" method="post" name="forgot_form">
						<div class="mfp-close"></div>
						<p class="modal__header">�������������� ������</p>
						<div class="tabs restore__tabs">
							<a id="tab_modal_rest_email" class="tab restore__tab tab--active" href="#rest_email">������ email</a>
							
						</div>
						<div id="modal_rest_email" class="input-item modal__input-item" style="display: block;">
							<div class="input-item__header input-item__header--required">Email:</div>
							<input class="input modal__input" type="text" name="forgot_email">
						</div>
						<button class="button button--big modal__button" name="send_passw" type="submit">�������</button>
					</form>
			</footer>
		</div>
		<script type="text/javascript" src="/templates/js/jquery.min.js"></script>
		<script type="text/javascript" src="/templates/js/plugins.js"></script>
		<script type="text/javascript" src="/templates/js/jquery.maskedinput.min.js"></script>
		<script src="/templates/js/jquery.cookie.js"></script>
		<script type="text/javascript" src="/templates/js/script.js"></script>
		<!--BEGIN JIVOSITE CODE {literal} -->
		<script type='text/javascript'>
		if (screen.width > 480) {
		(function(){
			var widget_id = 'Rp0o7gSVp3';
			var s =
            document.createElement('script'); s.type = 'text/javascript'; s.async = true;
			s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss =
            document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s,
            ss);})();
		}
		</script>
		<!-- {/literal} END JIVOSITE CODE -->
		<!--LiveInternet counter--><script type="text/javascript">
                        document.write("<a href='http://www.liveinternet.ru/click' "+
                            "target=_blank><img src='http://counter.yadro.ru/hit?t26.2;r"+
                            escape(document.referrer)+((typeof(screen)=="undefined")?"":
                            ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                                screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
                            ";"+Math.random()+
                            "' alt='' title='LiveInternet: �������� ����� ����������� ��"+
                            " �������' "+
                            "border='0' width='88' height='15'><\/a>")
                    </script>
		<!--/LiveInternet-->
		
<?php if ($city['domain'] === 'msk') { ?>        

        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter21951622 = new Ya.Metrika({id:21951622,
                            webvisor:true,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true});
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="//mc.yandex.ru/watch/21951622" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
		
<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter34974915 = new Ya.Metrika({ id:34974915, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/34974915" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

<?php } else { ?>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter42164999 = new Ya.Metrika({
                    id:42164999,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/42164999" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<?php } ?>

		<?php include('modals.php'); ?>
		<?php include('_counters.php'); ?>
		<script>
        $(document).ready(function(){
		$(".product-option__right .product-option__price").css("color", "red");
			});
		</script>
	</body>
</html>