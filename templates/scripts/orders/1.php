<div id="cart_step2" class="delivery__page">
    <div class="lCol" style="width:auto;margin-left: 0;">
        <link rel="stylesheet" href="/templates/css/cart.css">
        <style>
            .dostavka_item__metro_bottom {
                padding-left: 0;
                position: relative;
                top: 0;
            }
            .dostavka_item table {
                table-layout: fixed;
                width: 100%;
            }
            .courier__row td {
                position: relative;
            }
        </style>
        <script src="/templates/js/suggest_delivery.js"></script>
        <script>
            $(function(){
                AddressSuggestions.initForm();
            })
        </script>
        <p style="font-size:14px;">��� ��������-������� ������������ �������� �� ���� ������. � ��� ���� ��������� �������� ��������: �������� ��������, ����� ������, � ����� ������� ���������� ������� ����������. ����� ������, ����� �������� �� ������ �������� ����� � ����� �������, � ����� ����� � ��������� ��������, ������� ���� ��� ������ � �����/���������� �����. ������� ������������� ������� ��� ��������� ��� ��� �������� ��������, ����� � ����.</p>
        <p style="margin-bottom: 20px; margin-top: 10px;">�������, ����������, ������ ��������:</p>
        <div class="line">
            <label for="step2_region">������ / �������:<span class="star">*</span></label>
            <input name="region" value="" placeholder="������, ���������� ���, �������� ���, �������" id="step2_region" type="text" autocomplete="off">
        </div>

        <div class="line part" style="display: block;">
            <label for="step2_town">�������� �����:<span class="star">*</span></label>
            <select name="part" id="select_part">
                <option style="display: inline;" data-location="m" value="7700000000000">������ � �������� ����</option>
                <option style="display: inline;" data-location="m" value="0000000000001">��������</option>
                <option style="display: inline;" data-location="m" value="0000000000002">������</option>
                <option style="display: inline;" data-location="m" value="0000000000003">����������</option>
                <option style="display: inline;" data-location="m" value="0000000000004">��������</option>
                <option style="display: inline;" data-location="m" value="0000000000005">������</option>
                <option style="display: inline;" data-location="m" value="0000000000006">�������� � ����-�����������</option>
                <option style="display: inline;" data-location="m" value="0000000000007">����-�����������</option>
                <option style="display: inline;" data-location="m" value="0000000000009">������</option>
                <option style="display: inline;" data-location="m" value="0000000000010">�������</option>
                <option style="display: none" data-location="s" value="7800000000000">�����-��������� � �������� ���</option>
                <option style="display: none" data-location="s" value="7800000900000">������</option>
                <option style="display: none" data-location="s" value="7800000700000">��������</option>
                <option style="display: none" data-location="s" value="780000400000">������� ����</option>
                <option style="display: none" data-location="s" value="7800000800000">��������</option>
            </select>

            <div class="other" style="display: none;">
                <div style="clear: both"></div>
                <br>
                <label for="other" style="width:120px;float:left;">������:</label>
                <input style="padding:2px;font: 11px Arial;width:300px" value="" type="text" name="other" id="other">
            </div>
        </div>

        <div class="line">
            <label for="step2_town">����� / �����:<span class="star">*</span></label>
            <input name="city" value="" id="step2_town" placeholder="������� ���� �����" type="text" autocomplete="off">
        </div>
        <div class="line settlement" style="display: none;">
            <label for="step2_settlement">���������� �����:<span class="star">*</span></label>
            <input name="settlement" value="" id="step2_settlement" type="text" autocomplete="off">
        </div>


        <p style="margin-bottom: 20px; margin-top: 30px">� ��� �����/���������� ����� �� ������ ��������� ����� ���������� ���������:</p>
        <div id="couriers__wrapper">
            <div class="dostavka_item" id="courier" data-type="��������" data-modal="courier">
                <div class="l" style="position:relative;top:0px">
                    <div style="position:relative;top:-10px" class="text">��������  ��������</div>
                </div>
                <div class="r" style="margin-left: 0;">
                    <table style="margin: 0;">
                        <thead class="courier__table-header head">
                        <tr>
                            <th style="padding-left: 0;">������ ��������</th>
                            <th style="text-align:left;width:40px;padding-left:0">����</th>
                            <th style="width:50px">����</th>
                        </tr>
                        </thead>
                        <tbody class="courier__table-body"></tbody>
                        <tr style="display: none;">
                            <td class="tooltip_bg_blue">
                                <div data-width="250"  class="dostavka_item__checkbox" data-toggle="tooltip" data-content="�������� ��-�� � 10 �� 20:00. � ���� �������� ��� �������� ���-����������� � ������� �������� ��������, ������� ������������ ��� ��������.">b2bpl</div>
                            </td>
                            <td>
                                <div class="">4 ��.</div>
                            </td>
                            <td>117 �</td>
                        </tr>
                    </table>
                </div>
                <div class="clear"></div>
            </div>

            <div class="clear"></div>

            <div class="dostavka_item" id="post" data-type="�����" data-modal="post">
                <div class="l">
                    <div style="position:relative;top:-10px" class="text" >�����  ������</div>
                </div>
                <div class="r" style="margin-left: 0;">
                    <table style="margin: 0;">
                        <thead class="courier__table-header head">
                        <tr>
                            <th style="padding-left: 0;">������ ��������</th>
                            <th style="text-align:left;width:40px;padding-left:0">����</th>
                            <th style="width:50px">����</th>
                        </tr>
                        </thead>
                        <tbody class="courier__table-body"></tbody>
                        <tr style="display: none;">
                            <td class="tooltip_bg_blue">
                                <div class="dostavka_item__checkbox" data-width="250" data-toggle="tooltip" data-content="������ ���������� � ������� 1 ��� ���������� �������� 1 ������. �������� � �������� ����� �� ������� �� ����� �������� ���������. � ������ �������� ������� �� �������� ��������� �� ����� ������. ��� ������ ���������� �������� ����� ������ ������� �� 2 �� 5% �������� �� ������� ����� �� ��� ����.">����� ������</div>
                            </td>
                            <td>
                                <div >5-12 ��.</div>
                            </td>
                            <td><div style="cursor:pointer" data-toggle="tooltip" data-content="����� �������� ��������� ��������" class="green_label">117 �</div></td>
                        </tr>
                    </table>
                </div>
                <div class="clear"></div>
            </div>

            <div class="clear"></div>

            <div class="dostavka_item" id="self" data-type="���������" data-modal="self">
                <div class="l" >
                    <div style="position:relative;top:-10px" class="text">���������</div>
                </div>


                <div class="r" style="margin-left: 0;">
                    <table style="margin: 0;">
                        <thead class="courier__table-header head">
                        <tr>
                            <th style="padding-left: 0;">������ ������� ������</th>
                            <th style="text-align:left;width:40px;padding-left:0">����</th>
                            <th style="width:50px">����</th>
                        </tr>
                        </thead>
                        <tbody class="courier__table-body"></tbody>
                    </table>
                </div>

                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>