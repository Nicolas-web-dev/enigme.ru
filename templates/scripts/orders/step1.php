<script>
    $(function(){
        //index suggest
        AddressSuggestions.initName();
    })
</script>
<div class="cart-table">
    <table class="cart-table__content">
        <tr class="cart-table__header">
            <th class="cart-table__header-item" colspan="2">�������� ������</th>
            <th class="cart-table__header-item">����</th>
            <th class="cart-table__header-item">���-��</th>
            <th class="cart-table__header-item">���������</th>
            <th class="cart-table__header-item">&nbsp;</th>
        </tr>
		<?php if ($this->cart->getCount()!=0) { ?>
			<?php foreach ($this->cart->getItems() as $item_order) { ?>
                <tr class="cart-table__item tr_<?php echo $item_order['uid']; ?>">
				
                    <td class="cart-table__item-picture">
						<img class="cart-table__img" src="<?php echo $item_order['img_s']; ?>" alt="">
					</td>
                    <td class="cart-table__item-header">
						<?php echo $item_order['title']; ?>
						<?php if ($item_order['is_block'] == 1) { ?>
                            <span style="color:#dc3575;font-size:12px;;display:block;margin-top:8px;">�� ������ ����� ������ �� ����������������!</span>
                        <?php } ?>
					</td>
                    <td class="cart-table__item-price price">
							<!--
							<div class="hot-price"></div>
							-->
						<?php if ($this->sell) { ?>
                            <s>
                                <?= parsePrice(round(($item_order['price']) + (($item_order['price']) * $this->sell))) ?>
                            </s> &nbsp;&nbsp;
						<?php } ?>
						<?php echo parsePrice($item_order['price']); ?>
					</td>
                    <td class="cart-table__item-amount">
                        <div class="cart-table__amount-content">
							<div class="cart-table__amount-number"><?php echo $item_order['kol']; ?></div>
							<div id="korz_kol-vo" class="cart-table__amount-controls">
								<a data-action="asc" data-uid="<?php echo $item_order['uid']; ?>" class="cart-table__amount-controls-item cart-table__amount-controls-item--more plus" href="/orders/AscOrder/<?php echo $item_order['uid']; ?>/<?php echo $item_order['price']; ?>/"></a>
								<a data-action="desc" data-uid="<?php echo $item_order['uid']; ?>" class="cart-table__amount-controls-item cart-table__amount-controls-item--less minus" href="/orders/DescOrder/<?php echo $item_order['uid']; ?>/<?php echo $item_order['price']; ?>/"></a>
								<input type="hidden" name="kol_<?php echo $item_order['uid']; ?>" maxlength="2"  value="<?php echo $item_order['kol']; ?>" id="kol_<?php echo $item_order['uid']; ?>" class="input_cart"/>
							</div>
                        </div>
                    </td>
                    <td class="cart-table__item-price sum">
						<?php echo parsePrice($item_order['sum']); ?>
					</td>
                    <td class="cart-table__item-remove">
						<a class="cart-table__item-remove-content" href="/orders/deleteOrder/<?php echo $item_order['uid']; ?>/<?php echo $item_order['sum']; ?>/<?php echo $item_order['kol']; ?>/"></a>
					</td>
					  
                </tr>
			<?php } ?>		
        <?php } ?>           
    </table>
    <div class="cart-table__overall">
        <div class="cart-table__overall-item">
            <div class="cart-table__overall-header">
                <div class="cart-table__overall-header-content">����� ������:</div>
            </div>
            <div class="cart-table__overall-content"><span class="val_subTotal" id="big_sum"><?php echo $this->cart->getSubTotal(); ?></span> ���</div>
        </div>
        <div class="cart-table__overall-item">
            <div class="cart-table__overall-header">
                <div class="cart-table__overall-header-content">������������� ������:</div><a id="nakop_sk" class="cart-table__advice" href="#" style="position:relative;"></a>
					<div class="nakop_id" style="right:20px; left:auto; height: 170px; width: 430px; padding: 10px 20px; line-height: 20px; position: absolute; font-size: 12px; background: url(/images/podsk2.png) 0 0 no-repeat; color: white; text-align: left; display: none;">
						<span style="font-size:18px;display:block;padding-bottom:2px" class="georgia_head">������������� ������ � �������� Enigme.ru:</span>
						3% ��� ���������� ����� ������� �� 3000 �� 10 000 ���<br>
						5% ��� ���������� ����� ������� �� 20 001 �� 70 000 ���<br>
						7% ��� ���������� ����� ������� �� 70 001 �� 139 999 ���<br>
						10% ��� ���������� ����� ������� �� 70 001 �� 139 999 ���<br>
						15% ��� ���������� ����� ������� ����� 140 000 ���
					</div>
            </div>
            <div class="cart-table__overall-content"><?=($this->cart->getDiscountData('percent_d'))?($this->cart->getDiscountData('percent_d')):0?>%</div>
        </div>
        <div class="cart-table__overall-item">
            <div class="cart-table__overall-header">
                <div class="cart-table__overall-header-content">����� �����: </div>
            </div>
            <div class="cart-table__overall-content cart-table__final-price"><span class="val_total" id="big_sum_all"><?php echo $this->cart->getTotal(); ?></span> ���</div>
		</div>
    </div>
</div>

<a name="order"></a>
<?php if (!$this->is_login) { ?>
	<div class="cart__login">
                <p class="cart__login-header">��������� ����� ��� �������������:</p>
                <div class="cart__login-content">
                  <div class="cart__login-new">
                    <h3 class="cart__login-subheader">� ����� �������</h3>
                    <div class="cart__login-form">
                      <div class="input-item cart__login-input-item">
                        <div class="input-item__header input-item__header--required">�������:</div>
                        <input class="input cart__login-input" type="text" name="phone" id="step1_phone" value="<?php echo $this->data['phone']; ?>" required>
                      </div>
                      <div class="input-item cart__login-input-item">
                        <div class="input-item__header input-item__header--required">���:</div>
                        <input class="input cart__login-input" type="text" name="fio" id="step1_fio" value="<?php echo $this->data['fio']; ?>" required>
						<input name="fio_data" value="" type="hidden">
                      </div>
                      <div class="input-item cart__login-input-item">
                        <div class="input-item__header input-item__header--required">E-mail:</div>
                        <input class="input cart__login-input" type="text" name="email" id="step1_mail" required>
                      </div>
                      <button name="register" value="1" class="button button--big cart__login-button-new">�������� �����</button>
                    </div>
                  </div>
                  <div class="cart__login-old">
                    <h3 class="cart__login-subheader">� ��� ������� �����</h3>
                    <p class="cart__login-text">���� �� ��� ������ ����� � ����� ��������, �� �� ������ �������� ����� ����� ������ �������</p>
					<a class="button button--big modal-link" href="#orders_login">�������� ����� ������ �������</a>
                  </div>
                </div>
	</div>
	<script>
		$(function() {
			$('form[name="orders_list"]').validate({
				rules: {
				  phone: 'required',
				  email: {
					required: true,
					email: true
				  },
				  fio: 'required'
				},
				messages: {
				  fio: '����������, ������� ���� ���',
				  email: {
					required: '����������, ������� e-mail',
					email: '������ ������������ e-mail �����'
				  },
				  phone: '����������, ������� ��� �����'
				},
				submitHandler: function(form) {
				  form.submit();
				}
			});
		});
	</script>
<?php } else { ?>
	<div style="float: right; padding-top: 40px; padding-bottom: 40px;">
		<button type="submit" name="purchase" class="button button--big cart__login-button-new">�������� �����</button>
	</div>	
<?php } ?>	