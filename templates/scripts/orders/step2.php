
<div id="cart_step2" style="display:none">
<div class="news">

<div class="lCol" style="">
    <div class="title georgia_head">�������, ����������, ������ ��������:</div>
    <form action="">
        <div class="line"><label for="step2_region">������ / �������:<span class="star">*</span></label>
            <input placeholder="������, ���������� ���, �������� ���, �������" id="step2_region" type="text"></div>
        <div class="line"><label for="step2_town">����� / �����:<span class="star">*</span></label>
            <input id="step2_town" placeholder="������, �������, ���������� �-�, ���, ������" type="text"></div>
        <div class="line" style="display:none"><label for="step2_punkt">���������� �����:<span class="star">*</span></label><input id="step2_punkt" type="text"></div>

        <div style="padding-top:50px" class="title georgia_head">�������� ������� ������ �������� � ��� ������:</div>
        <div id="couriers__wrapper">
            <div class="dostavka_item active" id="courier" data-type="��������" data-modal="courier"
                <div class="l" style="position:relative;top:30px">
                    <div style="position:relative;top:10px" style="position:relative;top:10px" class="text">��������  ��������</div>
                </div>
                <div class="r">
                    <table>
                        <thead class="courier__table-header head">
                        <tr>
                            <th>������ ��������</th>
                            <th>����</th>
                            <th>����</th>
                        </tr>
                        </thead>
                        <tbody class="courier__table-body"></tbody>
                        <tr style="display: none;">
                            <td class="tooltip_bg_blue">
                                <div data-width="250"  class="dostavka_item__checkbox" data-toggle="tooltip" data-content="�������� ��-�� � 10 �� 20:00. � ���� �������� ��� �������� ���-����������� � ������� �������� ��������, ������� ������������ ��� ��������.">b2bpl</div>
                            </td>
                            <td>
                                <div class="">4 ��.</div>
                            </td>
                            <td>117 �</td>
                        </tr>
                    </table>
                </div>
                <div class="clear"></div>
                <div class="dop_info">
                    <div class="line fl_left" style="margin-right:15px;width:220px">
                        <label style="width:auto;padding:0" for="">�����:</label>
                        <input id="" type="text" style="width:206px;padding:6px;">
                    </div>
                    <div class="line fl_left" style="margin-right:15px;width:80px">
                        <label style="width:auto;padding:0" for="">���:</label>
                        <input type="text" style="width:68px;padding:6px;">
                    </div>
                    <div class="line fl_left" style="width:80px">
                        <label style="width:auto;padding:0" for="">������:</label>
                        <input type="text" style="width:68px;padding:6px;">
                    </div>
                    <div class="clear"></div>
                    <div class="line fl_left" style="margin-right:15px;width:75px">
                        <label style="width:auto;padding:0" for="">��������:</label>
                        <input type="text" style="width:63px;padding:6px;">
                    </div>
                    <div class="fl_left line" style="margin-right:15px;width:100px">
                        <label style="width:auto;padding:0" for="">����� ��������:</label>
                        <input type="text" style="width:86px;padding:6px;">
                    </div>
                    <div class="line fl_left" style="width:110px;margin-right:15px;">
                        <label style="width:auto;padding:0" for="">����� ��������:</label>
                        <input type="text" style="width:98px;padding:6px;">
                    </div>
                    <div class="line fl_left" style="width:80px">
                        <label style="width:auto;padding:0" for="">������:</label>
                        <input type="text" style="width:68px;padding:6px;">
                    </div>
                    <div class="clear"></div>
                    <div class="line fl_left" style="margin-right:15px;width:330px">
                        <label style="width:auto;padding:8px 8px 8px 0;" for="">��������� ������� �����:</label>
                        <select name="" id="" style="width:153px;border:1px solid #cecccd;padding:8px;">
                            <option value="">&mdash;</option>
                        </select>
                    </div>

                </div>
            </div>

            <div class="clear"></div>

            <div class="dostavka_item" id="post" data-type="�����" data-modal="post">
                <div class="l">
                    <div style="position:relative;top:10px" class="text" >�����  ������</div>
                </div>
                <div class="r">
                    <table>
                        <thead class="courier__table-header head">
                        <tr>
                            <th>������ ��������</th>
                            <th>����</th>
                            <th>����</th>
                        </tr>
                        </thead>
                        <tbody class="courier__table-body"></tbody>
                        <tr style="display: none;">
                            <td class="tooltip_bg_blue">
                                <div class="dostavka_item__checkbox" data-width="250" class="dostavka_item__checkbox " data-toggle="tooltip" data-content="������ ���������� � ������� 1 ��� ���������� �������� 1 ������. �������� � �������� ����� �� ������� �� ����� �������� ���������. � ������ �������� ������� �� �������� ��������� �� ����� ������. ��� ������ ���������� �������� ����� ������ ������� �� 2 �� 5% �������� �� ������� ����� �� ��� ����.">����� ������</div>
                            </td>
                            <td>
                                <div >5-12 ��.</div>
                            </td>
                            <td><div style="cursor:pointer" data-toggle="tooltip" data-content="����� �������� ��������� ��������" class="green_label">117 �</div></td>
                        </tr>
                    </table>
                </div>
                <div class="clear"></div>
                <div class="dop_info" style="padding-top:5px;display:none">
                    <div class="line fl_left" style="margin-right:15px;width:220px">
                        <label style="width:auto;padding:0" for="">�����:</label>
                        <input id="" type="text" style="width:206px;padding:6px;">
                    </div>
                    <div class="line fl_left" style="margin-right:15px;width:80px">
                        <label style="width:auto;padding:0" for="">���:</label>
                        <input type="text" style="width:68px;padding:6px;">
                    </div>
                    <div class="line fl_left" style="width:80px">
                        <label style="width:auto;padding:0" for="">������:</label>
                        <input type="text" style="width:68px;padding:6px;">
                    </div>
                    <div class="clear"></div>
                    <div class="line fl_left" style="margin-right:15px;width:75px">
                        <label style="width:auto;padding:0" for="">��������:</label>
                        <input type="text" style="width:63px;padding:6px;">
                    </div>
                    <div class="fl_left line" style="margin-right:15px;width:100px">
                        <label style="width:auto;padding:0" for="">����� ��������:</label>
                        <input type="text" style="width:86px;padding:6px;">
                    </div>
                    <div class="line fl_left" style="width:110px;margin-right:15px;">
                        <label style="width:auto;padding:0" for="">����� ��������:</label>
                        <input type="text" style="width:98px;padding:6px;">
                    </div>
                    <div class="line fl_left" style="width:80px">
                        <label style="width:auto;padding:0" for="">������:</label>
                        <input type="text" style="width:68px;padding:6px;">
                    </div>
                </div>
            </div>

            <div class="clear"></div>

            <div class="dostavka_item" id="self" data-type="���������" data-modal="self">
                <div class="l" >
                    <div style="position:relative;top:10px" style="position:relative;top:10px" style="position:relative;top:10px" class="text">���������</div>
                </div>
                <div class="r">
                    <table>
                        <thead class="courier__table-header head">
                        <tr>
                            <th>������ ��������</th>
                            <th>����</th>
                            <th>����</th>
                        </tr>
                        </thead>
                        <tbody class="courier__table-body"></tbody>
                        <tr style="display: none;">
                            <td>
                                <div class="dostavka_item__checkbox ">b2bpl</div>
                            </td>
                            <td>
                                <div style="cursor:pointer" data-toggle="tooltip" data-content="����� ������� ��������" class="green_label">4 ��.</div>
                            </td>
                            <td>117 �</td>
                        </tr>
                    </table>
                </div>
                <div class="clear"></div>
                <div class="dop_info" style="display:none">
                    <div style="padding:15px 0;">�������� �� ����� ������������ ��� ����� ���������� � ������� "�������"</div>
                    <div id="self_selected"></div>
                    <div id="dostavka_map">

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
    <? include ('orders/step2_sidebar.php')?>
</div>
</div>
<script>
    var yMap,
        ymapsDef = $.Deferred(),
        ymapsResolved = false;



    /**
     * ������� ���������� ����� �������� �� ���������� ������ ������������
     * ��� �� �� ��� ��������� ������, ������� ��������� ��� ��������� �������� �� ������
     * ������ �������� ������, ���� ������� ��� ������ �� ���������� ������
     *
     * @param {Number} kladr_id - ������������� ������������� ������
     * @param {String} city - �������� ������
     * @param {Object} resp - ������ XHR ������� �� ��������� "������������� ��������"
     */
    function fillCouriers(kladr_id, city, resp) {


            AddressSuggestions.getLogistic(kladr_id, city).then(function() {

                var $wrapper = $('#couriers__wrapper'),
                    $logistic = $('#self_selected'),
                    arr = [];

                if (resp.delivery_type != "���������") {
                    $wrapper.find('.courier__row[data-courier="'+resp.id+'"]').find('.dostavka_item__checkbox').addClass('checked');
                } else {

                    arr.push("<span>������������ ������: <b>"+resp.courier_name+"</b></span>");
                    arr.push("<span>����� �������� �� "+resp.delivery_days+" ��.</span>");
                    arr.push("<span>��������� �������� �� "+resp.delivery_cost_3+" �.</span>");
                    $logistic.html(arr.join(""));
                }

                $("#courier_address_id").val(resp.id);
            });

    }

    $.ajax({
        url: "/api/logistic_get/",
        data: {
            k: $("#courier_address_id").val()
        },
        success: function(resp) {

            // ���� ����� �������, �� ���� ��� ������ ������ ��������
            if (resp.id != null && resp.id != 0) {

                fillCouriers(resp.kladr_id_city, resp.name_city, resp);

                // ���� �� ������ �������� �� ��� ������ ������ ������ �� ������ �����
            } else {

                var addr = [],
                    result,
                    answer;

                // ���������� ������ ������ �� ����� ������������
                addr.push($('#step2_region').val());
                addr.push($('#select_part').val());
                addr.push($('#step2_town').val());
                addr.push($('#step2_mail').val());
                addr.join(" ");

                answer = DadataApi.clean(
                    { "structure": [ "ADDRESS" ], "data": [
                        [addr]
                    ]}
                );

                answer.then(function(msg) {

                    if (typeof msg != 'undefined') {

                        result = msg.data[0][0];

                        fillCouriers(result.kladr_id, addr, resp);
                    }
                });
            }
        }
    });
</script>

<!-- Google Code for Enigme-&#1087;&#1086;&#1082;&#1091;&#1087;&#1082;&#1072; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1055677808;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "NinmCMvqz2gQ8Lqx9wM";
var google_conversion_value = 600.00;
var google_conversion_currency = "RUB";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1055677808/?value=600.00&amp;currency_code=RUB&amp;label=NinmCMvqz2gQ8Lqx9wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
