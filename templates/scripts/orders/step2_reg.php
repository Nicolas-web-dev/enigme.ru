<script>
    $(function(){
        //index suggest
        AddressSuggestions.initForm();
    })
</script>
<div id="cart_step2">
<div class="news">

<div class="lCol">
    <div class="title georgia_head">�������, ����������, ������ ��������:</div>
    <form action="">

        <div class="line">
            <label for="step2_region">������ / �������:<span class="star">*</span></label>
            <input name="region" value="<?=$this->cart->getUser('address_region')?>" placeholder="������, ���������� ���, �������� ���, �������" id="step2_region" type="text">
        </div>

        <div class="line part">
            <label for="step2_town">�������� �����:<span class="star">*</span></label>
            <select name="part" id="select_part">
                <? foreach ($this->capital_districts as $letter => $capital): ?>
                    <? foreach ($capital as $key => $item): ?>
                        <option  <?=($this->cart->getUser('address_part')==$key)?'SELECTED':''?> style="display: none" data-location="<?= $letter ?>"
                                                                                        value="<?= $key ?>"><?= $item ?></option>
                    <? endforeach; ?>
                <? endforeach; ?>
            </select>

            <div class="other" style="display: none">
                <div style="clear: both"></div>
                <br>
                <label for="other" style="width:120px;float:left;">������:</label>
                <input style="padding:2px;font: 11px Arial;width:300px" value="<? print_r($this->cart->getUser('address_other'))?>"
                       type="text" name="other" id="other">
            </div>
        </div>

        <div class="line">
            <label for="step2_town">����� / �����:<span class="star">*</span></label>
            <input name="city" value="<?=$this->cart->getUser('address_city')?>" id="step2_town" placeholder="������� ���� �����" type="text">
        </div>
        <div class="line settlement" style="display:none">
            <label for="step2_settlement">���������� �����:<span class="star">*</span></label>
            <input name="settlement" value="<?=$this->cart->getUser('address_settlement')?>" id="step2_settlement" type="text">
        </div>




        <div style="padding-top:30px" class="title georgia_head">�������� ������� ������ �������� � ��� ������:</div>

        <div id="couriers__wrapper">
            <div class="dostavka_item" id="courier" data-type="��������" data-modal="courier">
                <div class="l" style="position:relative;top:0px">
                    <div style="position:relative;top:-10px" class="text">��������  ��������</div>
                </div>
                <div class="r" style="margin-left: 0;">
                    <table>
                        <thead class="courier__table-header head">
                        <tr>
                            <th>������ ��������</th>
                            <th style="text-align:left;width:40px;padding-left:0">����</th>
                            <th style="width:50px">����</th>
                        </tr>
                        </thead>
                        <tbody class="courier__table-body"></tbody>
                        <tr style="display: none;">
                            <td class="tooltip_bg_blue">
                                <div data-width="250"  class="dostavka_item__checkbox" data-toggle="tooltip" data-content="�������� ��-�� � 10 �� 20:00. � ���� �������� ��� �������� ���-����������� � ������� �������� ��������, ������� ������������ ��� ��������.">b2bpl</div>
                            </td>
                            <td>
                                <div class="">4 ��.</div>
                            </td>
                            <td>117 �</td>
                        </tr>
                    </table>
                </div>
                <div class="clear"></div>
                <div class="dop_info">
                    <div class="line fl_left" style="margin-right:15px;width:230px">
                        <label style="width:auto;padding:0" for="">�����:</label>
                        <input name="street" value="<?=$this->cart->getUser('address_street')?>" id="" type="text" style="width:216px;padding:6px;">
                    </div>
                    <div class="line fl_left" style="margin-right:15px;width:80px">
                        <label style="width:auto;padding:0" for="">���:</label>
                        <input name="house" value="<?=$this->cart->getUser('address_house')?>" type="text" style="width:68px;padding:6px;">
                    </div>
                    <div class="line fl_left" style="width:100px;margin-right:15px">
                        <label style="width:auto;padding:0" for="">������:</label>
                        <input name="corpus" value="<?=$this->cart->getUser('address_corpus')?>" type="text" style="width:88px;padding:6px;">
                    </div>
                    
                    <div class="line fl_left" style="width:100px">
                        <label style="width:auto;padding:0" for="">��������:</label>
                        <input name="stroenie" value="<?=$this->cart->getUser('address_stroenie')?>" type="text" style="width:93px;padding:6px;">
                    </div>
                    <div class="clear"></div>
                    <div class="fl_left line" style="margin-right:15px;width:100px">
                        <label style="width:auto;padding:0" for="">����� ��������:</label>
                        <input name="flat" value="<?=$this->cart->getUser('address_flat')?>" type="text" style="width:86px;padding:6px;">
                    </div>
                    <div class="line fl_left" style="width:112px;margin-right:17px;">
                        <label style="width:auto;padding:0" for="">����� ��������:</label>
                        <input name="domophone" value="<?=$this->cart->getUser('address_domophone')?>" type="text" style="width:100px;padding:6px;">
                    </div>
                    <div class="line fl_left" style="width:81px;margin-right:15px">
                        <label style="width:auto;padding:0" for="">������:</label>
                        <input name="regs_postcode" value="<?=$this->cart->getUser('postcode')?>" type="text" style="width:69px;padding:6px;">
                    </div>
                    <div class="line fl_left metro" style="width:230px">
                        <label style="width:auto;padding:0" for="">��������� ������� �����:</label>
                        <select name="metro" style="width:153px;border:1px solid #cecccd;padding:8px;">
                            <option value="0">�� �������</option>
                            <? foreach($this->aMetro as $metro):?>
                                <option class="metro-1 metro-option" value="<?=$metro['id']?>"><?=$metro['name']?></option>
                            <? endforeach;?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="clear"></div>

            <div class="dostavka_item" id="post" data-type="�����" data-modal="post">
                <div class="l">
                    <div style="position:relative;top:10px" class="text" >�����  ������</div>
                </div>
                <div class="r" style="margin-left: 0;">
                    <table>
                        <thead class="courier__table-header head">
                        <tr>
                            <th>������ ��������</th>
                            <th style="text-align:left;width:40px;padding-left:0">����</th>
                            <th style="width:50px">����</th>
                        </tr>
                        </thead>
                        <tbody class="courier__table-body"></tbody>
                        <tr style="display: none;">
                            <td class="tooltip_bg_blue">
                                <div class="dostavka_item__checkbox" data-width="250" data-toggle="tooltip" data-content="������ ���������� � ������� 1 ��� ���������� �������� 1 ������. �������� � �������� ����� �� ������� �� ����� �������� ���������. � ������ �������� ������� �� �������� ��������� �� ����� ������. ��� ������ ���������� �������� ����� ������ ������� �� 2 �� 5% �������� �� ������� ����� �� ��� ����.">����� ������</div>
                            </td>
                            <td>
                                <div >5-12 ��.</div>
                            </td>
                            <td><div style="cursor:pointer" data-toggle="tooltip" data-content="����� �������� ��������� ��������" class="green_label">117 �</div></td>
                        </tr>
                    </table>
                </div>
                <div class="clear"></div>
                <div class="dop_info">
                    <div class="line fl_left" style="margin-right:15px;width:220px">
                        <label style="width:auto;padding:0" for="">�����:</label>
                        <input name="street" value="<?=$this->cart->getUser('address_street')?>" id="" type="text" style="width:206px;padding:6px;">
                    </div>
                    <div class="line fl_left" style="margin-right:15px;width:80px">
                        <label style="width:auto;padding:0" for="">���:</label>
                        <input name="house" value="<?=$this->cart->getUser('address_house')?>" type="text" style="width:68px;padding:6px;">
                    </div>
                    <div class="line fl_left" style="width:80px">
                        <label style="width:auto;padding:0" for="">������:</label>
                        <input name="corpus" value="<?=$this->cart->getUser('address_corpus')?>" type="text" style="width:68px;padding:6px;">
                    </div>
                    <div class="clear"></div>
                    <div class="line fl_left" style="margin-right:15px;width:75px">
                        <label style="width:auto;padding:0" for="">��������:</label>
                        <input name="stroenie" value="<?=$this->cart->getUser('address_stroenie')?>" type="text" style="width:63px;padding:6px;">
                    </div>
                    <div class="fl_left line" style="margin-right:15px;width:100px">
                        <label style="width:auto;padding:0" for="">����� ��������:</label>
                        <input name="flat" value="<?=$this->cart->getUser('address_flat')?>" type="text" style="width:86px;padding:6px;">
                    </div>
                    <div class="line fl_left" style="width:110px;margin-right:15px;">
                        <label style="width:auto;padding:0" for="">����� ��������:</label>
                        <input name="domophone" value="<?=$this->cart->getUser('address_domophone')?>" type="text" style="width:98px;padding:6px;">
                    </div>
                    <div class="line fl_left" style="width:80px">
                        <label style="width:auto;padding:0" for="">������:</label>
                        <input name="regs_postcode" value="<?=$this->cart->getUser('postcode')?>" type="text" style="width:68px;padding:6px;">
                    </div>
                    <div class="clear"></div>
                    <div class="line fl_left metro" style="margin-right:15px;width:330px">
                        <label style="width:auto;padding:8px 8px 8px 0;" for="">��������� ������� �����:</label>
                        <select name="metro" style="width:153px;border:1px solid #cecccd;padding:8px;">
                            <option value="0">�� �������</option>
                            <? foreach($this->aMetro as $metro):?>
                            <option value="<?=$metro['id']?>"><?=$metro['name']?></option>
                            <? endforeach;?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="clear"></div>

            <div class="dostavka_item" id="self" data-type="���������" data-modal="self">
                <div class="l" >
                    <div style="position:relative;top:-10px" class="text">���������</div>
                </div>
                

                <div class="r" style="margin-left: 0;">
                    <table>
                        <thead class="courier__table-header head">
                        <tr>
                            <th>������ ��������</th>
                            <th style="text-align:left;width:40px;padding-left:0">����</th>
                            <th style="width:50px">����</th>
                        </tr>
                        </thead>
                        <tbody class="courier__table-body"></tbody>
                    </table>
                </div>

                <div class="clear"></div>
                <div class="dop_info" style="display: none!important;">
                    <div style="padding:15px 0;">�������� �� ����� ������������ ��� ����� ���������� � ������� "�������"</div>
                    <div id="self_selected"></div>
                    <div id="dostavka_map" style="">

                    </div>
                </div>
            </div>
    </div>
        <input type="hidden" name="courier_address_id" id="courier_address_id" value=""/>
    </form>
</div>

<? include ('step2_sidebar.php');?>

</div>
</div>
<script>

    /**
     * ������� ���������� ����� �������� �� ���������� ������ ������������
     * ��� �� �� ��� ��������� ������, ������� ��������� ��� ��������� �������� �� ������
     * ������ �������� ������, ���� ������� ��� ������ �� ���������� ������
     *
     * @param {Number} kladr_id - ������������� ������������� ������
     * @param {String} city - �������� ������
     * @param {Object} resp - ������ XHR ������� �� ��������� "������������� ��������"
     */
    function fillCouriers(kladr_id, city, resp) {


            AddressSuggestions.getLogistic(kladr_id, city).then(function() {

                var $wrapper = $('#couriers__wrapper'),
                    $logistic = $('#self_selected'),
                    arr = [];

                if (resp.delivery_type != "���������") {
                    $wrapper.find('.courier__row[data-courier="'+resp.id+'"]').find('.dostavka_item__checkbox').trigger('click');//.addClass('checked');
                } else {

                    arr.push("<div class='wrr'>");
                    arr.push("<span>�� �������: <b>"+resp.courier_name+"</b></span>");
                    arr.push("<span>����� �������� �� "+resp.delivery_days+" ��.</span>");
                    arr.push("<span>��������� �������� �� "+resp.delivery_cost_3+" �.</span>");
                    arr.push("</div>");
                    $logistic.html(arr.join(""));

                    isDeliveryCostInited = true;
                }

                $("#courier_address_id").val(resp.id);
            });

    }

    $.ajax({
        url: "/api/logistic_get/",
        data: {
            k: $("#courier_address_id").val()
        },
        success: function(resp) {

            // ���� ����� �������, �� ���� ��� ������ ������ ��������
            if (resp.id != null && resp.id != 0) {

                fillCouriers(resp.kladr_id_city, resp.name_city, resp);

                // ���� �� ������ �������� �� ��� ������ ������ ������ �� ������ �����
            } else {

                var addr = [],
                    result,
                    answer,
                    $cart_step2 = $('#cart_step2');

                console.log()

                addr.push($cart_step2.find('[name="region"]').val());
                // ���������� ������ ������ �� ����� ������������
                if ($cart_step2.find('[name="region"]').val() != $('#step2_town').val()){
                    addr.push($('#step2_town').val());
                }
                addr.push($('#step2_settlement').val());

                answer = DadataApi.clean(addr.join(" "));
                answer.then(function(msg) {

                    if (typeof msg != 'undefined') {
                        result = msg.suggestions[0]['data'];
                        fillCouriers(result.kladr_id, addr, resp);
                    }
                });
            }
        }
    });
</script>

<!-- Google Code for Enigme-&#1087;&#1086;&#1082;&#1091;&#1087;&#1082;&#1072; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1055677808;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "NinmCMvqz2gQ8Lqx9wM";
var google_conversion_value = 600.00;
var google_conversion_currency = "RUB";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1055677808/?value=600.00&amp;currency_code=RUB&amp;label=NinmCMvqz2gQ8Lqx9wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
