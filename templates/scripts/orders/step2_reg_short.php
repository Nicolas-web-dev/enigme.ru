<script>           
		$(function(){AddressSuggestions.initForm();})        
</script> 
<div class="cart__step2">
        <div class="col-md-8 l-col">
            <div class="delivery__section-header">�������, ����������, ������ ��������:</div>
            <form action="" method="POST">
                <input type="hidden" name="complete" value="1">
				<div class="delivery__form-content">
					<div class="input-item">
						<label for="step2_region" class="input-item__header">������ / �������:</label>
						<input name="region" value="<?= $this->cart->getUser('address_region') ?>"
							   placeholder="������, ���������� ���, �������� ���, �������" id="step2_region" class="input registration__input" type="text">
					</div>
					<div class="input-item line part">
						<label for="select_part" class="input-item__header">�������� �����:</label>
						<select name="part" id="select_part" class="input select delivery__select">
							<? foreach ($this->capital_districts as $letter => $capital): ?>
								<? foreach ($capital as $key => $item): ?>
									<option <?= ($this->cart->getUser('address_part') == $key) ? 'SELECTED' : '' ?>
										style="display: inline" data-location="<?= $letter ?>"
										value="<?= $key ?>"><?= $item ?></option>
								<? endforeach; ?>
							<? endforeach; ?>
						</select>
						<div class="other" style="display: none">
							<div style="clear: both"></div>
							<br>
							<label for="other" style="width:120px;float:left;" class="input-item__header">������:</label>
							<input style="padding:2px;font: 11px Arial;width:300px"
								   value="<? print_r($this->cart->getUser('address_other')) ?>"
								   type="text" name="other" id="other">
						</div>
					</div>
					<div class="input-item">
						<label for="step2_town" class="input-item__header">����� / �����:&nbsp;<span class="star">*</span></label>
						<input name="city" value="<?= $this->cart->getUser('address_city') ?>" id="step2_town" class="input registration__input"
							   placeholder="������� ���� �����" type="text">
					</div>
				</div>
				<div class="line settlement" style="display:none">
					<label for="step2_settlement">���������� �����:</label>
					<input name="settlement" value="<?= $this->cart->getUser('address_settlement') ?>"
						   id="step2_settlement" type="text">
				</div>
                <div class="delivery__options-container">
					<div class="delivery__section-header">�������� ������� ������ �������� � ���
						������:
					</div>
					<div id="couriers__wrapper">
						<div class="dostavka_item" id="courier" data-type="��������" data-modal="courier">
							<div class="delivery__option-header">�������� ��������</div>
							<div class="delivery__option r">
								<table class="delivery__option-item">
									<thead class="delivery__option-item-header">
										<tr>
											<th class="delivery__option-item-title">������ ��������</th>
											<th class="delivery__option-item-title">����</th>
											<th class="delivery__option-item-title">����</th>
										</tr>
									</thead>
									<tbody class="courier__table-body"></tbody>
										<tr style="display: none;">
											<td class="tooltip_bg_blue">
												<div data-width="250" class="dostavka_item__checkbox" data-toggle="tooltip"
													 data-content="�������� ��-�� � 10 �� 20:00. � ���� �������� ��� �������� ���-����������� � ������� �������� ��������, ������� ������������ ��� ��������.">
													b2bpl
												</div>
											</td>
											<td>
												<div class="">4 ��.</div>
											</td>
											<td>117 �</td>
										</tr>
								</table>
							</div>
							<div class="delivery__option-add dop_info">
								<div class="delivery__form-content">
									<div class="input-item" style="width: 100%;">
										<label class="input-item__header">�����:&nbsp;<span class="star">*</span></label>
										<input name="street" value="<?= $this->cart->getUser('address_street') ?>"
											   type="text" class="input" style="width: 100%;">
									</div>
								</div>
								<div class="delivery__form-content">
									<div class="input-item">
										<label class="input-item__header" for="">���:&nbsp;<span class="star">*</span></label>
										<input name="house" value="<?= $this->cart->getUser('address_house') ?>" type="text"
											   class="input registration__input" style="width: 72px;">
									</div>
									<div class="input-item line">
										<label class="input-item__header">������:</label>
										<input name="corpus" value="<?= $this->cart->getUser('address_corpus') ?>"
											   type="text" class="input registration__input" style="width: 72px;">
									</div>
									<div class="input-item">
										<label class="input-item__header">��������:</label>
										<input name="stroenie" value="<?= $this->cart->getUser('address_stroenie') ?>"
											   type="text" class="input registration__input" style="width: 72px;">
									</div>
									<div class="input-item">
										<label class="input-item__header">��������:&nbsp;<span class="star">*</span></label>
										<input name="flat" value="<?= $this->cart->getUser('address_flat') ?>" type="text"
											   class="input registration__input" style="width: 72px;">
									</div>
									<div class="input-item">
										<label class="input-item__header">����� ��������:</label>
										<input name="domophone" value="<?= $this->cart->getUser('address_domophone') ?>"
											   type="text" class="input registration__input" style="width: 72px;">
									</div>
									<div class="input-item">
										<label class="input-item__header">������:</label>
										<input name="regs_postcode" value="<?= $this->cart->getUser('postcode') ?>"
											   type="text" class="input registration__input" style="width: 236px;">
									</div>
									<div class="input-item" style="display: none;">
										<label class="input-item__header">��������� ������� �����:</label>
										<select name="metro" class="input select delivery__select">
											<option value="0">�� �������</option>
											<? foreach ($this->aMetro as $metro): ?>
												<option class="metro-1 metro-option"
														value="<?= $metro['id'] ?>"><?= $metro['name'] ?></option>
											<? endforeach; ?>
										</select>
									</div>
								</div>
							</div>
						</div>

						<div class="dostavka_item" id="post" data-type="�����" data-modal="post">
							<div class="delivery__option-header">����� ������</div>
							<div class="delivery__option r">
								<table class="delivery__option-item">
									<thead class="delivery__option-item-header">
									<tr>
										<th class="delivery__option-item-title">������ ��������</th>
										<th class="delivery__option-item-title">����</th>
										<th class="delivery__option-item-title">����</th>
									</tr>
									</thead>
									<tbody class="courier__table-body"></tbody>
									<tr style="display: none;">
										<td class="tooltip_bg_blue">
											<div class="dostavka_item__checkbox" data-width="250" data-toggle="tooltip"
												 data-content="������ ���������� � ������� 1 ��� ���������� �������� 1 ������. �������� � �������� ����� �� ������� �� ����� �������� ���������. � ������ �������� ������� �� �������� ��������� �� ����� ������. ��� ������ ���������� �������� ����� ������ ������� �� 2 �� 5% �������� �� ������� ����� �� ��� ����.">
												����� ������
											</div>
										</td>
										<td>
											<div>5-12 ��.</div>
										</td>
										<td>
											<div style="cursor:pointer" data-toggle="tooltip"
												 data-content="����� �������� ��������� ��������" class="green_label">
												117 �
											</div>
										</td>
									</tr>
								</table>
							</div>
							<div class="delivery__option-add dop_info">
								<div class="delivery__form-content">
									<div class="input-item" style="width: 100%;">
										<label class="input-item__header">�����:&nbsp;<span class="star">*</span></label>
										<input name="street" value="<?= $this->cart->getUser('address_street') ?>"
											   type="text" class="input" style="width: 100%;">
									</div>
								</div>
								<div class="delivery__form-content">
									<div class="input-item">
										<label class="input-item__header" for="">���:&nbsp;<span class="star">*</span></label>
										<input name="house" value="<?= $this->cart->getUser('address_house') ?>" type="text"
											   class="input registration__input" style="width: 72px;">
									</div>
									<div class="input-item line">
										<label class="input-item__header">������:</label>
										<input name="corpus" value="<?= $this->cart->getUser('address_corpus') ?>"
											   type="text" class="input registration__input" style="width: 72px;">
									</div>
									<div class="input-item">
										<label class="input-item__header">��������:</label>
										<input name="stroenie" value="<?= $this->cart->getUser('address_stroenie') ?>"
											   type="text" class="input registration__input" style="width: 72px;">
									</div>
									<div class="input-item">
										<label class="input-item__header">��������:&nbsp;<span class="star">*</span></label>
										<input name="flat" value="<?= $this->cart->getUser('address_flat') ?>" type="text"
											   class="input registration__input" style="width: 72px;">
									</div>
									<div class="input-item">
										<label class="input-item__header">����� ��������:</label>
										<input name="domophone" value="<?= $this->cart->getUser('address_domophone') ?>"
											   type="text" class="input registration__input" style="width: 72px;">
									</div>
									<div class="input-item">
										<label class="input-item__header">������:</label>
										<input name="regs_postcode" value="<?= $this->cart->getUser('postcode') ?>"
											   type="text" class="input registration__input" style="width: 236px;">
									</div>
									<div class="input-item" style="display: none;">
										<label class="input-item__header">��������� ������� �����:</label>
										<select name="metro" class="input select delivery__select">
											<option value="0">�� �������</option>
											<? foreach ($this->aMetro as $metro): ?>
												<option class="metro-1 metro-option"
														value="<?= $metro['id'] ?>"><?= $metro['name'] ?></option>
											<? endforeach; ?>
										</select>
									</div>
								</div>
							</div>
						</div>

						<div class="dostavka_item" id="self" data-type="���������" data-modal="self">
							<div class="delivery__option-header">���������</div>

							<div class="delivery__option r">
								<table class="delivery__option-item">
									<thead class="delivery__option-item-header">
									<tr>
										<th class="delivery__option-item-title">������ ������� ������</th>
										<th class="delivery__option-item-title">����</th>
										<th class="delivery__option-item-title">����</th>
									</tr>
									</thead>
									<tbody class="courier__table-body"></tbody>
								</table>
							</div>

							<div class="dop_info" style="display: none!important;">
								<div style="padding:15px 0;">�������� �� ����� ������������ ��� ����� ���������� �
									�������
									"�������"
								</div>
								<div id="self_selected"></div>
								<div id="dostavka_map" style="">

								</div>
							</div>
						</div>
					</div>
					<input type="hidden" name="courier_address_id" id="courier_address_id" value=""/>
                </div>
            </form>
            <div style="left: 30px;top: 180px; margin-left: 650px; width: 255px;display: none;" class="order_tooltip">
                ����� ��������� ���������� ������, ��� ���������� ������� ������ ��������: ��������� �������������� ������ � ����� �������� � �������� �������� ������� ������ ��������
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">����������</h4>
                    </div>
                    <div class="modal-body">
                        ���� � ��� �� �����-���� ������� �� ���������� ��������� �����, ��� �� ������������ � ������ ������� ��������, �� ����������) �� ��� ����� ��� ����� � �������� � ���� �� �������� � ��������� �����. ��������� ������� ��� ������� ����� �������� � ������� ������� ��������.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">�������</button>
                    </div>
                </div>
            </div>
        </div>
        <? include('step2_sidebar.php'); ?>
</div>
<script>
    /**
     * ������� ���������� ����� �������� �� ���������� ������ ������������
     * ��� �� �� ��� ��������� ������, ������� ��������� ��� ��������� �������� �� ������
     * ������ �������� ������, ���� ������� ��� ������ �� ���������� ������
     *
     * @param {Number} kladr_id - ������������� ������������� ������
     * @param {String} city - �������� ������
     * @param {Object} resp - ������ XHR ������� �� ��������� "������������� ��������"
     */
    function fillCouriers(kladr_id, city, resp) {
            AddressSuggestions.getLogistic(kladr_id, city).then(function () {

                var $wrapper = $('#couriers__wrapper'),
                    $logistic = $('#self_selected'),
                    arr = [];

                if (resp.delivery_type != "���������") {
                    $wrapper.find('.courier__row[data-courier="' + resp.id + '"]').find('.dostavka_item__checkbox').trigger('click');//.addClass('checked');
                } else {

                    arr.push("<div class='wrr'>");
                    arr.push("<span>�� �������: <b>" + resp.courier_name + "</b></span>");
                    arr.push("<span>����� �������� �� " + resp.delivery_days + " ��.</span>");
                    arr.push("<span>��������� �������� �� " + resp.delivery_cost_3 + " �.</span>");
                    arr.push("</div>");
                    $logistic.html(arr.join(""));

                    isDeliveryCostInited = true;
                }

                $("#courier_address_id").val(resp.id);
            });
    }

    $.ajax({
        url: "/api/logistic_get/",
        data: {
            k: $("#courier_address_id").val()
        },
        success: function (resp) {

            // ���� ����� �������, �� ���� ��� ������ ������ ��������
            if (resp.id != null && resp.id != 0) {

                fillCouriers(resp.kladr_id_city, resp.name_city, resp);

                // ���� �� ������ �������� �� ��� ������ ������ ������ �� ������ �����
            } else {

                var addr = [],
                    result,
                    answer,
                    $cart_step2 = $('#cart_step2');

                console.log()

                addr.push($cart_step2.find('[name="region"]').val());
                // ���������� ������ ������ �� ����� ������������
                if ($cart_step2.find('[name="region"]').val() != $('#step2_town').val()) {
                    addr.push($('#step2_town').val());
                }
                addr.push($('#step2_settlement').val());

                answer = DadataApi.clean(addr.join(" "));
                answer.then(function (msg) {

                    if (typeof msg != 'undefined') {
                        result = msg.suggestions[0]['data'];
                        fillCouriers(result.kladr_id, addr, resp);
                    }
                });
            }
        }
    });

    $('#orders').on('submit',function(e){
        //e.preventDefault();
        if ($("#orders").data('validate') !== true){
            $('.order_tooltip').show();

            $('.select_delivery').addClass('highlight');
            $('.scroll_to_city').click();

            return false;
        }
        $('.order_tooltip').hide();
    });

    $('.submit-js').on('click',function(e){
        e.preventDefault();
        $('#orders').submit();
    });
	
	$(function() {
		$('.delivery__option-add').hide();

		$('.courier__table-body').click(function() {
			$('.dostavka_item').children('.delivery__option-add').slideUp();
			if ($(this).parents('.dostavka_item').attr("class") == "dostavka_item active") {
				$('.dostavka_item').removeClass('active');
				$(this).parents('.dostavka_item').children('.delivery__option-add').slideUp();
			} else {
				$('.dostavka_item').removeClass('active');
				$(this).parents('.dostavka_item').addClass('active');
				$(this).parents('.dostavka_item').children('.delivery__option-add').slideDown();
			}
		});
	});
</script>
