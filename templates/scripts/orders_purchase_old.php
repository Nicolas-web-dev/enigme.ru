<?
$disp = (@$_SESSION['_GLCID'] == 4) ? ' style="display:none;"' : '';
?>

<div class="bread_crumps" style="width:917px">
    <a href="/">�������</a>&nbsp;/
    <a href="/orders/">������� �������</a>&nbsp;/
    ���������� ������
</div>
<div class="news">
<br/>

<?php if (isset($this->korzina_message)) { ?>
    <div class="news">
        <br/>
        <strong>� ������ ������ ���� ������� �����.</strong> <br/><br/>

        ��� ���������� ������� ���������� ������� ������. <br/>
        ��� ����� ������� ����������� ���������: <br/>
        - ����� <a href="http://enigme.ru/catalog/">������� �������</a>; <br/>
        - ����� <a href="http://enigme.ru/girl/">������� ������� ��������</a>;<br/>
        - ��� <a href="http://enigme.ru/men/">������� ��������</a>;<br/>
        - ��������� ����� �� ����� (� ������� ������ ����);<br/><br/>

        <strong>������ ��� �������� �������!</strong><br/>
    </div>
<?php } else { ?>

    <br/>


    <div class="b-purchase left">
        <h3 style="font-size:20px;line-height:20px">���������� ������ <br/><br/>(�� ����� -
            <strong data-sum="<? echo $this->discount['big_sum']; ?>" id="delivery_cost-calc"><? echo $this->discount['big_sum']; ?></strong> ���, � ���������� -
            <strong><? echo $this->korzinaS_kol; ?></strong> ��)</h3>

        <span class="desc-text" style="font-size:11px">��� �������������������� �������������</span><br/>


        <form style="font-size:12px" action="/orders/purchase/send-yes/" autocomplete="off" method="post"
              enctype="application/x-www-form-urlencoded" id="reg_form_new" name="reg_form_new">

            <div class="enter_d1"><label for="log_fio2" style="width:120px;display:block;float:left;">��� *:</label>
                <input class="required" style="padding:2px;font: 11px Arial;width:380px" value="<? echo $this->data['fio']; ?>"
                       type="text" name="regs_fio" id="log_fio2">
                <div class="error_descr" style="display:none;color:red;font-size:11px;">
                    <strong>������� ���������� ���� ���</strong>
                </div>

                <? if (isset($this->err_fio)) { ?>
                    <br/><span style="color:red;font-size:11px;"><strong><? echo $this->err_fio; ?></strong></span>
                <? } ?>
            </div>
            <br/>


            <div class="enter_d1"><label for="login2" style="width:120px;display:block;float:left;">E-mail *:</label>
                <input class="required" style="padding:2px;font: 11px Arial;width:380px" value="<? echo $this->data['email']; ?>"
                       type="text" name="regs_email" id="login2">
                <div class="error_descr" style="display:none;color:red;font-size:11px;">
                    <strong>������� ���������� ��� email</strong>
                </div>
                <? if (isset($this->err_email)) { ?>
                    <br/><span
                        style="color:red;font-size:11px;">&nbsp;&nbsp;<strong><? echo $this->err_email; ?></strong></span>
                <? } ?>
            </div>
            <br/>

            <div class="enter_d1"><label for="phone2" style="width:120px;display:block;float:left;">������� *:</label>
                <input class="phone_input required" value="<? echo $this->data['phone']; ?>" style="padding:2px;font: 11px Arial;width:380px"
                       type="text" name="regs_phone" id="phone2">
                <div class="error_descr" style="display:none;color:red;font-size:11px;">
                    <strong>������� ���������� ����� ������ ��������</strong>
                </div>

                <? if (isset($this->err_phone)) { ?>
                    <br/><span
                        style="color:red;font-size:11px;">&nbsp;&nbsp;<strong><? echo $this->err_phone; ?></strong></span>
                <? } ?>
            </div>
            <br/><br/>


            <div>
                <div class="region" style="margin-bottom: 40px">
                    <label for="region" style="width:120px;display:block;float:left;">������/������� *:</label>
                    <input class="required" style="padding:2px;font: 11px Arial;width:300px" value="<?=$this->data['address-region']; ?>"
                           type="text" name="region" id="region">
                    <div class="error_descr" style="display:none;color:red;font-size:11px;">
                        <strong>������� ���������� �������</strong>
                    </div>
                </div>

                <div class="part" style="margin-bottom: 40px">
                    <label for="part" style="width:120px;display:block;float:left;">�������� ����� *:</label>
                    <select  name="part" id="select_part" style="padding:2px;">
                        <? foreach ($this->capital_districts as $letter => $capital): ?>
                            <? foreach ($capital as $key => $item): ?>
                                <option  <?=($this->data['address-part']==$key)?'SELECTED':''?> style="display: none" data-location="<?= $letter ?>"
                                                                                                value="<?= $key ?>"><?= $item ?></option>
                            <? endforeach; ?>
                        <? endforeach; ?>
                    </select>

                    <div class="other" style="display: none">
                        <div style="clear: both"></div>
                        <br>
                        <label for="other" style="width:120px;float:left;">������:</label>
                        <input style="padding:2px;font: 11px Arial;width:300px" value="<?=$this->data['address-other']; ?>"
                               type="text" name="other" id="other">
                    </div>
                </div>

                <div class="city" style="margin-bottom: 40px">
                    <label for="city" style="width:120px;display:block;float:left;">����� / ����� *:</label>
                    <input class="" style="padding:2px;font: 11px Arial;width:300px" value="<?=$this->data['address-city']; ?>"
                           type="text" name="city" id="city">
                    <div class="error_descr" style="display:none;color:red;font-size:11px;">
                        <strong>������� ��� �����</strong>
                    </div>
                </div>

                <div class="settlement" style="margin-bottom: 40px;display: none">
                    <label for="settlement" style="width:120px;display:block;float:left;">���������� ����� *:</label>
                    <input class="" style="padding:2px;font: 11px Arial;width:300px" value="<?=$this->data['address-settlement']; ?>"
                           type="text" name="settlement" id="settlement">
                    <div class="error_descr" style="display:none;color:red;font-size:11px;">
                        <strong>������� ��� ���������� �����</strong>
                    </div>
                </div>


                <div class="street" style="margin-bottom: 40px">
                    <label for="street" style="width:120px;display:block;float:left;">����� *:</label>
                    <input class="required" style="padding:2px;font: 11px Arial;width:300px" value="<?=$this->data['address-street']; ?>"
                           type="text" name="street" id="street">
                    <div class="error_descr" style="display:none;color:red;font-size:11px;">
                        <strong>������� ���������� ���� �����</strong>
                    </div>
                </div>


                <div class="house" style="margin-bottom: 40px">
                    <label for="house" style="width:120px;display:block;float:left;">���:</label>
                    <input style="padding:2px;font: 11px Arial;width:300px" value="<?=$this->data['address-house']; ?>"
                           type="text" name="house" id="house">
                    <br><br>
                    <label for="corpus" style="width:120px;display:block;float:left;">������:</label>
                    <input style="padding:2px;font: 11px Arial;width:300px" value="<?=$this->data['address-corpus']; ?>"
                           type="text" name="corpus" id="corpus">
                    <br><br>
                    <label for="stroenie" style="width:120px;display:block;float:left;">��������:</label>
                    <input style="padding:2px;font: 11px Arial;width:300px" value="<?=$this->data['address-stroenie']; ?>"
                           type="text" name="stroenie" id="stroenie">
                    <br><br>
                    <label for="floor" style="width:120px;display:block;float:left;">����:</label>
                    <input style="padding:2px;font: 11px Arial;width:300px" value="<?=$this->data['address-floor']; ?>"
                           type="text" name="floor" id="floor">
                    <br><br>
                    <label for="flat" style="width:120px;display:block;float:left;">����� ��������:</label>
                    <input style="padding:2px;font: 11px Arial;width:300px" value="<?=$this->data['address-flat']; ?>"
                           type="text" name="flat" id="flat">
                    <br>
                    <br>
                    <label for="domophone" style="width:120px;display:block;float:left;">����� ��������:</label>
                    <input style="padding:2px;font: 11px Arial;width:300px" value="<?=$this->data['address-domophone']; ?>"
                           type="text" name="domophone" id="domophone">
                </div>
                <div class="regs_postcode house">
                    <label for="regs_postcode" style="width:120px;display:block;float:left;">������:</label>
                    <input style="padding:2px;font: 11px Arial;width:300px" value="<?=$this->data['postcode']; ?>"
                           type="text" name="regs_postcode" id="regs_postcode">
                </div>

                <br>
                <br>
            </div>

            <br>
            <br>
            <br>
            <br>
            <br>


            <div<?= $disp ?>>
                <div class="enter_d1">
                    <label for="comm2" style="width:120px;display:block;float:left;">����������� <br/>� ������:</label>
                    <textarea style="padding:2px;font: 11px Arial;width:380px;height:80px;border:1px solid black"
                              type="text" name="regs_comment" id="comm2"></textarea>
                </div>
                <br/>

                <div>
                    <input id="pass2" class="pass-rad" style="width:18px;position:relative;top:-1px" type="radio"
                           name="regs_p"
                           value="1" <? if ($this->data['p'] == 1 OR !isset($this->data['p'])) echo 'checked'; ?> />
                    <label style="padding-right:50px" for="pass2">������ ������������� ������������� (���������� ��
                        �����)</label><br/>

                    <input id="pass3" class="pass-rad" style="width:18px;position:relative;top:-1px;margin-top:2px"
                           type="radio" name="regs_p" value="2" <? if ($this->data['p'] == 2) echo 'checked'; ?> />
                    <label for="pass3">������ ������</label>
                </div>
                <br/>

                <div style="<? if ($this->data['p'] == 1 OR !isset($this->data['p'])) echo 'display:none;'; ?>"
                     id="pass-wrap" class="enter_d1">
                    <label for="pass4" style="width:120px;display:block;float:left;">������ *:</label>
                    <input style="padding:2px;font: 11px Arial;width:380px" value="" type="password" name="regs_passw"
                           id="pass4"><br/><br/>

                    <label for="pass5" style="width:120px;display:block;float:left;">������ ������ *:</label>
                    <input style="padding:2px;font: 11px Arial;width:380px" value="" type="password" name="regs_repass"
                           id="pass5">

                    <? if (isset($this->err_passw)) { ?>
                        <br/><span
                            style="color:red;font-size:11px;">&nbsp;&nbsp;<strong><? echo $this->err_passw; ?></strong></span>
                    <? } ?>
                    <br/><br/>
                </div>

                <div>
                    <input style="width:15px;margin-right:5px;" id="regs_maillist" type="checkbox"
                           name="regs_maillist" <? if ($this->data['is_maillist'] == 1 OR !isset($this->data['is_maillist'])) echo 'checked'; ?> />
                    <label for="regs_maillist">�������� �� ������� � �����</label><br/>
                </div>
                <br/>
            </div>
            <div>
                <div style="float:left;position:relative;"><a class="why-need" href="">����� ����� �����������?</a>

                    <div id="why-need-popup">������������������ ������������ ����� ���������� ��� ���������������������:<br/>
                        - ����������� ������������ ��������� ������;<br/>
                        - ����������� ���������� ������ � ���������;<br/>
                        - ������ ������� ������, � ����������� �� ����� ��������� �������;<br/>
                        - ������������� ������;<br/>
                        - ����������� ��� �� ������ � ������ ������������� ������;
                    </div>
                </div>
                <div style="float:right;padding-right:10px;">
                    <input type="hidden" name="register">
                    <input style="cursor:pointer;font-size:14px;height:30px;"  type="submit" name="register_submit"
                           value="����������"/>
                </div>
            </div>

            <input hidden name="fio_data"
                   value="<? echo htmlentities($this->data['fio_data'], ENT_COMPAT, 'cp1251'); ?>">
            <input hidden name="address_data"
                   value="<? echo htmlentities($this->data['address_data'], ENT_COMPAT, 'cp1251'); ?>">

            <input type="hidden" id="courier_address_id" name="courier_address_id" value="<?= $this->data['courier_address_id']; ?>"/>
            <input type="hidden" id="delivery_cost" name="delivery_cost" value="<?= $this->data['delivery_cost']; ?>"/>

            <div class="clear"></div>
        </form>
    </div>
    <div class="b-purchase right" style="text-align:center;border:0">
        <a href="" id="show2form"
           style="background:#9966cc;height:30px;line-height:30px;display:inline-block;padding:0 25px;color:white;text-decoration:none;font-size:16px;border-radius:6px">����������
            ����������?</a>
            <script src="/templates/js/jquery.maskedinput-1.2.2.js"></script>
        <script>
            $(function () {
                check_form($("#reg_form_new"))
                $("#show2form").click(function (e) {
                    e.preventDefault()
                    $(this).parent().slideUp(300)
                    $(this).parent().next().slideDown(300)
                })

                //suggetions fio
                $("input[name=regs_fio]").suggestions({
                    serviceUrl: DadataApi.DADATA_API_URL + "suggest/fio",
                    selectOnSpace: true,
                    token: "15309822a7c43aacc9f604670c10489fc4b19a0b",
                    /* ����������, ����� ������������ �������� ���� �� ��������� */
                    onSelect: function(suggestion) {
                        $('input[name=fio_data]').val(JSON.stringify(suggestion));
                    },
                    onSelectNothing: function (query) {
                        console.log(query)
                    }
                });

                // ��������� ������ �� ��� ������ ������ ���� � ��� � ������
                $('#select_part').on('change', function(e) {

                    var addr = $("#region").val();
                    addr += ", "+$(this).find("option[value='"+$(this).val()+"']").text();

                    AddressSuggestions.getLogistic($(this).val(), addr);
                });

                //index suggest
                AddressSuggestions.initForm();

            })
        </script>
        <style>
            .error_descr {
                padding-left:120px;
            }
            .footer_cols {
                display: none
            }
            input.error, select.error {
                border: 1px solid red;
            }
            #footer {
                height: 55px;
                margin: -55px auto 0;
                border-top: 1px solid #000;
            }

            .footer_inner {
                height: 45px;
                padding-top: 10px;
            }

            #rebrand #top_menu {
                display: none
            }

            #rebrand #brands {
                display: none
            }

            #rebrand #head_fon, #rebrand #header {
                height: 150px
            }

            body .b-purchase.left input[type=text] {
                padding: 5px !important;
                width: 375px !important;
            }

            body .b-purchase.right input[type=text] {
                padding: 5px !important;
                width: 220px !important;
            }

            body .b-purchase.right input[type=password] {
                padding: 5px !important;
                width: 220px !important;
            }

            #middle {
                padding: 0 0 100px;
            }

            .enter_d1 label {
                padding-top: 4px;
                font-size: 13px;
            }
        </style>
    </div>
    <div class="b-purchase right" style="display:none">
        <h3>�������� ����� ����� ������ �������</h3>
        <span class="desc-text" style="font-size:11px">��� ������������������ �������������</span>

        <p style="font-size:12px">���� �� ������� ���� e-mail � ������, �� ������� �� � ��������������� ����</p>

        <form style='font-size:12px' action="/users/login/" autocomplete="off" method="post"
              enctype="multipart/form-data" name="reg_form_purchase">

            <div class="enter_d1"><label for="login1" style="width:80px;display:block;float:left;">e-mail</label>
                <input style="padding:2px;font: 11px Arial;width:230px" type="text" name="log_email" id="login1"></div>
            <br/>

            <div class="enter_d1"><label for="pass1" style="width:80px;display:block;float:left;">������</label>
                <input style="padding:2px;font: 11px Arial;width:230px" type="password" name="log_passw" id="pass1">
            </div>
            <br/>

            <div style="overflow:hidden">
                <div style="float:left;"><a class="forgot" href="/users/forgot-password/">������ ������?</a></div>
                <div style="float:right;padding-right:30px"><input style="cursor:pointer;font-size:14px;height:30px;"
                                                                   type="submit" name="register" value="����������"/>
                </div>
            </div>

            <input type="hidden" name="back_url" value="purchase"/>
        </form>

    </div>
    <div style="margin-left:525px;margin-top:20px;">
        <div class="inf" style="font-size:12px">
            
                <? echo $this->dostavka['text_2']; ?>
                <!--h2 class="georgia_head slider_text"
                    style="font-size:20px;line-height:20px;cursor:pointer;text-decoration:underline">�������� ��
                    ������</h2>

                <div style="display:none">
                    <? echo $this->dostavka['text_3']; ?>
                </div-->
                <div id="logistic">
                    <div class="logistic__type" data-type="���������" data-modal="byself">
                        <div class="logistic__type-header">���������</div>
                        <div class="logistic__type-body"></div>
                        <div class="logistic__type-footer">
                            <span class="type-cost"></span>
                            <span class="type-disabled_text">������ ������ �������� ���������� ��� ������ �������.</span>
                        </div>
                    </div>
                    <div class="logistic__type" data-type="��������" data-modal="courier">
                        <div class="logistic__type-header">������</div>
                        <div class="logistic__type-body"></div>
                        <div class="logistic__type-footer">
                            <span class="type-cost"></span>
                            <span class="type-disabled_text">������ ������ �������� ���������� ��� ������ �������.</span>
                        </div>
                    </div>
                    <div class="logistic__type" data-type="�����" data-modal="post">
                        <div class="logistic__type-header">����� �����</div>
                        <div class="logistic__type-body"></div>
                        <div class="logistic__type-footer">
                            <span class="type-cost"></span>
                            <span class="type-disabled_text">������ ������ �������� ���������� ��� ������ �������.</span>
                        </div>
                    </div>
                </div>
            
        </div>
    </div>
    <div class="clear"></div>
    <div style="font-size:12px;line-height:18px">
        ����, ���������� &laquo;*&raquo; - ����������� ��� ����������!
        <br/><br/>
        ����� ����������� �� �������� ������ � ���������������� �������.
        <br/><br/>
        ������ ��������, ���������� � ������������ ��������-�������� ��� ����������� ��� �����-���� ���� �������, ��
        ����� ��� ���������� ������������� ������������ ������� ������������ � ����� �� ����������� ��������, �����
        ����� ������� ����� ��� �������� �������.
    </div>
<?php } ?>
</div>
<div class="g-hidden">
    <div id="couriers" class="box-modal couriers__modal">
        <a class="box-modal_close arcticmodal-close"><span></span></a>
        <div class="couriers__block">
            <div id="courier" class="courier__block">
                <div class="courier__title">�������� ��������</div>
                <table class="courier__table">
                    <thead class="courier__table-header">
                        <tr>
                            <th>������ ��������</th>
                            <th>����</th>
                            <th>����</th>
                        </tr>
                    </thead>
                    <tbody class="courier__table-body"></tbody>
                </table>
            </div>
            <div id="byself" class="courier__block">
                <div class="courier__title">������ ����������</div>
                <table class="courier__table">
                    <thead class="courier__table-header">
                    <tr>
                        <th>������ ��������</th>
                        <th>����</th>
                        <th>����</th>
                    </tr>
                    </thead>
                    <tbody class="courier__table-body"></tbody>
                </table>
            </div>
            <div id="post" class="courier__block">
                <div class="courier__title">����� ������. 1 �����</div>
                <table class="courier__table">
                    <thead class="courier__table-header">
                    <tr>
                        <th>������ ��������</th>
                        <th>����</th>
                        <th>����</th>
                    </tr>
                    </thead>
                    <tbody class="courier__table-body"></tbody>
                </table>
            </div>
        </div>
        <div id="map"></div>
    </div>
</div>
<script>

    var yMap,
        ymapsDef = $.Deferred(),
        ymapsResolved = false;

    function initYmaps() {

        yMap = new ymaps.Map('map', {
            center: [55.753994, 37.622093],
            zoom: 8,
            controls: ['zoomControl', 'typeSelector']
        });

        ymapsDef.resolve();
    }

    $.ajax({
        url: "/api/logistic_get/",
        data: {
            k: $("#courier_address_id").val()
        },
        success: function(response) {

            var resp = response;

            // ���� ����� �������, �� ���� ��� ������ ������ ��������
            if (resp.id != null) {

                AddressSuggestions.getLogistic(resp.kladr_id_city, resp.name_city, false).then(function() {

                    var $logistic = $('.logistic__type'),
                        type = "";

                    // ������� ��
                    $logistic.removeClass('logistic__type-active');
                    $logistic.find('.type-cost').text("");
                    $logistic.find('.logistic__type-body').text("");

                    if (resp.delivery_type == "���������") {
                        type = "byself";
                    } else if (resp.delivery_type == "��������") {
                        type = "courier";
                    } else {
                        type = "post";
                    }

                    // ������� � ������ ����������� ������
                    $.each($logistic, function(i, item) {

                        var $item = $(item);

                        if ($item.attr('data-modal') == type) {

                            $item.addClass('logistic__type-active');
                            $item.find('.type-cost')
                                .text(resp.delivery_cost+"���., "+resp.delivery_days+" ��.");

                            $item.find('.logistic__type-body').text(resp.courier_name);
                        }
                    });
                });
            }
        }
    });
</script>