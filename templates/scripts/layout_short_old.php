<?php require_once(__DIR__ . '/../../includes/geo_domain.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">

<head>
    <script>
        _udn = ".enigme.ru";
    </script>

    <title><?php echo geo_replace($city, $this->main['meta_title']); 	?></title>
    <meta name="description" content='<?php echo geo_replace($city, $this->main['meta_description']); ?>'/>
    <meta name="keywords" content="<?=geo_replace($city, $this->main['meta_keywords']); ?>"/>

    <? if ($this->main['meta_noindex'] == true) echo '<meta name="robots" content="noindex"/>' ?>
    <? if ($this->main['og:title'] != '') echo '<meta property="og:title" content="' . geo_replace($city, $this->main['og:title']) . '" /> ' ?>
    <? if ($this->main['og:image'] != '') echo '<meta property="og:image" content="' . $this->main['og:image'] . '" /> ' ?>
    <? if ($this->main['og:description'] != '') echo '<meta property="og:description" content="' . geo_replace($city, $this->main['og:description']) . '" /> ' ?>
    <? if ($this->main['og:url'] != '') echo '<meta property="og:url" content="http://'. $host . $this->main['og:url'] . '" /> ' ?>


    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
    <meta name="google-site-verification" content="L4HRi6k9b9NEITAlU4iI4tYExUfLLY9RR5ZRbVURHsw"/>
    <meta name="robots" content="noyaca"/>
    <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
    <meta name='wmail-verification' content='72d530550fbb2ed9'/>


    <link href='http://fonts.googleapis.com/css?family=Cuprum&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="/templates/js/bootstrap-select/bootstrap-select.min.css">


    <link rel="stylesheet" href="/templates/short/css/style_new.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/templates/short/css/cart.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/templates/short/bootstrap_full/css/bootstrap.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/templates/js/arcticmodal/jquery.arcticmodal-0.3.css" type="text/css" media="screen"/>


    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico"/>

    <link href="/templates/css/suggestions.css" type="text/css" rel="stylesheet"/>

    <script language="javascript" type="text/javascript" src="/js/jquery/jquery-1.3.2.min.js?v=1"></script>
    <script type="text/javascript" src="/js/jquery/jquery-1.11.0.min.js?v=1"></script>
    <script src="/js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="/templates/js/dadata/suggestions-jquery-4.1.min.js?v=1"></script>

    <script type="text/javascript" src="/templates/js/content.js?v=2"></script>
    <script type="text/javascript" src="/templates/js/dadata.js?v=2"></script>
    <script type="text/javascript" src="/templates/js/comments.js?v=2"></script>
    <script type="text/javascript" src="/templates/js/suggest.js?v=2"></script>
    <script type="text/javascript" src="/templates/js/footer.js?v=2"></script>
    <script type="text/javascript" src="/templates/js/swfobject.js?v=2"></script>
    <script type="text/javascript" src="/templates/js/combo.js?v=2"></script>
    <script type="text/javascript" src="/templates/lib/star-rating/jquery.rating.pack.js?v=2"></script>


    <script src="/templates/js/bootstrap-select/bootstrap-select.min.js"></script>

    <script src="/templates/css/bootstrap/bootstrap.min.js"></script>

    <!-- AJAX SUCCESS TEST FONCTION
        <script>function callSuccessFunction(){alert("success executed")}
                function callFailFunction(){alert("fail executed")}
        </script>
    -->
    <!--<script type="text/javascript" src="/templates/js/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="/templates/js/jScrollPane.js"></script>-->
    <!--[if IE 6]>
    <style type="text/css">
        body {
            behavior: url("/images/default/csshover.htc");
        }
    </style>
    <script type="text/javascript" src="http://enigme.ru/js/DD.js"></script>
    <script type="text/javascript">
        DD_belatedPNG.fix('.png');
    </script>
    <![endif]-->

    <script>
        var region_name = '<?=convert_region($city['region']);?>';
        var city_name = '<?=convert_city($city['name']);?>';
    </script>

    <!-- Put this script tag to the <head> of your page -->
    <!--<script type="text/javascript" src="http://userapi.com/js/api/openapi.js?18"></script>-->


    <script type="text/javascript">
        VK.init({apiId: 2024589, onlyWidgets: true});
    </script>
    <script type="text/javascript" src="http://apis.google.com/js/plusone.js">
        {
            lang: 'ru'
        }
    </script>


    <link rel="apple-touch-icon" sizes="72x72" href="/templates/css/touch-icon-ipad.png"/>
    <link rel="apple-touch-icon" sizes="114x114" href="/templates/css/touch-icon-iphone4.png"/>
    <script src="/templates/js/arcticmodal/jquery.arcticmodal-0.3.min.js?v=1"></script>



    <? /*
    <link rel="stylesheet" href="/templates/css/town_popup.css" />
    <script src="/templates/js/town_popup.js?v=1"></script>
    */ ?>
</head>

<body id="rebrand" style="overflow-x:hidden;">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TR7T5K"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TR7T5K');</script>
<!-- End Google Tag Manager -->


<div class="wrap">
    <nav class="navbar-fixed-top">
        <div class="container" style="background-color: #FFF">
            <div class="row header">
                <div class="col-md-3 text-center header_logo header_block1">
                    <div class="header_phone"><?=$city['phone']?></div>
                </div>
                <div class="col-md-3 header_block2">
                    <div class="header_order">
                        ��� ����� ������ <b><?=$this->order['id']?></b><br>
                        ����� ������ <b class="val_total"><?=$this->order['dSum']?></b> ���
                    </div>
                </div>
                <div class="col-md-3 header_block3">
                    <div class="header_delivery">
                        <span data-default="��������! ����� � ������<br>�������� �� �������.">
                            ��������! ����� � ������<br>
                            �������� �� �������.
                        </span>
                        <a class="scroll_to_city" style="position: relative;top: -10px; left: 40px;color: #f0008e;border-bottom:1px dashed #f0008e" href="">�������</a>
                    </div>

                </div>
                <div class="col-md-3 text-center header_block4">
                    <div class="header_complete">
                        <a href="" class="cart_btn1 submit-js">��������� ����������</a>

                    </div>
                </div>
            </div>
        </div>

    </nav>


    <div class="container" style="margin-top: 70px;">
        <div class="row">
            <div class="col-md-12">
            <?= $this->main['content']; ?>
            </div>
        </div>
    </div>





</div>


</body>
</html>