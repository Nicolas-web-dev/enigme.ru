<?php require_once(__DIR__ . '/../../includes/geo_domain.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="title" content="" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
</head>

<body>

<div id="wrapper">
<div id="header">
</div><!-- #header-->


	<div id="content">
	
		<div style="width:600px">
		
			<table>
				<tr>
					<td width="130"><img src="http://<?php echo $hodt; ?>/images/default/enigme_l.png"></td>
					<td valign="bottom">
						<h2 style="font: 18px/18px Tahoma;font-weight:normal">Здравствуйте, <?= iconv('windows-1251', 'utf-8', $this->data['fio']);?>!</h2>
						<font style="font: 12px/18px Tahoma">Благодарим вас за выбор интернет-магазина <a style="font: 12px/18px Tahoma;color:black" target="_blank" href="http://<?php echo $hodt; ?>/"><?php echo $hodt; ?></a></font><br>
						
						<font color="#b079df"><b style="font: 12px/18px Tahoma;font-weight:bold;color:#b079df;">
						
							Ваш заказ № <?= $this->data['unic'];?>
						
						</b></font>

					</td>
				</tr>
			</table>
			<br>
			
			<table border="0" cellpadding="3" cellspacing="0">
			
				<?php foreach ($this->data['orders'] as $orders) {?>
			
				<tr>
					<td style="font: 12px/18px Tahoma" width="400" valign="top" align="left">
						<?= geo_replace_utf8($city_utf8, iconv('windows-1251', 'utf-8', $orders['title']));?>
					</td>
					<td style="font: 12px/18px Tahoma" width="50" valign="top" align="center">
						<?= $orders['kol'];?>
					</td>
					<td style="font: 12px/18px Tahoma" valign="top" align="left">
						RUR <?= $orders['price'];?>.00<br>
					</td>
				</tr>
				
				<?php } ?>
				
				<tr>
					<td width="400" valign="top" align="left">
						<font style="font: 12px/18px Tahoma" color="#b079df"><b style="font: 12px/18px Tahoma;font-weight: bold">Сумма заказа:</b></font>
					</td>
					<td></td>
					<td valign="top" align="left">
						<font color="#b079df" size="2"><b style="font: 12px/18px Tahoma;font-weight:bold;">RUR <?php echo $this->discount['sum'];?>.00</b></font>
					</td>
				</tr>
				<tr>
					<td width="400" valign="top" align="left">
						<font style="font: 12px/18px Tahoma" color="#b079df"><b style="font: 12px/18px Tahoma;font-weight: bold">Скидка:</b></font>
					</td>
					<td></td>
					<td valign="top" align="left">
						<font color="#b079df" size="2"><b style="font: 12px/18px Tahoma;font-weight:bold;"><?php echo $this->discount['percent'];?> %</b></font>
					</td>
				</tr>
				<tr>
					<td width="400" valign="top" align="left">
						<font style="font: 12px/18px Tahoma" color="#b079df"><b style="font: 12px/18px Tahoma;font-weight: bold">Всего:</b></font>
					</td>
					<td></td>
					<td valign="top" align="left">
						<font color="#b079df" size="2"><b style="font: 12px/18px Tahoma;font-weight:bold;">RUR <?php echo $this->discount['big_sum'];?>.00</b></font>
					</td>
				</tr>
			</table>

			<br>
			<span style="font: 12px/18px Tahoma">Ваш контактный телефон: <?php echo $this->phone;?><br />
			В ближайшее время менеджер магазина Вам перезвонит.</span>
			<br>
			<br>
			
			<!--<table width="100%"><tr><td height="30" bgcolor="#b079df">
				<span style="font-family:Tahoma;font-weight:bold;font-size:14px;color:white">&nbsp;&nbsp;&nbsp;Обратите внимание на наши спецпредложения:</span>
			</td></tr></table>
			
			<br>
			
			<?php foreach ($this->catalog_mail as $catalog_item) {?>
			
			<div style="font: 12px/18px Tahoma;float:left;height:170px;max-height:170px;overflow:hidden;width:300px;">
				<div style="font-family:Tahoma;position: relative;float:left;margin-bottom:315px;margin-right:22px;margin-top:15px;width:130px;" >
					<a style="color:black;text-decoration:underline;" href="<?php echo $catalog_item['url'];?>" >
					<img style="border:0" height="130" width="130" src="<?php echo $catalog_item['img'];?>">
					</a>
				</div>
				<div style="font-family:Tahoma;">
					<a style="font-family:Tahoma;color:#B178DF;font-size:14px;font-weight:bold;text-decoration:none;width:145px;"  href="<?php echo $catalog_item['url'];?>">
					<b style="color:black;font-size:18px;font-weight:normal;font-family:Tahoma;text-decoration:none"><?php echo geo_replace_utf8($city_utf8, $catalog_item['brend_title']);?></b><br><span style="font-family:Tahoma;color:#B178DF;"><?php echo geo_replace_utf8($city_utf8, $catalog_item['title']);?></span></a>
				</div>
				<div style="font-family:Tahoma;float:left;font-size:11px;line-height:14px;padding-bottom:5px;padding-top:5px;width:145px;">
					<?php echo geo_replace($city, $catalog_item['anot']);?>
				</div>

				<div style="font-family:Tahoma;color:#B178DF;float:left;font-size:16px;font-weight:bold;padding-right:5px;width:110px;"><strong>от</strong> <?php echo $catalog_item['price_s'];?> <strong>руб</strong><br>
				<div style="font-size:11px !important"><a href="<?php echo $catalog_item['url'];?>"><span style="color:black;font:11/18px Tahoma">подробнее</span></a></div>	
				</div>
			</div>
			
			<?php } ?>-->

			<div style="clear:both;font:12px/18px Tahoma"></div><br /><br />
			телефон для справок: <strong><?=$city_utf8['phone']?></strong>
			
		</div>
		
	</div><!-- #content-->

	
	
	
	<div id="footer"></div><!-- #footer --></div><!-- #wrapper -->
</body>
</html>