<?php
/**
 * Created by PhpStorm.
 * User: loram
 * Date: 03.08.14
 * Time: 14:19
 */

require_once $base_path . "/app/Abstract.php";
require_once $base_path . "/app/User.php";
//require_once $_SERVER['DOCUMENT_ROOT'] . "/includes/library/Zend/Date.php";
//require_once $_SERVER['DOCUMENT_ROOT'] . "/includes/library/Zend/Locale.php";

class Order_Row_Item extends Row
{
    /**
     * ������������� �������� ������������� ������
     * �� �������� ����� ����������� setRow
     * @var string
     */
    protected $rowClass = "Order_Row_Item";

    /**
     * ���������� ���� �� ����� ������
     * @var array
     */
    private $_post = array(
        'delivery_days' => '5-12',
        'delivery_type' => '���������',
        'delivery_payment' => '��������� ��� ���������',
        'delivery_cost_1' => 200,
        'delivery_cost_3' => 375,
        'courier_id' => 5,
        'courier_name' => '����� ������',
        'id' => 0,
        'name_city' => null,
        'delivery_news_to_operator' => "������ ���������� � ������� 1 ��� ���������� �������� 1 ������. �������� � �������� ����� �� ������� �� ����� �������� ���������. � ������ �������� ������� �� �������� ��������� �� ����� ������. ��� ������ ���������� �������� ����� ������ ������� �������� �� 2 �� 5 % �� ������� ����� �� ��� ����."
    );

    /**
     * ����������� id � �������� ����� ������ ������
     * @var array
     */
    private $_paymentType = array(
        0 => "�� �������",
        1 => "��������� ��� ���������",
        2 => "���������� �� ���������",
        3 => "���������� Robokassa"
    );

    /**
     * ������ �� ������������� ��������
     * @var null|array
     */
    private $_logistic = null;

    /**
     * ������ ������� � ������
     * @var null|array
     */
    private $_products = null;

    /**
     * ��������� ID ������ � ���������
     * @return string
     */
    public function getFullId() {

        return $this->isRegion() ? "RE-" . $this->getRow()->id : "ME-" . $this->getRow()->id;
    }

    /**
     * ������� ��������� ������������� �������� ��� ������
     * @return array
     */
    public function getLogistic()
    {
        if ($this->_logistic == null) {

            $id = $this->courier_address_id;

            // ���� �������� �� �����������, �� ������� ����� ������
            if ((int)$id === 0) {
                $this->_logistic = $this->_post;
            } else {
                $this->_logistic = mysql_fetch_assoc(
                    mysql_query("SELECT * FROM couriers_addresses WHERE id='{$id}' LIMIT 1 ")
                );
            }
        }

        return $this->_logistic;
    }

    /**
     * ��������� ���������� � ������ ��������
     * @return string
     */
    public function getCourier()
    {
        $logistic = $this->getLogistic();
        $courier = mysql_fetch_assoc(
            mysql_query("SELECT * FROM couriers WHERE couriersId='{$logistic[courier_id]}' LIMIT 1 ")
        );

        if ($courier === false) {
            return "�� ������";
        }

        return $courier['name'];
    }

    /**
     * ��������� ���� ������ ����������
     * @return string|null
     */
    public function getDeliveryCode()
    {
        $logistic = $this->getLogistic();
        if (!isset($logistic['delivery_code'])) {
            return null;
        }

        $delivery_code = preg_replace("/\.0000/", "", $logistic['delivery_code']);
        return $delivery_code == 0 ? null : $delivery_code;
    }

    /**
     * ��������� ���� �������� � ������� dd-MM-YYYY, HH:mm
     * ���� ��������� ��������� � ���, ��� ���� �������� �� �������
     * @return string
     */
    public function getDeliveryDate()
    {
        // ��������� ������������� ����
        if (!isset($this->Delivery_Date)) {
            $this->Delivery_Date = $this->DateEnd;
        }

        if ($this->Delivery_Date === "" || $this->Delivery_Date === "0000-00-00 00:00:00") {
           return "�� �������";
        }

        $locale = new Zend_Locale("ru_RU");
        $date = new Zend_Date($this->Delivery_Date, null, $locale);
        return $date->toString("dd-MM-YYYY, HH:mm");
    }

    /**
     * ��������� ���� ������� � ������
     * @return array|null
     */
    public function getProducts()
    {

        if ($this->_products == null) {

            $id = $this->id;
            $sql = mysql_query("SELECT * FROM m_mag_OrdersSum WHERE id_order='{$id}'");

            while ($item = mysql_fetch_assoc($sql)) {

                // ����������� ������ ������ ���������
                $productId = $item['id_catalog_data_order'];
                $product = mysql_fetch_assoc(mysql_query("SELECT * FROM m_catalog_data_order WHERE id='{$productId}'"));
                $item['articul'] = $product['articul'];

                $this->_products[] = $item;
            }
        }

        return $this->_products;
    }

    /**
     * ��������� ���-�� ������� � ������
     * @return int
     */
    public function getProductsCount()
    {
        $id = $this->id;
        $item = mysql_fetch_assoc(mysql_query("SELECT COUNT(*) as cnt FROM m_mag_OrdersSum WHERE id_order='{$id}'"));
        return (int)$item['cnt'];
    }

    public function getInsuranceCost(){
        return $this->insurance_cost;
    }

    /**
     * ��������� ��������� �������� ��� ������
     * ������������ ����� ������
     * ��� ������ ��������� � ��� �������� ���� ������ ��������� ����� delivery_code_3
     * ����� ��������� �������� ����� ����������
     *
     * @return int
     */
    public function getDeliveryCost()
    {
        // ������������� ��������� ��������� ��������, ����� �������� ��� ����������
        return $this->delivery_cost;

        $logistic = $this->getLogistic();
        $cost = $logistic['delivery_cost_3'];

        if ($this->getPaymentType() !== "��������� ��� ���������" && $this->getPaymentType() !== "�� �������") {
            $cost = $logistic['delivery_cost_1'];
        }

        return $cost;
    }

    /**
     * ��������� ������� ��������
     * @return string
     */
    public function getDeliveryType()
    {
        $logistic = $this->getLogistic();
        return ucfirst($logistic['delivery_type']);
    }

    /**
     * ��������� ������� ����� ��� ��������
     * @return string|null
     */
    public function getDeliveryMetro()
    {
        $user = $this->getUser();
        $id = $user['metro'];

        $item = mysql_fetch_assoc(mysql_query("SELECT * FROM m_geo_metro WHERE id = {$id}"));
        return $item !== false ? $item['name'] : "�� �������";
    }

    /**
     * ��������� �������� ���� ������ ������
     * @return string
     */
    public function getPaymentType()
    {
        if ($this->payment_type == null) {
            $this->payment_type = 0;
        }

        return $this->_paymentType[$this->payment_type];
    }

    /**
     * ����������� ��������� ��� ���
     * @return bool
     */
    public function isLogisticSelf()
    {
        $logistic = $this->getLogistic();
        return mb_strtolower($logistic['delivery_type']) === "���������" ? true : false;
    }

    /**
     * ����������� �������������� ��������
     * @return bool
     */
    public function isRegion()
    {
        $user = $this->getUser();
        return preg_replace('/� /', '', trim(mb_strtolower($user['city']))) === "������" ? false : true;
    }

    /**
     * ��������� ����� ��������� ������,
     * � ��������� � ���
     *
     * @param bool $withDelivery
     *
     * @return int
     */
    public function getTotal($withDelivery = false)
    {
        if ($withDelivery) {
            return (int)($this->dSum + $this->getDeliveryCost() + $this->getInsuranceCost());
        } else {
            return (int)$this->dSum;
        }
    }

    public function getCancelReason(){
        if ($this->cancel_cause_id>0){
            $item = mysql_fetch_assoc(mysql_query("SELECT * FROM m_cancel_causes WHERE cause_id = {$this->cancel_cause_id}"));
            return $item['cause_name'];
        }
        return '';
    }

    /**
     * ������� ��������� ���� ����������� ���������� � ������������
     * @return array
     */
    public function getUser()
    {
        $userId = $this->id_user;
        return User_Row_Item::collectUserInfo($userId, $this);
    }


} 