<?php
/**
 * Created by PhpStorm.
 * User: loram
 * Date: 01.08.14
 * Time: 17:45
 */

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/Abstract.php";

class User_Row_Item extends Row
{

    /**
     * ���������� ����� id � select � ���������� ������� ������
     * @var array
     */
    private static $_districts = array(
        '7700000000000' => '������ � �������� ����',
        '0000000000001' => '��������',
        '0000000000002' => '������',
        '0000000000003' => '����������',
        '0000000000004' => '��������',
        '0000000000005' => '������',
        '0000000000006' => '�������� � ����-�����������',
        '0000000000007' => '����-�����������',
        '0000000000009' => '������',
        '0000000000010' => '�������',
        '7800000000000' => '�����-��������� � �������� ���',
        '7800000900000' => '������',
        '7800000700000' => '��������',
        '780000400000' => '������� ����',
        '7800000800000' => '��������'
    );

    /**
     * �������������� �������� �� ���� 7904**
     * � ��� 8904**
     *
     * @param null $phone
     *
     * @return null|string
     */
    static function formatPhone($phone = null)
    {
        if ($phone == null) {
            return "";
        }

        if ($phone[0] == 7) {
            $phone = "8" . substr($phone, 1);
        }

        return $phone;
    }

    /**
     * ������� ��������� ���� ����������� ���������� � ������������
     *
     * @param $userId int
     * @param $content Order_Row_Item
     *
     * @return array
     */
    static function collectUserInfo($userId, $content)
    {
        $user = mysql_fetch_assoc(mysql_query("SELECT * FROM m_mag_Users WHERE id='{$userId}' LIMIT 1 "));
        //��������� �������
        foreach($user as &$col){
            $col = trim($col);
        }

        // ���� ������������ �� ���� � ������
        // �� ���� ����� ������, ����� ������ ����� �������� ������
        // ��� �������� � ������������
        if ($user == false) {

            $user['address'] = $content->pAddress;
            $user['fullName'] = $content->pFio;
            $user['phone'] = self::formatPhone($content->pPhone);

            // ������ ������������ ���������� ������������
            $user['city'] = "������";
            $user['region'] = "";
            $user['rayon'] = "";
            $user['postcode'] = "";
            $user['email'] = "";
            $user['address_street'] = "";
            $user['address_settlement'] = "";
            $user['address_house'] = "";
            $user['address_flat'] = "";
            $user['address_stroenie'] = "";
            $user['address_corpus'] = "";
        } else {

            // ���� ����� ������������ ���������,
            // �� � ���� ��������� ��� ����
            // ����� ������������ ����� �� ������ �����
            if (trim($user['address_city']) === "") {

                // �. ������ ������������ � ������, ���������
                if (trim($user['address_region']) !== "") {
                    $user['city'] = $user['address_region'];
                }

                $user['rayon'] = "";
            } else {

                $user['city'] = $user['address_city'];
                $user['region'] = $user['address_region'];
                $user['rayon'] = self::$_districts[$user['address_part']];
                $user['address'] = preg_replace(
                    '/ , ,*/',
                    ' ',
                    trim(
                        trim(
                            implode(
                                ', ',
                                array(
                                    $user['address_street'],
                                    $user['address_house'] == null ? "" : "� " . $user['address_house'],
                                    $user['address_corpus'] == null ? "" : "���� " . $user['address_corpus'],
                                    $user['address_stroenie'] == null ? "" : "��� " . $user['address_stroenie'],
                                    $user['address_flat'] == null ? "" : "�� " . $user['address_flat']
                                )
                            ),
                            " "
                        ),
                        ","
                    )
                );
                //������ ������
                $user['address'] = trim($user['address']);
                $user['address'] = trim($user['address'],',');

                // ���� � ������ ���� "���������" address_settlement,
                // �� � ������ ������ ������� ������� �������� ���� address_city,
                // � ������� address_settlement
                if ($user['address_settlement'] != "") {
                    $user['city'] = $user['address_settlement'];
                    $user['rayon'] = $user['address_city'];
                }
            }

            $user['fullName'] = trim($user['name']) === "" ? $user['fio'] : trim(
                implode(
                    ' ',
                    array(
                        $user['surname'],
                        $user['name'],
                        $user['patronymic']
                    )
                )
            );

            $user['phone'] = self::formatPhone($user['phone']);
        }

        $user['fullAddress'] = preg_replace(
            '/ , /',
            ' ',
            trim(
                trim(
                    implode(
                        ', ',
                        array(
                            $user['region'],
                            $user['rayon'],
                            $user['city'],
                            $user['address_street'],
                            $user['address_house'] == null ? "" : "� " . $user['address_house'],
                            $user['address_corpus'] == null ? "" : "���� " . $user['address_corpus'],
                            $user['address_stroenie'] == null ? "" : "��� " . $user['address_stroenie'],
                            $user['address_flat'] == null ? "" : "�� " . $user['address_flat']
                        )
                    ),
                    " "
                ),
                ","
            )
        );
        //������ ������
        $user['fullAddress'] = trim($user['fullAddress']);
        $user['fullAddress'] = trim($user['fullAddress'],',');

        // ������ ���������� mos�ow � ������� ������
        $user['fullAddress'] = preg_replace('/(moscow, |region, )/', "", $user['fullAddress']);

        // �������� ������ ������� �� �����
        foreach ($user as $k => $v) {
            $user[$k] = trim($v);
        }

        return $user;
    }

    /**
     * �������� ��� ����������� �������, ���� ��� �� ������������, �������� ��������
     * @return mixed|string
     */
    public function getFio()
    {
        return ($this->name != '' || $this->surname != '' || $this->patronymic != '') ? $this->surname . ' ' . $this->name . ' ' . $this->patronymic : $this->fio;
    }

}