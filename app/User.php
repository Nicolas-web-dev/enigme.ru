<?php
/**
 * Created by PhpStorm.
 * User: loram
 * Date: 03.08.14
 * Time: 16:34
 */

require_once "User/Row/Item.php";

class User
{
    /**
     * �������� �������
     * @var string
     */
    private $_table = "m_mag_Users";

    /**
     * �������� ������������ �� id
     *
     * @param $id int
     *
     * @return User_Row_Item|null
     */
    public function getById($id)
    {
        $content = mysql_fetch_assoc(
            mysql_query("SELECT * FROM $this->_table WHERE id = {$id} LIMIT 1")
        );

        if ($content) {
            return new User_Row_Item(array(
                'data' => $content,
                'table' => $this->_table,
                'stored' => true
            ));
        } else {
            return null;
        }
    }

    /**
     * ��������� ������������� �� ������� id
     *
     * @param $ids array
     *
     * @return array|null
     */
    public function getByArray($ids)
    {
        if (!is_array($ids)) {
            return null;
        }

        $ids = implode(", ", $ids);
        $result = array();
        $sql = mysql_query("SELECT * FROM $this->_table WHERE id IN ({$ids})");

        while ($content = mysql_fetch_assoc($sql)) {
            $result[] = new User_Row_Item(array(
                'data' => $content,
                'table' => $this->_table,
                'stored' => true
            ));
        }

        return $result;
    }
} 