<?php

/**
 * ������ �����������
 */
class MagLogger {

    public function get_history($module_name,$pid){
        global $db;
        $select = $db->select();
        $select->from(array('l'=>'m_mag_Log'));
        $select->joinLeft(array('u'=>'base_users'),'l.user_id=u.id',array('user_name'=>'login'));
        $select->where('module_name=?',$module_name);
        $select->where('pid=?',$pid);
        $select->order('l.id DESC');

        $aRows = $db->fetchAll($select);


        foreach($aRows as $row){
            list($action,$text) =$this->getText($module_name,$row);
            $result[] = array(
                'user_name'=>$row['user_name'],
                'date'=>date('Y-m-d',strtotime($row['date'])),
                'time'=>date('H:i:s',strtotime($row['date'])),
                'action'=>$action,
                'text'=>$text
            );
        }

        return $result;
    }

    private function getText($module_name,$row){
        global $db;
        $row['text'] = iconv('utf-8','cp1251',$row['text']);
        if ($module_name=='mag_orders'){
            $row['value_before'] = iconv('utf-8','cp1251',$row['value_before']);
            $row['value_after'] = iconv('utf-8','cp1251',$row['value_after']);


            switch($row['action']){
                case 'change_kol':
                    $action ='����������';
                    $text = " ({$row['text']}) {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'payment_type':
                    $action ='��� ������';
                    $values = array(1=>'��������� ��� ���������',2=>'���������� �� ���������',3=>'���������� Robokassa');
                    if ($row['value_before']==''){
                        $text = "{$values[$row['value_after']]}";
                    }else{
                        $text = "{$values[$row['value_before']]}->{$values[$row['value_after']]}";
                    }
                    break;
                case 'pComment':
                    $action ='�����������';
                    $text = "{$row['value_before']}->{$row['value_after']}";
                    break;
                case 'DateEnd':
                    $action ='���� ��������';
                    $text = "{$row['value_before']}->{$row['value_after']}";
                    break;
                case 'is_send_rect':
                    $action ='���������:';
                    $text = "��������� ���������: {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'track':
                    $action ='���� �����:';
                    $text = "{$row['value_before']}->{$row['value_after']}";
                    break;
                case 'track_date':
                    $action ='���� ����:';
                    $text = "���� ���� {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'delete':
                    $action ='������';
                    $text = "{$row['text']}";
                    break;
                case 'add':
                    $action ='��������';
                    $text = "{$row['text']} {$row['value_after']} ��";
                    break;
                case 'logistic_add':
                    $values = array(1=>'���������',2=>'�������',3=>'������ ��������',4=>'�������',5=>'������� �������');
                    $action ='���������';
                    $text = $values[$row['value_after']];
                    break;
                case 'logistic_delete':
                    $values = array(1=>'���������',2=>'�������',3=>'������ ��������',4=>'�������',5=>'������� �������');
                    $action ='��������� (������)';
                    $text = $values[$row['value_before']];
                    break;
                case 'payment_date':
                    $action ='���� ������:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'DateMoneyIncome':
                    $action ='���� ����������� �����';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'TotalMoneyIncome':
                    $action ='����������� �����:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'delivery_cost_fact':
                    $action ='��������� �������� ������.';
                    $text = "{$row['value_before']}->{$row['value_after']}";
                    break;
                case 'firm':
                    $action ='�����';
                    $text = "{$row['value_before']}->{$row['value_after']}";
                    break;
                case 'courier_address_id':
                    $action ='��� ��������';

                    $select = "SELECT couriers_addresses.*, couriers.name AS courier_name FROM couriers_addresses
                        LEFT JOIN couriers ON couriers_addresses.courier_id = couriers.couriersId
                        WHERE couriers_addresses.id = ?";

                    // ���� ������ �� ������ - ������ ����� ������ �� ���������
                    if ($row == null) {
                        $row['id'] = 0;
                    }

                    if ($row['value_before']==''){
                        $value = $db->fetchRow($select, $row['value_after']);
                        $text = "{$value['courier_name']}";
                        $text = iconv('utf-8','cp1251',$text);
                    }else{
                        if (!$value_before = $db->fetchRow($select, $row['value_before'])){
                            $value_before['courier_name'] = '����� ������';
                        }else{
                            $value_before['courier_name'] = iconv('utf-8','cp1251',$value_before['courier_name']);
                        }

                        if (!$value_after = $db->fetchRow($select, $row['value_after'])){
                            $value_after['courier_name'] = '����� ������';
                        }else{
                            $value_after['courier_name'] = iconv('utf-8','cp1251',$value_after['courier_name']);
                        }

                        $text = "{$value_before['courier_name']}->{$value_after['courier_name']}";
                    }
                    break;
                case 'address_region':
                    $action ='���� [������]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_part':
                    $action ='���� [��������� �����]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_other':
                    $action ='���� [������ ������]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_city':
                    $action ='���� [�����]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_settlement':
                    $action ='���� [���. �����]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_street':
                    $action ='���� [�����]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_house':
                    $action ='���� [���]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_corpus':
                    $action ='���� [������]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_stroenie':
                    $action ='���� [��������]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_floor':
                    $action ='���� [������]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_flat':
                    $action ='���� [������]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_domophone':
                    $action ='���� [������]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'postcode':
                    $action ='���� [������]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'fio':
                    $action ='���� [���]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'email':
                    $action ='���� [�����]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'phone':
                    $action ='���� [�������]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'pol':
                    $action ='���� [���]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'status':
                    $action ='������ ������:';
                    if ($row['value_after']==1){
                        $text = "�����";
                    }
                    if ($row['value_after']==2){
                        $text = "�����������";
                    }
                    if ($row['value_after']==3){
                        $text = "���������";
                    }
                    if ($row['value_after']==3){
                        $text = "� ���������";
                    }
                    if ($row['value_after']==4){
                        $text = "�� �����";
                    }
                    if ($row['value_after']==0){
                        $text = "��������";
                    }

                    break;
            }
        }

        return array($action,$text);
    }

    static public function add($module_name, $text){

		mysql_query("
			INSERT INTO m_mag_Log
			(
				user_id, 
				user_name, 
				module_name, 
				text, 
				date
			)
			VALUES 
			(
				'{$_SESSION['user_id']}',
				'{$_SESSION['admin_user_login']}',
				'$module_name',
				'$text',
				NOW()
			)
		");
	}
}
?>