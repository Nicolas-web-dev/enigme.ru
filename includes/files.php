<?php
// ��� �����
function file_type($filename){
	preg_match("|\.([a-z0-9]{2,4})$|i", $filename, $fileSuffix);

	switch(strtolower($fileSuffix[1])){
		case "csv" :
			return "csv";
		default :
			return false;
	}
}

// ���������� ������ ����� (bytes, Kb, Mb, Gb)
function file_size($size)
{
	if ($size < 1024) $new_size = "$size bytes";
	elseif ($size <= 1048576) $new_size = round($size / 1024, 0).' Kb';
	elseif ($size <= 1073741824) $new_size = round($size / 1048576, 2).' Mb';
	else $new_size = round($size / 1073741824, 3).' Gb';
	return $new_size;
}

function file_check_name($filename){
	if (!(strpos($filename,'..') === false) || !(strpos($filename,'{') === false)) return false;
	else return true;
}

//�������� ���������� ������ � ����������
function delete_directory ($directory_name){
	//if (!file_check_name($directory_name)) return false;
	if (is_dir($directory_name)){
		$dir=opendir($directory_name);

		//�������� ������ �� ����������
		while (($file = readdir($dir))!==false){
			if ($file != '.' && $file != '..'){
				if (is_dir($directory_name.'/'.$file)){
					delete_directory($directory_name.'/'.$file);
				}else{
					unlink ($directory_name.'/'.$file);
				}
			}
		}
		rmdir ($directory_name);
		closedir($dir);
	}
}
?>