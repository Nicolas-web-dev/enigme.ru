<?php
	/***** DEFINED CONSTANTS *****/
	
	define('NOGDLIB','�� ����������� ���������� (\'GD\')');
	define('NOURL','the image url string was empty');
	define('NOIMAGE','there is no image to retrieve information from');
	define('NOMIME','�������� MIME ������ �����������');
	define('NOWIDTH','�� ����������� ������ �����������');
	define('NOHEIGHT','�� ����������� ������ �����������');
	define('NODEPTH','���������� ���������� ���������� ��� �����������');
	define('NOCHANNELS','���������� ���������� ���������� ������� �����������');
	define('NOTYPE','�������� ��� ����� �����������');
	define('NOTTHUMBDIR','thumbnail directory is not a directory or does not exist');
	define('NODIR','directory cited is either not a directory or does not exist');
	define('NOIMGDATA','��� ������');
	define('FAILEDSAVETHUMB','failed to save thumbnail to file');
	define('FAILEDSAVE','failed to save image to a file');
	define('FAILEDLOADIMAGE','�������� ��� �������� �����������');
	define('FAILEDCROP','�������� ��� ������� �����������');
	define('FAILEDROTATE','�������� ��� ��������� �����������');
	define('FAILEDRESIZE','�������� ��� ��������� ������� �����������');
	define('FAILEDINFO','��� ������ �� �����������');
	define('FAILEDTHUMB','failed to create thumbnail image');
	define('FAILEDTHMBSIZE','failed to set class object thumb size');
	define('INVALIDPARAM','invalid parameters for method');
	define('OUTOFBOUNDS','out of the image\'s boundaries');
?>