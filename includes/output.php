<?php
// ������� ������ RSS ���� �� ����
function RSS($NameRSS, $UrlSite, $tableNews, $where, $numPosts = 10)
{
	$numPosts = IntVal($numPosts);
	$now = date("r");
	$charset = "Windows-1251";
	$language = "ru";

	$rssText = "";
	$rssText .= "<"."?xml version=\"1.0\" encoding=\"".$charset."\"?".">\n\n";
	$rssText .= "<rss version=\"2.0\">\n";
	$rssText .= " <channel>\n";
	$rssText .= "	<title>".$NameRSS."</title>\n";
	$rssText .= "	<link>".$UrlSite."</link>\n";
	$rssText .= "	<language>".$language."</language>\n";
	$rssText .= "	<docs></docs>\n";
	$rssText .= "	<pubDate>".$now."</pubDate>\n";
	$rssText .= "\n";

	$query = mysql_query("SELECT * FROM $tableNews $where LIMIT $numPosts");
	while ($content = mysql_fetch_array($query)){
		$rssText .= "    <item>\n";
		$rssText .= "      <title>".format_text_out($content['title'])."</title>\n";
		$rssText .= "      <description>".format_text_out(HTMLToTxt(html_entity_decode($content['anot'])))."</description>\n";
		$rssText .= "      <link>".$UrlSite . '/'.$content['url'].'-'.$content['id']. '/' ."</link>\n";
		$rssText .= "      <guid>".$UrlSite . '/'.$content['url'].'-'. $content['id'] .'/' ."</guid>\n";
		$rssText .= "      <pubDate>".$content['registerDate']."</pubDate>\n";
		$rssText .= "    </item>\n";
		$rssText .= "\n";
	}

	$rssText .= "  </channel>\n</rss>";
	return $rssText;
}

// ������� ������ ���������� �� �����
function mySortYear ($id_cat, $table){
	$year = array();
	$query = mysql_query("SELECT YEAR(registerDate) as year FROM $table WHERE id_cat='$id_cat' ORDER BY registerDate DESC");
	while ($content = mysql_fetch_array($query)){$year[] = $content['year'];}
	if (array_count_values($year) > 0) {$year = array_unique($year);}
	return $year;
}

// ������� ������ ���������� �������
function mySortMonth ($id_cat, $table, $year){
	$month = array();
	$a_month = array(1=>'������',2=>'�������',3=>'����',4=>'������',5=>'���',6=>'����',7=>'����',8=>'������',9=>'��������',10=>'�������',11=>'������',12=>'�������');
	$query = mysql_query("SELECT MONTH(registerDate) as month FROM $table WHERE id_cat='$id_cat' and YEAR(registerDate)='$year' ORDER BY registerDate ASC");
	while ($content = mysql_fetch_array($query)){
		$month[$content['month']] = $a_month[$content['month']];
	}
	//if (array_count_values($month) > 0){$month = array_unique($month);}
	return $month;
}

// ������� ������ ��������� � �������
function myPrintHeader ($header, $img, $actions = 'EMPTY'){
	global $valinorConfig;

	$tmp['img'] = $img;
	$tmp['header'] = $header;
	if ($actions != 'EMPTY'){
		foreach ($actions as $a){
			$tmp['actions'] .=
			'<a class="actions_hover" title="'
			.$a['ALT'].'" href="'
			.$a['LINK']
			.'"><img src="images/actions/'
			.$a['IMG'].'" width=46 height=45 border=0></a>';
		}
	}else{
		$tmp['actions'] = '';
	}
	parser('header.tpl', $tmp);
}

// ������� ������ ���������� � �������
function myPrintHeaderNav($arr){
	global $valinorConfig;

	foreach ($arr as $nav) {
		$tmp[] .= ($nav['ACTIVE'] == "1") ? "<a href='{$nav['LINK']}'>{$nav['NAME']}</a>" : "{$nav['NAME']}";
		$tmp[] .= '&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">&nbsp;&nbsp;';
	}
	array_pop($tmp);
	foreach ($tmp as $nav) {$m['items'] .= $nav;}
	parser('header_nav.tpl', $m);
}

// ��������
function go($url)
{
	header("Request-URI: $url");
	header("Content-Location: $url");
	header("Location: $url");
	exit();
}


function extract_date_format($strFormat){
	$format_array = array();
	$pieces = split('[:/.\ \-]', $strFormat);
	return $pieces;
}


// ������� ���� � ��������� ���������
function rusdate($unix_time, $format){

	if ($unix_time=='0000-00-00 00:00:00'){
		return '�� �������';
	}


	$unix_time = strtotime($unix_time);

	$num_month = date("n", $unix_time);     // �� 1 �� 12
	$num_month2 = date("m", $unix_time);    // �� 01 �� 12
	$year = date("Y", $unix_time);          // ��� YYYY
	$weekday = date("w", $unix_time);       // �� 0 �� 6
	$day = date("j", $unix_time);           // �� 1 �� 31
	$day2 = date("d", $unix_time);          // �� 01 �� 31
	$hour = date("G", $unix_time);          // �� 0 �� 23
	$hour2 = date("H", $unix_time);         // �� 00 �� 23
	$minut = date("i", $unix_time);         // ��  00 �� 59

	$month = array(1=>'������',
	2=>'�������',
	3=>'�����',
	4=>'������',
	5=>'���',
	6=>'����',
	7=>'����',
	8=>'�������',
	9=>'��������',
	10=>'�������',
	11=>'������',
	12=>'�������');

	$month2 = array(1=>'������',
	2=>'�������',
	3=>'����',
	4=>'������',
	5=>'���',
	6=>'����',
	7=>'����',
	8=>'������',
	9=>'��������',
	10=>'�������',
	11=>'������',
	12=>'�������');

	$week = array(0=>'�����������',
	1=>'�����������',
	2=>'�������',
	3=>'�����',
	4=>'�������',
	5=>'�������',
	6=>'�������');


	if ($format == 1) $rusdate = "$day2.$num_month2.$year";
	if ($format == 2) $rusdate = $week[$weekday].", $day ".$month[$num_month].", $year";
	if ($format == 3) $rusdate = "$day2-$num_month2-$year, $hour2:$minut";
	if ($format == 4) $rusdate = $month2[$num_month]." $year ����";
	if ($format == 5) $rusdate = "$day ".$month[$num_month]." $year, � $hour:$minut";
	if ($format == 6) $rusdate = $month2[$num_month]." - $year";
	if ($format == 7) $rusdate = "$hour:$minut, $day ".$month[$num_month];
	if ($format == 8) $rusdate = "$day2 ".$month[$num_month]." $hour:$minut";
	if ($format == 9) $rusdate = "$day ".$month[$num_month].", $year";
	if ($format == 10) $rusdate = "$day2 ".$month[$num_month];

	return $rusdate;
}

//==========================================================+
// PAGES
//==========================================================+


function parser_subs($path_template, $values, $mode = 1) {
	global $valinorConfig;

	$path = '/home/a11401/enigme.ru/html/admin/'.$valinorConfig['interface.themes'] . '/smarty/templates/' . $path_template;
	$template = implode('', file($path));

	foreach ($values as $key=>$value) {
		$template = str_replace('{$'.$key.'}', $value, $template);
	}
	if ($mode == 0) return $template;
	else print $template;
}


// ���������� ������ � ����������
function parser($path_template, $values, $mode = 1) {
	global $valinorConfig;

	$path = $valinorConfig['interface.themes'] . '/smarty/templates/' . $path_template;
	$template = implode('', file($path));

	foreach ($values as $key=>$value) {
		$template = str_replace('{$'.$key.'}', $value, $template);
	}
	if ($mode == 0) return $template;
	else print $template;
}

// ���������� ������ � ���������� (��������������� ������� ��� ������������ ���������)
function parser_out($path_template, $values) {
	$path = './templates/scripts/' . $path_template;
	$template = implode('', file($path));

	foreach ($values as $key=>$value) {
		$template = str_replace('{$'.$key.'}', $value, $template);
	}
	return $template;
}

// ����� ������������ ���������
function pages($all_items, $show_items, $path_template, $mode_url = 'dinamic')
{
	$print_pages = 10;
	if ($mode_url == 'dinamic')
	{
		if (isset($_GET['page']))
		{
			$request = preg_replace("/(page=)\d+/i",'$1PG',$_SERVER['REQUEST_URI']);
			$page = (int)$_GET['page'];
			$lister['start'] = $show_items * ($page - 1);
			$lister['end'] = $lister['start'] + $show_items;
		}
		else
		{
			$lister['start'] = 0;
			$lister['end'] = $show_items;
			$page = 1;
			if (preg_match("/\?/i",$_SERVER['REQUEST_URI'])) $request = $_SERVER['REQUEST_URI'].'&page=PG';
			else $request = $_SERVER['REQUEST_URI'].'?page=PG';
		}
	}
	else
	{
		GLOBAL $page;
		if (isset($page))
		{
			$request = preg_replace("/(page-)\d+/i",'$1PG', $_SERVER['REQUEST_URI']);
			$page = (int)$page;
			$lister['start'] = $show_items * ($page - 1);
			$lister['end'] = $lister['start'] + $show_items;
		}
		else
		{
			$lister['start'] = 0;
			$lister['end'] = $show_items;
			$page = 1;
			if (preg_match("/\-/i", $_SERVER['REQUEST_URI'])) $request = preg_replace('/\.html/i','_page-PG.html', $_SERVER['REQUEST_URI']);
			else $request = $_SERVER['REQUEST_URI'].'page-PG.html';
		}
	}

	$width_td = strlen((string)$page) * 3 + 13;

	if ($show_items <= $all_items)
	{
		$all_pages = ceil($all_items / $show_items);
		if ($page <= $print_pages)
		{
			for ($i = 1; $i <= $print_pages; $i++)
			{
				if ($i <= $all_pages)
				{
					$url = str_replace('PG', $i, $request);
					if ($page == $i) $paga["page_$i"] = "<td width=$width_td class=page_selected >$i</td>";
					else $paga["page_$i"] = "<td width=$width_td><a href=$url>$i</a></td>";
				}
				else $paga["page_$i"] = '';
			}

			$tmp_var = $print_pages + 1;
			$url = str_replace('PG', $tmp_var, $request);

			if ($all_pages > $print_pages) $paga['next'] = "<td width=$width_td><a href=$url ><img src='images/nav/next.gif' border=0 align=left width=16 height=16></a></td>";
			else $paga['next'] = "<td width=$width_td><img src='images/nav/next_dis.gif' border=0 align=left width=16 height=16></td>";

			$paga['prev'] = "<td width=$width_td><img src='images/nav/prev_dis.gif' border=0 align=left width=16 height=16></td>";
			$lister['pages'] = parser($path_template, $paga, 0);
		}
		elseif ($page > $print_pages)
		{
			$section = ceil($page / $print_pages);
			if (($section * $print_pages) < $all_pages)
			{
				$limit_start = ($section - 1) * $print_pages + 1;
				$limit_end = $limit_start + $print_pages;
				$tmp_var = $limit_end;
				$tmp_var2 = $limit_start - 1;
				$url = str_replace('PG', $tmp_var, $request);
				$url2 = str_replace('PG',$tmp_var2,$request);
				$paga['next'] = "<td width=$width_td ><a href=$url ><img src='images/nav/next.gif' border=0 align=left width=16 height=16></a></td>";
				$paga['prev'] = "<td width=$width_td ><a href=$url2 ><img src='images/nav/prev.gif' border=0 align=left width=16 height=16></a></td>";
			}
			else
			{
				$limit_end = $all_pages;
				$limit_start = ($section - 1) * $print_pages + 1;
				$tmp_var2 = $limit_start - 1;
				$url = str_replace('PG', $tmp_var2, $request);
				$paga['next'] = "<td width=$width_td><img src='images/nav/next_dis.gif' border=0 align=left width=16 height=16></td>";
				$paga['prev'] = "<td width=$width_td><a href=$url><img src='images/nav/prev.gif' border=0 align=left width=16 height=16></a></td>";
			}

			for ($i = $limit_start, $j = 1; $i <= $limit_end, $j <= $print_pages; $i++, $j++)
			{
				$url = str_replace('PG', $i, $request);
				if ($i <= $limit_end)
				{
					if ($page==$i) $paga["page_$j"] = "<td width=$width_td class=page_selected>$i</td>";
					else $paga["page_$j"] = "<td width=$width_td><a href=$url>$i</a></td>";
				}
				else $paga["page_$j"] = '';
			}

			$lister['pages'] = parser($path_template, $paga,0);
		}
	}
	else
	{
		$lister['start'] = 0;
		$lister['end'] = $show_items;
		for ($i = 2; $i <= $print_pages; $i++) $paga["page_$i"] = '';
		$paga["page_1"] = "<td width=18 class=page_selected>1</td>";
		$paga['next'] = "<td width=$width_td><img src='images/nav/next_dis.gif' border=0 align=left width=16 height=16></td>";
		$paga['prev'] = "<td width=$width_td><img src='images/nav/prev_dis.gif' border=0 align=left width=16 height=16></td>";
		$lister['pages'] = $lister['pages'] = parser($path_template, $paga,0);
	}

	return $lister;
}

// ����� ������������ ��������� ��� ��������
function pages_content($all_items, $show_items, $page, $path_template){

	$print_pages = 10;
	if (isset($page))
	{
		$request = preg_replace("/(page\/)\d+/i",'$1PG', $_SERVER['REQUEST_URI']);
		$page = intval($page);
		$lister['start'] = $show_items * ($page - 1);
		$lister['end'] = $lister['start'] + $show_items;
	}
	else
	{
		$lister['start'] = 0;
		$lister['end'] = $show_items;
		$page = 1;
		$request = $_SERVER['REQUEST_URI'].'page/PG/';
	}

	// ������ ������
	$width_td = strlen((string)$page) * 3 + 13;

	if ($show_items <= $all_items){
		$all_pages = ceil($all_items / $show_items);

		if ($page <= $print_pages){
			for ($i = 1; $i <= $print_pages; $i++){
				if ($i <= $all_pages)
				{
					$url = str_replace('PG', $i, $request);
					if ($page == $i) {
						$paga["page_$i"] = "<span><b>$i</b></span>";

						if ($i == $all_pages) {$url3 = str_replace('PG', $all_pages, $request); $next_s = '1';}
						else {$url3 = str_replace('PG', $i+1, $request); $next_s = '0';}

						if ($i-1 == 0) {$url4 = str_replace('PG', '1', $request); $prev_s = '1';}
						else {$url4 = str_replace('PG', $i-1, $request);$prev_s = '0';}

					}else {
						$paga["page_$i"] = "<a href=$url>$i</a>";
					}
				}
				else $paga["page_$i"] = '';
			}

			$tmp_var = $print_pages + 1;
			$url = str_replace('PG', $tmp_var, $request);


			// ���. ������ - �����
			if ($all_pages > $print_pages) $paga['tnext'] = "<a href=$url>�����</a>";
			else $paga['tnext'] = '';
			$paga['tprev'] = '';

			// ����. � ����.
			if ($next_s == '1') $paga['next'] = "<span class=page_top_selected>���������&nbsp;&raquo;</span>";
			else $paga['next'] = "<a href=$url3>���������&nbsp;&raquo;</a>";
			if ($prev_s == '1') $paga['prev'] = "<span class=page_top_selected>&laquo;&nbsp;����������</span>";
			else $paga['prev'] = "<a href=$url4>&laquo;&nbsp;����������</a>";

			// ������ � �����
			if ($prev_s == '1') {
				$paga['start'] = "<span class=page_top_selected>&laquo;&laquo;&nbsp;� ������</span>";
			}else{
				$url6 = str_replace('PG', '1', $request);
				$paga['start'] = "<a href=$url6>&laquo;&laquo;&nbsp;� ������</a>";
			}

			if ($next_s == '1') {
				$paga['end'] = "<span class=page_top_selected>� �����&nbsp;&raquo;&raquo;</span>";
			}else{
				$url5 = str_replace('PG', $all_pages, $request);
				$paga['end'] = "<a href=$url5>� �����&nbsp;&raquo;&raquo;</a>";
			}

			$lister['pages'] = parser_out($path_template, $paga);
			$lister['pages'] = parser_out($path_template, $paga);

		}elseif ($page > $print_pages){

			$section = ceil($page / $print_pages);
			if (($section * $print_pages) < $all_pages){

				$limit_start = ($section - 1) * $print_pages + 1;
				$limit_end = $limit_start + $print_pages;
				$tmp_var = $limit_end;
				$tmp_var2 = $limit_start - 1;
				$url = str_replace('PG', $tmp_var, $request);
				$url2 = str_replace('PG',$tmp_var2,$request);

				// ���. ������ - �����
				$paga['tnext'] = "<a href=$url>�����</a>";
				$paga['tprev'] = "<a href=$url2>�����</a>";
			}else{

				$limit_end = $all_pages;
				$limit_start = ($section - 1) * $print_pages + 1;
				$tmp_var2 = $limit_start - 1;
				$url = str_replace('PG', $tmp_var2, $request);

				// ���. ������ - �����
				$paga['tnext'] = "";
				$paga['tprev'] = "<a href=$url>�����</a>";
			}

			for ($i = $limit_start, $j = 1; $i <= $limit_end, $j <= $print_pages; $i++, $j++){

				$url = str_replace('PG', $i, $request);
				if ($i <= $limit_end){
					if ($page==$i) {

						if ($i == $all_pages) {$url3 = str_replace('PG', $all_pages, $request);$next_s = '1';}
						else {$url3 = str_replace('PG', $i+1, $request);$next_s = '0';}

						$url4 = str_replace('PG', $i-1, $request);

						$paga["page_$j"] = "<span><b>$i</b></span>";
					}else {
						$paga["page_$j"] = "<a href=$url>$i</a>";
					}
				}else {
					$paga["page_$j"] = '';
				}
			}

			// ����. � ����.
			if ($next_s == '1') $paga['next'] = "<span class=page_top_selected>���������&nbsp;&raquo;</span>";
			else $paga['next'] = "<a href=$url3>���������&nbsp;&raquo;</a>";

			$paga['prev'] = "<a href=$url4>&laquo;&nbsp;����������</a>";

			// ������ � �����
			$url6 = str_replace('PG', '1', $request);
			$paga['start'] = "<a href=$url6>&laquo;&laquo;&nbsp;� ������</a>";

			if ($next_s == '1') {
				$paga['end'] = "<span class=page_top_selected>� �����&nbsp;&raquo;&raquo;</span>";
			}else {
				$url5 = str_replace('PG', $all_pages, $request);
				$paga['end'] = "<a href=$url5>� �����&nbsp;&raquo;&raquo;</a>";
			}


			$lister['pages'] = parser_out($path_template, $paga);
		}

	}else{
		$lister['start'] = 0;
		$lister['end'] = $show_items;
		for ($i = 2; $i <= $print_pages; $i++) $paga["page_$i"] = '';
		$paga["page_1"] = "";

		$paga['next'] = "";
		$paga['prev'] = "";

		$paga['tnext'] = "";
		$paga['tprev'] = "";

		$paga['start'] = "";
		$paga['end'] = "";

		$lister['pages'] = $lister['pages'] = parser_out($path_template, $paga);
	}

	return $lister;
}

//==========================================================+
// TEXT
//==========================================================+

// mime type
if(!function_exists('mime_content_type')) {
	function mime_content_type($filename) {

		$mime_types = array(

		'txt' => 'text/plain',
		'htm' => 'text/html',
		'html' => 'text/html',
		'php' => 'text/html',
		'css' => 'text/css',
		'js' => 'application/javascript',
		'json' => 'application/json',
		'xml' => 'application/xml',
		'swf' => 'application/x-shockwave-flash',
		'flv' => 'video/x-flv',

		// images
		'png' => 'image/png',
		'jpe' => 'image/jpeg',
		'jpeg' => 'image/jpeg',
		'jpg' => 'image/jpeg',
		'gif' => 'image/gif',
		'bmp' => 'image/bmp',
		'ico' => 'image/vnd.microsoft.icon',
		'tiff' => 'image/tiff',
		'tif' => 'image/tiff',
		'svg' => 'image/svg+xml',
		'svgz' => 'image/svg+xml',

		// archives
		'zip' => 'application/zip',
		'rar' => 'application/x-rar-compressed',
		'exe' => 'application/x-msdownload',
		'msi' => 'application/x-msdownload',
		'cab' => 'application/vnd.ms-cab-compressed',

		// audio/video
		'mp3' => 'audio/mpeg',
		'qt' => 'video/quicktime',
		'mov' => 'video/quicktime',

		// adobe
		'pdf' => 'application/pdf',
		'psd' => 'image/vnd.adobe.photoshop',
		'ai' => 'application/postscript',
		'eps' => 'application/postscript',
		'ps' => 'application/postscript',

		// ms office
		'doc' => 'application/msword',
		'rtf' => 'application/rtf',
		'xls' => 'application/vnd.ms-excel',
		'ppt' => 'application/vnd.ms-powerpoint',

		// open office
		'odt' => 'application/vnd.oasis.opendocument.text',
		'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
		);

		$ext = strtolower(array_pop(explode('.',$filename)));
		if (array_key_exists($ext, $mime_types)) {
			return $mime_types[$ext];
		}
		elseif (function_exists('finfo_open')) {
			$finfo = finfo_open(FILEINFO_MIME);
			$mimetype = finfo_file($finfo, $filename);
			finfo_close($finfo);
			return $mimetype;
		}
		else {
			return 'application/octet-stream';
		}
	}
}

function format_text($str)
{
	$str = trim($str);
	$str = preg_replace('/(\")([^\"]+)(\")/i', '&laquo;$2&raquo;', $str);
	$str = str_replace("�","&laquo;", $str);
	$str = str_replace("�","&raquo;", $str);
	$str = str_replace("'","&#039;", $str);
	return $str;
}

function format_text_out($str){
	$str = trim($str);
	$str = str_replace("&mdash;","-", $str);
	$str = str_replace("&ndash;","-", $str);
	$str = str_replace("&ldquo;","�", $str);
	$str = str_replace("&rdquo;","�", $str);
	$str = str_replace("&nbsp;"," ", $str);
	$str = str_replace("&laquo;","�", $str);
	$str = str_replace("&raquo;","�", $str);
	$str = str_replace("&#039;","'", $str);
	return $str;
}

//������� ������ �� ������������� ���. ����
function maxsite_str_word($text, $counttext = 10, $sep = ' ') {
	$words = split($sep, $text);

	if ( count($words) > $counttext )
	$text = join($sep, array_slice($words, 0, $counttext));

	return $text;
}

// ������� �� ������ ��������
function textwrap($text, $width = 75) {
	if ($text) return preg_replace("/([^\n\r ?&\.\/<>\"\\-]{".$width."})/i"," \\1\n",$text);
}

// ��������
function translit ($str, $c = '-'){
	$str=mb_strtolower(trim($str));

	$ru_to_en = array('�' => 'a', '�' => 'b', '�' => 'v', '�' => 'g', '�' => 'd', '�' => 'e', '�' => 'jo', '�' => 'zh','�' => 'z', '�' => 'i', '�' => 'j', '�' => 'k', '�' => 'l', '�' => 'm', '�' => 'n', '�' => 'o', '�' => 'p', '�' => 'r', '�' => 's', '�' => 't', '�' => 'u', '�' => 'f', '�' => 'h', '�' => 'c', '�' => 'ch', '�' => 'sh', '�' => 'sch', '�' => '', '�' => 'y', '�' => '', '�' => 'je', '�' => 'ju', '�' => 'ja');

	$str = strtr($str, $ru_to_en);

	$str = preg_replace('/^\W+|\W+$/', '', $str); // remove all non-alphanumeric chars at begin & end of string
	$str = preg_replace('/\s+/', $c, $str); // compress internal whitespace and replace with -
	$str = preg_replace("/[^a-zA-Z0-9$c]/", '', $str);
	$str = preg_replace("/[$c*]+/", $c, $str);

	return $str;
}

// �������� ��������
function translit_out ($str, $c = '-'){
	$str=(trim($str));

	$en_to_ru = array('a' => '�', 'b' => '�', 'v' => '�', 'g' => '�', 'd' => '�', 'e' => '�', 'jo' => '�', 'zh' => '�', 'z' => '�', 'i' => '�', 'j' => '�', 'k' => '�', 'al' => '���','l' => '�', 'm' => '�', 'n' => '�', 'o' => '�', 'p' => '�', 'r' => '�', 's' => '�', 't' => '�', 'u' => '�', 'f' => '�', 'h' => '�', 'c' => '�', 'ch' => '�', 'sh' => '�', 'sch' => '�', 'y' => '�', 'je' => '�', 'ju' => '�', 'ja' => '�');

	$str = strtr($str, $en_to_ru);

	$str = preg_replace('/^\W+|\W+$/', '', $str);
	$str = preg_replace('/\s+/', $c, $str);
	$str = preg_replace("/[^a-zA-Z�-���0-9$c]/", '', $str);
	$str = preg_replace("/[$c*]+/", $c, $str);

	return $str;
}

// ������ ����� �� ������
function word_to_link($text, $word, $url) {

	$word = iconv('WINDOWS-1251', 'UTF-8', $word);
	return preg_replace('#(?!<.*)(?<!\w)(' . $word . ')(?!\w|[^<>]*>)#i', '<a href="'.$url.'/">\1</a>', $text, 1);
}

function translit_tag ($tag) {
	if ($tag != '') {
		$tag = explode(',', $tag);
		foreach ($tag as $value) {
			$items .= translit($value).',';
		}
		return substr($items,0,-1);
	}else {
		return false;
	}
}

// ������� �������������� HTML � ������� �����.
function HTMLToTxt($str, $strSiteUrl="", $aDelete=array(), $maxlen=0)
{
	//get rid of whitespace
	$str = preg_replace("/[\\t\\n\\r]/", " ", $str);

	//replace tags with placeholders
	static $search = array (
	"'<script[^>]*?>.*?</script>'si",
	"'<style[^>]*?>.*?</style>'si",
	"'<select[^>]*?>.*?</select>'si",
	"'&(quot|#34);'i",
	//				"'&(amp|#38);'i",
	//				"'&(lt|#60);'i",
	//				"'&(gt|#62);'i",
	//				"'&(nbsp|#160);'i",
	"'&(iexcl|#161);'i",
	"'&(cent|#162);'i",
	"'&(pound|#163);'i",
	"'&(copy|#169);'i",
	"'&#(\d+);'e", // evaluate as php
	);

	static $replace = array (
	"",
	"",
	"",
	"\"",
	//				"&",
	//				"<",
	//				">",
	//				" ",
	"\xa1",
	"\xa2",
	"\xa3",
	"\xa9",
	"(intval('\\1')>=848 ? chr(intval('\\1')-848) : chr(intval('\\1')))",
	);

	$str = preg_replace($search, $replace, $str);

	$str = eregi_replace("<[/]{0,1}(b>|i>|u>|em>|small>|strong>)", "", $str);
	$str = eregi_replace("<[/]{0,1}(font|div|span)[^>]*>", "", $str);

	//���� ������
	$str = eregi_replace("<ul[^>]*>", "\r\n", $str);
	$str = eregi_replace("<li[^>]*>", "\r\n  - ", $str);

	//������ �� ��� �������
	for($i = 0; $i<count($aDelete); $i++)
	$str = eregi_replace($aDelete[$i], "", $str);

	//���� ��������
	$str = eregi_replace('<img[ ]+src[ ]*=[ ]*[\"\'](/[^\"\'>]+)[\"\'][^>]*>', "[".chr(1).$strSiteUrl."\\1".chr(1)."] ", $str);
	$str = eregi_replace('<img[ ]+src[ ]*=[ ]*[\"\']([^\"\'>]+)[\"\'][^>]*>', "[".chr(1)."\\1".chr(1)."] ", $str);

	//���� ������
	$str = eregi_replace('<a[ ]+href[ ]*=[ ]*[\"\'](/[^\"\'>]+)[\"\'][^>]*>([^>]+)</a>', "\\2 [".chr(1).$strSiteUrl."\\1".chr(1)."]", $str);
	$str = eregi_replace('<a[ ]+href[ ]*=[ ]*[\"\']([^\"\'>]+)[\"\'][^>]*>([^>]+)</a>', "\\2 [".chr(1)."\\1".chr(1)."]", $str);

	//���� <br>
	$str = eregi_replace("<br[^>]*>", "\r\n", $str);

	//���� <p>
	$str = eregi_replace("<p[^>]*>", "\r\n\r\n", $str);

	//���� <hr>
	$str = str_replace("<hr>", "\r\n----------------------\r\n", $str);

	//���� �������
	$str = eregi_replace("</{0,1}(thead|tbody)[^>]*>", "", $str);
	$str = eregi_replace("<(/{0,1})th[^>]*>", "<\\1td>", $str);

	$str = eregi_replace("</td>", "\t", $str);
	$str = eregi_replace("</tr>", "\r\n", $str);
	$str = eregi_replace("<table[^>]*>", "\r\n", $str);

	$str = eregi_replace("\r\n[ ]+", "\r\n", $str);

	//����� ������ ��� ���������� ����
	$str = eregi_replace("</{0,1}[^>]+>", "", $str);

	$str = ereg_replace("[ ]+ ", " ", $str);
	$str = str_replace("\t", "    ", $str);

	//��������� ������� ������
	if($maxlen > 0)
	$str = ereg_replace("([^\n\r]{".$maxlen."}[^ \r\n]*[] ])([^\r])","\\1\r\n\\2",$str);

	$str = str_replace(chr(1), " ",$str);
	return trim($str);
}

// ������� ��������
function myDelBr ($str){
	return str_replace(array("\r\n", "\r", "\n"), "<br />", $str);
}

// �������������� ��������
function myAddBr ($str){
	return str_replace("<br />", "\r\n", $str);
}

function banner_all_out ($data){
	if ($data['is_target'] == 1) $banner_target = 'target="_blank"';else $banner_target = '';

	if ($data['url'] == ''){
		if ($data['is_code'] == 1){
			$banner_country = '<div>'.$data['text'].'</div>';
		}else{
			$banner_country = '<div class="banners">'.$data['text'].'</div>';
		}

	}else{
		$banner_links = '/banner_all/'.$data['id'].'/';
		$text = $data['text'];
		$text = eregi_replace('<a[ ]+href[ ]*=[ ]*[\"\'](/[^\"\'>]+)[\"\'][^>]*>([^>]+)</a>', "", $text);
		$text = eregi_replace('<a[ ]+href[ ]*=[ ]*[\"\']([^\"\'>]+)[\"\'][^>]*>([^>]+)</a>', "", $text);
		$banner_country = '<a href="'. $banner_links .'" '. $banner_target .'>'.'<div class="banners_link">'.$text.'</div>'.'</a>';
	}

	return $banner_country;
}

?>