<?php
function geo_redirect_def_domain() {
	$def_domain = trim(file_get_contents(__DIR__ . '/geo_def_domain.php'));
	$json = trim(file_get_contents(__DIR__ . '/geo_json.php'));
	$cities = json_decode_cp1251_m($json,true);
	$result = false;
	$domain_url = explode('.', $_SERVER['HTTP_HOST']);
	if (count($domain_url) > 2){
		
		header("HTTP/1.1 404 Not Found");
		require('protect/error.html');
		exit();
		
		$result = true;
		$subdomain_url = current($domain_url);
		
		if ($subdomain_url === 'www') {
			$result = false;
		}
		else {
			foreach ($cities as $value) {                
                if ($value['domain'] === $subdomain_url) {   
					if ($subdomain_url != 'msk'){
						$result = false;
					}	
                }				
			}
		}
	} else {
		$subdomain_url = 'msk';
	}
	$url = $_SERVER['REQUEST_URI'];
	if ($subdomain_url === 'msk' || $subdomain_url === 'www' || $result) {
		$GLOBALS["user_segment"] = '1';
	} else {
		$GLOBALS["user_segment"] = '2';
	}
	if ($result) {
		header( 'Location: http://' . $def_domain . $url, true, 301);
		exit;
	}
}


 function json_decode_cp1251_m($json_cp1251, $assoc) {
	$json_utf8 = iconv("CP1251", "UTF-8", $json_cp1251);
	$data_decode = json_decode($json_utf8, $assoc);
	return conv_to_cp1251_m($data_decode);
 }
 
 function conv_to_cp1251_m($ar_utf8) {
	foreach ($ar_utf8 as $key1 => $value1) {
		foreach ($value1 as $key2 => $value2)
		{
			$ar_cp1251[$key1][$key2] = iconv("UTF-8", "CP1251", $value2);
		}
	}
	return $ar_cp1251;
}
?>