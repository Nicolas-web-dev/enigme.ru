<?php
// загружаем библиотеку Smarty
require('Smarty.class.php');

class Smarty_Admin extends Smarty {

	function Smarty_Admin($path){
		$this->Smarty();

		$this->template_dir = $path . '/smarty/templates/';
		$this->compile_dir  = $path . '/smarty/templates_c/';
		$this->config_dir   = $path . '/smarty/configs/';
		$this->cache_dir    = $path . '/smarty/cache/';
		//$this->force_compile = true;

		$this->caching = false;
		$this->assign('app_name', 'Adminka');
	}
}
?>