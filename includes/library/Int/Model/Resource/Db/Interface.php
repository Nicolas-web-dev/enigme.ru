<?php

interface Int_Model_Resource_Db_Interface extends Int_Model_Resource_Interface {

    /** ZF methods */
    public function info($key = null);

    public function createRow(array $data = array());

    /** Int methods */
    public function saveRow($info, $row = null);
}