<?
use TheIconic\Tracking\GoogleAnalytics\Analytics;
include_once($base_path.'/app/Order.php');

/**
 * Class Cart
 * Class for work with cart
 */
class Measurement
{

    public $currency = 'RUB';

    public $order_before;
    public $order_after;
    public $analytics;

    public function __construct($tracking_id=null)
    {
        global $db;
        if (!$tracking_id){
            $tracking_id = 'UA-7807440-1';
        }
        $this->analytics = new Analytics(true);
        $this->db = $db;

        if ($this->gaParseCookie()){
            $this->analytics->setProtocolVersion('1')
                ->setTrackingId($tracking_id)
                ->setClientId($this->gaParseCookie())
                ->setIpOverride($_SERVER['REMOTE_ADDR']);

            return $this->analytics;
        }

        return null;
    }

    public function gaParseCookie() {
        if (isset($_COOKIE['_ga'])) {
            list($version,$domainDepth, $cid1, $cid2) = split('[\.]', $_COOKIE["_ga"],4);
            $contents = array('version' => $version, 'domainDepth' => $domainDepth, 'cid' => $cid1.'.'.$cid2);
            $cid = $contents['cid'];
        }
        //else $cid = gaGenUUID();
        return $cid;
    }

    /**
     * @param null $order_id
     */
    public function Purchase($order_id=null){




        // Then, include the transaction data
        $order = (new Order())->getById($order_id);

        $aProducts = $order->getProducts();

        $margin = 0;
        foreach ($aProducts as $oProduct) {
            $margin+= $oProduct['margin']*$oProduct['kol'];
        }


        $this->analytics->setTransactionId('02-' . $order_id)
            ->setProtocolVersion('1')
            ->setRevenue($margin)
            ->setTax(0)
            ->setCurrencyCode($this->currency)
            ->setShipping($order->getDeliveryCost())
            ->setCustomDimension($_COOKIE['_ga'], 1);





        foreach ($aProducts as $oProduct) {
            $productData = [
                'sku' => $oProduct['articul'],
                'name' => $oProduct['title'],
                'price' => $oProduct['sum']/$oProduct['kol'],
                'quantity' => $oProduct['kol']
            ];

            foreach ($productData as &$item) {
                $item = iconv('cp1251','utf8',$item);
            }
            $this->analytics->addProduct($productData);
        }


        $this->analytics->setEventCategory('Ecommerce')->setProductActionToPurchase();
        $this->analytics->setEventAction('Purchase')->sendEvent();
    }

    /**
     * @param $order_id
     * @param $aArticul
     */
    public function RefundProduct($order_id,$aArticul){
        if (!is_array($aArticul)){
            $aArticul = [$aArticul];
        }
        $order = (new Order())->getById($order_id);
        $this->analytics->setTransactionId($order_id)
            ->setNonInteractionHit(1);

        $aProducts = $order->getProducts();

        foreach ($aProducts as $oProduct) {
            if (in_array($oProduct['articul'],$aArticul)){
                $data = [
                    'sku' => $oProduct['articul'],
                    'name' => $oProduct['title'],
                    'price' => $oProduct['sum']/$oProduct['kol'],
                    'quantity' => $oProduct['kol']
                ];
                foreach ($data as &$item) {
                    $item = iconv('cp1251','utf8',$item);
                }
                $this->analytics->addProduct($data);
            }
        }
        $this->analytics->setProductActionToRemove();
        $status = $this->analytics->setEventCategory('Ecommerce')->setEventAction('Refund')->sendEvent();
    }

    /**
     * @param null $order_id
     */
    public function Refund($order_id=null){
        $order = (new Order())->getById($order_id);
        $this->analytics->setTransactionId($order_id)
            ->setNonInteractionHit(1)->setProductActionToRefund();
        $status = $this->analytics->setEventCategory('Ecommerce')->setEventAction('Refund')->sendEvent();
    }
}