{literal}
<html>
<head>
<title>������ ��������������</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<link rel="stylesheet" href="style.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/tabs/style.css">
<link rel="stylesheet" type="text/css" href="css/thickbox/thickbox.css">

<script language="javascript" type="text/javascript" src="js/func.js"></script>
<script language="javascript" type="text/javascript" src="tiny_mce/plugins/filemanager/js/mcfilemanager.js"></script>
<script language="javascript" type="text/javascript" src="/js/tooltip/tooltip.js"></script>

<script language="javascript" type="text/javascript" src="/js/jquery/jquery-1.2.6.min.js"></script>
<script language="javascript" type="text/javascript" src="/js/jquery/ui/ui.core.min.js"></script>
<script language="javascript" type="text/javascript" src="/js/jquery/ui/ui.tabs.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$("#tabs > ul").tabs();
	$(".actions_hover").hover(
	function () {
		$('#actions_header').show();
		$('#actions_header').append($(this).attr('title'));
	},
	function () {
		$('#actions_header').empty();
		$('#actions_header').hide();
	}
	);
});
</script>

</head>

<body>
{/literal}

{strip}

<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
<!-- HEADER -->
<tr><td height="50" bgcolor="#789cac">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="245" align="center" class="company">
				--= ENIGME =--
			</td>
			
			<td align="right">
				<table width="100%" height="51" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td align="right" valign="bottom" class="host_header">
						www.{$host}
						</td>
					</tr>
					<tr>
						<td align="right" valign="top" class="user_header">
						|&nbsp;{$smarty.session.copy_login}&nbsp;|
						</td>
					</tr>
				</table>
			</td>
			<td width="60" align="center" class="exit" >
				<a href="?index.php&other=exit" style="color:#e5f2f4;">[EXIT]</a>
			</td>
		</tr>
	</table>
</td></tr>

<!-- CONTENT -->
<tr><td valign="top" bgcolor="#FFFFFF">
	<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" >
		<td width="245" valign="top" bgcolor="#f9f9f9">{include file="menu.tpl"}</td>
		<td width="10"><img src="/images/default/1x1.gif" width="1" height="1" border="0"></td>
		<td valign="top"><div class="text" ><br>{$content}</div><br></td>
		<td width="10"><img src="/images/default/1x1.gif" width="1" height="1" border="0"></td>
		</tr>
	</table>
</td></tr>

<!-- FOOTER -->
<tr><td height="10" bgcolor="#789cac"></td></tr>
</table>
</body>
</html>
{/strip}